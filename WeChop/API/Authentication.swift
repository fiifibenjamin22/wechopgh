//
//  ServersideFunctions.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

func signInUser(_ vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN signInUser | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
    
    headers = [
        "x-api-key": apiKey
    ]
    
    parameters = [
        "sessionToken": userCurrentSessionToken,
        "deviceId": userCurrentDeviceID
    ]
    
//    if let parms = parameters {
//        print("sign in | params: ", parms)
//    }
    
    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token") else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    //Re-Sign in with the Authentication Provider
                    
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    //Specified user is unknown to us
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    _ = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    return completion(999)
                }
            }
            
            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    if JSON["sessionToken"] != nil {
                        userCurrentSessionToken = JSON["sessionToken"]! as! String
                        print("session token: ", userCurrentSessionToken)
                    }
                    if let deviceId = JSON["deviceId"] {
                        userCurrentDeviceID = "\(deviceId)"
                    }
                    saveToUserDefaults()
                }
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
    
}

let signInRequestHandler: (Int) -> Void = {
    result in
    
    if result == 200 {
        print("IN signInRequestHandler | success")
        
        if comingFrom.launchScreenToLogin == true {
            currentRootView = .menu
            callNotificationItem("showMainVC")
        }
        getMenu()
        createNewUserProfile() //set New user profile as empty
        getUserData(true, completion: getUserDataHandler)

    } else if result == 401 {
        //Re-Sign in with the Authentication Provider
        print("IN signInRequestHandler | failed 401")
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //Specified user is unknown to us
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        print("IN signInRequestHandler | failed 404")
        globalLoadingSpinner.stopAnimating()

    } else if result == 500 {
        print("IN signInRequestHandler | failed 500")
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN signInRequestHandler | failed nil")
        globalLoadingSpinner.stopAnimating()
    }
    
    if result != 200 && comingFrom.launchScreenToLogin == true {
        print("IN signInRequestHandler | failed From Launch")
        callNotificationItem("showMainVC")
    }
}

func signOutUser(_ vc: UIViewController, completion: @escaping (Int) -> Void) {

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = [
        "x-api-key": apiKey
    ]

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token/\(userCurrentSessionToken)") else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.delete,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 404 {
                    //Specified user is unknown to us
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    _ = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "We had trouble signing you out. Please try again later. Contact support for futher questions.", color: UIWarningErrorColor)

                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 200 {
                //unexpected return

                return completion(200)
            } else if response.response?.statusCode == 204 {
                //successful operation
                return completion(204)
            }
            return completion((response.response?.statusCode)!)
    }
}

//signOutUser
let signOutUserRequestHandler: (Int) -> Void = {
    result in

    if result == 204 {
        print("IN signOutUserRequestHandler | success 201")

        refreshAllDateInformation()
        setCurrentlySelectedMenuDay()
        //go to login page
        callNotificationItem("signOutSuccessful")
        globalLoadingSpinner.stopAnimating()

    } else if result == 200 {
        print("IN signOutUserRequestHandler | success 200 - UNEXPECTED")
        //do nothing because this is unexpected
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //unknown user
        animateUIBanner("Error", subText: "We had trouble signing you out. Please try again later. Contact support for futher questions.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN signOutUserRequestHandler | failed: ", result)
        globalLoadingSpinner.stopAnimating()
    }
}

func getMenu() {
    print("in get menu func")
    if comingFrom.launchScreenToLogin == false {
        print("in get menu func | login from launch false")
        callNotificationItem("getCurrentMenu")
    } else {
        print("in get menu func | login from launch TRUE")
        callNotificationItem("getCurrentMenuFromLaunch")
    }
} //used in many auth files to call menuAPI
