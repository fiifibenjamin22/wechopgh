//
//  Menu.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON
import Kingfisher

func tryToGetMenuData(_ date: String, vc: UIViewController, fromLogin: Bool, completion: @escaping(Int) -> Void) {
    print("IN tryToGetMenuData | start | date", date)

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if comingFrom.menuDaySelectionPressed == true {
        comingFrom.menuDaySelectionPressed = false
        DispatchQueue.main.async {
            if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
                print("IN tryToGetMenuData | spinner wasn't animating")
                spinner.startAnimating()
            } else {
                print("IN tryToGetMenuData | spinner wasn't created")
                createGlobalLoadingSpinner(vc.view)
            }
        }
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
    
    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    print("headers: ", headers)

    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/menu?date=\(date)") else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            todaysMenu.removeAll() //clear out old data
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    if fromLogin == true {
                        return completion(4042)
                    } else {
                        return completion(404)
                    }
                }  else if response.response?.statusCode == 409 {
                    if fromLogin == true {
                        return completion(4092)
                    } else {
                        return completion(409)
                    }
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN tryToGetMenuData | success ")

            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let json = JSON(result)
//                    print("restaurant data: ")
//                    dump(json)
                    let restaurants = json["restaurant"]
                    if restaurants.count > 0 {

                        var urlList: [URL] = [URL]()
                        for itemJSON in restaurants {
                            let item: JSON = itemJSON.1
                            var tempFinalMenuItems: [MenuData] = [MenuData]()
                            let tempMenuItems = item["items"]
                            
                            for tempMenuItemJSON in tempMenuItems {
                                let tempMenuItem: JSON = tempMenuItemJSON.1
                                
                                urlList.append(tempMenuItem["picture"].url!)
                                tempFinalMenuItems.append(MenuData(id: String(tempMenuItem["id"].int!), name: tempMenuItem["name"].string!, description: tempMenuItem["description"].string!, price: Double(tempMenuItem["price"].int!), image: UIImage(named: "menu_sample1")!, imageURL: tempMenuItem["picture"].url!, restaurantName: item["name"].string!, restaurantID: item["name"].string!, soldout: tempMenuItem["isSoldOut"].bool!, date: date))
                            }
                            
                            todaysMenu.append(RestaurantData(name: item["name"].string!, id: item["name"].string!, tag: " ", genre: item["genre"].string!, rating: Double(item["rating"].int!), currency: item["currency"].string!, picture: UIImage(named: "menu_sample1")!, menuItems: tempFinalMenuItems))
                        }
                        
                        //start downloading images for menu
                        let prefetcher = ImagePrefetcher(urls: urlList) { skippedResources, failedResources, completedResources in
//                            print("These images are skipped: \(skippedResources)")
//                            print("These images are failedResources: \(failedResources)")
//                            print("These images are prefetched: \(completedResources)")
                        }
                        prefetcher.start()
                    }
                    
                    if fromLogin == true {
                        if comingFrom.launchScreenToLogin == true {
                            return completion(2003)
                        } else {
                            return completion(2002)
                        }
                    } else {
                        return completion(200)
                    }
                    
                }
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToGetMenuDataHandler: (Int) -> Void = { result in
    print("in tryToGetMenuDataHandler | ", result)

    if result == 200 {
        //NOT FROM LOGIN

        userData?.AddressSupported = true
        callNotificationItem("recreateMenu")
        print("in tryToGetMenuDataHandler | restaurant count", todaysMenu.count)
        globalLoadingSpinner.stopAnimating()

    } else if result == 2002 {
        //FROM LOGIN
        print("in tryToGetMenuDataHandler | about to refresh page")
        print("in tryToGetMenuDataHandler | restaurant count", todaysMenu.count)

        userData?.AddressSupported = true
        delay(globalMenuLoadingDelay, closure: {

            callNotificationItem("refreshPage")
            //        callNotificationItem("toggleViewCartButton")
            callNotificationItem("checkToggleUpcomingOrderCount")
            callNotificationItem("toggleDisplayRateOrder")

        })
        globalLoadingSpinner.stopAnimating()

    } else if result == 2003 {
        
        //from launch screen directly to menu
        print("in tryToGetMenuDataHandler | about to refresh page")
        print("in tryToGetMenuDataHandler | restaurant count", todaysMenu.count)

        userData?.AddressSupported = true
        if comingFrom.launchScreenToLogin == true {
            delay(globalMenuLoadingDelay, closure: {
                callNotificationItem("refreshPage")
                callNotificationItem("checkToggleUpcomingOrderCount")
                callNotificationItem("toggleDisplayRateOrder")
            })
        }
    } else if result == 401 {
        
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        //send back to login??

        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //No menu available at that date
        //also getting 404 for unsupported user location
        animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 4042 {
        //coming from login page!
        //No menu available at that date
        //also getting 404 for unsupported user location
        callNotificationItem("moveToMenu")
        animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        delay(globalMenuLoadingDelay, closure: {
            callNotificationItem("refreshPage")
            callNotificationItem("checkToggleUpcomingOrderCount")
            callNotificationItem("toggleDisplayRateOrder")
        })

        globalLoadingSpinner.stopAnimating()
    } else if result == 409 {
        //No delivery at User location

        userData?.AddressSupported = false
        print("in tryToGetMenuDataHandler | result 409", userData?.AddressSupported)
        animateUIBanner("Warning", subText: "We do not yet deliver to this location. Contact support for further questions.", color: UIWarningInformationColor, shouldDismiss: false)
        
        globalLoadingSpinner.stopAnimating()
    } else if result == 4092 {
        //No delivery at User location FROM LOGIN
        userData?.AddressSupported = false

        if comingFrom.launchScreenToLogin == false {
            callNotificationItem("moveToMenu")
        }

        delay(globalMenuLoadingDelay, closure: {
            print("in 4092, about to refresh: ", userData?.AddressSupported)

            callNotificationItem("refreshPage")
            //            callNotificationItem("toggleViewCartButton")
            callNotificationItem("checkToggleUpcomingOrderCount")
            callNotificationItem("toggleDisplayRateOrder")

        })
        
        globalLoadingSpinner.stopAnimating()
        
    } else {

        globalLoadingSpinner.stopAnimating()

    }
    
    if result != 200 && result != 2002 && result != 2003 && result != 4092 && comingFrom.launchScreenToLogin == true {
        currentRootView = .login
        callNotificationItem("showMainVC")
    }
}

func tryToGetMenuDataForUpdateCart(_ date: String, vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToGetMenuDataForUpdateCart | start | date", date)

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]

    print("headers: ", headers)

    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/menu?date=\(date)") else {return completion(999)} // TODO remove test date values

    Alamofire.request(url,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else if response.response?.statusCode == 409 {
                    return completion(409)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN tryToGetMenuDataForUpdateCart | success ")

            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let json = JSON(result)
                    let restaurants = json["restaurant"]
                    if restaurants.count > 0 {

                        cartMenu.removeAll() //clear out old data

                        var urlList: [URL] = [URL]()
                        for itemJSON in restaurants {
                            let item: JSON = itemJSON.1
                            var tempFinalMenuItems: [MenuData] = [MenuData]()
                            let tempMenuItems = item["items"]

                            for tempMenuItemJSON in tempMenuItems {
                                let tempMenuItem: JSON = tempMenuItemJSON.1

                                urlList.append(tempMenuItem["picture"].url!)
                                tempFinalMenuItems.append(MenuData(id: String(tempMenuItem["id"].int!), name: tempMenuItem["name"].string!, description: tempMenuItem["description"].string!, price: Double(tempMenuItem["price"].int!), image: UIImage(named: "menu_sample1")!, imageURL: tempMenuItem["picture"].url!, restaurantName: item["name"].string!, restaurantID: item["name"].string!, soldout: tempMenuItem["isSoldOut"].bool!, date: date))
                            }

                            cartMenu.append(RestaurantData(name: item["name"].string!, id: item["name"].string!, tag: " ", genre: item["genre"].string!, rating: Double(item["rating"].int!), currency: item["currency"].string!, picture: UIImage(named: "menu_sample1")!, menuItems: tempFinalMenuItems))
                        }

                        //start downloading images for menu
                        let prefetcher = ImagePrefetcher(urls: urlList) { skippedResources, failedResources, completedResources in
//                            print("These images are skipped: \(skippedResources)")
//                            print("These images are failedResources: \(failedResources)")
//                            print("These images are prefetched: \(completedResources)")
                        }
                        prefetcher.start()
                    }
                    return completion(200)
                }
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToGetMenuDataForUpdateCartHandler: (Int) -> Void = { result in
    print("in tryToGetMenuDataForUpdateCartHandler | ", result)

    if result == 200 {
        globalLoadingSpinner.stopAnimating()
        callNotificationItem("goToMenuDetailsFromNewMenuData")
    } else if result == 401 {

        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //No menu available at that date
        animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 409 {
        //No delivery at User location
        userData?.AddressSupported = false
        animateUIBanner("Warning", subText: "We do not yet deliver to this location. Contact support for further questions.", color: UIWarningInformationColor, shouldDismiss: false)
        globalLoadingSpinner.stopAnimating()
    } else {
        globalLoadingSpinner.stopAnimating()

    }
}
