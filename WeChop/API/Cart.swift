//
//  Cart.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

func tryToPutCartItems(_ vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToPutCartItems | start ")
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    var allOrderItems = [Any]()
    var paramString: String = String()
    
    if userCartOrder.OrderedItems.count > 0 {
        for item in userCartOrder.OrderedItems {
            let rowItem = ["id": item.ID, "quantity": item.Quantity]
            allOrderItems.append(rowItem)
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: allOrderItems, options: []) else {
            return
        }
        paramString = String(data: data, encoding: String.Encoding.utf8)!
        
    } else {
        return completion(999)
    }
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/cart") else {return completion(999)}
    
    var request = URLRequest(url: url)
    request.httpMethod = HTTPMethod.put.rawValue
    request.allHTTPHeaderFields = headers
    request.httpBody = paramString.data(using: .utf8)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    Alamofire.request(request).responseString { response in
        
        guard response.result.isSuccess else {
            
            if response.response?.statusCode == 401 {
                return completion(401)
            }  else {
                if let errorString = response.result.error {
                    print("Error: \(errorString)")
                }
                let errorMSG = response.result.error!.localizedDescription
                animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                return completion((response.response?.statusCode)!)
            }
        }
        print("IN tryToGetServerCartItems | success ")
        
        if response.response?.statusCode == 200 {
            return completion(200)
        }
        return completion((response.response?.statusCode)!)
    }
}

func tryToGetServerCartItems(_ vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToGetServerCartItems | start ")
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    var parameters: Parameters?
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/cart") else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else if response.response?.statusCode == 424 {
                    return completion(424)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN tryToGetServerCartItems | success ")
            
            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let json = JSON(result)

                    print("IN tryToGetServerCartItems | removing orderedItems ")

                    userCartOrder.OrderedItems.removeAll()
                    
                    for item1 in json {
                        let item = item1.1
                        
                        let q = item["quantity"].int!
                        let p = item["price"].int!
                        let id = item["menuItemId"].int!
                        
                        userCartOrder.OrderedItems.append(OrderDataLines(name: item["name"].string!, id: "\(id)", quantity: "\(q)", price: "\(p)", restaurant: String(item["vendorName"].string!)))
                        userCartOrder.Date = item["cartDate"].string!
                        print("IN tryToGetServerCartItems | appended items ")
                        //                        dump(userCartOrder.OrderedItems)

                    }
                }
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToGetServerCartItemsHandler: (Int) -> Void = { result in
    print("in tryToGetServerCartItemsHandler | ", result)

    if result == 200 {
        callNotificationItem("toggleViewCartButtonFromGetCartAPI")

        if comingFrom.cart == true {
            callNotificationItem("refreshCartPageData")
        }
    } else if result == 401 {
        print("in tryToGetServerCartItemsHandler | 401 | relogin")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
    } else if result == 404 {
        //There is no cart currently
        print("in tryToGetServerCartItemsHandler | 404 | no cart ")
    } else if result == 424 {
        //Something wrong with DB
        print("in tryToGetServerCartItemsHandler | 424 | something wrong with db ")
    }
}

let tryToGetServerCartParametersPromoCodeHandler: (Int) -> Void = { result in
    print("in tryToGetServerCartParametersPromoCodeHandler | ", result)

    if result == 200 {

        //get cart data and then dismiss cart detailsvc
        callNotificationItem("getUserCurrentCartFromCartDetails")
        animateUIBanner("Success", subText: "Promo code applied successfully.", color: UIWarningSuccessColor, removePending: true)
        globalLoadingSpinner.stopAnimating()

    } else if result == 202 {
        //invalid promo code
        animateUIBanner("Warning", subText: "Could not apply promo code. Either a code is already applied or it's invalid.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 401 {
        print("in tryToGetServerCartParametersPromoCodeHandler | 401 | relogin")
        animateUIBanner("Warning", subText:  "Could not apply promo code. Either a code is already applied or it's invalid.", color: UIWarningErrorColor)

        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        print("in tryToGetServerCartParametersPromoCodeHandler | 404 | no cart ")

        globalLoadingSpinner.stopAnimating()

    } else if result == 424 {
        globalLoadingSpinner.stopAnimating()

    } else {
        globalLoadingSpinner.stopAnimating()
    }
}

func tryToGetServerCartParameters(_ vc: UIViewController, promoCode: String, completion: @escaping(Int) -> Void) {
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    print("IN tryToGetServerCartParameters | start ")

    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    var parameters: Parameters?
    
    var urlString: URL = URL(string: "\(SwaggerClientAPI.basePath)/cart/parameters")!
    
    if promoCode != "" {
        urlString = URL(string: "\(SwaggerClientAPI.basePath)/cart/parameters?promo_code=\(promoCode)")!
        if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
            spinner.startAnimating()
        } else {
            createGlobalLoadingSpinner(vc.view)
        }
    }
    
    Alamofire.request(urlString,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                print("IN tryToGetServerCartParameters | failure ")

                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else if response.response?.statusCode == 424 {
                    if promoCode != "" {
                        animateUIBanner("Warning", subText: "Could not apply promo code. Either a code is already applied or it's invalid.", color: UIWarningErrorColor)
                    }
                    return completion(424)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    return completion(999)
                }
            }
            print("IN tryToGetServerCartParameters | success ")
            
            if response.response?.statusCode == 200 {
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    userCartOrder.Amount = Double(json["total"].stringValue)!
                    let destination = json["destination"]
                    userCartOrder.Address = destination["address"].string!
                    let additionals1 = json["additionals"]
                    userCartOrder.Additionals.removeAll()
                    userCartOrder.OrderedItems.removeAll()
                    print("IN tryToGetServerCartParameters | items ")

                    for item1 in additionals1 {
                        let item = item1.1
                        //                        dump(item)
                        userCartOrder.Additionals.append(OrderDataAdditionals(label: item["label"].string!, value: item["text"].string!))
                        
                        if item["label"].string! == "VAT (17.5%)" {
                            userCartOrder.VatCost = item["text"].string!
                        }
                        if item["label"].string! == "Delivery fee" {
                            userCartOrder.DeliveryCost = item["text"].string!
                        }
                        if item["label"].string! == "Discount" {
                            userCartOrder.Discount = item["text"].string!
                        }
                    }
                }
                
                tryToGetServerCartItems(vc, completion: tryToGetServerCartItemsHandler)
                
                return completion(200)
            } else if response.response?.statusCode == 202 {
                // promo code didn't work
                
                tryToGetServerCartItems(vc, completion: tryToGetServerCartItemsHandler)
                
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    print("IN tryToGetServerCartParameters | response: ")
                    //                    dump(json)

                    userCartOrder.Amount = Double(json["total"].stringValue)!
                    
                    let destination = json["destination"]
                    userCartOrder.Address = destination["address"].string!
                    
                    let additionals1 = json["additionals"]
                    
                    userCartOrder.Additionals.removeAll()
                    userCartOrder.OrderedItems.removeAll()
                    
                    for item1 in additionals1 {
                        let item = item1.1
                        userCartOrder.Additionals.append(OrderDataAdditionals(label: item["label"].string!, value: item["text"].string!))
                        
                        if item["label"].string! == "VAT(17.5%)" {
                            userCartOrder.VatCost = item["text"].string!
                        }
                        if item["label"].string! == "Delivery fee" {
                            userCartOrder.DeliveryCost = item["text"].string!
                        }
                        if item["label"].string! == "Discount" {
                            userCartOrder.Discount = item["text"].string!
                        }
                    }
                }
                
                return completion(202)
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToGetServerCartParametersHandler: (Int) -> Void = { result in
    print("in tryToGetServerCartParametersHandler | ", result)

    if result == 200 {
        callNotificationItem("toggleViewCartButtonFromGetCartAPI")
        globalLoadingSpinner.stopAnimating()
    } else if result == 202 {
        //invalid promo code
        animateUIBanner("Warning", subText: "Could not apply promo code. Either a code is already applied or it's invalid.", color: UIWarningErrorColor)
        callNotificationItem("toggleViewCartButtonFromGetCartAPI")
        globalLoadingSpinner.stopAnimating()
    } else if result == 401 {
        print("in tryToGetServerCartParametersHandler | 401 | relogin")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 404 {
        //cart is empty??
        print("in tryToGetServerCartParametersHandler | 404 | no cart ")
        //remove items from cart and refresh order button on menu page
        userCartOrder = emptyOrder
        userCartOrder.OrderedItems.removeAll()

        if comingFrom.cartOrderComplete == true {
            print("in tryToGetServerCartItemsHandler | refreshing cart page data")
            callNotificationItem("toggleViewCartButtonFromGetCartAPI")
            callNotificationItem("refreshCartPageData")
            //callNotificationItem
            //comingFromCartOrderComplete = false
            comingFrom.cartOrderComplete = false
            callNotificationItem("goToOrderVCPageFromOrderComplete")
            globalLoadingSpinner.stopAnimating()
        }
    } else if result == 409 {
        print("in tryToGetServerCartParametersHandler | 409 | something wrong with DB ")
        globalLoadingSpinner.stopAnimating()

    } else if result == 424 {
        globalLoadingSpinner.stopAnimating()

    } else {
        globalLoadingSpinner.stopAnimating()

    }
}

func tryToDeleteCart(_ vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToDeleteCart | start ")
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    var parameters: Parameters?
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/cart") else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.delete,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else if response.response?.statusCode == 424 {
                    return completion(424)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN tryToDeleteCart | success ")
            return completion((response.response?.statusCode)!)
    }
}

let tryToDeleteCartHandler: (Int) -> Void = { result in
    print("in tryToDeleteCartHandler | ", result)

    if result == 200 {
        print("in tryToDeleteCartHandler | 200 | Unknown Success")
        //unknown success return
        globalLoadingSpinner.stopAnimating()
    } else if result == 204 {
        print("in tryToDeleteCartHandler | 204 | Success")
        //success deleting cart

        if comingFrom.menuDetailsCreateNewCart != true {
            userCartOrder = emptyOrder //create empty cart
            userCartOrder.Additionals.removeAll()
            userCartOrder.OrderedItems.removeAll()
            callNotificationItem("dismissCartVC") //dismiss Cart
        } else {
            comingFrom.menuDetailsCreateNewCart = false
            //comingFromStartNewCart = false //reset variable
            callNotificationItem("refreshPage")
            callNotificationItem("addNewItemsToCart")
        }

        if comingFrom.menuDetailsToDeleteCart == true {
            //tryToDeleteCartComingFromMenuDetails = false
            comingFrom.menuDetailsToDeleteCart = false
            callNotificationItem("dismissMenuDetailsVC")
        }
        globalLoadingSpinner.stopAnimating()
    } else if result == 401 {
        print("in tryToDeleteCartHandler | 401 | Authentication Failure ")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 404 {
        print("in tryToDeleteCartHandler | 404")
    } else {
        globalLoadingSpinner.stopAnimating()
    }
}

func tryToPlaceCartOrder(_ vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToPlaceCartOrder | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
    
    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    // TODO pass params
    
    var allOrderItems = [Any]()
    var paramString: String = String()
    
    if userCartOrder.OrderedItems.count > 0 {
        for item in userCartOrder.OrderedItems {
            let rowItem = ["id": item.ID, "quantity": item.Quantity]
            allOrderItems.append(rowItem)
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: allOrderItems, options: []) else {
            return
        }
        paramString = String(data: data, encoding: String.Encoding.utf8)!
        
    } else {
        return completion(999)
    }
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/cart/checkout") else {return completion(999)}
    
    var request = URLRequest(url: url)
    request.httpMethod = HTTPMethod.post.rawValue
    request.allHTTPHeaderFields = headers
    request.httpBody = paramString.data(using: .utf8)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    Alamofire.request(request).responseString { response in
        guard response.result.isSuccess else {
            
            if response.response?.statusCode == 400 {
                return completion(400)
            } else if response.response?.statusCode == 401 {
                return completion(401)
            } else if response.response?.statusCode == 402 {
                return completion(402)
            } else if response.response?.statusCode == 404 {
                return completion(404)
            }   else if response.response?.statusCode == 409 {
                return completion(409)
            }  else {
                if let errorString = response.result.error {
                    print("Error: \(errorString)")
                }
                return completion((response.response?.statusCode)!)
            }
        }
        
        if response.response?.statusCode == 201 || response.response?.statusCode == 200 {
            
            //update orders
            getUserUpcomingAndCompletedOrders(vc)
            return completion((response.response?.statusCode)!)
        }
        return completion((response.response?.statusCode)!)
    }
}

let tryToPlaceCartOrderHandler: (Int) -> Void = { result in
    print("in tryToPlaceCartOrderHandler | ", result)
    
    if result == 200 || result == 201 {
        print("in tryToPlaceCartOrderHandler | 200 | success")
        animateUIBanner("Success", subText: "Your order was successfully placed.", color: UIWarningSuccessColor, removePending: true)
        
        //update order items
        comingFrom.cartOrderComplete = true
        //comingFromCartOrderComplete = true
        //shouldMoveToOrdersFromCart = true
        comingFrom.cartToOrders = true
        //shouldMoveFromOrdersVCToMenu = true
        comingFrom.ordersToMenu = true
        //get cart data and then dismiss cart detailsvc
        callNotificationItem("getUserCurrentCartFromCartDetails")

    } else if result == 400 {
        print("in tryToPlaceCartOrderHandler | 400 | ")
        
        //The Cart is stale; some of the items belong to menus that were scheduled for a date in the past.
        animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 401 {
        print("in tryToPlaceCartOrderHandler | 401 | ")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        
        animateUIBanner("Error", subText:  "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 402 {
        print("in tryToPlaceCartOrderHandler | 402 | ")
        //Payment failed.
        
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 404 {
        //There is no Order stored for this user.
        
        print("in tryToPlaceCartOrderHandler | 404 | ")
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 409 {
        print("in tryToPlaceCartOrderHandler | 409 | ")
        //Time is passed the cutoff time
        
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        
        globalLoadingSpinner.stopAnimating()

    } else {
        globalLoadingSpinner.stopAnimating()
        
    }
}
//handlers

let tryToPutCartItemsHandler: (Int) -> Void = { result in
    print("in tryToPutCartItemsHandler | ", result)
    
    if result == 200 {
        print("in tryToPutCartItemsHandler | 200 | Success")
        
        //get cart data and then dismiss menu detailsvc
        callNotificationItem("getUserCurrentCartFromMenuDetails")

        delay(0.5, closure: {
            callNotificationItem("toggleViewCartButtonFromGetCartAPI")
            callNotificationItem("refreshPage")
            callNotificationItem("dismissMenuDetailsVC")
        })
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        
        print("in tryToPutCartItemsHandler | 401 | Authentication Failure ")
        globalLoadingSpinner.stopAnimating()
        
    } else {
        globalLoadingSpinner.stopAnimating()
        
    }
}
