//
//  Order.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

func tryToGetOrderData(_ type: String, vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToGetOrderData | start ")
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
    
    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    //type Available values: history, future, current, active
    //page Page number we want to show 1
    //size Number of items per Page
    
    //    let query: String = "?type=\(type)&page=1&size=10"
    let query: String = "?type=\(type)"
    
    print("query: ", query)
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/order\(query)") else {return completion(999)} // TODO remove test date values
    
    Alamofire.request(url,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                                        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN tryToGetOrderData | success ")
            
            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    if json.count > 0 {
//                        print("more than one order | count: ", json.count)
                        
                        var orders: [OrderData] = [OrderData]()
                        let emptyOrderedItems: OrderDataLines = OrderDataLines(name: "", id: "", quantity: "", price: "")
                        let emptyAdditionals: OrderDataAdditionals = OrderDataAdditionals(label: "", value: "")
                        var itemResAddressUrl: URL = URL(string: "http://google.com")!
                        
                        for jsonItem in json {
                            let item = jsonItem.1
                            let itemResDetails = item["restaurant"]
                            
                            if let picture = itemResDetails["picture"].string {
                                itemResAddressUrl = URL(string: picture)!
                            }
                            
                            orders.append(OrderData(restaurantName: itemResDetails["name"].string!, rating: item["rating"].int!, orderID: item["ordernum"].string!, date: item["timestamp"].string!, address: item["destination_address"].string!, amount: Double(item["total"].int!), vatCost: "", deliveryCost: "", status: item["status_line"].string!, orderedItems: [emptyOrderedItems], additionals: [emptyAdditionals], isComplete: item["is_complete"].bool!, isCancellable: item["is_cancellable"].bool!, isRated: item["is_rated"].bool!))
                        }
                        
                        fullOrderList.append(contentsOf: orders)
                        
                        for item in orders {
                            if item.IsComplete == true {
                                completedOrders.append(item)
                                print("completed orders")
                            } else {
                                upcomingOrders.append(item)
                            }
                        }
                        
                        if type == "history" {
                            callNotificationItem("toggleDisplayRateOrder")
                        }
                    }
                }
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
}

func getOrderDetailsData(_ queryOrderID: String, completion: @escaping(Int) -> Void) {
    //    print("IN getOrderDetailsData | start ")

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
    let order = fullOrderList.first(where: {$0.OrderID == queryOrderID})
    
    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    //    let query: String = "?type=\(type)&page=1&size=10"
    let query: String = "/\(queryOrderID)"
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/order\(query)") else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.get,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }

                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            
            if response.response?.statusCode == 200 {
                //success
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    if json.count > 0 {
                        var orderDetails: [OrderDataLines] = [OrderDataLines]()
                        var orderAdditionals: [OrderDataAdditionals] = [OrderDataAdditionals]()
                        
                        let lines = json["lines"]
                        
                        if lines.count > 0 {
                            for item1 in lines {
                                let item = item1.1
                                orderDetails.append(OrderDataLines(name: item["name"].string!, id: "", quantity: String(item["quantity"].int!), price: String(item["price"].int!)))
                            }
                        }
                        
                        let additionals = json["additionals"]
                        if additionals.count > 0 {
                            for item1 in additionals {
                                let item = item1.1
                                orderAdditionals.append(OrderDataAdditionals(label: item["label"].string!, value: item["text"].string!))
                            }
                        }
                        
                        if orderDetails.count > 0 {
                            order?.OrderedItems.removeAll()
                            order?.OrderedItems.append(contentsOf: orderDetails)
                        }
                        
                        if orderAdditionals.count > 0 {
                            order?.Additionals.removeAll()
                            order?.Additionals.append(contentsOf: orderAdditionals)
                            
                            for item in orderAdditionals {

                                if item.Label == "VAT" {
                                    order?.VatCost = item.Value
                                }
                                if item.Label == "Delivery fee" {
                                    order?.DeliveryCost = item.Value
                                }
                                if item.Label == "Discount" {
                                    order?.Discount = item.Value
                                }
                            }
                        }
                        
                        if let index = fullOrderList.index(where: {$0.OrderID == queryOrderID}) {
                            fullOrderList[index] = order!                            
                        }
                    }
                }
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
}

func setOrderRating(_ queryOrderID: String, feedback: Feedback, vc: UIViewController, completion: @escaping(Int) -> Void) {
        print("IN setOrderRating | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
                    spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    var headers: HTTPHeaders = HTTPHeaders()
    let parameters: Parameters?
    let order = fullOrderList.first(where: {$0.OrderID == queryOrderID})
    
    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    let query: String = "/\(queryOrderID)/rating"
    
    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/order\(query)") else {return completion(999)}
    
    var paramString: String = String()
    
    var issues = [String]()
    var issueParameters: [String: Any] = ["score": "\(feedback.Score)"]

    if feedback.Comment != "" {
        issueParameters["comment"] = "\(feedback.Comment)"
    }
    
    if feedback.Issues.count > 0 {
        for item in feedback.Issues {
            issues.append(item.IssueNumber)
        }
        issueParameters["issues"] = issues
    }
    
    guard let data = try? JSONSerialization.data(withJSONObject: issueParameters, options: []) else {
        return completion(999)
    }
    paramString = String(data: data, encoding: String.Encoding.utf8)!

    var request = URLRequest(url: url)
    request.httpMethod = HTTPMethod.put.rawValue
    request.allHTTPHeaderFields = headers
    request.httpBody = paramString.data(using: .utf8)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    Alamofire.request(request).responseString { response in
      
            guard response.result.isSuccess else {
                
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            
            if response.response?.statusCode == 200 {
                //success
                
                //update order item locally
                order?.IsRated = true
                order?.Rating = Int(feedback.Score)!

                if let index = fullOrderList.index(where: {$0.OrderID == queryOrderID}) {
                    fullOrderList[index] = order!
                }
                
                if let index = completedOrders.index(where: {$0.OrderID == queryOrderID}) {
                    completedOrders[index] = order!
                }
                
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
}

let setOrderRatingHandler: (Int) -> Void = { result in
        print("in setOrderRatingHandler | ", result)

    if result == 200 {
        print("in setOrderRatingHandler | 200 | success")

        animateUIBanner("Success", subText: "Thank you for your feedback!", color: UIWarningSuccessColor, removePending: true)
        // get refreshed new order history data
        callNotificationItem("reloadOrders")
        
        delay(1.0, closure: {
            globalLoadingSpinner.stopAnimating()
            callNotificationItem("dismissOrderRatingVC")
        })

    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.

        print("in setOrderRatingHandler | 401 | relogin")
        animateUIBanner("Error", subText: "Could not submit ratings. Please try again later.", color: UIWarningErrorColor, removePending: true)

        callNotificationItem("reloadOrders")

        delay(1.0, closure: {
            globalLoadingSpinner.stopAnimating()
            callNotificationItem("dismissOrderRatingVC")
        })
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //There is no Order stored for this user.

        print("in setOrderRatingHandler | 404 | no results")
        animateUIBanner("Error", subText: "Could not submit ratings. Please try again later.", color: UIWarningErrorColor, removePending: true)

        callNotificationItem("reloadOrders")

        delay(1.0, closure: {
            globalLoadingSpinner.stopAnimating()
            callNotificationItem("dismissOrderRatingVC")
        })
        globalLoadingSpinner.stopAnimating()

    } else {
        callNotificationItem("reloadOrders")
        animateUIBanner("Error", subText: "Could not submit ratings. Please try again later.", color: UIWarningErrorColor, removePending: true)

        delay(1.0, closure: {
            globalLoadingSpinner.stopAnimating()
            callNotificationItem("dismissOrderRatingVC")
        })
        globalLoadingSpinner.stopAnimating()

    }
}

let getOrderDetailsDataHandler: (Int) -> Void = { result in
//    print("in getOrderDetailsDataHandler | ", result)

    if result == 200 {
        //        print("in getOrderDetailsDataHandler | 200 | success")
        callNotificationItem("reloadOrders")
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        
        print("in getOrderDetailsDataHandler | 401 | relogin")
        
    } else if result == 404 {
        //There is no Order stored for this user.
        print("in getOrderDetailsDataHandler | 404 | no results")
        
    } else {
    }
}

//used in the order Details page to update the currently selected order

let getOrderDetailsSelectedOrderDataHandler: (Int) -> Void = { result in
    //    print("in getOrderDetailsSelectedOrderDataHandler | ", result)
    
    if result == 200 {
//        print("in getOrderDetailsSelectedOrderDataHandler | 200 | success")
        callNotificationItem("refreshOrderPageData")
    } else if result == 401 {
        print("in getOrderDetailsSelectedOrderDataHandler | 401 | relogin")
    } else if result == 404 {
        print("in getOrderDetailsSelectedOrderDataHandler | 404 | no results")
    } else {
    }
}

let tryToRefreshOrderHandler: (Int) -> Void = { result in
    print("in tryToRefreshOrderHandler | ", result)
    
    if result == 200 {
        //get order details
        if fullOrderList.count > 0 {
            for item in fullOrderList {
                getOrderDetailsData(item.OrderID, completion: getOrderDetailsDataHandler)
            }
        }
        
        //update menu page order count icon
        
        callNotificationItem("checkToggleUpcomingOrderCount")
        callNotificationItem("reloadOrders")
        callNotificationItem("dismissOrderRatingVC")
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 401 {
        print("in tryToRefreshOrderHandler | 401 | relogin")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //There is no Order stored for this user.

        print("in tryToRefreshOrderHandler | 404 | no results")
        globalLoadingSpinner.stopAnimating()

    } else {
        
        globalLoadingSpinner.stopAnimating()

    }
}

let tryToGetOrderHandler: (Int) -> Void = { result in
    print("in tryToGetOrderHandler | ", result)
    
    if result == 200 {
        //get order details
        if fullOrderList.count > 0 {
            for item in fullOrderList {
                getOrderDetailsData(item.OrderID, completion: getOrderDetailsDataHandler)
            }
        } else {
            callNotificationItem("reloadOrders")
        }
        
        //update menu page order count icon
        callNotificationItem("checkToggleUpcomingOrderCount")

    } else if result == 401 {
        print("in tryToGetOrderHandler | 401 | relogin")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

    } else if result == 404 {
        //There is no Order stored for this user.

        print("in tryToGetOrderHandler | 404 | no results")

    } else {
        
    }
}

func tryToCancelOrder(_ orderID: String, vc: UIViewController, completion: @escaping(Int) -> Void) {
    print("IN tryToCancelOrder | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    //set cancel order id used in completion
    cancelledOrderID = orderID

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?
//    let order = fullOrderList.first(where: {$0.OrderID == orderID})

    headers = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]

    guard let url = URL(string: "\(SwaggerClientAPI.basePath)/order/\(orderID)") else {return completion(999)}

    print("IN tryToCancelOrder | url ", url)

    Alamofire.request(url,
                      method: HTTPMethod.delete,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 401 {
                    //Authentication Failure: either the API Key or the Auth Token are invalid.

                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    //Order identifier does not exist or is not associated with this user.
                    return completion(404)
                }  else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }

                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 204 {
                //success
                return completion(204)
            } else {
                if let code = response.response?.statusCode {
                    return completion(code)
                } else {
                    return completion(999)
                }
            }
//            return completion((response.response?.statusCode)!)
    }
}

let tryToCancelOrderHandler: (Int) -> Void = { result in
    print("in tryToCancelOrderHandler | ", result)

    if result == 204 {
        //reload page
        callNotificationItem("removeCancelledOrder")
        animateUIBanner("Success", subText: "Order #\(cancelledOrderID) successfully canceled.", color: UIWarningSuccessColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 401 {
        print("in tryToGetOrderHandler | 401 | relogin")
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Authentication failed.  Please re-login.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //There is no Order stored for this user.

        print("in tryToGetOrderHandler | 404 | no results")
        globalLoadingSpinner.stopAnimating()

    } else {
        print("in tryToGetOrderHandler | ", result)
        globalLoadingSpinner.stopAnimating()

    }
}
