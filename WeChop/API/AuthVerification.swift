//
//  AuthVerification.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

func tryToVerifyConfirmationCode(_ authenticationType: Int, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryToVerifyConfirmationCode | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    let urlString: URL = URL (string: "\(SwaggerClientAPI.basePath)/auth/token-confirm?sign-in=1")!

    var envIsProd: Bool = true

    if urlString.absoluteString.contains("gamma") {
        envIsProd = false
    }

    print("envIsProd: ", envIsProd)

    var parameters: Parameters?

        if envIsProd == false { //for ppe todo remove for prod
            phoneNumberValidationCode = "99999"
        }

    if authenticationType == 0 { //for WeChop validation
        userCurrentID = (userData?.Phone)!
        userCurrentAuthToken = userRegistrationPassword
    } else if authenticationType == 1 {
        userRegistrationPassword = (userData?.Phone)!
    }

    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "authtype": String(authenticationType),
        "authid": userCurrentID,
        "authtoken": userCurrentAuthToken,
        "deviceid": userCurrentDeviceID
    ]

    parameters = [
        "verifier": phoneNumberVerifierResponse,
        "token": phoneNumberValidationCode,
        "password": userRegistrationPassword,
        "sendernumber": (userData?.Phone)!
    ]

//        print("headers: ", headers)
//
//        if let params = parameters {
//            print("parameters: ", params)
//        }

    guard let url = URL (string: urlString.absoluteString) else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 401 {

                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    //code is wrong or user doesn't exist

                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription

                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 202 {
                //address not supported!

                print("IN tryToVerifyConfirmationCode | Success ")
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    if JSON["sessionToken"] != nil {
                        userCurrentSessionToken = JSON["sessionToken"]! as! String
                        print("session token: ", userCurrentSessionToken)
                    }
                    //                    if JSON["deviceId"] != nil {
                    //                        userCurrentDeviceID = JSON["deviceId"]! as! String
                    //                    }
                    saveToUserDefaults()
                }

            } else if response.response?.statusCode == 200 {
                //success
                print("IN tryToVerifyConfirmationCode | Success ")
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    if JSON["sessionToken"] != nil {
                        userCurrentSessionToken = JSON["sessionToken"]! as! String
                        print("session token: ", userCurrentSessionToken)
                    }
                    if JSON["deviceId"] != nil {
                        userCurrentDeviceID = JSON["deviceId"]! as! String
                    }
                    saveToUserDefaults()
                }
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToVerifyConfirmationCodeHandler: (Int) -> Void = {
    result in

    print("IN tryToVerifyConfirmationCodeHandler | ", result)

    if result == 200 {
        print("IN tryToVerifyConfirmationCodeHandler | success 200")
        
        if comingFrom.register == true { //unwind to login
            print("IN tryToVerifyConfirmationCodeHandler | moving to menu")
            callNotificationItem("dismissToLogin")
        }

        animateUIBanner("Success", subText: "Confirmation code verified.", color: UIWarningSuccessColor, removePending: true)

        delay(1, closure: {
            //move to menu
            callNotificationItem("moveToMenu")
            getMenu()
            getUserData(true, completion: getUserDataHandler)
            globalLoadingSpinner.stopAnimating()
        })

    } else if result == 202 {
        print("IN tryToVerifyConfirmationCodeHandler | success 202")
        //address unsupported

        if comingFrom.register == true { //unwind to login
            print("IN tryToVerifyConfirmationCodeHandler | moving to menu")
            callNotificationItem("dismissToLogin")
        } else {
            animateUIBanner("Error", subText: "We do not yet deliver to this location. Contact support for further questions.", color: UIWarningInformationColor, shouldDismiss: false)
        }

        delay(1, closure: { //move to menu
            callNotificationItem("moveToMenu")
            getMenu()
            getUserData(false, completion: getUserDataHandler)
            globalLoadingSpinner.stopAnimating()
        })

    } else if result == 404 {
        //Verifier token does not exist or Confirmation code is wrong

        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        print("IN tryToVerifyConfirmationCodeHandler | failed 404")
        globalLoadingSpinner.stopAnimating()

    } else if result == 500 {
        print("IN tryToVerifyConfirmationCodeHandler | failed 500")
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN tryToVerifyConfirmationCodeHandler | failed nil")
        globalLoadingSpinner.stopAnimating()

    }
}

func requestResendVerifyConfirmationCode(_ vc: UIViewController, completion: @escaping (Int) -> Void) {

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    print("IN requestResendVerifyConfirmationCode | start ")

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = [
        "x-api-key": apiKey
    ]

    print("headers: ", headers)
    print("IN requestResendVerifyConfirmationCode | verifier: ", phoneNumberVerifierResponse)

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token-sending?verifierToken=\(phoneNumberVerifierResponse)") else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseString { response in

            guard response.result.isSuccess else {

                //401
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                } else if response.response?.statusCode == 420 {
                    return completion(420)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 201 {
                //success

                return completion(201)

            } else if response.response?.statusCode == 200 {

                return completion(201)
            }
            return completion((response.response?.statusCode)!)
    }
}

let requestResendVerifyRequestHandler: (Int) -> Void = {
    result in

    if result == 201 || result == 200 {

        print("IN requestResendVerifyRequestHandler | success ", result)
        animateUIBanner("Success", subText: "Re-sent the verification code successfully.", color: UIWarningSuccessColor, removePending: true)
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //Verifier token does not exist
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 420 {
        //Too many such requests have been made
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else {
        print("IN requestResendVerifyRequestHandler | failed: ", result)
        globalLoadingSpinner.stopAnimating()

    }
}
