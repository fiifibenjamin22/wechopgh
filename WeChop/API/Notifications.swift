//
//  Notifications.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 11/3/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

func ackNotification(_ uniqueID: String, completion: @escaping (Int) -> Void ) {

    print("IN ackNotification | uniqueID", uniqueID)
    if hasInternetCheck() == false {
        return completion(999)
    }

    let headers: HTTPHeaders = [
        "x-api-key": apiKey
    ]

    if currentUserIP == "" || currentUserIP == nil {
        getPublicIP()
    }

    let parameters = [
        "DeviceName": currentDeviceModel,
        "DeviceOs": currentDeviceVersion,
        "DeviceIp": currentUserIP,
        "AppVersion": appVersion
    ]

    //    print("headers: ", headers)
    //    print("params: ", parameters)
    //    print("uniqueID: ", uniqueID)

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/notification/ack/\(uniqueID)") else {return completion(500)}

    //    print("IN ackNotification | url", url)

    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseString { response in

            if let code = response.response?.statusCode {
                print("IN ackNotification | response", code)
            }

            guard response.result.isSuccess else {

                //401
                if response.response?.statusCode == 404 {
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error in ackNotification: \(errorString)")
                    }
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            return completion((response.response?.statusCode)!)
    }
}

let notificationHandler: (Int) -> Void = {
    result in
    print("in ackNotificationHandler | ", result)

    if result == 200 {

    } else if result == 404 {
        //unknown notification
    } else {

    }
}

func dismissNotification(_ uniqueID: String, isclear: Bool = true, completion: @escaping (Int) -> Void ) {

    print("IN dismissNotification | uniqueID", uniqueID)
    if hasInternetCheck() == false {
        return completion(999)
    }

    let headers: HTTPHeaders = [
        "x-api-key": apiKey
    ]

    if currentUserIP == "" || currentUserIP == nil {
        getPublicIP()
    }

    var parameters: Parameters?

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/notification/dismiss/\(uniqueID)?isclear=\(isclear)") else {return completion(500)}

    print("IN dismissNotification | url", url)

    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                //401
                if response.response?.statusCode == 404 {
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error in dismissNotification: \(errorString)")
                    }
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN dismissNotification | success")
            return completion((response.response?.statusCode)!)
    }
}

func openNotification(_ uniqueID: String, completion: @escaping (Int) -> Void ) {

    print("IN openNotification | uniqueID", uniqueID)
    if hasInternetCheck() == false {
        return completion(999)
    }

    let headers: HTTPHeaders = [
        "x-api-key": apiKey
    ]

    var parameters: Parameters?

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/notification/open/\(uniqueID)") else {return completion(500)}

    print("IN openNotification | url", url)

    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: nil,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                //401
                if response.response?.statusCode == 404 {
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error in openNotification: \(errorString)")
                    }
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("IN openNotification | success")
            return completion((response.response?.statusCode)!)
    }
}
