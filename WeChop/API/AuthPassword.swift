//
//  Password.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON

//Reset Password
func resetUserPassword(_ vc: UIViewController, completion: @escaping (Int) -> Void) {

    print("IN resetUserPassword | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = [
        "x-api-key": apiKey
    ]

    parameters = [
        "authId": userCurrentID,
        "authToken": userCurrentAuthToken,
        "authtype": "0", //for WC
        "firstName": (userData?.FirstName)!,
        "lastName": (userData?.LastName)!,
        "phone": (userData?.Phone)!,
        "address": (userData?.Address)!,
        "localeCode": (userData?.LocaleCode)!
    ]

//    print("params: ", parameters)

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/reset") else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 400 {
                    return completion(400)
                } else if response.response?.statusCode == 420 {
                    return completion(420)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            print("in resetUserPassword | success: ", response.response?.statusCode)
            if response.response?.statusCode == 201 {

                if let result = response.result.value {
                    let JSON = result as! NSDictionary

                    if JSON["verifier"] != nil {
                        phoneNumberVerifierResponse = JSON["verifier"]! as! String
                    }

                    if JSON["token"] != nil {
                        if JSON["token"] is NSString {
                            phoneNumberValidationCode = ((JSON["token"] as! NSString) as String)
                            print("in resetUserPassword | phoneNumberValidationCode: ", phoneNumberValidationCode)
                        }
                    }
                }
                return completion(201)
            }
            return completion((response.response?.statusCode)!)
    }
}

//completion handlers
let resetUserPasswordHandler: (Int) -> Void = {
    result in

    print("IN resetUserPasswordHandler | result: ", result)

    if result == 201 {

        //success go to validation page

        callNotificationItem("moveToResetVerificationVC")

        globalLoadingSpinner.stopAnimating()

    } else if result == 400 {
        //unknown user
        animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)

        print("IN resetUserPasswordHandler | failed 401")
        globalLoadingSpinner.stopAnimating()

    } else if result == 420 {
        //too many requests
        animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)
        print("IN resetUserPasswordHandler | failed 404")
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN resetUserPasswordHandler | failed nil")
        globalLoadingSpinner.stopAnimating()

    }
}

let VerifyConfirmationCodePasswordResetHandler: (Int) -> Void = { //not used
    result in

    print("IN VerifyConfirmationCodePasswordResetHandler | ", result)

    globalLoadingSpinner.stopAnimating()
    if result == 200 {
        print("IN VerifyConfirmationCodePasswordResetHandler | success 200")

        animateUIBanner("Success", subText: "Your password has been updated.", color: UIWarningSuccessColor, removePending: true)
        //goto main login page
        callNotificationItem("goToLoginPageFromPWReset")
    } else if result == 202 {
        print("IN VerifyConfirmationCodePasswordResetHandler | success 202")
        //address unsupported
        animateUIBanner("Success", subText: "Your password has been updated.", color: UIWarningSuccessColor, removePending: true)
        callNotificationItem("goToLoginPageFromPWReset")

    } else if result == 404 {
        //Verifier token does not exist or Confirmation code is wrong
        print("IN VerifyConfirmationCodePasswordResetHandler | failed 404")
        animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)
    } else if result == 500 {
        print("IN VerifyConfirmationCodePasswordResetHandler | failed 500")
    } else {
        print("IN VerifyConfirmationCodePasswordResetHandler | failed nil")
    }
}

//for password update

func tryToVerifyPasswordConfirmationCode(_ vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryToVerifyPasswordConfirmationCode | start ")

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    let urlString: URL = URL (string: "\(SwaggerClientAPI.basePath)/auth/reset-confirm")!

    var envIsProd: Bool = true

    if urlString.absoluteString.contains("gamma") {
        envIsProd = false
    }

    if envIsProd == false { //for ppe todo remove for prod
        phoneNumberValidationCode = "99999"
    }

    var parameters: Parameters?

        userCurrentID = (userData?.Phone)!
        userCurrentAuthToken = userRegistrationPassword

    let headers: HTTPHeaders = [
        "x-api-key": apiKey
//        "authtype": "0",
//        "authid": userCurrentID,
//        "authtoken": userCurrentAuthToken,
//        "deviceid": userCurrentDeviceID
    ]

    parameters = [
        "verifier": phoneNumberVerifierResponse,
        "token": phoneNumberValidationCode,
        "password": userRegistrationPassword,
        "sendernumber": (userData?.Phone)!
    ]

    print("headers: ", headers)

//    if let params = parameters {
//        print("parameters: ", params)
//    }

    guard let url = URL (string: urlString.absoluteString) else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseString { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 401 {

                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    //code is wrong or user doesn't exist

                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription

                    animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)

if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 200 {
                //success

                print("IN tryToVerifyPasswordConfirmationCode | Success 200")
//                if let result = response.result.value {
//                    let JSON = result as! NSDictionary
//                    if JSON["sessionToken"] != nil {
//                        userCurrentSessionToken = JSON["sessionToken"]! as! String
//                        print("session token: ", userCurrentSessionToken)
//                    }
//                    if JSON["deviceId"] != nil {
//                        userCurrentDeviceID = JSON["deviceId"]! as! String
//                    }
//                    saveToUserDefaults()
//                }
                return completion(200)
            }
            return completion((response.response?.statusCode)!)
    }
}

let tryToVerifyPasswordConfirmationCodeHandler: (Int) -> Void = {
    result in

    print("IN tryToVerifyPasswordConfirmationCodeHandler | ", result)

    if result == 200 {
        print("IN tryToVerifyPasswordConfirmationCodeHandler | success 200")
        //success

        animateUIBanner("Success", subText: "Confirmation code verified.", color: UIWarningSuccessColor, removePending: true)

//Re-sent the verification code successfully.
        globalLoadingSpinner.stopAnimating()

        callNotificationItem("goToLoginPageFromPWReset")

    } else if result == 404 {
        //Verifier token does not exist or Confirmation code is wrong

        animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)
        print("IN tryToVerifyPasswordConfirmationCodeHandler | failed 404")
        globalLoadingSpinner.stopAnimating()

    } else if result == 500 {
        print("IN tryToVerifyPasswordConfirmationCodeHandler | failed 500")
        animateUIBanner("Error", subText: "We're having trouble changing your password. Please contact support at info@wechopgh.com.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN tryToVerifyConfirmationCodeHandler | failed nil")
        globalLoadingSpinner.stopAnimating()

    }
}
