//
//  Profile.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON
import Branch

var passOnAddressOkay = true

func getUserData(_ addressOkay: Bool, completion: @escaping (Int) -> Void ) {
    DispatchQueue.global().async {
        print("IN getUserData | start aSuport: ", addressOkay)
        if hasInternetCheck() == false {
            return completion(999)
        }

        let headers: HTTPHeaders = [
            "x-api-key": apiKey,
            "x-auth-token": userCurrentSessionToken
        ]

        guard let url = URL (string: "\(SwaggerClientAPI.basePath)/user-profile") else {return completion(500)}

        Alamofire.request(url,
                          method: HTTPMethod.get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {

                    //401
                    if response.response?.statusCode == 401 {
                        return completion(401)
                    } else if response.response?.statusCode == 404 {
                        return completion(404)
                    } else if response.response?.statusCode == 500 {
                        return completion(500)
                    } else {
                        if let errorString = response.result.error {
                            print("Error while trying to get profile information: \(errorString)")
                        }
                        if let code = response.response?.statusCode {
                            return completion(code)
                        } else {
                            return completion(999)
                        }
                    }
                }

                if let result = response.result.value {
                    print("IN getUserData | results ")
                    let JSON = result as! NSDictionary
                    //                dump(JSON)
                    if JSON["firstName"] != nil {
                        userData?.FirstName = JSON["firstName"]! as! String
                    }
                    if JSON["lastName"] != nil {
                        userData?.LastName = JSON["lastName"]! as! String
                    }
                    if JSON["destinationAddress"] != nil {
                        userData?.Address = JSON["destinationAddress"]! as! String
                    }
                    if JSON["company"] != nil {
                        userData?.Company = JSON["company"]! as! String
                    }
                    if JSON["phone"] != nil {
                        let result = JSON["phone"]! as! String
                        userData?.Phone = String(result.keepOnlyNumbers)
                    }
                    if JSON["paymentMethod"] != nil {

                        //                    print("payment method")
                        //                    dump(JSON["paymentMethod"])
                        //                    userData?.PaymentMethod = JSON["paymentMethod"]! as! String
                    } else {
                        print("payment method is nil")
                        userData?.PaymentMethod = emptyPaymentType
                        userData?.PaymentMethod.MobileMoney.removeAll()
                        userData?.PaymentMethod.CreditCard.removeAll()

                    }
                    if JSON["inviteUrl"] != nil {
                        //userData?.InviteUrl = JSON["inviteUrl"]! as! String

                        // MARK: Setting setting link with Branch information
                        if let link: String = branchInstance.getShortURL() {
                            userData?.InviteUrl = link
                        }
                    }

                    if JSON["localeCode"] != nil {
                        //                    userData?.LocaleCode = JSON["localeCode"]! as! String
                    }
                    if JSON["totalreferralcredits"] != nil {
                        userData?.TotalReferralCredits  = JSON["totalreferralcredits"]! as! Double
                    }
                    if JSON["expiringreferralcredits"] != nil {
                        userData?.ExpiringReferralCredits = JSON["expiringreferralcredits"]! as! Double
                    }

                    if addressOkay == false {
                        passOnAddressOkay = false
                    } else {
                        passOnAddressOkay = true
                    }
                }
                return completion(200)
        }
    }
}

func setUserPhoneNumber(_ phoneNumber: String, vc: UIViewController, completion: @escaping (Int) -> Void) {
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    var parameters: Parameters?
    
    parameters = [
        "text": phoneNumber
    ]
    
    print("IN setUserPhoneNumber | start ")
    
    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/user-profile/phone") else {return completion(500)}
    
    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                
                //401
                if response.response?.statusCode == 400 {
                    return completion(400)
                } else if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 409 {
                    return completion(409)
                } else {
                    
                    if let errorString = response.result.error {
                        print("Error while trying to get profile information: \(errorString)")
                    }
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            
            if response.result.value != nil {
                print("IN setUserPhoneNumber | success")
                //                dump(response.result.value)
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    if JSON["verifier"] != nil {
                        phoneNumberVerifierResponse = JSON["verifier"]! as! String
                    }
                    
                    if JSON["token"] != nil {
                        if JSON["token"] is NSString {
                            phoneNumberValidationCode = ((JSON["token"] as! NSString) as String)
                        }
                    }
                }
            }
            return completion(200)
    }
}

func tryToVerifyUserPhoneConfirmationCode(_ newPhoneNumber: String, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryToVerifyConfirmationCode | start ")
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    let urlString: URL = URL (string: "\(SwaggerClientAPI.basePath)/user-profile/phone/confirmation")!
    
    var envIsProd: Bool = true
    
    if urlString.absoluteString.contains("gamma") {
        envIsProd = false
    }

    if envIsProd == false { //for ppe todo remove for prod
        phoneNumberValidationCode = "99999"
    }
    
    var parameters: Parameters?
    
    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    parameters = [
        "verifier": phoneNumberVerifierResponse,
        "token": phoneNumberValidationCode,
        "password": "",
        "sendernumber": newPhoneNumber.keepOnlyNumbers
    ]
    guard let url = URL (string: urlString.absoluteString) else {return completion(999)}
    
    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in
            
            guard response.result.isSuccess else {
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                } else if response.response?.statusCode == 409 {
                    return completion(409)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            
            if response.response?.statusCode == 200 {
                //success
            }
            return completion((response.response?.statusCode)!)
    }
}

func setUserAddress(_ company: String, _ newAddress: String, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN setUserAddress | start | in coming from menu? ", sourceView == .menu)
    print("IN setUserAddress | start | newAddress ", newAddress)
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    let parameters: Parameters = [
        "company": company,
        "address": newAddress
    ]
    
    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]

    print("url: ", "\(SwaggerClientAPI.basePath)/user-profile/address")

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/user-profile/address") else {
        return completion(500)
    }

//    print("headers: ", headers)
//    print("parameters: ", parameters)

    Alamofire.request(url,
                      method: HTTPMethod.put,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseString { response in
            
            guard response.result.isSuccess else {
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(409)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }
            
            return completion((response.response?.statusCode)!)
    }
}

func setUserPayment(_ vc: UIViewController, paymentType: Int, completion: @escaping (Int) -> Void) { // TODO verify this logic
    print("IN setUserPayment | start | paymentType: ", paymentType)
    
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }
    
    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }
    
    var paymentObject = [Any]()
    var paramString: String = String()

    var item: [String: Int] = [String: Int]()
    
    if paymentType != 1 {
        item = ["type": paymentType]
    } else {

    }
    
    //    dump(paymentObject)
    guard let data = try? JSONSerialization.data(withJSONObject: item, options: []) else {
        return completion(999)
    }
    
    paramString = String(data: data, encoding: String.Encoding.utf8)!
    
    //    if paymentType == 0 {
    //        paramString = "{"type" : 0 } "
    //    }
    
    //    dump(paramString)

    let headers: HTTPHeaders = [
        "x-api-key": apiKey,
        "x-auth-token": userCurrentSessionToken
    ]
    
    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/user-profile/payment") else {return completion(500)}
    
    var request = URLRequest(url: url)
    request.httpMethod = HTTPMethod.put.rawValue
    request.allHTTPHeaderFields = headers
    request.httpBody = paramString.data(using: .utf8)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    Alamofire.request(request).responseString { response in

        guard response.result.isSuccess else {
            if response.response?.statusCode == 401 {
                return completion(401)
            } else if response.response?.statusCode == 404 {
                return completion(409)
            } else {
                if let errorString = response.result.error {
                    print("Error: \(errorString)")
                }
                return completion((response.response?.statusCode)!)
            }
        }
        
        if let code = response.response?.statusCode {
            print("IN setUserPayment | success ", code)
            
        }
        return completion((response.response?.statusCode)!)
    }
}

//Completion Handlers

let setUserPaymentHandler: (Int) -> Void = {  // TODO verify this logic
    result in
    print("in setUserPaymentHandler | ", result)
    
    if result == 200 {
        if comingFrom.cart == true {
            callNotificationItem("refreshCartPageData")
        }
        globalLoadingSpinner.stopAnimating()
    } else if result == 400 {
        globalLoadingSpinner.stopAnimating()
    } else if result == 401 {
        globalLoadingSpinner.stopAnimating()
    } else if result == 404 {
        globalLoadingSpinner.stopAnimating()
    } else if result == 409 {
        globalLoadingSpinner.stopAnimating()
    } else {
        globalLoadingSpinner.stopAnimating()
    }
}

let setUserAddressHandler: (Int) -> Void = { result in
    print("in setUserAddressHandler | ", result)
    
    if result == 200 {
        animateUIBanner("Success", subText: "Delivery address updated successfully.", color: UIWarningSuccessColor, removePending: true)
        getUserData(true, completion: getUserDataHandler)
        if sourceView == .menu {
            callNotificationItem("moveToRootLoginPage")
            globalLoadingSpinner.stopAnimating()
        }
        
        //reload menu data
        getMenu()
    } else if result == 202 {
        //unsupported address
        getUserData(false, completion: getUserDataHandler)
        
        if sourceView != .menu {
            //refresh AddressVC

            //show modal notification // TODO
            //animateUIBanner("Warning", subText: "Unable to change the address. Please contact info@wechopgh.com if the issue continues.", color: UIWarningErrorColor)

            globalLoadingSpinner.stopAnimating()

            // there should be some reason to call this below getmanu; commenting out for now
            //getMenu()
        } else {
            sourceView = .addressUpdate
            globalLoadingSpinner.stopAnimating()
        }
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 404 {
        //Invalid Address (not found on Google Maps)
        
        animateUIBanner("Error", subText: "We do not yet deliver to this location. Contact support for further questions.", color: UIWarningInformationColor, shouldDismiss: false)
        globalLoadingSpinner.stopAnimating()
    } else {
        globalLoadingSpinner.stopAnimating()
    }
}

let tryToVerifyUserPhoneConfirmationCodeHandler: (Int) -> Void = {
    result in
    
    print("IN tryToVerifyUserPhoneConfirmationCodeHandler | ", result)
    
    if result == 200 {
        print("IN tryToVerifyUserPhoneConfirmationCodeHandler | success 200")
        //success
        
        getUserData((userData?.AddressSupported)!, completion: getUserDataHandler)
        callNotificationItem("updateUserPhoneNumberAndDismissVC")
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        
        print("IN tryToVerifyUserPhoneConfirmationCodeHandler | failed 401")
        
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 404 {
        //Invalid or Unrecognized confirmation token
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        
        print("IN tryToVerifyUserPhoneConfirmationCodeHandler | failed 404")
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 409 {
        //Phone number is being used by someone else
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        
        print("IN tryToVerifyUserPhoneConfirmationCodeHandler | failed 409")
        globalLoadingSpinner.stopAnimating()
        
    } else {
        print("IN tryToVerifyUserPhoneConfirmationCodeHandler | failed nil")
        globalLoadingSpinner.stopAnimating()
    }
}

let setUserPhoneNumberHandler: (Int) -> Void = {
    result in
    print("in setUserPhoneNumberHandler | ", result)
    
    if result == 200 {
        //success move to validate phone number
        callNotificationItem("moveToValidatePhoneVC")
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 400 {
        //Wrong Phone number format
        animateUIBanner("Warning", subText: "Incorrect phone number format.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 404 {
        globalLoadingSpinner.stopAnimating()
        
    } else if result == 409 {
        //Phone number already in use
        animateUIBanner("Error", subText: "There is already an account with this phone number. Try logging in instead.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else {
        globalLoadingSpinner.stopAnimating()
    }
}

let getUserDataHandler: (Int) -> Void = {
    result in
    print("in getUserDataHandler | ", result)

    if result == 200 {
        
        logUser()
        
        if userData?.InviteUrl != nil {
            userShareCodeUrl = "\((userData?.InviteUrl)!)"
        }
        
        //get user map
        setLocationFromUserAddress({ (result) -> Void in
            //            print("user Coordinates Set")
            setUserAddressMapImage({ (result) -> Void in })
        })
        
        if sourceView == .addressUpdate {
            callNotificationItem("refreshAddressVCPage")
            callNotificationItem("moveToDismissUpdateAddress")

            if passOnAddressOkay != true {
                userData?.AddressSupported = passOnAddressOkay
                delay(0.5, closure: {
                    callNotificationItem("showUnsupportedAddressNotification")
                })
            }
        }
    } else if result == 401 {
        //Authentication Failure: either the API Key or the Auth Token are invalid.
        
    } else if result == 500 {
        //user information malformed or incomplete
    } else {
        
    }
}
