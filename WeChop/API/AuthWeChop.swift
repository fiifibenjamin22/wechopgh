//
//  AuthWeChop.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON
import Branch

func tryWCLogin(_ method: HTTPMethod, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryWCLogin | start | ", method.rawValue)

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    if method == HTTPMethod.put {
        headers = ["x-api-key": apiKey]

        parameters = [
            "sessionToken": userCurrentSessionToken,
            "deviceId": userCurrentDeviceID
        ]

    } else { // for .get
        headers = [
            "x-api-key": apiKey,
            "authtype": "0", //for facebook
            "authid": userRegistrationPhone,
            "authtoken": userRegistrationPassword,
            "deviceid": userCurrentDeviceID
        ]
    }

//    print("headers: ", headers)
//    if let parms = parameters {
//        print("WC login parameters: ", parms)
//    }

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token") else {return completion(500)}

    Alamofire.request(url,
                      method: method,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {
                if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if let result = response.result.value {
                let JSON = result as! NSDictionary
                if JSON["sessionToken"] != nil {
                    userCurrentSessionToken = JSON["sessionToken"]! as! String
                    print("session token: ", userCurrentSessionToken)
                }

                if JSON["deviceId"] != nil {
                    if JSON["deviceId"] is NSString {
                        userCurrentDeviceID = (JSON["deviceId"] as! NSString) as String
                    }
                }
                saveToUserDefaults()
            }
            return completion(200)
    }
}

let tryWCLoginHandler: (Int) -> Void = {
    result in
    print("in tryWCLoginHandler | start")
    comingFrom.register = false

    if result == 200 {
        print("IN tryWCLoginHandler | success")
        //move to menu
        callNotificationItem("moveToMenu")

        BranchEvent.customEvent(withName: "User_Signed_In").logEvent()

        getMenu()
        getUserData(true, completion: getUserDataHandler)
        globalLoadingSpinner.stopAnimating()

    } else if result == 401 {

        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        callNotificationItem("animateFailedLoginForWCInputs")
        print("IN tryWCLoginHandler | failed 401 | resign in")
        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        print("IN tryWCLoginHandler | failed 404 | unknown user")

        //create new user
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 500 {
        print("IN tryWCLoginHandler | failed 500")
        globalLoadingSpinner.stopAnimating()
    } else {
        print("IN tryWCLoginHandler | ", result)
        globalLoadingSpinner.stopAnimating()
    }
}

func tryWCRegister(_ referrer: Int, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryWCRegister | start ")

    //loginInFromLaunchScreen = false
    comingFrom.launchScreenToLogin = false
    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    let urlString: URL = URL (string: "\(SwaggerClientAPI.basePath)/auth/token?referrer=\(referrer)")!
    print("url: ", urlString)

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = ["x-api-key": apiKey,
               "deviceid": userCurrentDeviceID]

    parameters = [
        "authId": (userData?.Phone)!,
        "authToken": userRegistrationPassword,
        "authtype": 0, //for WC
        "firstName": (userData?.FirstName)!,
        "lastName": (userData?.LastName)!,
        "phone": (userData?.Phone)!,
        "address": (userData?.Address)!,
        "localeCode": (userData?.LocaleCode)!
    ]

//    print("register headers: ", headers)
//    if let parms = parameters {
//        print("parameters: ", parms)
//    }
    guard let url = URL (string: urlString.absoluteString) else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                if response.response?.statusCode == 400 {
                    //Phone number format is invalid
                    return completion(400)
                } else if response.response?.statusCode == 401 {
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    return completion(404)
                } else if response.response?.statusCode == 409 {
                    return completion(409)
                } else if response.response?.statusCode == 420 {
                    return completion(420)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
//                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 200 {
                //already registered
            } else if response.response?.statusCode == 201 {
                print("IN tryWCRegister | Success ")

                if let result = response.result.value {
                    let JSON = result as! NSDictionary

                    if JSON["verifier"] != nil {
                        phoneNumberVerifierResponse = JSON["verifier"]! as! String
                    }

                    if JSON["token"] != nil {
                        if JSON["token"] is NSString {
                            phoneNumberValidationCode = ((JSON["token"] as! NSString) as String)
                        }
                    }
                }

            }
            return completion((response.response?.statusCode)!)
    }
}

let tryWCRegisterHandler: (Int) -> Void = {
    result in

    if result == 200 {
        print("IN tryWCRegisterHandler | success 200")
        //already registered move to login
        animateUIBanner("Error", subText: "User already registered. Please login", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 201 {
        print("IN tryWCRegisterHandler | success 201")
        //success move to validate account

        if let user = userData {
            BranchAssistant.app.signedUp(user: user)
        }
        comingFrom.register = true
        callNotificationItem("triggerSegueToValidatePhoneWeChop")
        globalLoadingSpinner.stopAnimating()

    } else if result == 400 {
        //Phone number format is invalid
        animateUIBanner("Error", subText: "Please check the phone number and try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()

    } else if result == 401 {
        //resign in
        print("IN tryWCRegisterHandler | failed 401")
        callNotificationItem("tryFacebookLogin")

        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //create new user
        print("IN tryWCRegisterHandler | failed 404")
        callNotificationItem("triggerSegueToRegisterVCFromFacebook")
        globalLoadingSpinner.stopAnimating()

    } else if result == 409 {
        animateUIBanner("Error", subText: "There is already an account with this phone number. Try logging in instead.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 420 {
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 500 {
        print("IN tryWCRegisterHandler | failed 500")
        globalLoadingSpinner.stopAnimating()
    } else {
        print("IN tryWCRegisterHandler | failed nil")
        globalLoadingSpinner.stopAnimating()
    }
}
