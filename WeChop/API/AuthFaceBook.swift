//
//  AuthFaceBook.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import SwiftyJSON
import Branch

//facebook
func getFacebookClientAuthAndID(_ vc: UIViewController) { //if a current access Token get the userID and AuthToken

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    print("in getFacebookClientAuthAndID | start")
    if FBSDKAccessToken.currentAccessTokenIsActive() == true {
        if FBSDKAccessToken.current()?.userID != nil {
            userCurrentID = (FBSDKAccessToken.current()?.userID)!
        }
        if FBSDKAccessToken.current()?.tokenString != nil {
            userCurrentAuthToken = (FBSDKAccessToken.current()?.tokenString)!
        }

    }
}

func tryFBLogin(_ method: HTTPMethod, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryFBLogin | start | ", method.rawValue)

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    if method == HTTPMethod.put {
        headers = ["x-api-key": apiKey]

        parameters = [
            "sessionToken": userCurrentSessionToken,
            "deviceId": userCurrentDeviceID
        ]

    } else {
        headers = [
            "x-api-key": apiKey,
            "authtype": "1", //for facebook
            "authid": userCurrentID,
            "authtoken": userCurrentAuthToken,
            "deviceid": userCurrentDeviceID
        ]
    }

//    print("headers: ", headers)

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token") else {return completion(500)}

    Alamofire.request(url,
                      method: method,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {

                //401
                if response.response?.statusCode == 401 {
                    // re-auth user on fb

                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    // unknown user
                    return completion(404)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if let result = response.result.value {
                let JSON = result as! NSDictionary
                if JSON["sessionToken"] != nil {
                    userCurrentSessionToken = JSON["sessionToken"]! as! String
                    print("session token: ", userCurrentSessionToken)
                }

                if JSON["deviceId"] != nil {
                    if JSON["deviceId"] is NSString {
                        userCurrentDeviceID = (JSON["deviceId"] as! NSString) as String
                    }
                }
                saveToUserDefaults()
            }
            return completion(200)
    }
}

let tryFBLoginHandler: (Int) -> Void = {
    result in
    comingFrom.register = false

    if result == 200 {
        print("IN tryFBLoginHandler | success")

        BranchEvent.customEvent(withName: "User_Signed_In").logEvent()

        //move to menu
        callNotificationItem("moveToMenu")

        getMenu()
        getUserData(true, completion: getUserDataHandler)
        globalLoadingSpinner.stopAnimating()
    } else if result == 401 {
        //resign in
        print("IN tryFBLoginHandler | failed 401")

        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        // start request to re-sign in
        callNotificationItem("tryFacebookLogin")

        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //create new user

        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)

        //force logout of facebook todo validate that this is needed
        //        if FBSDKAccessToken.currentAccessTokenIsActive() == true {
        //            FBSDKLoginManager().logOut()
        //        }

        print("IN tryFBLoginHandler | failed 404")
        FBSDKAccessToken.refreshCurrentAccessToken(nil)
        globalLoadingSpinner.stopAnimating()
         
    } else if result == 500 {
        print("IN tryFBLoginHandler | failed 500")
        globalLoadingSpinner.stopAnimating()

    } else {
        print("IN tryFBLoginHandler | failed nil")
        globalLoadingSpinner.stopAnimating()
    }
}

func tryFBRegister(_ referrer: Int, vc: UIViewController, completion: @escaping (Int) -> Void) {
    print("IN tryFBRegister | start ")

    comingFrom.launchScreenToLogin = false

    if hasInternetCheck() == false {
        noInternetWarning()
        return completion(999)
    }

    if let spinner =  vc.view.viewWithTag(1) as? NVActivityIndicatorView {
        spinner.startAnimating()
    } else {
        createGlobalLoadingSpinner(vc.view)
    }

    var headers: HTTPHeaders = HTTPHeaders()
    var parameters: Parameters?

    headers = ["x-api-key": apiKey,
               "deviceid": userCurrentDeviceID]
    
    parameters = [
        "authId": userCurrentID,
        "authToken": userCurrentAuthToken,
        "authtype": "1", //for FB
        "firstName": (userData?.FirstName)!,
        "lastName": (userData?.LastName)!,
        "phone": (userData?.Phone)!,
        "address": (userData?.Address)!,
        "localeCode": (userData?.LocaleCode)!
    ]

//    print("headers: ", headers)
//
//    if let parms = parameters {
//        print("parameters: ", parms)
//    }

    let urlString: URL = URL (string: "\(SwaggerClientAPI.basePath)/auth/token?referrer=\(referrer)")!
    print("url: ", urlString)

    guard let url = URL (string: "\(SwaggerClientAPI.basePath)/auth/token?referrer=\(referrer)") else {return completion(999)}

    Alamofire.request(url,
                      method: HTTPMethod.post,
                      parameters: parameters,
                      encoding: JSONEncoding.default,
                      headers: headers)
        .validate()
        .responseJSON { response in

            guard response.result.isSuccess else {
                if response.response?.statusCode == 400 {
                    //Phone number format is invalid
                    return completion(400)
                } else if response.response?.statusCode == 401 {
                    //Authentication type is unrecognized / Id does not match token
                    return completion(401)
                } else if response.response?.statusCode == 404 {
                    //User is already registered with a different phone / Phone number is already registered with a different user
                    return completion(404)
                } else if response.response?.statusCode == 409 {
                    //phone already registered
                    return completion(409)
                } else if response.response?.statusCode == 420 {
                    //phone already registered
                    return completion(420)
                } else {
                    if let errorString = response.result.error {
                        print("Error: \(errorString)")
                    }
                    let errorMSG = response.result.error!.localizedDescription
                    animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
if let code = response.response?.statusCode {
                        return completion(code)
                    } else {
                        return completion(999)
                    }
                }
            }

            if response.response?.statusCode == 200 {
                //already registered

            } else if response.response?.statusCode == 201 {
                //success

                print("IN tryFBRegister | Success ")

                if let result = response.result.value {
                    let JSON = result as! NSDictionary

                    if JSON["verifier"] != nil {
                        phoneNumberVerifierResponse = JSON["verifier"]! as! String
                    }

                    if JSON["token"] != nil {
                        if JSON["token"] is NSString {
                            phoneNumberValidationCode = ((JSON["token"] as! NSString) as String)
                        }
                    }
                }

            }

            return completion((response.response?.statusCode)!)
    }
}

let tryFBRegisterHandler: (Int) -> Void = {
    result in

    if result == 200 {
        print("IN tryFBRegisterHandler | success 200")
        animateUIBanner("Error", subText: "User already registered. Please login", color: UIWarningErrorColor)

        //already registered move to login
        globalLoadingSpinner.stopAnimating()

    } else if result == 201 {
        print("IN tryFBRegisterHandler | success 201")

        if let user = userData {
            BranchAssistant.app.signedUp(user: user)
        }

        //success move to validate account
        comingFrom.register = true
        callNotificationItem("moveToRegisterVerificationVC")
        globalLoadingSpinner.stopAnimating()

    } else if result == 400 {
        animateUIBanner("Error", subText: "Please check the phone number and try again.", color: UIWarningErrorColor)

        globalLoadingSpinner.stopAnimating()
    } else if result == 401 {
        //resign in
        print("IN registerFacebookRequestHandler | failed 401")

        callNotificationItem("tryFacebookLogin")

        globalLoadingSpinner.stopAnimating()

    } else if result == 404 {
        //create new user

        animateUIBanner("Error", subText: "There is already an account with this phone number. Try logging in instead.", color: UIWarningErrorColor)

        print("IN registerFacebookRequestHandler | failed 404")

        callNotificationItem("triggerSegueToRegisterVCFromFacebook")

        globalLoadingSpinner.stopAnimating()

    } else if result == 409 {
        //phone already registered?

        print("IN tryFBRegisterHandler | failed 409")
        animateUIBanner("Error", subText: "There is already an account with this phone number. Try logging in instead.", color: UIWarningErrorColor)

        globalLoadingSpinner.stopAnimating()

    } else if result == 420 {
        print("IN tryFBRegisterHandler | failed 420")
        animateUIBanner("Error", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
        globalLoadingSpinner.stopAnimating()
    } else if result == 500 {
        print("IN tryFBRegisterHandler | failed 500")
        globalLoadingSpinner.stopAnimating()
    } else {
        print("IN tryFBRegisterHandler | failed nil")
        globalLoadingSpinner.stopAnimating()
    }
}

func getFacebookUserInformation(_ completion: @escaping (Bool) -> Void) {
    print("In getFacebookUserInformation | start")

    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
        if error == nil {
            let JSON = result as! NSDictionary
            if JSON["first_name"] != nil {
                userData?.FirstName = JSON["first_name"]! as! String
            }
            if JSON["last_name"] != nil {
                userData?.LastName = JSON["last_name"]! as! String
            }
            return completion(true)
        } else {
            print(error?.localizedDescription ?? "Not found")
            return completion(false)
        }
    })
}

let getFacebookUserInformationHandler: (Bool) -> Void = { result in
    globalLoadingSpinner.stopAnimating()
    if result == true {
        print("in getFacebookUserInformationHandler | success")

    } else {
        print("in getFacebookUserInformationHandler | failure")

    }
}
