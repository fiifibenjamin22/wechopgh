//
//  MenuNavigationVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 8/27/18.
//  Copyright © 2019 WeChop. All rights reserved.
//import UIKit
import Hero// Used for status bar hidden/visible logic
class TopNavigationViewController: UINavigationController {    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        print("in TopNavigationViewController")
        super.viewDidLoad()

        self.navigationController?.hero.isEnabled = true
        if currentRootView == .login { //go to login
            segue(name: "LoginSegueFromRoot", v: self, type: .cover(direction: .up))
        } else { //goto Menu
            segue(name: "menuSegueFromRoot", v: self, type: .none)
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        print("in TopNavigationViewController | start")
        self.navigationController?.hero.isEnabled = true
    }
}
