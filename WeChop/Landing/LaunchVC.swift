//
//  LaunchVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 8/27/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import SwiftDate

open class LaunchVC: UIViewController {
    open var pulsing: Bool = false
    var logoImageText: UILabel = UILabel()

    public init() {
        super.init(nibName: nil, bundle: nil)
        print("IN LaunchVC | start")
        UIApplication.shared.isStatusBarHidden = true
        createLoadingText()

        let currentCount = defaults.integer(forKey: "launchCount") // get current number of times app has been launched
        defaults.set(currentCount+1, forKey: "launchCount") // increment received number by one
        defaults.synchronize() // save changes to disk
        
        //get app version
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }

        if let build = Bundle.main.infoDictionary?["CFBundleVersion"]  as? String {
            appVersion = "\(appVersion).\(build)"
        }
    }
    
    func createLoadingText() {
        
        logoImage.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: self.view.frame.width - 100)
        logoImage.center.x = view.center.x
        logoImage.center.y = view.center.y
        logoImage.isUserInteractionEnabled = false
        view.addSubview(logoImage)
        
        logoImageText.frame = CGRect(x: 0, y: logoImage.frame.maxY - 42, width: self.view.frame.width - 100, height: 50)
        logoImageText.center.x = view.center.x
        logoImageText.text = appDescription
        logoImageText.font = logoImageText.font.withSize(21)
        logoImageText.textAlignment = .center
        logoImageText.textColor = UIColor.black
        logoImageText.isUserInteractionEnabled = false
        view.addSubview(logoImageText)
        self.view.bringSubview(toFront: logoImageText)
        
        //UI reformating ofr different screens
        if isIPhoneSE == true { // SE
            logoImageText.frame = CGRect(x: 0, y: logoImage.frame.maxY - 30, width: self.view.frame.width - 100, height: 35)
            logoImageText.font = logoImageText.font.withSize(16)
            logoImageText.center.x = view.center.x
        } else if self.view.frame.width > 380 { // 7 and 8 Plus
            logoImageText.frame = CGRect(x: 0, y: logoImage.frame.maxY - 45, width: self.view.frame.width - 100, height: 50)
            logoImageText.font = logoImageText.font.withSize(24)
            logoImageText.center.x = view.center.x
        } else if isIPhoneX == true {
            //no changes needed
        }        
    }
        
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
