//
//  LaunchRootVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 8/27/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import Hero
import Kingfisher

class LaunchRootViewController: UIViewController {
    
    fileprivate var rootViewController: UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hero.isEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(showMainVC), name: NSNotification.Name(rawValue: "showMainVC"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentMenuFromLaunch), name: NSNotification.Name(rawValue: "getCurrentMenuFromLaunch"), object: nil)
        
        showLaunchVC()
    }
    
    @objc func getCurrentMenuFromLaunch() {
        print("in LaunchVC getCurrentMenuFromLaunch | start")
        globalGetUserMenu(self, useSelectedDate: false, fromLogin: true)
    }
    
    func showLaunchVC() {
        print("In showLaunchVC | start")
        
        //for image cacheing
        ImageDownloader.default.downloadTimeout = 60.0 //60 seconds timeout on loading images
        ImageCache.default.maxCachePeriodInSecond = 60 * 60 * 24 * 30 //30 days for the image cache before its downloaded again

        //device setup
        getPublicIP()
        setDeviceType(view)
        userCurrentLocaleCode = "\(Locale.current.languageCode!)-GH"

        //Configure Branch Links
        configureBranch()

        getSavedUserDefaults()
        setCurrentlySelectedMenuDay() //sets the current day as today
        moveOutOfLaunchScreen()
    }
    
    /// Does not transition to any other UIViewControllers, LaunchVC only
    func moveOutOfLaunchScreen() {
        print("IN moveOutOfLaunchScreen | start ")

        rootViewController?.willMove(toParentViewController: nil)
        rootViewController?.removeFromParentViewController()
        rootViewController?.view.removeFromSuperview()
        rootViewController?.didMove(toParentViewController: nil)
        
        let LaunchVCTemp = LaunchVC()
        rootViewController = LaunchVCTemp
        LaunchVCTemp.pulsing = true
        LaunchVCTemp.willMove(toParentViewController: self)
        addChildViewController(LaunchVCTemp)
        view.addSubview(LaunchVCTemp.view)
        LaunchVCTemp.didMove(toParentViewController: self)
        
        //Check if user has cached credentials and if so try to login and move to menu | if login fails move to login screen
        if userCurrentSessionToken != "" && userCurrentDeviceID != "" {
            if hasInternetCheck() == true {
                //loginInFromLaunchScreen = true
                comingFrom.launchScreenToLogin = true
                delay(0.5) { //delay to see launch logo
                    signInUser(self, completion: signInRequestHandler)
                }
            } else {
                noInternetWarning()
                self.showMainVC()
            }
        } else {
            delay(0.5) { //delay is used to mitigate flicker in loading screen
                self.showMainVC()
            }
        }
    }

    /// Displays the main Navigation Controller
    @objc func showMainVC() {
        print("in moving FromLaunchVC")
        guard !(rootViewController is TopNavigationViewController) else { return }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nav =  storyboard.instantiateViewController(withIdentifier: "TopNavigationViewController") as! UINavigationController
        nav.willMove(toParentViewController: self)
        addChildViewController(nav)

        UIApplication.shared.isStatusBarHidden = false

        if let rootViewController = self.rootViewController {
            self.rootViewController = nav
            rootViewController.willMove(toParentViewController: nil)
            transition(from: rootViewController, to: nav, duration: 0.0, options: [.transitionCrossDissolve, .curveEaseOut],
                animations: {() -> Void in
            }, completion: { _ in
                nav.didMove(toParentViewController: self)
                rootViewController.removeFromParentViewController()
                rootViewController.didMove(toParentViewController: nil)
            })
        } else {
            rootViewController = nav
            view.addSubview(nav.view)
            nav.didMove(toParentViewController: self)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        switch rootViewController {
        case is LaunchVC:
            return false
        case is TopNavigationViewController:
            return false
        default:
            return false
        }
    }
}
