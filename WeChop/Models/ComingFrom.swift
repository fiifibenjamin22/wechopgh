//
//  NavigationTrackClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 12/3/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

final class ComingFrom {
    static let shared = ComingFrom()

    var register: Bool = false //comingFromRegister
    var menuToUpdateAddress: Bool = false //comingFromMenuToUpdateAddress
    var menuDetailsToDeleteCart: Bool = false //tryToDeleteCartComingFromMenuDetails
    var cart: Bool = false //comingFromCart
    var menu: Bool = false //isInMenuPage
    var inviteShowInviteDialogue: Bool = false //shouldShowShareDialogueOnLoad
    var menuDaySelectionPressed: Bool = false //didSelectMenuFromSelectionTable
    var cartOrderComplete: Bool = false //comingFromCartOrderComplete
    var menuDetailsCreateNewCart: Bool = false //comingFromStartNewCart
    var cartToMenuDetails: Bool = false //comingFromSCartToMenuDetails
    var launchScreenToLogin: Bool = false //loginInFromLaunchScreen
    var cartToOrders: Bool = false //shouldMoveToOrdersFromCart
    var ordersToMenu: Bool = false //shouldMoveFromOrdersVCToMenu
    var ordersUpcomingTableView: Bool = false //onOrdersVCUpcomingView

    private init() {}
}
