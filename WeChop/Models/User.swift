//
//  ProfileClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 8/27/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import CoreLocation

class User: NSObject {
    var FirstName: String = "Jane"
    var LastName: String = "Doe"
    var Company: String
    var Address: String
    var Phone: String
    var Location: CLLocationCoordinate2D
    var PaymentMethod: Payment
    var InviteUrl: String
    var LocaleCode: String
    var TotalReferralCredits: Double = 0.0
    var ExpiringReferralCredits: Double = 0.0
    var AddressSupported: Bool = true

    init(firstName: String, lastName: String, company: String, address: String, phone: String, location: CLLocationCoordinate2D, paymentMethod: Payment, inviteUrl: String, localeCode: String, totalReferralCredits: Double = 0.0, expiringReferralCredits: Double = 0.0, addressSupported: Bool = true) {
        self.FirstName = firstName
        self.LastName = lastName
        self.Company = company
        self.Address = address
        self.Phone = phone
        self.Location = location
        self.PaymentMethod = paymentMethod
        self.InviteUrl = inviteUrl
        self.LocaleCode = localeCode
        self.TotalReferralCredits = totalReferralCredits
        self.ExpiringReferralCredits = expiringReferralCredits
        self.AddressSupported = addressSupported

    }
}

class Payment: NSObject {
    var PaymentType: Int
    var MobileMoney: [MobileMoney]
    var CreditCard: [CreditCard]
    var Instructions: [String]
    
    init(paymentType: Int, mobileMoney: [MobileMoney], creditCard: [CreditCard], instructions: [String]) {
        self.PaymentType = paymentType
        self.MobileMoney = mobileMoney
        self.CreditCard = creditCard
        self.Instructions = instructions
    }
}

class MobileMoney: NSObject {
    var Network: Int
    var Phone: String
    
    init(network: Int, phone: String) {
        self.Network = network
        self.Phone = phone
    }
}

class CreditCard: NSObject {
    var Number: Int
    var Expiration: String
    var CVV: Int
    
    init(number: Int, expiration: String, cvv: Int) {
        self.Number = number
        self.Expiration = expiration
        self.CVV = cvv
    }
}
