//
//  MenuItem.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/7/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class MenuData: NSObject {
    var ID: String
    var Name: String
    var Description: String
    var Price: Double
    var Image: UIImage = UIImage(named: "no_dishImage")!
    var ImageURL: URL = URL(string: "https://s3-eu-west-1.amazonaws.com/wechop-data-objects/pictures/dishes/no_dishImage.png")!
    var RestaurantName: String
    var RestaurantID: String
    var OrderCount: Int = 0
    var Soldout: Bool = false
    var TaxApplicable: Bool = false
    var HeroID: String = ""
    var Date: String = ""

    init(id: String, name: String, description: String, price: Double, image: UIImage = UIImage(named: "no_dishImage")!, imageURL: URL = URL(string: "https://s3-eu-west-1.amazonaws.com/wechop-data-objects/pictures/dishes/no_dishImage.png")!, restaurantName: String, restaurantID: String, orderCount: Int = 0, soldout: Bool = false, taxApplicable: Bool = false, heroID: String = "", date: String = "") {
        self.ID = id
        self.Name = name
        self.Description = description
        self.Price = price
        self.Image = image
        self.ImageURL = imageURL
        self.RestaurantName = restaurantName
        self.RestaurantID = restaurantID
        self.OrderCount = orderCount
        self.Soldout = soldout
        self.TaxApplicable = taxApplicable
        self.HeroID = heroID
        self.Date = date

    }
}
