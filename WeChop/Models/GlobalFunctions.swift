//
//  GlobalFunctions.swift
//  WeChop
//
//  Created by Alex on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import CoreLocation
import GradientView
import MapKit
import FBSDKLoginKit
import StoreKit
import Kingfisher
import UserNotifications
import NotificationBannerSwift
import MarqueeLabel
import SwiftMessages
import Crashlytics
import SwiftyGif
import Ipify
import SwiftyJSON
import EzPopup
import Branch

func configureBranch() {
    branchObject.title = "Share WeChop"
    branchObject.contentDescription = "Share WeChop to save on your next order."
    branchObject.imageUrl = "https://wechopgh.com/img/logo.png"
    branchObject.publiclyIndex = true
    branchObject.locallyIndex = true
    branchObject.contentMetadata.customMetadata["key1"] = "value1"

    branchLinkProperty.channel = "facebook"
    branchLinkProperty.feature = "sharing"
    branchLinkProperty.campaign = "content 123 launch"
    branchLinkProperty.stage = "new user"
    branchLinkProperty.tags = ["one", "two", "three"]

    branchLinkProperty.addControlParam("$desktop_url", withValue: "https://wechopgh.com/download.html")
    branchLinkProperty.addControlParam("$ios_url", withValue: "https://wechopgh.com/download.html")
    branchLinkProperty.addControlParam("$ipad_url", withValue: "https://wechopgh.com/download.html")
    branchLinkProperty.addControlParam("$android_url", withValue: "https://wechopgh.com/download.html")
    branchLinkProperty.addControlParam("$match_duration", withValue: "2000")
    branchLinkProperty.addControlParam("custom_data", withValue: "yes")
    branchLinkProperty.addControlParam("look_at", withValue: "this")
    branchLinkProperty.addControlParam("nav_to", withValue: "over here")
    branchLinkProperty.addControlParam("random", withValue: UUID.init().uuidString)
}

public func track(_ message: String, file: String = #file, function: String = #function, line: Int = #line ) {
    print("\(message) called from \(function) \(file):\(line)")
}

private func showNetworkActivity() {
    // Turn on network indicator in status bar
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
}

private func hideNetworkActivity() {
    // Turn off network indicator in status bar
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
}

func getPublicIP() {
    Ipify.getPublicIPAddress { result in
        switch result {
        case .success(let ip):
            currentUserIP = ip
        case .failure(let error):
            print("getPublicIP error: ", error.localizedDescription)
        }
    }
}

func decodeNotification(_ payload: [AnyHashable: Any]) -> NotificationData? {

    if let userInfo = payload as? [String: Any] {
        let rootJSON = JSON(userInfo)
//        let decoder = JSONDecoder()
        var data: NotificationData?

        do {
            data = try JSONDecoder().decode(NotificationData.self, from: rootJSON.rawData())
        } catch let error {
            print("jSON decode error: ", error.localizedDescription)
        }

        return data
    } else {
        return nil
    }
}

func modelIdentifier() -> String {
    if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
    var sysinfo = utsname()
    uname(&sysinfo) // ignore return value
    return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
}

func create<T>(_ setup: ((T) -> Void)) -> T where T: NSObject { //used to more quickly instantiate views
    let obj = T()
    setup(obj)
    return obj
}

func hasInternetCheck() -> Bool {
    if Reachability.isConnectedToNetwork() {
        return true
    } else {
        return false
    }
}

func application(_ application: UIApplication,
                 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    
    print("in didRegisterForRemoteNotificationsWithDeviceToken | start")
    let tokenParts = deviceToken.map { data -> String in
        return String(format: "%02.2hhx", data)
    }
    
    let token = tokenParts.joined()
    userCurrentDeviceID = token
    defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
    defaults.synchronize()
    print("Device Token: \(token)")
}

func refreshAllDateInformation() {
    print("IN populateAllDateInformation | start")

//    var localTimeZoneName = TimeZone.current.identifier
//
//    if localTimeZoneName == "Asia/Saigon" {
//        localTimeZoneName = "Asia/Ho_Chi_Minh"
//    }
//
//    if let zone = Zones.init(rawValue: localTimeZoneName) {
//        currentUserRegion = Region(calendar: Calendars.gregorian, zone: zone, locale: Locales.english) // set local with English
//    } else {
//        currentUserRegion = Region(calendar: Calendars.gregorian, zone: Zones.africaAccra, locale: Locales.english) // set ghana region with English
//    }

    currentUserRegion = Region(calendar: Calendars.gregorian, zone: Zones.africaAccra, locale: Locales.english) // set local with English

    currentGhanaRegion = Region(calendar: Calendars.gregorian, zone: Zones.africaAccra, locale: Locales.english) // set ghana region with English
    currentUserTime = DateInRegion(Date(), region: currentUserRegion)
    currentGhanaTime = DateInRegion(Date(), region: currentGhanaRegion)
    currentGhanaCutoffTime = currentGhanaTime.dateBySet(hour: 10, min: 30, secs: 0)!
    
    if currentUserTime > currentGhanaCutoffTime || (currentUserTime.weekdayName(.default) == "Saturday" || currentUserTime.weekdayName(.default) == "Sunday") {
        isCurrentTimeAfterTodaysCutoffTime = true
    } //checks if day is after
    
    print("IN populateAllDateInformation | CGT: ", currentUserTime.toFormat("yy-MM-dd"))
}

func setCurrentlySelectedMenuDay() { //this sets the currently selected menu day which indicates which menu to load.  Is seperated from refreshAllDateInformation because there are times that this field is updated (if time is after 11:00Am or if user is looking at future menu
    currentlySelectedMenuDay = currentUserTime
}

func getDaySuffix (dayOfMonth: Int) -> String {
    
    switch dayOfMonth {
    case 1, 21, 31: return "st"
    case 2, 22: return "nd"
    case 3, 23: return "rd"
    default: return "th"
    }
}

func createGlobalLoadingSpinner(_ v: UIView) {
    //    print("in createGlobalLoadingSpinner | start")
    globalLoadingSpinner.backgroundColor = .clear
//    globalLoadingSpinner.layer.borderColor = UIColor.clear.cgColor
//    globalLoadingSpinner.layer.cornerRadius = globalLoadingSpinner.frame.width / 12
    globalLoadingSpinner.center.x = v.center.x
    globalLoadingSpinner.center.y = v.center.y
    globalLoadingSpinner.tag = 1
    globalLoadingSpinner.layer.zPosition = 1000000000
    
//    globalLoadingSpinner.layer.shadowColor = UIColor.black.cgColor
//    globalLoadingSpinner.layer.shadowOpacity = 1.0
//    globalLoadingSpinner.layer.shadowOffset = globalShadowOffset
//    globalLoadingSpinner.layer.shadowRadius = 1
//    globalLoadingSpinner.layer.masksToBounds = true
    v.addSubview(globalLoadingSpinner)
    globalLoadingSpinner.startAnimating()
}

//consistant buttons

func createNavigationLeftBackButton (v: UIView) -> SpringButton { //seperated from nav title creation because sometimes only left button is needed
    let button = SpringButton(frame: CGRect(x: globalXAxisIndent - 15, y: globalStatusBarHeight, width: 50, height: 50))
    
    if isIPhoneX == true {
        button.frame = CGRect(x: globalXAxisIndent - 15, y: globalIPhoneXTopYPadding + (globalStatusBarHeight / 2), width: 50, height: 50)
    }
    button.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
    button.setImage(UIImage(named: "back_arrow.png")?.addImagePadding(x: 100, y: 100), for: .normal)
    button.tag = 12
    button.contentVerticalAlignment = .center
    button.contentHorizontalAlignment = .center
    button.backgroundColor = .clear
    button.layer.zPosition = 2
    v.addSubview(button)
    return button
}

func formatPrice(p: Double) -> String { // to two decimal places
    return String(format: "%.2f", p)
}

func getPaymentMethodString() -> String { // TODO update this logic with real values
    
    switch userData?.PaymentMethod.PaymentType {
    case 0: return "Cash"
    case 1: return "Mobile money"
    case 2: return "Mobile money"
    case 3: return "Mobile money"
    default:
        return ""
    }
}

func setLocationFromUserAddress(_ completed: @escaping (Bool) -> Void) {
    print("IN getLocationFromUserAddress | start")
    //    print("IN getLocationFromUserAddress | address: ", userData?.DestinationAddress)
    
    if userData?.Address != nil {
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString((userData?.Address)!) { (placemarks, error) in
            guard
                let placemarks = placemarks
                else
            {
                print("IN getLocationFromUserAddress | No location found from addresss \(String(describing: userData?.Address))")
                return // TODO action if no address
            }
            print("IN getLocationFromUserAddress | location found \(String(describing: placemarks.first?.location))")
            userData?.Location = (placemarks.first?.location?.coordinate)!
            completed(true)
        }
    } else {
        print("IN getLocationFromUserAddress | using default location")
        userData?.Location = CLLocationCoordinate2D(latitude: 18.495651759752, longitude: 73.809987567365)
        completed(false)
    }
}

func setUserAddressMapImage(_ completed: @escaping (Bool) -> Void) {
    
    print("In setUserAddressMapImage | start")
    
    var coords = CLLocationCoordinate2D()
    
    if userData?.Location != nil {
        coords = (userData?.Location)!
    } else {
        completed(false)
    }
    let distanceInMeters: Double = 1000
    
    let options = MKMapSnapshotter.Options()
    options.region = MKCoordinateRegionMakeWithDistance(coords, distanceInMeters, distanceInMeters)
    //    options.mapType = .standard
    //    options.showsPointsOfInterest = true
    //    options.showsBuildings = true
    options.size = CGSize(width: 200, height: 150)
    
    let bgQueue = DispatchQueue.global(qos: .background)
    let snapShotter = MKMapSnapshotter(options: options)
    let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
    pinView.pinTintColor = hexRed.Color
    let pinImage = pinView.image
    
    snapShotter.start(with: bgQueue, completionHandler: { (snapshot, error) in
        guard error == nil else {
            
            userAddressMapImage = UIImage(named: "map_example")!
            return completed(false)
            
        }
        
        if let snapShotImage = snapshot?.image, let coordinatePoint = snapshot?.point(for: coords) {
            
            UIGraphicsBeginImageContextWithOptions(snapShotImage.size, false, snapShotImage.scale)
            snapShotImage.draw(at: CGPoint.zero)
            let fixedPinPoint = CGPoint(x: coordinatePoint.x - pinImage!.size.width / 2, y: coordinatePoint.y - pinImage!.size.height)
            pinImage!.draw(at: fixedPinPoint)
            
            let mapImage = UIGraphicsGetImageFromCurrentImageContext()
            
            DispatchQueue.main.async {
                userAddressMapImage = mapImage!
                completed(true)
            }
            UIGraphicsEndImageContext()
        }
    })
}

func resetUserInformation() {
    print("in resetUserInformation | start")

    branchInstance.logout()
    loginVCIsUserInLoginAreaAndNotInRegisterArea = true
    ImageCache.default.clearDiskCache() //clears cache of meun images
    userData = nil
    userCurrentSessionToken = ""
    userCurrentAuthToken = ""
//    userCurrentDeviceID = ""
    userCurrentID = ""
    userRegistrationPassword = ""

    loginManager.logOut()
//    loginManager.loginBehavior = .web //change to web login to completly log out user

    //comingFromCart = false
    comingFrom.cart = false

    //comingFromMenuToUpdateAddress = false
    comingFrom.menuToUpdateAddress = false
    //comingFromRegister = false
    comingFrom.register = false
    //loginInFromLaunchScreen = false
    comingFrom.launchScreenToLogin = false
    //clear defaults session and device ID
    defaults.set("", forKey: "userSession")
    defaults.synchronize()
    createNewUserProfile()
}

func createNewUserProfile() {
    print("in createNewUserProfile | start")
    userData = emptyNewUser
    userData?.PaymentMethod.MobileMoney.removeAll()
    userData?.PaymentMethod.CreditCard.removeAll()
}

func getUserUpcomingAndCompletedOrders(_ v: UIViewController) {
    print("in getUserUpcomingAndCompletedOrders | start")

    DispatchQueue.global().async {
        completedOrders.removeAll()
        upcomingOrders.removeAll()
        fullOrderList.removeAll()
        tryToGetOrderData("history", vc: v, completion: tryToGetOrderHandler)
        tryToGetOrderData("future", vc: v, completion: tryToGetOrderHandler)
        tryToGetOrderData("current", vc: v, completion: tryToGetOrderHandler)
    }
}

func getUserCompletedOrders(_ v: UIViewController) {
    print("in getUserCompletedOrders | start")
    
    completedOrders.removeAll()
    fullOrderList.removeAll()
    tryToGetOrderData("history", vc: v, completion: tryToRefreshOrderHandler)
}

func checkIfUserShouldRateApp() {
    //check app load count and load review request
    if defaults.integer(forKey: "launchCount") > 0 {
        let appCountNumber = defaults.integer(forKey: "launchCount")
        
        if appCountNumber == 5 {
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                // Fallback on earlier versions
            }
        } else if appCountNumber == 50 { // reset after 50 times opening it
            defaults.set(1, forKey: "launchCount")
            defaults.synchronize()
        } else if appCountNumber == 1 { //replace when ready
            //            } else {
        }
    }
}

func createNavigationTitle(title: String, v: UIView, leftButton: SpringButton) -> SpringButton {
    
    let button = SpringButton(frame: CGRect(x: 0, y: globalStatusBarHeight, width: v.frame.width, height: globalLargeButtonHeight))
    
    if isIPhoneX == true {
        
        button.frame = CGRect(x: 0, y: globalIPhoneXTopYPadding, width: v.frame.width, height: globalLargeButtonHeight)
    }
    
    button.titleLabel?.font = UIFont.Theme.size17

    button.setTitleColor(newHexBlack1.Color, for: .normal)
    button.setTitle(title, for: .normal)
    button.contentVerticalAlignment = .center
    button.contentHorizontalAlignment = .left
    button.isUserInteractionEnabled = false
    button.tag = 12
    button.layer.zPosition = 1
    button.backgroundColor = UIColor.white
    
    button.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalNavTitleIndentSize, bottom: 0, right: 0)
    
    if v.frame.width < 330 {
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalNavTitleIndentSize - 15, bottom: 0, right: 0)
    }
    
    button.layer.shadowColor = hexDarkGray.cgColor
    button.layer.shadowOpacity = 0.5
    button.layer.shadowOffset = globalShadowOffset
    button.layer.shadowRadius = 1
    button.center.y = leftButton.center.y
    button.contentVerticalAlignment = .center
    v.addSubview(button)
    
    let border = SpringButton()
    border.frame = CGRect(x: 0, y: button.frame.maxY, width: v.frame.width, height: 1)
    border.center.x = v.center.x
    //    border.backgroundColor = hexLightGray
    //    border.layer.shadowColor = hexDarkGray.cgColor
    //    border.layer.shadowOpacity = 0.5
    //    border.layer.shadowOffset = CGSize(width: -1, height: 1)
    //    border.layer.shadowRadius = 1
    
    border.isUserInteractionEnabled = false
    border.layer.zPosition = 3
    v.addSubview(border)
    
    return border
}

func createFooterButton (text: String, v: UIView) -> SpringButton {
    
    var button = SpringButton(frame: CGRect(x: 0, y: v.frame.height - globalLargeButtonHeight, width: v.frame.width + 2, height: globalLargeButtonHeight))

    if isIPhoneX == true {
        button.frame = CGRect(x: 0, y: v.frame.height - (globalLargeButtonHeight + globalIPhoneXTopYPadding), width: v.frame.width, height: globalLargeButtonHeight + globalIPhoneXTopYPadding)
    }

    button.setTitle(text, for: .normal)
    button.titleLabel?.font = UIFont.Theme.size17
    button.center.x = v.center.x
    button.contentVerticalAlignment = .center

    if isIPhoneX == true {
        button.contentVerticalAlignment = .top
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    button.backgroundColor = hexRed.Color
    button.tag = 13
    v.addSubview(button)
    return button
}

// detect and set iOS device type
func setDeviceType(_ v: UIView) {
    
    //device information
    currentDeviceModel = modelIdentifier()
    currentDeviceVersion = UIDevice.current.systemVersion
    
    //detect ipad for sizing
    let hardwareModel = modelIdentifier()
    print("MODEL: ", hardwareModel)
    
    if hardwareModel.contains("iPad") {
        isIpad = true
        if hardwareModel == "iPad6,8" || hardwareModel == "iPad7,1"{
        } else {
            isShortIpad = true // used for UI Changes for smaller iPads
        }
    }
    
    //check if iphone X
    if #available(iOS 11.0, *) {
        if UIScreen.main.bounds.height >= 812 {
            isIPhoneX = true
        } else {
            isIPhoneX = false
        }
    }
    
    if v.frame.width < 330 {
        isIPhoneSE = true
    } else {
        isIPhoneSE = false
    }
    
    //iphoneX
    if #available(iOS 11.0, *) {
        func prefersHomeIndicatorAutoHidden() -> Bool {
            return true
        }
    }
    
    //check if simulator
    #if targetEnvironment(simulator)
    isSimulator = true
    
    #else
    
    #endif
    
    //getIP
    
}

func logUser() {
    // MARK: Set branch and Firebase ID
    if userData != nil {
        if let phoneNumber = userData?.Phone {
            Crashlytics.sharedInstance().setUserIdentifier(phoneNumber)
            branchInstance.setIdentity(phoneNumber)
        } else {
            print("Error: No Branch | Crashlytics identify set for user.")
        }
    }
}

func writeToFireBase(_ string: String) {
    CLSLogv("%@", getVaList([string]))
}

func getSavedUserDefaults() {
    print("IN getSavedUserDefaults | start")
    
    if isKeyPresentInUserDefaults(key: "userWasNotifiedOfTwoRestaurantCart") != false {
        userWasNotifiedOfTwoRestaurantCart = (defaults.bool(forKey: "userWasNotifiedOfTwoRestaurantCart"))
        print("userWasNotifiedOfTwoRestaurantCart: ", userWasNotifiedOfTwoRestaurantCart)
    }
    
    if isKeyPresentInUserDefaults(key: "lastOrderRated") != false {
        lastOrderIDPromptedForRating = (defaults.string(forKey: "lastOrderRated")!)
        print("lastOrderRated: ", lastOrderIDPromptedForRating)
    }
    
    if isKeyPresentInUserDefaults(key: "userSession") != false {
        userCurrentSessionToken = (defaults.string(forKey: "userSession")!)
        print("session token: ", userCurrentSessionToken)
    }
    
    if isKeyPresentInUserDefaults(key: "userDeviceID") != false {
        userCurrentDeviceID = (defaults.string(forKey: "userDeviceID")!)
        print("device ID: ", userCurrentDeviceID)
        
        if userCurrentDeviceID == "" {
            //            registerForPushNotifications() // try registering for notifications again
        }
    }
}

func saveToUserDefaults() {
    print("IN saveToUserDefaults | start")
    defaults.set(userCurrentSessionToken, forKey: "userSession")
    defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
    defaults.synchronize()
}

//check if userdefaults key exists
func isKeyPresentInUserDefaults(key: String) -> Bool {
    return defaults.object(forKey: key) != nil
}

// main notification UI
func createNextDayMenuNotification(v: UIView, vc: UIViewController) -> SpringButton { // TODO move this to menu VC to remove objects fully
    
    // TODO make sure animation leaves full frame of iphone X
    
    print("in createNextDayMenuNotification | start")
    let b = SpringButton(frame: v.frame)
    b.isUserInteractionEnabled = true
    b.layer.zPosition = 100
    b.backgroundColor = hexBlack.Color.withAlphaComponent(0.7)
    b.tag = 1111
    b.addTarget(vc, action: #selector(MenuVC.self.triggerRemoveNextDayNotification), for: .touchUpInside)
    v.addSubview(b)
    
    let w = SpringButton(frame: CGRect(x: 20, y: 0, width: v.frame.width - 40, height: v.frame.height / 2.5))
    w.center = v.center
    w.isUserInteractionEnabled = true
    w.layer.zPosition = 101
    w.backgroundColor = UIColor.white
    w.layer.shadowColor = hexBlack.Color.cgColor
    w.layer.shadowOffset = globalShadowOffset
    w.layer.shadowOpacity = 1.0
    w.tag = 2222
    v.addSubview(w)
    
    let orderCutoff = SpringLabel(frame: CGRect(x: 0, y: 50, width: w.frame.width, height: 25))
    
    if v.frame.width < 330 { //se todo
        orderCutoff.frame = CGRect(x: 0, y: 30, width: w.frame.width, height: 25)
    }
    
    orderCutoff.text = "Order Cut-off Time: 11:00am"
    orderCutoff.textColor = hexYellowDark.Color
    orderCutoff.textAlignment = .center
    orderCutoff.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.regular)
    orderCutoff.isUserInteractionEnabled = false
    orderCutoff.layer.zPosition = 102
    orderCutoff.tag = 3333
    orderCutoff.backgroundColor = .clear
    w.addSubview(orderCutoff)
    
    let orderNote = SpringTextView(frame: CGRect(x: 0, y: orderCutoff.frame.maxY + 10, width: w.frame.width, height: 150))
    
    if v.frame.width < 330 {
        //            orderNote.frame = CGRect(x: 0, y: Int(self.view.frame.height - globalLargeButtonHeight), width: columnWidth + 50, height: 25)
    }
    
    orderNote.textColor = hexDarkGray
    var day = (currentUserTime + 1.days).weekdayName(.default)
    if day == "Saturday" || day == "Sunday" {
        day = "Monday"
    }
    orderNote.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    orderNote.layer.zPosition = 102
    
    if userData?.FirstName != "" {
        
        let name = userData?.FirstName
        orderNote.text = "\(name ?? "User"), sorry we missed you today but dont worry.  Place your order for \(day) now. Check out the menu." // TODO format multiline
    } else {
        orderNote.text = "Sorry we missed you today but dont worry. Place your order for \(day) now. Check out the menu." // TODO format multiline
    }
    
    orderNote.isUserInteractionEnabled = false
    orderNote.isSelectable = false
    orderNote.backgroundColor = .clear
    orderNote.textAlignment = .center
    orderNote.tag = 4444
    orderNote.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
    w.addSubview(orderNote)
    
    let menuButton = SpringButton(frame: CGRect(x: 0, y: (w.frame.maxY - 90) - w.frame.minY, width: 100, height: globalLargeButtonHeight - 5))
    
    if v.frame.width < 330 { //se todo
        menuButton.frame = CGRect(x: 0, y: (w.frame.maxY - 70) - w.frame.minY, width: 100, height: globalLargeButtonHeight - 5)
    }
    menuButton.setTitle("VIEW MENU", for: .normal)
    menuButton.setTitleColor(UIColor.white, for: .normal)
    menuButton.center.x = w.center.x - (w.frame.minX)
    menuButton.contentVerticalAlignment = .center
    menuButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
    menuButton.isUserInteractionEnabled = true
    menuButton.layer.zPosition = 102
    menuButton.backgroundColor = hexRed.Color
    menuButton.setBackgroundColor(color: hexYellowDark.Color, forState: .highlighted)
    menuButton.tag = 5555
    menuButton.addTarget(vc, action: #selector(MenuVC.self.triggerRemoveNextDayNotification), for: .touchUpInside)
    
    w.addSubview(menuButton)
    
    delay(1) { 
        removeNextDayNotification(v: v)
    }
    
    let time: CGFloat = 0.8
    
    w.animation = "squeezeUp"
    w.curve = "spring"
    w.duration = time
    w.delay = 0.3
    w.animate()
    
    return menuButton
}

func removeNextDayNotification(v: UIView) { //not currently used
    print("in removeNextDayNotification | start")
    //animate button dropping off screen
    if let b =  v.viewWithTag(2222) as? SpringButton {
        
        let time: CGFloat = 1.25
        
        b.animation = "squeezeUp"
        b.curve = "spring"
        b.duration = time
        b.delay = 0.05
        b.animateTo()
        
        delay(Double(time + 0.1)) {
            if let c =  b.viewWithTag(3333) as? SpringLabel {
                c.removeFromSuperview()
            }
            if let c =  b.viewWithTag(4444) as? SpringTextView {
                c.removeFromSuperview()
            }
            if let c =  b.viewWithTag(5555) as? SpringButton {
                c.removeFromSuperview()
            }
            b.isHidden = true
        }
        
    }
    delay(0.45) {
        if let g =  v.viewWithTag(1111) as? SpringButton {
            g.isHidden = true
        }
    }
}

func showShareDialogue(vc: UIViewController, button: UIButton) {
//
//    if let myWebsite = NSURL (string: userShareCodeUrl) {
//        let objectsToShare = [textToShare, myWebsite] as [Any]
//        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//        //New Excluded Activities Code
//        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
//        activityVC.popoverPresentationController?.sourceView = button
//        vc.present(activityVC, animated: true, completion: nil)
//    }

//    if let user = userData {
//        BranchAssistant.app.share(user: user)
//    }
//
//    let message = "Save GHS 5 on your next order when trying WeChop!"
//    branchObject.showShareSheet(with: branchLinkProperty, andShareText: message, from: vc) { (activityType, completed) in
//        print(activityType ?? "")
//    }

    animateUIBanner("Coming Soon", subText: "This feature will be available in a future release.", color: UIWarningSuccessColor, shouldDismiss: true)
}

func callNotificationItem(_ name: String) {
    if name != "reloadOrders" {
        print("in callNotificationItem | ", name)
    }

    NotificationCenter.default.post(name: NSNotification.Name(rawValue: name), object: nil)
}

func globalGetUserMenu(_ vc: UIViewController, useSelectedDate: Bool, fromLogin: Bool) {
    print("in globalGetUserMenu | start")

    refreshAllDateInformation()
    
    if isCurrentTimeAfterTodaysCutoffTime == true {
        
        //update target menu date
        if currentlySelectedMenuDay.weekdayName(.default) == currentUserTime.weekdayName(.default) {
            if currentlySelectedMenuDay.weekdayName(.default) == "Friday"{
                currentlySelectedMenuDay = currentlySelectedMenuDay + 3.days // add 3 days
            } else if currentlySelectedMenuDay.weekdayName(.default) == "Saturday"{
                currentlySelectedMenuDay = currentlySelectedMenuDay + 2.days // add 2 days
            } else { //move to next day
                currentlySelectedMenuDay = currentlySelectedMenuDay + 1.days // add one days
            }
        }

        DispatchQueue.global().async {
            tryToGetMenuData(currentlySelectedMenuDay.toFormat("yyyy-MM-dd"), vc: vc, fromLogin: fromLogin, completion: tryToGetMenuDataHandler)
        }
    } else if useSelectedDate == true {
        //update target menu date
        if currentlySelectedMenuDay.weekdayName(.default) == currentUserTime.weekdayName(.default) {
            if currentlySelectedMenuDay.weekdayName(.default) == "Friday"{
                currentlySelectedMenuDay = currentlySelectedMenuDay + 3.days // add 3 days
            } else if currentlySelectedMenuDay.weekdayName(.default) == "Saturday"{
                currentlySelectedMenuDay = currentlySelectedMenuDay + 2.days // add 2 days
            } else if currentlySelectedMenuDay.weekdayName(.default) == "Sunday"{
                currentlySelectedMenuDay = currentlySelectedMenuDay + 1.days // add 2 days
            } else {
                //don't change date
            }
        }
        DispatchQueue.global().async {
            tryToGetMenuData(currentlySelectedMenuDay.toFormat("yyyy-MM-dd"), vc: vc, fromLogin: fromLogin, completion: tryToGetMenuDataHandler)
        }
        
    } else {

        DispatchQueue.global().async {
            tryToGetMenuData(currentUserTime.toFormat("yyyy-MM-dd"), vc: vc, fromLogin: fromLogin, completion: tryToGetMenuDataHandler)
        }
        
    }
    
}

//indicate phone doesnt' have internet
func noInternetWarning() {
    animateUIBanner("Warning", subText: "No internet connection. Please check your connection and try again.", color: UIWarningInformationColor, shouldDismiss: true)
}

func dismissAllNotificationBanners() {
    bannerNotificationQueue.removeAll()
}

func animateUIBanner(_ text: String, subText: String, color: UIColor, removePending: Bool = false, shouldDismiss: Bool = true) {
    print("in animateUIBanner | start: ", text)

    //old style
//    var banner: NotificationBanner?
////    let successView = UIImageView(image: UIImage(named: "success"))
////    let errorView = UIImageView(image: UIImage(named: "danger"))
//
//    banner = NotificationBanner(title: subText)
//
////    if color == UIWarningSuccessColor {
////        banner = NotificationBanner(title: "", subtitle: subText, style: .none)
////    } else {
////        banner = NotificationBanner(title: "", subtitle: subText, style: .none)
////    }
//    banner?.duration = 1.75
//    banner?.backgroundColor = color
//
//    if removePending == true {
//        bannerNotificationQueue.removeAll()
//    }
//
//    banner?.titleLabel?.font = UIFont.Theme.size15
//    banner?.titleLabel?.numberOfLines = 2
////    banner?.titleLabel?.backgroundColor = .green
//
//    if isIPhoneX == true {
//    } else if isIPhoneSE == true {
//        banner?.titleLabel?.font = defaultFont14
////        banner?.subtitleLabel?.font = defaultFont12
//    }
//
////    if subText.count > 50 {
////        print("long time", subText.count)
////        banner?.duration = 10
////    }
//    banner?.dismissOnSwipeUp = true
//    banner?.onTap = { banner?.dismiss() }
//    print("here about to show")
//    banner?.show(bannerPosition: .bottom, queue: bannerNotificationQueue)

    if removePending == true {
        SwiftMessages.hideAll()
    }
    
    let view = MessageView.viewFromNib(layout: .messageView)
    view.configureDropShadow()
    view.configureContent(title: "", body: subText)
    view.configureBackgroundView(sideMargin: 0)
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    view.button?.isHidden = true
    view.titleLabel?.isHidden = true
    view.configureTheme(backgroundColor: color, foregroundColor: .white)
    
    var config = SwiftMessages.defaultConfig
    config.duration = .seconds(seconds: 1.75)
    if shouldDismiss == false {
        config.duration = .forever
    }

    config.interactiveHide = true
    config.presentationStyle = .bottom
    SwiftMessages.show(config: config, view: view)
}

// animation for stardard button press
func animateButtonPress(senderTag: Int, localView: UIView) {
    
    //    if let button =  localView.viewWithTag(senderTag) as? SpringButton {
    //        button.animation = "pop"
    //        button.curve = "easeOutSine"
    //        button.duration = 0.40
    //        button.damping = 0.1
    //        button.velocity = 0.0
    //        button.animate()
    //    }
    
    //    playSound(sound: buttonPress, volume: 0.3)
}

func segue(name: String, v: UIViewController, type: HeroDefaultAnimationType) {
    v.navigationController?.hero.navigationAnimationType = type
    v.performSegue(withIdentifier: name, sender: v)
}

func animateIncorrectInput(buttonID: Int, localView: UIView) {
    
    if let button =  localView.viewWithTag(buttonID) as? SpringTextField {
        button.animation = "shake"
        button.curve = "easeInSine"
        button.duration = 0.5
        button.force = 0.1
        button.damping = 0.0
        button.animate()
    }
}

func animateIncorrectButton(buttonID: Int, localView: UIView) {
    
    if let button =  localView.viewWithTag(buttonID) as? SpringButton {
        button.animation = "shake"
        button.curve = "easeInSine"
        button.duration = 0.5
        button.force = 0.1
        button.damping = 0.0
        button.animate()
    }
}

func delay(_ delay: Double, closure: @escaping () -> Void ) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

// a print space function for debugging
func space () {
    print("_________________________________________")
    print("_________________________________________")
    print("_________________________________________")
}

// for debugging
func slog(_ format: String, _ args: CVarArg...) {
    guard ProcessInfo.processInfo.arguments.contains("--log-sync") else { return }
    print("[SYNC] " + format, args)
}
