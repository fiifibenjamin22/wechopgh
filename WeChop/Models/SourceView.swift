//
//  SourceView.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 10/19/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import Foundation

enum SourceView: CaseIterable { //used mainly in API completion to determin right completion action

    case login
    case loginResetPW
    case registerWC
    case registerFB
    case registerPassword
    case registerValidatePhone
    case menu
    case menuDetails
    case menuDaySelection
    case orders
    case orderDetails
    case orderRating
    case cart
    case profile
    case settings
    case settingsEdit
    case settingsEditPhone
    case settingsValidatePhone
    case address
    case addressUpdate
    case payment
    case updatePayment
    case invite
    case about
}

enum RootView: String { //used to set name of root view controller
    case login = "LoginVC"
    case menu = "MenuVC"
    case test = "PaymentVC"
}
