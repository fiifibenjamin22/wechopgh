//
//  VerifierClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/23/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import Foundation

class VerifierClass: NSObject {
    var Verifier: String
    var Token: String
    var Password: String
    var Sendnumber: String

    init(verifier: String, token: String, password: String, sendnumber: String) {
        self.Verifier = verifier
        self.Token = token
        self.Password = password
        self.Sendnumber = sendnumber
    }
}
