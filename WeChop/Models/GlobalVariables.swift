//
//  GlobalVariables.swift
//  WeChop
//
//  Created by Alex on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//
import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import MapKit
import NotificationBannerSwift
import MarqueeLabel
import FBSDKLoginKit
import Branch

//launch variables
let appTitle: String = "WeChop"
let appDescription: String = "Lunch without wahala"
let apiURl: String = "https://api.gamma.wechop.biz" //Change this for Test / Prod Access
let apiKey: String = "d3767e6a-5430-4a73-84bd-2b83f30fae34"
var appVersion: String = String()
//var hasInternet: Bool = true //checks if internet connection is available
let defaults: UserDefaults = UserDefaults.standard

var fullOrderList: [OrderData] = [OrderData]() //main variable for capturing all orders
var upcomingOrders: [OrderData] = [OrderData]() //main variable for listing upcoming orders
var completedOrders: [OrderData] = [OrderData]() //main variable for listing completed orders
var todaysMenu: [RestaurantData] = [RestaurantData]() //global variable for listing todays menu
var cartMenu: [RestaurantData] = [RestaurantData]() //global variable for listing menu called for updating cart

var selectedMenuItem: MenuData! //used to transfer selected menu item to item details page
var selectedOrderItem: OrderData! //used to transfer selected upcoming or history orders to the order details page
var userCartOrder: OrderData! //main variable for tracking current order before it is placed
var userCurrentOrderFeedback: Feedback! //main variable collecting feedback on an order
var currentPushNotification: NotificationData! //used to capture last push notification

//facebook SDK
let loginManager = FBSDKLoginManager()

//Branch
let branchInstance: Branch = Branch.getInstance()
let branchObject = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
let branchLinkProperty: BranchLinkProperties = BranchLinkProperties()

//User
var userData: User?
var userCurrentSessionToken: String = ""
var userCurrentAuthToken: String = ""
var userCurrentDeviceID: String = ""
var userCurrentID: String = ""
var userRegistrationPassword: String = ""
var userRegistrationPhone: String = ""
var userCurrentLocaleCode: String = ""
var userShareCodeUrl: String = ""
var userNewPaymentType: Payment = emptyPaymentType
var userWasNotifiedOfTwoRestaurantCart: Bool = false

var phoneNumberToValidate: String = "" //used to try and set / validate a number users phone number
var phoneNumberValidationCode: String = String() //used to try and set / validate a number users phone number
var phoneNumberVerifierResponse: String = String() //returned when registering or verifying phone code

//Hardware variables
var isIPhoneX: Bool = false //used to change UI elements for long screen
var isIPhoneSE: Bool = false //used to change UI elements for small screens
var isIpad: Bool = false
var isShortIpad: Bool = false
var isSimulator: Bool = false

//Register Variables
var loginVCIsUserInLoginAreaAndNotInRegisterArea: Bool = true //used to indicate what login buttons should do based on which type of login or register is selected
var emptyNewUser: User = User(firstName: "", lastName: "", company: "", address: "", phone: "", location: CLLocationCoordinate2D(latitude: 0, longitude: 0), paymentMethod: Payment(paymentType: 2, mobileMoney: [MobileMoney(network: 1, phone: "")], creditCard: [CreditCard(number: 0, expiration: "", cvv: 0)], instructions: [""]), inviteUrl: "", localeCode: "") // used to start a new user when registering
let emptyOrder: OrderData = OrderData(restaurantName: "", rating: 0, orderID: "", date: "", address: "", amount: 0.0, vatCost: "", deliveryCost: "", status: "", orderedItems: [OrderDataLines(name: "", id: "", quantity: "0", price: "0")], additionals: [OrderDataAdditionals(label: "", value: "")]) //used when importing orders from the server
let emptyMenuItem: MenuData = MenuData(id: "", name: "", description: "", price: 0.0, image: UIImage(named: "no_dishImage")!, imageURL: URL(string: "http://google.com")!, restaurantName: "", restaurantID: "", orderCount: 0, soldout: false, taxApplicable: false, heroID: "")
let emptyUserFeedback: Feedback = Feedback(score: "0", comment: "", issues: [FeedbackIssues(issueNumber: "0")])
let emptyPaymentType: Payment = Payment(paymentType: 0, mobileMoney: [MobileMoney(network: 0, phone: "")], creditCard: [CreditCard(number: 0, expiration: "", cvv: 000)], instructions: [""])
var cancelledOrderID: String = ""
var cancelledOrderIndexSet: IndexSet = IndexSet()

//Menu
var loadingMenuTimer: Timer = Timer() //used to text changes on loading text for menu

//Date Variables
var currentlySelectedMenuDay: DateInRegion = DateInRegion() //this field drives most of the logic for which menu day to show and iterate off of.  This should only be changed by the default check, by the switch to next day function and menu day selector
var currentLocalUserTime: DateInRegion = DateInRegion()
var currentUserTime: DateInRegion = DateInRegion()
var currentGhanaTime: DateInRegion = DateInRegion()
var currentGhanaCutoffTime: DateInRegion = DateInRegion()
var currentUserRegion: Region = Region()
var currentGhanaRegion: Region = Region()
var isCurrentTimeAfterTodaysCutoffTime: Bool = false
let globalRefreshGIFName: String = "foodGifTransparent2"
var lastOrderIDPromptedForRating: String = "" //used to store last completed order ID that set off a prompt to be rated.
var userRatingSelectedOnOrderPage: Int = 0
var globalMenuLoadingDelay: Double = 1.5

//navigation Workflow
var currentRootView: RootView = .login //used to set first VC to go to after launch screen
var sourceView: SourceView = .login //used mainly in API completion to determin right completion action
var comingFrom = ComingFrom.shared

//localization
let ghanaCities: [String] = ["Accra", "Kumasi", "Sekondi-Takoradi", "Ashiaman", "Sunyani", "Tamale", "Cape Coast", "Obuasi", "Teshie", "Tema", "Madina", "Koforidua", "Wa"]

//Device
var currentUserIP: String = ""
var currentDeviceModel: String = String()
var currentDeviceVersion: String = String()

//UI Elements
let globalShadowOffset: CGSize = CGSize(width: 0, height: 1)
let globalNavIconSize: CGFloat = 20
var globalXAxisIndent: CGFloat = 15
var globalYAxisHeight: CGFloat = 50
let globalNavTitleIndentSize: CGFloat = 70
var bannerNotificationQueue: NotificationBannerQueue = NotificationBannerQueue()
var globalStatusBarHeight: CGFloat = 0
let globalLargeButtonHeight: CGFloat = 50
let globalIPhoneXTopYPadding: CGFloat = 25
var globalLoadingSpinner: NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 68.75, height: 68.75), type: .circleStrokeSpin, color: hexRed.Color, padding: 17) //main red spinning loading element | use createGlobalLoadingSpinner() to call element
var logoImage: UIImageView = UIImageView(image: UIImage(named: "mainLogo")) //global variable to set sizing reference for other UI elements like globalLoadingSpinner
var isWarningAnimating: Bool = false //used when showing error warnings to stop new warnings until current warning is done.
var userAddressMapImage: UIImage? = UIImage() //global value to store users address map image
var refreshControl: UIRefreshControl!
let UIWarningErrorColor = Main(color: hexStringToUIColor(hex: "350c11")).Color
let UIWarningSuccessColor = Main(color: hexStringToUIColor(hex: "444444")).Color
let UIWarningInformationColor = Main(color: hexStringToUIColor(hex: "39322e")).Color

let menuAnimationDuration: Double = 0.5

//URLs
let termsURL: String = "https://wechopgh.com/terms_and_conditions.html"
let privacyURL: String = "https://wechopgh.com/privacy.html"
let facebookURL: String = "https://www.facebook.com/wechopgh/"
let twitterURL: String = "https://www.twitter.com/wechopgh/"
let linkedInURL: String = "https://www.linkedin.com/company/wechopgh/"
let instagramURL: String = "https://www.instagram.com/wechopgh/"

//Fonts
var pinkFont: String = "All_Things_Pink_Skinny"
var pinkFontBold: String = "AllThingsPink"
var globalFont: String = "NeutraText-LightAlt"
var globalFontBold: String = "HiraginoSans-W6"
var globalFontSize: CGFloat = 50
let defaultFont8 = UIFont.systemFont(ofSize: 8, weight: UIFont.Weight.regular)

let defaultFont10 = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
let defaultFont12 = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
let defaultFont14 = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
let defaultFont16 = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
let defaultFont18 = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular)
