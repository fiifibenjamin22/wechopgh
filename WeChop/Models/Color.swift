//
//  Color.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import AVFoundation
import NotificationBannerSwift
import MarqueeLabel

// HEX color Class
class Main {
    var Color: UIColor
    init(color: UIColor) {
        self.Color = color
    }
}

func hexStringToUIColor(hex: String) -> UIColor {
    var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if cString.hasPrefix("#") {
        cString.remove(at: cString.startIndex)
    }
    if cString.count != 6 {
        return UIColor.gray
    }
    var rgbValue: UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

// MARK: New Color Theme
let newHexBlack1: Main = Main(color: hexStringToUIColor(hex: "1E1C1E"))
let newHexBlack2: Main = Main(color: hexStringToUIColor(hex: "2d2a2c"))
let newHexGray: Main = Main(color: hexStringToUIColor(hex: "4B464A"))
let newHexGray2: Main = Main(color: hexStringToUIColor(hex: "5A5358"))
let hexDarkTan: Main = Main(color: hexStringToUIColor(hex: "A598A1"))
let hexRed: Main = Main(color: hexStringToUIColor(hex: "EB3349"))

// MARK: Old Color Theme
let hexRedDark: Main = Main(color: hexStringToUIColor(hex: "C12A3C"))
let hexYellow: Main = Main(color: hexStringToUIColor(hex: "feb860"))
let hexYellowDark: Main = Main(color: hexStringToUIColor(hex: "F29E2E"))
let hexYellowLight: Main = Main(color: hexStringToUIColor(hex: "FFF200"))
let hexPeach: Main = Main(color: hexStringToUIColor(hex: "CEB5A7"))
let hexGreen: Main = Main(color: hexStringToUIColor(hex: "01A650"))
let hexTan: Main = Main(color: hexStringToUIColor(hex: "D5C2B7"))
let hexDarkGray: UIColor = UIColor.black.withAlphaComponent(0.8)
let hexLightGray: UIColor = hexGray.withAlphaComponent(0.2)
let hexGray: UIColor = UIColor.gray
let hexGrayCart: Main = Main(color: hexStringToUIColor(hex: "FBFBFB"))
let hexButtonHighlight: Main = Main(color: hexStringToUIColor(hex: "EBEBEB"))
//let hexGrayStatus: Main = Main(color: hexStringToUIColor(hex: "7D7D7D"))

let hexGrayPayment: Main = Main(color: hexStringToUIColor(hex: "959595"))

let hexRefGray: Main = Main(color: hexStringToUIColor(hex: "e6e2e5"))
let hexRefRed: Main = Main(color: hexStringToUIColor(hex: "711c28")) //profile referral button
let hexBlack: Main = Main(color: hexStringToUIColor(hex: "282828"))
let hexBlue: Main = Main(color: hexStringToUIColor(hex: "0F74BA"))

//used for center screen notifications
let hexGreenHex = UInt(0x3fad62)
let hexRedHex = UInt(0xd2545b)
