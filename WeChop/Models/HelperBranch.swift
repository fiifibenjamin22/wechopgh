//
//  HelperBranch.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 12/9/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Branch

// MARK: Main App Helper methods
class BranchAssistant {

    static var app: BranchAssistant = {
        return BranchAssistant()
    }()

    func viewItem(item: MenuData) {
        print("in Branch ViewItem: Item: ", item.Name)
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = item.ID
        branchUniversalObject.title               = item.Name
        branchUniversalObject.contentMetadata = BranchContentMetadata(dictionary: [".setSku": item.ID, "        .setProductName": item.Name, ".setPrice": item.Price])

        let event = BranchEvent.standardEvent(.viewItem)
        event.eventDescription = "User viewed an item in their cart."
        event.contentItems     = [ branchUniversalObject ]
        event.currency = BNCCurrency(rawValue: "GHS")
        event.customData       = [
            "Restaurant": item.RestaurantName,
            "MenuDate": item.Date
        ]
        event.logEvent()
    }

    func addItem(item: MenuData) {
        print("in Branch addItem: Item: ", item.Name)
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = item.ID
        branchUniversalObject.title               = item.Name
        branchUniversalObject.contentMetadata = BranchContentMetadata(dictionary: [".setSku": item.ID, "        .setProductName": item.Name, ".setPrice": item.Price])

        let event = BranchEvent.standardEvent(.addToCart)
        event.eventDescription = "User added an item to their cart."
        event.contentItems     = [ branchUniversalObject ]
        event.currency = BNCCurrency(rawValue: "GHS")
        event.customData       = [
            "Restaurant": item.RestaurantName,
            "MenuDate": item.Date
        ]
        event.logEvent()
    }

    func viewCart(cart: OrderData) {
        print("in Branch viewCart")
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = "UserCart"
        branchUniversalObject.title               = cart.Date

        let event = BranchEvent.standardEvent(.viewCart)
        event.eventDescription = "User viewed their cart."
        event.contentItems     = [ branchUniversalObject ]
        event.transactionID = "Pending"
        event.currency = BNCCurrency(rawValue: "GHS")
        event.revenue = cart.Amount as? NSDecimalNumber
        let orderCount = String(cart.OrderedItems.count)

        event.customData       = [
            "Restaurant": cart.RestaurantName,
            "MenuDate": cart.Date,
            "UniqueMenuItems": orderCount
        ]
        event.logEvent()
    }

    func purchaseItems(cart: OrderData) {
        print("in Branch purchaseItems")
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = "UserCart"
        branchUniversalObject.title               = cart.Date

        let event = BranchEvent.standardEvent(.initiatePurchase)
        event.contentItems     = [ branchUniversalObject ]
        event.eventDescription = "User Initiated an item purchase."
        event.transactionID = "Pending"
        event.currency = BNCCurrency(rawValue: "GHS")
        event.revenue = cart.Amount as? NSDecimalNumber
        let orderCount = String(cart.OrderedItems.count)

        event.customData       = [
            "Restaurant": cart.RestaurantName,
            "MenuDate": cart.Date,
            "UniqueMenuItems": orderCount
        ]
        event.logEvent()
    }

    func share(user: User) {
        print("in Branch share")
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = user.Phone
        branchUniversalObject.title               = user.FirstName
        //        branchUniversalObject.contentMetadata = BranchContentMetadata(dictionary: [".setSku": item.ID, "        .setProductName": item.Name, ".setPrice": item.Price])

        let event = BranchEvent.standardEvent(.share)
        event.eventDescription = "User clicked the share button."

        event.contentItems     = [ branchUniversalObject ]
        //        event.customData       = [
        //            "Restaurant": item.RestaurantName,
        //            "MenuDate": item.Date
        //        ]
        event.logEvent()
    }

    func signedUp(user: User) {
        print("in signedUp share")
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = user.Phone
        branchUniversalObject.title               = user.FirstName
        //        branchUniversalObject.contentMetadata = BranchContentMetadata(dictionary: [".setSku": item.ID, "        .setProductName": item.Name, ".setPrice": item.Price])

        let event = BranchEvent.standardEvent(.completeRegistration)
        event.contentItems     = [ branchUniversalObject ]
        event.eventDescription = "User completed Registration."

        //        event.customData       = [
        //            "Restaurant": item.RestaurantName,
        //            "MenuDate": item.Date
        //        ]
        event.logEvent()
    }

    func completedTutorial() {
        print("in completedTutorial share")
        let branchUniversalObject = BranchUniversalObject.init()
        branchUniversalObject.canonicalIdentifier = "New User"
        branchUniversalObject.title               = "New User"
        //        branchUniversalObject.contentMetadata = BranchContentMetadata(dictionary: [".setSku": item.ID, "        .setProductName": item.Name, ".setPrice": item.Price])

        let event = BranchEvent.standardEvent(.completeTutorial)
        event.contentItems     = [ branchUniversalObject ]
        event.eventDescription = "User completed Tutorial."

        //        event.customData       = [
        //            "Restaurant": item.RestaurantName,
        //            "MenuDate": item.Date
        //        ]
        event.logEvent()
    }

}
