//
//  RestaurantClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/7/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class RestaurantData: NSObject {
    var Name: String
    var ID: String = "0"
    var Tag: String
    var Genre: String
    var Rating: Double = 0.0
    var Currency: String
    var Picture: UIImage
    var MenuItems: [MenuData]
    
    init(name: String, id: String = "0", tag: String, genre: String, rating: Double = 0.0, currency: String, picture: UIImage, menuItems: [MenuData]) {
        self.Name = name
        self.ID = id
        self.Tag = tag
        self.Genre = genre
        self.Rating = rating
        self.Currency = currency
        self.Picture = picture
        self.MenuItems = menuItems
    }
}
