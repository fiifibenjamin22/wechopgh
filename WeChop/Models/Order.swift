//
//  OrderClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/13/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

//
//  RestaurantClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/7/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class OrderData: NSObject {
    var RestaurantName: String
    var RestaurantPictureURL: URL = URL(string: "http://google.com")!
    var Rating: Int
    var OrderID: String
    var Date: String
    var Address: String
    var Amount: Double
    var Status: String
    var VatCost: String = ""
    var DeliveryCost: String  = ""
    var Discount: String = ""
    var OrderedItems: [OrderDataLines]
    var Additionals: [OrderDataAdditionals]
    var IsComplete: Bool = false
    var IsCancellable: Bool = false
    var IsRated: Bool = false

    init(restaurantName: String, restaurantPictureURL: URL = URL(string: "http://google.com")!, rating: Int, orderID: String, date: String, address: String, amount: Double, vatCost: String, deliveryCost: String, discount: String = "", status: String, orderedItems: [OrderDataLines], additionals: [OrderDataAdditionals], isComplete: Bool = false, isCancellable: Bool = false, isRated: Bool = false) {
        self.RestaurantName = restaurantName
        self.RestaurantPictureURL = restaurantPictureURL
        self.Rating = rating
        self.OrderID = orderID
        self.Date = date
        self.Address = address
        self.Amount = amount
        self.VatCost = vatCost
        self.DeliveryCost = deliveryCost
        self.Discount = discount
        self.Status = status
        self.OrderedItems = orderedItems
        self.Additionals = additionals
        self.IsComplete = isComplete
        self.IsCancellable = isCancellable
        self.IsRated = isRated
    }
}

//additional Order Class information returned by APIs

class CartItem: NSObject { //used when sending cart item to Server in put /cart
    var ID: Int
    var Quantity: Int
    
    init(id: Int, quantity: Int) {
        self.ID = id
        self.Quantity = quantity
    }
}

class OrderDataAdditionals: NSObject {
    var Label: String
    var Value: String
   
    init(label: String, value: String) {
        self.Label = label
        self.Value = value
    }
}

class OrderDataLines: NSObject {
    var Name: String
    var ID: String
    var Quantity: String
    var Price: String
    var Restaurant: String = ""

    init(name: String, id: String, quantity: String, price: String, restaurant: String = "") {
        self.Name = name
        self.ID = id
        self.Quantity = quantity
        self.Price = price
        self.Restaurant = restaurant
    }
}
