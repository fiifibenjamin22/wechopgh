//
//  Onboard.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 12/30/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Onboard

// New User Information Slides and Signup
func showNewUserTutorial(originalView: UIViewController) {
    print("in showNewUserTutorial | start")

    var onboardingVC = OnboardingViewController()
    let firstPage = OnboardingContentViewController(title: "", body: "", image: UIImage(named: "Page_1.png"), buttonText: "skip") { () -> Void in
        BranchAssistant.app.completedTutorial()
        onboardingVC.dismiss(animated: true, completion: {})
    }

    firstPage.topPadding = 0
    firstPage.iconHeight = firstPage.view.frame.height
    firstPage.iconWidth = firstPage.view.frame.width
    firstPage.actionButton.titleLabel?.font =
        UIFont(name: globalFont, size: 16)

    let secondPage = OnboardingContentViewController(title: "", body: "", image: UIImage(named: "Page_2.png"), buttonText: "skip") { () -> Void in
        BranchAssistant.app.completedTutorial()
        onboardingVC.dismiss(animated: true, completion: {})
    }

    secondPage.actionButton.setTitleColor(.black, for: .normal)
    secondPage.topPadding = 0
    secondPage.iconHeight = secondPage.view.frame.height
    secondPage.iconWidth = secondPage.view.frame.width
    secondPage.actionButton.titleLabel?.font =
        UIFont(name: globalFont, size: 16)

    let thirdPage = OnboardingContentViewController(title: "", body: "", image: UIImage(named: "Page_3.png"), buttonText: "skip") { () -> Void in
        BranchAssistant.app.completedTutorial()
        onboardingVC.dismiss(animated: true, completion: {})
    }

    thirdPage.actionButton.setTitleColor(hexBlack.Color, for: .normal)
    thirdPage.topPadding = 0
    thirdPage.iconHeight = thirdPage.view.frame.height
    thirdPage.iconWidth = thirdPage.view.frame.width
    thirdPage.actionButton.titleLabel?.font =
        UIFont(name: globalFont, size: 16)

    let fourthPage = OnboardingContentViewController(title: "", body: "", image: UIImage(named: "Page_4.png"), buttonText: "Got it!") { () -> Void in
        BranchAssistant.app.completedTutorial()
        onboardingVC.dismiss(animated: true, completion: {})
    }

    fourthPage.topPadding = 0
    fourthPage.iconHeight = fourthPage.view.frame.height
    fourthPage.iconWidth = fourthPage.view.frame.width
    fourthPage.actionButton.titleLabel?.font =
        UIFont(name: globalFontBold, size: 16)
    fourthPage.actionButton.setTitleColor(.white, for: .normal)
    fourthPage.actionButton.backgroundColor = hexGreen.Color
    onboardingVC = OnboardingViewController(backgroundImage: UIImage(named: ""), contents: [firstPage, secondPage, thirdPage, fourthPage])
    onboardingVC.shouldMaskBackground = false
    onboardingVC.shouldBlurBackground = false
    onboardingVC.skipButton.setTitleColor(.black, for: .normal)
    onboardingVC.shouldFadeTransitions = true
    onboardingVC.fadePageControlOnLastPage = true
    onboardingVC.fadeSkipButtonOnLastPage = true
    onboardingVC.swipingEnabled = true
    onboardingVC.allowSkipping = false
    onboardingVC.skipHandler = {
        onboardingVC.dismiss(animated: true, completion: {
            BranchAssistant.app.completedTutorial()
        })
    }

    originalView.present(onboardingVC, animated: true, completion: {
        BranchAssistant.app.completedTutorial()
        })
}
