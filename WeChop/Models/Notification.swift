//  NotificationData.Swift
//  NotificationClass.swift
//
//Created by Benjamin Acquah on 9/20/19 11/5/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

struct NotificationData: Codable {
    var orderNumber: String = ""
    var vendorName: String = ""
    var payload: NotificationPayload

    enum CodingKeys: String, CodingKey {
        case orderNumber = "order_number"
        case vendorName = "vendor_name"
        case payload = "aps"
    }
}

extension NotificationData {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let orderNumber = try container.decodeIfPresent(String.self, forKey: .orderNumber)
        let vendorName = try container.decodeIfPresent(String.self, forKey: .vendorName)
        let payload = try container.decodeIfPresent(NotificationPayload.self, forKey: .payload)

        self.init(orderNumber: orderNumber ?? "DEFAULT",
            vendorName: vendorName ?? "DEFAULT",
            payload: payload ?? NotificationPayload(id: "default", urgency: "default", actionType: "default", isVisible: false)
        )
    }

    func encode(to encoder: Encoder) throws {

    }
}

struct NotificationPayload: Codable {
    var id: String = ""
    var urgency: String = "0"
    var actionType: String = "0"
    var isVisible: Bool = false

    enum CodingKeys: String, CodingKey {
        case id = "unique_identifier"
        case urgency = "urgency"
        case actionType = "action_type"
        case isVisible = "is_visible"
    }
}

extension NotificationPayload {

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decodeIfPresent(String.self, forKey: .id)
        let urgency = try container.decodeIfPresent(String.self, forKey: .urgency)
        let actionType = try container.decodeIfPresent(String.self, forKey: .actionType)
        let isVisible = try container.decodeIfPresent(Bool.self, forKey: .isVisible)

        self.init(
                  id: id ?? "",
                  urgency: urgency ?? "5",
                  actionType: actionType ?? "0",
                  isVisible: isVisible ?? false
        )
    }
    
    func encode(to encoder: Encoder) throws {
    }
}
