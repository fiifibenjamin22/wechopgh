//
//  SessionClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/25/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class SessionClass: NSObject {
    var SessionToken: String
    var DeviceId: String = "Device"
    
    init(sessionToken: String, deviceId: String = "Device") {
        self.SessionToken = sessionToken
        self.DeviceId = deviceId
    }
}

class PhoneVerificationClass: NSObject {
    var Verifier: String
    var Token: String
    var Password: String
    var Sendernumber: String

    init(verifier: String, token: String, password: String, sendernumber: String) {
        self.Verifier = verifier
        self.Token = token
        self.Password = password
        self.Sendernumber = sendernumber
    }
}
