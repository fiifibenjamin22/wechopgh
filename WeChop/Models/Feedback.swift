//
//  FeedbackClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 10/16/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class Feedback: NSObject {
    var Score: String = "0"
    var Comment: String = ""
    var Issues: [FeedbackIssues]
    
    init(score: String, comment: String, issues: [FeedbackIssues]) {
        self.Score = score
        self.Comment = comment
        self.Issues = issues
    }
}

class FeedbackIssues: NSObject {
    var IssueNumber: String
    init(issueNumber: String) {
        self.IssueNumber = issueNumber
    }
}
