//
//  CheckboxClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/22/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    // Images
    
    let checked = UIImage(named: "checkbox")?.withRenderingMode(.alwaysTemplate)
    let unchecked = UIImage(named: "fedback_box")?.withRenderingMode(.alwaysTemplate)
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(checked, for: UIControlState.normal)
            } else {
                self.setImage(unchecked, for: UIControlState.normal)
            }
            self.tintColor = hexDarkGray.withAlphaComponent(0.6)
            self.isUserInteractionEnabled = true
            self.layer.zPosition = 10000
        }
    }
}
