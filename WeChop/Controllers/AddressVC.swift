//
//  AddressVC   .swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import EzPopup

class AddressVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft = SpringButton() 
    let borderColor = hexLightGray
    var navigationTitleBorder: SpringButton = SpringButton()
    var addressIcon: SpringButton = SpringButton()
    var addressText: SpringTextView = SpringTextView()
    var setNewAddress: SpringButton = SpringButton()
    var addIcon: SpringButton = SpringButton()

    //unsupported address dialogue
    let whiteBox: SpringButton = SpringButton()
    let text: UITextView = UITextView()
    var cancelButton: SpringButton = SpringButton()
    var inviteButton: SpringButton = SpringButton()
    var tap: UITapGestureRecognizer = UITapGestureRecognizer()
    let backgroundDark: UIView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN AddressVC | ViewDidLoad")
        //writeToFireBase("in AddressVC viewdidload")

        NotificationCenter.default.addObserver(self, selector: #selector(refreshAddressVCPage), name: NSNotification.Name(rawValue: "refreshAddressVCPage"), object: nil) 

        NotificationCenter.default.addObserver(self, selector: #selector(showUnsupportedAddressNotification), name: NSNotification.Name(rawValue: "showUnsupportedAddressNotification"), object: nil) 

        tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        tap.numberOfTapsRequired = 1
        tap.isEnabled = false
        view.addGestureRecognizer(tap)

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .up)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white
        
        createMainContent()
        createUnsupportedAddressContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        print("in addressVC viewWillAppear | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

        super.viewWillAppear(true)
        loadAddress()
    }

    @objc func showUnsupportedAddressNotification() {
        print("in showUnsupportedAddressNotification | start")
        toggleUnsupportedAddressContent(show: true)
    }

    @objc func loadAddress() {
        print("IN loadAddress | start")
        
        addressText.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY + 5, width: view.frame.width, height: globalLargeButtonHeight + 10)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        
        let attributes = [NSAttributedStringKey.paragraphStyle: style]

        if let userAddress = userData?.Address {
            addressText.attributedText = NSAttributedString(string: userAddress, attributes: attributes)
        } else {
            addressText.attributedText = NSAttributedString(string: "No Address Defined", attributes: attributes)
        }
        addressText.textColor = hexDarkGray
        addressText.isUserInteractionEnabled = false
        addressText.isSelectable = false
        addressText.backgroundColor = .clear
        addressText.textAlignment = .left
        addressText.font = UIFont.Theme.size15Light
        
//        if isIPhoneSE == true {
//            addressText.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
//        }

        addressText.contentInset = UIEdgeInsets(top: 0, left: globalXAxisIndent * 3, bottom: 0, right: globalXAxisIndent * 3)
        addressText.centerVertically()
    }

    @objc func refreshAddressVCPage() {
        createMainContent()
        createUnsupportedAddressContent()
    }

    func createMainContent() {

        view.subviews.forEach({ $0.removeFromSuperview() })

        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Delivery Address", v: self.view, leftButton: navigationLeft)

        loadAddress()
        view.addSubview(addressText)
        
        let imageSize = Int(globalNavIconSize + 5)

        addressIcon.frame = CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize)
        addressIcon.center.y = addressText.center.y
        addressIcon.setImage(UIImage(named: "location_bubble_grey"), for: .normal)
        addressIcon.isUserInteractionEnabled = false
        addressIcon.contentHorizontalAlignment = .left
        addressIcon.contentVerticalAlignment = .center
        addressIcon.backgroundColor = .clear
        self.view.addSubview(addressIcon)

        setNewAddress.frame = CGRect(x: 0, y: addressText.frame.maxY + 5, width: self.view.frame.width, height: globalLargeButtonHeight)
        setNewAddress.tag = 13
        setNewAddress.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        setNewAddress.backgroundColor = .clear
        setNewAddress.isUserInteractionEnabled = true
        setNewAddress.titleLabel?.font = addressText.font
        setNewAddress.setTitleColor(hexDarkGray, for: .normal)
        setNewAddress.setBackgroundColor(color: .clear, forState: .normal)
        setNewAddress.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        setNewAddress.setTitle("Update your delivery address", for: .normal)
        setNewAddress.contentVerticalAlignment = .center
        setNewAddress.contentEdgeInsets = UIEdgeInsets(top: 0, left: (globalXAxisIndent * 3) + 5, bottom: 0, right: globalXAxisIndent * 3)
        setNewAddress.contentHorizontalAlignment = .left
        self.view.addSubview(setNewAddress)

        addIcon.frame = CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize)

        addIcon.center.y = setNewAddress.center.y + 2
        
        let imagePadding: CGFloat = 100
        addIcon.setImage(UIImage(named: "address_plus")?.addImagePadding(x: imagePadding, y: imagePadding), for: .normal)
        addIcon.isUserInteractionEnabled = false
        addIcon.contentHorizontalAlignment = .left
        addIcon.contentVerticalAlignment = .center
        addIcon.backgroundColor = .clear
        self.view.addSubview(addIcon)
    }

    func createUnsupportedAddressContent() {

        backgroundDark.frame = view.frame
        backgroundDark.layer.zPosition = 100
        backgroundDark.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        backgroundDark.isHidden = true
        view.addSubview(backgroundDark)

        var boxHeight = self.view.frame.height / 2.55

        if isIPhoneX == true {
            boxHeight = self.view.frame.height / 3
        } else if isIPhoneSE == true {
            boxHeight = self.view.frame.height / 2.25
        }

        whiteBox.frame = CGRect(x: 0, y: self.view.frame.height - boxHeight, width: self.view.frame.width, height: boxHeight)
        whiteBox.isUserInteractionEnabled = true
        whiteBox.backgroundColor = .white
        whiteBox.center.x = view.center.x
        whiteBox.layer.zPosition = 101
        whiteBox.isHidden = true
        view.addSubview(whiteBox)

        text.frame = CGRect(x: 0, y: globalXAxisIndent, width: self.view.frame.width - (2 * globalXAxisIndent), height: 150)
        text.textColor = newHexBlack2.Color
        let spacing = NSMutableParagraphStyle()
        spacing.lineSpacing = 6
        spacing.alignment = .center
        text.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue: spacing, NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 17)]

        if isIPhoneSE == true {
            text.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue: spacing, NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 13)]
        }

        text.text = "Sorry, we currently don't deliver to this area. But the more people that signup from the area, the faster we can support it. Plus for every new signup and order, you can get GHS 5 towards your lunches!"

        text.backgroundColor  = .clear
        text.isScrollEnabled = false
        text.isEditable = false
        text.layer.zPosition = 102
        text.isUserInteractionEnabled = false
        text.isSelectable = false
        text.center.x = view.center.x
        text.textAlignment = .center
        text.centerVertically()
        whiteBox.addSubview(text)

        let buttonWidth = self.view.frame.width / 2 - ((globalXAxisIndent) + 5)
        let buttonHeight = globalLargeButtonHeight

        cancelButton = createFooterButton(text: "Cancel", v: self.view)
        cancelButton.frame = CGRect(x: 0, y: whiteBox.frame.height - ((globalLargeButtonHeight) + (globalXAxisIndent * 1.5)), width: buttonWidth, height: buttonHeight)
        cancelButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cancelButton.tag = 20
        cancelButton.center.x = view.center.x / 2 + 2.5
        cancelButton.setTitleColor(newHexBlack2.Color, for: .normal)
        cancelButton.setBackgroundColor(color: .white, forState: .normal)
        cancelButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        cancelButton.layer.borderColor = hexDarkGray.cgColor
        cancelButton.layer.borderWidth = 0.5
        cancelButton.titleLabel?.font = UIFont.Theme.size17Medium

        whiteBox.addSubview(cancelButton)

        inviteButton = createFooterButton(text: "Invite", v: self.view)
        inviteButton.frame = CGRect(x: 0, y: whiteBox.frame.height - ((globalLargeButtonHeight) + (globalXAxisIndent * 1.5)), width: buttonWidth, height: buttonHeight)
        inviteButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        inviteButton.tag = 21
        inviteButton.center.x = view.center.x / 2 + view.center.x - 2.5
        inviteButton.setTitleColor(.white, for: .normal)
        inviteButton.setBackgroundColor(color: newHexBlack2.Color, forState: .normal)

        inviteButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        inviteButton.layer.borderColor = hexDarkGray.cgColor
        inviteButton.layer.borderWidth = 0.5
        inviteButton.titleLabel?.font = UIFont.Theme.size17Medium
        whiteBox.addSubview(inviteButton)

        cancelButton.layer.zPosition = 102
        inviteButton.layer.zPosition = 102

        if isIPhoneSE == true {

        } else if isIPhoneX == true {

        }

    }

    func toggleUnsupportedAddressContent(show: Bool) {

        if show == true {
            tap.isEnabled = true
            whiteBox.isHidden = false
            backgroundDark.isHidden = false
        } else {
            tap.isEnabled = false
            whiteBox.isHidden = true
            backgroundDark.isHidden = true
        }
    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            print("Button pressed: comingFrom.menuToUpdateAddress", comingFrom.menuToUpdateAddress)
            if comingFrom.menuToUpdateAddress == true {
                if let address = userData?.AddressSupported {
                    print("Button pressed: address", address)
                    if address == true {
                        //go to menu and refreshpage

                        getMenu()

                        delay(1) {
                            currentRootView = .menu
                            //comingFromMenuToUpdateAddress = false
                            comingFrom.menuToUpdateAddress = false
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
                            UIApplication.shared.keyWindow?.rootViewController = viewController

                            self.navigationController?.hero.navigationAnimationType = .fade
                            self.navigationController?.popToRootViewController(animated: true) //go back to menu page
                        }

                    } else {
                        //in ezpopup dismiss
                        self.dismiss(animated: true) {}
                    }
                }
            } else {
                self.navigationController?.hero.navigationAnimationType = .slide(direction: .down)
                self.hero.dismissViewController()
            }
        } else if sender.tag == 13 {
            //goto set new address

            let contentVC = UpdateAddressVC()
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
            present(popupVC, animated: true)

        } else if sender.tag == 20 {
            toggleUnsupportedAddressContent(show: false)
        } else if sender.tag == 21 {

            //shouldShowShareDialogueOnLoad = true
            comingFrom.inviteShowInviteDialogue = true

            let contentVC = InviteVC()
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
            present(popupVC, animated: true)

            //showShareDialogue(vc: self, button: sender)
        }

    }

    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("background pressed")
        toggleUnsupportedAddressContent(show: false)
    }

}
