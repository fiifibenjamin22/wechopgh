//
//  MenuDetails.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import Foundation

import UIKit
import Spring
import NVActivityIndicatorView
import Hero
import SwiftDate
import NotificationBannerSwift
import MarqueeLabel

class MenuDetailsVC: UIViewController, UIScrollViewDelegate {

    var navigationLeft: SpringButton = SpringButton() 
    var addSubtractButtonSize: CGFloat = 50

    //Menu items
    var itemImage: UIImageView = UIImageView(image: UIImage(named: "no_dishimage.png")) 
    var itemTitle: UITextView = UITextView()
    var itemTitleSeperator: UITextView = UITextView()
    var itemDescription: UITextView = UITextView()
    
    //Order Button and UI
    var minusButton: SpringButton = SpringButton()
    var plusButton: SpringButton = SpringButton()
    var orderCount: UITextView = UITextView()
    var orderCountValue = 1
    var orderButton: SpringButton = SpringButton()
    var totalPrice: UITextView = UITextView()
    var totalPriceValue: Double = 0.00

    var isItemAlreadyInCart: Bool = false //used to update add to cart language for item already in the cart

    //clearcartbutton
//    var tap: UITapGestureRecognizer = UITapGestureRecognizer()
//    let backgroundDark: UIView = UIView()
//    let whiteBox: SpringButton = SpringButton()
//    let cartText: UITextView = UITextView()
//    let text: UITextView = UITextView()
//    var cancelButton: SpringButton = SpringButton()
//    var yesButton: SpringButton = SpringButton()

    var isItemFromADifferentDate: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN MenuDetailsVC | ViewDidLoad")

        if let item = userCartOrder.OrderedItems.first(where: {$0.ID == selectedMenuItem.ID}) {
            print("item already in cart | updating quantity")
            
            isItemAlreadyInCart = true
            orderCountValue = Int(item.Quantity)!
        }

//        tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
//        tap.numberOfTapsRequired = 1
//        tap.isEnabled = false
//        view.addGestureRecognizer(tap)

        self.navigationController?.hero.isEnabled = true
        self.view.hero.modifiers = [.duration(0.1)]

        self.view.hero.id = "menuItem\(selectedMenuItem.HeroID)"
        print("details view id: ", self.view.hero.id!)
//        self.navigationController?.hero.navigationAnimationType = .selectBy(presenting: .auto, dismissing: .auto)

        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .up)

        NotificationCenter.default.addObserver(self, selector: #selector(dismissVC), name: NSNotification.Name(rawValue: "dismissMenuDetailsVC"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(getUserCurrentCartFromMenuDetails), name: NSNotification.Name(rawValue: "getUserCurrentCartFromMenuDetails"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(addNewItemsToCart), name: NSNotification.Name(rawValue: "addNewItemsToCart"), object: nil)

        //addNewItemsToCart
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white

        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.hero.id = "navLeft"
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        createMenuItemAndOrderButtons()
        //createClearCartButton()
        navigationLeft.setBackgroundColor(color: .clear, forState: .highlighted)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }
    @objc func getUserCurrentCartFromMenuDetails() {
        tryToGetServerCartParameters(self, promoCode: "", completion: tryToGetServerCartParametersHandler)
    }
    
    func createMenuItemAndOrderButtons() {
    
        itemImage.frame = CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width, height: self.view.frame.height / 2.75)
        itemImage.contentMode =  UIViewContentMode.scaleAspectFill
        itemImage.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
        itemImage.kf.indicatorType = .activity
        itemImage.kf.setImage(with: selectedMenuItem.ImageURL, placeholder: UIImage(named: "no_dishImage"))
        itemImage.hero.id = "menuImage\(selectedMenuItem.HeroID)"
        itemImage.hero.modifiers = [
//            .cornerRadius(50),
//            .contentsRect(itemImage.frame),
//            .masksToBounds(true),
            .duration(0.25),
//            .overlay(color: hexRed.Color, opacity: 0.7),
            .arc(intensity: 2)
        ]
        
        itemImage.clipsToBounds = true
        itemImage.layer.zPosition = 1
        self.view.addSubview(itemImage)
        
        itemTitle.frame = CGRect(x: 0, y: itemImage.frame.maxY + 15, width: self.view.frame.width - 20, height: 25)
        itemTitle.text = selectedMenuItem.Name
//        itemTitle.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.regular)
        itemTitle.font = UIFont.Theme.size17
        itemTitle.textAlignment = .center
        itemTitle.textColor = newHexBlack1.Color
        itemTitle.sizeToFit()
        itemTitle.center.x = self.view.center.x
        itemTitle.backgroundColor = .clear
        itemTitle.isUserInteractionEnabled = false
        itemTitle.layer.zPosition = 2
        itemTitle.hero.id = "menuTitle\(selectedMenuItem.HeroID)"
        view.addSubview(itemTitle)
        
        itemTitleSeperator.frame = CGRect(x: 0, y: itemTitle.frame.maxY - 15, width: self.view.frame.width, height: globalNavIconSize - 2)
        itemTitleSeperator.text = "______"
        itemTitleSeperator.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        itemTitleSeperator.textAlignment = .center
        itemTitleSeperator.textColor = UIColor.black.withAlphaComponent(0.7)
        itemTitleSeperator.sizeToFit()
        itemTitleSeperator.center.x = self.view.center.x
        itemTitleSeperator.backgroundColor = .clear
        itemTitleSeperator.isUserInteractionEnabled = false
        itemTitleSeperator.layer.zPosition = 2
        view.addSubview(itemTitleSeperator)

        itemDescription.frame = CGRect(x: 0, y: itemTitleSeperator.frame.maxY - 5, width: self.view.frame.width - 20, height: 50)
        itemDescription.text = selectedMenuItem.Description
//        itemDescription.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        itemDescription.font = UIFont.Theme.size15

        itemDescription.textAlignment = .center
        itemDescription.centerVertically()
        itemDescription.textColor = newHexBlack2.Color
        itemDescription.sizeToFit()
        itemDescription.center.x = self.view.center.x
        itemDescription.backgroundColor = .clear
        itemDescription.isUserInteractionEnabled = false
        itemDescription.layer.zPosition = 2
        itemDescription.hero.id = "menuDetails\(selectedMenuItem.HeroID)"
        itemDescription.hero.modifiers = [.contentsRect(itemDescription.frame), .size(CGSize(width: 0, height: 0))]
        view.addSubview(itemDescription)
        
        let countButtonHeight = (self.view.frame.height - 70) - 75
        let imagePadding = CGFloat(15.0)

        minusButton.frame = CGRect(x: 0, y: countButtonHeight, width: addSubtractButtonSize, height: addSubtractButtonSize)
        minusButton.center.x = self.view.center.x - (self.view.frame.width / 8)
        minusButton.setImage(UIImage(named: "order_minus.png")?.addImagePadding(x: imagePadding, y: imagePadding), for: .normal)
        minusButton.backgroundColor = .clear
        minusButton.tag = 10
        minusButton.titleLabel?.font = defaultFont14
        minusButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        self.view.addSubview(minusButton)
        
        plusButton.frame = CGRect(x: 0, y: countButtonHeight, width: addSubtractButtonSize, height: addSubtractButtonSize)
        plusButton.center.x = self.view.center.x + (self.view.frame.width / 8)
        plusButton.backgroundColor = .clear
        plusButton.setImage(UIImage(named: "order_plus.png")?.addImagePadding(x: imagePadding, y: imagePadding), for: .normal)
        plusButton.tag = 11
        plusButton.titleLabel?.font = defaultFont14
        plusButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        self.view.addSubview(plusButton)
        
        orderCount.frame = CGRect(x: 0, y: Int(countButtonHeight), width: 100, height: Int(addSubtractButtonSize))
        orderCount.text = String(orderCountValue)
//        orderCount.textColor = hexDarkGray
        orderCount.isEditable = false
        orderCount.isUserInteractionEnabled = false
        orderCount.center.x = view.center.x
        orderCount.backgroundColor = .clear
        orderCount.font = UIFont.Theme.size15
        orderCount.textColor = newHexBlack2.Color
        orderCount.textAlignment = .center
        orderCount.centerVertically()
        self.view.addSubview(orderCount)

        if isItemAlreadyInCart == true {
            orderButton = createFooterButton(text: "Update cart item", v: self.view)
        } else {
            orderButton = createFooterButton(text: "Add \(orderCountValue) to cart", v: self.view)
        }

        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.backgroundColor = hexRed.Color
        
        totalPrice.frame = CGRect(x: orderButton.frame.maxX - 110, y: self.view.frame.height - 70, width: 100, height: addSubtractButtonSize)
        totalPriceValue = Double(orderCountValue) * selectedMenuItem.Price
        totalPrice.text = "GHS \(formatPrice(p: totalPriceValue))"
        totalPrice.textColor = UIColor.white
        totalPrice.isUserInteractionEnabled = false
        totalPrice.isEditable = false
        totalPrice.backgroundColor = .clear
        totalPrice.font = UIFont.boldSystemFont(ofSize: 10)
        totalPrice.textAlignment = .right
        totalPrice.hero.id = "menuPrice\(selectedMenuItem.HeroID)"
        totalPrice.hero.modifiers = [.contentsRect(totalPrice.frame), .size(CGSize(width: 0, height: 0)), .duration(0.1)]
        totalPrice.centerVertically()
        totalPrice.center.y = orderButton.center.y

        if isIPhoneX == true {
            totalPrice.center.y = orderButton.center.y - 5
        }
        self.view.addSubview(totalPrice)
    }
    
    func callRemoteNavigationActions() {
        callNotificationItem("toggleMenusView")
        callNotificationItem("refreshPage")
        callNotificationItem("reloadTables")
    }

    func tryToAddItemToCart() {
        print("in tryToAddItemToCart | start")
        if orderCountValue != 0 {
            //check if order is for same day as current cart items
            if userCartOrder.OrderedItems.count > 0 {
                let cartOderDate = userCartOrder.Date.toDate()
                let currentItemDate = selectedMenuItem.Date.toDate()

                if let date1 = cartOderDate {
                    print("add to cart button pressed | cart date: ", date1)
                }
                if let date2 = currentItemDate {
                    print("add to cart button pressed | item date: ", date2)
                }

                if (cartOderDate?.isInside(date: currentItemDate!, granularity: .day))! {
                    isItemFromADifferentDate = false
                } else {
                    print("different cart day!")
                    isItemFromADifferentDate = true
                    //toggleClearCartUI(show: true)

                    let alert = UIAlertController(title: "Cart", message: "You have items from a different day in your cart. Clear cart and start a new one?", preferredStyle: UIAlertControllerStyle.alert)

                    UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .extraLight)

                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { action in

                    }))

                    alert.addAction(UIAlertAction(title: "Clear Cart", style: UIAlertActionStyle.destructive, handler: { action in
                        print("different cart day | clear pressed")
                        comingFrom.menuDetailsCreateNewCart = true
                        userCartOrder = emptyOrder //create empty cart
                        userCartOrder.Additionals.removeAll()
                        userCartOrder.OrderedItems.removeAll()

                        userCartOrder.OrderedItems.append(OrderDataLines(name: selectedMenuItem.Name, id: selectedMenuItem.ID, quantity: String(self.orderCountValue), price: String(selectedMenuItem.Price)))


                        BranchAssistant.app.addItem(item: selectedMenuItem)
                        //delete cart
                        callNotificationItem("refreshPage")

                        tryToDeleteCart(self, completion: tryToDeleteCartHandler)
                    }))
                    self.present(alert, animated: true) {

                    }
                }
            }

            if isItemFromADifferentDate != true {

                if let item = userCartOrder.OrderedItems.first(where: {$0.ID == selectedMenuItem.ID}) { //check if item is already in cart
                    print("item already in cart")

                    item.Quantity = String(orderCountValue)
                    if let row = userCartOrder.OrderedItems.index(where: {$0.ID == selectedMenuItem.ID}) {
                        userCartOrder.OrderedItems[row] = item
                    }
                } else {
                    print("adding new item in cart")
                    userCartOrder.OrderedItems.append(OrderDataLines(name: selectedMenuItem.Name, id: selectedMenuItem.ID, quantity: String(orderCountValue), price: String(selectedMenuItem.Price)))
                    BranchAssistant.app.addItem(item: selectedMenuItem)
                }
                tryToPutCartItems(self, completion: tryToPutCartItemsHandler)
            }

        } else {
            //order count is zero

            if userCartOrder.OrderedItems.count > 1 {//if cart has more than one item
                print("removing one item from multiple item cart | cart count before: ", userCartOrder.OrderedItems.count)
                if let index = userCartOrder.OrderedItems.index(where: {$0.ID == selectedMenuItem.ID}) {
                    userCartOrder.OrderedItems.remove(at: index)
                }
                tryToPutCartItems(self, completion: tryToPutCartItemsHandler)
            } else if userCartOrder.OrderedItems.count == 1 {//if cart only has this item
                print("only one item in cart and zero quantity - deleting cart")

                callNotificationItem("refreshPage")
                //tryToDeleteCartComingFromMenuDetails = true
                comingFrom.menuDetailsToDeleteCart = true
                tryToDeleteCart(self, completion: tryToDeleteCartHandler)
            }
        }
    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: SpringButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 10 {
            if orderCountValue > 0 {
                orderCountValue -= 1
                updateOrderCostFields()
            }
        } else if sender.tag == 11 {
            orderCountValue += 1
            updateOrderCostFields()
        } else if sender.tag == 12 {
            print("back button pressed")
            dismissVC()
        } else if sender.tag == 13 {
            print("add to cart button pressed")

            if hasInternetCheck() == true {
                tryToAddItemToCart()
            } else {
                noInternetWarning()
            }
        }

            //not used
//        else if sender.tag == 20 {
//            //cancel button pressed on clear car
//            //toggleClearCartUI(show: false)
//
//        } else if sender.tag == 21 {
//            //clear button pressed on clear car
//            print("different cart day | clear pressed")
//            //comingFromStartNewCart = true
//            comingFrom.menuDetailsCreateNewCart = true
//            userCartOrder = emptyOrder //create empty cart
//            userCartOrder.Additionals.removeAll()
//            userCartOrder.OrderedItems.removeAll()
//
//            userCartOrder.OrderedItems.append(OrderDataLines(name: selectedMenuItem.Name, id: selectedMenuItem.ID, quantity: String(self.orderCountValue), price: String(selectedMenuItem.Price)))
//
//            //delete cart
//            callNotificationItem("refreshPage")
//
//            tryToDeleteCart(self, completion: tryToDeleteCartHandler)
//        }
    }

//    @objc func tapFunction(sender: UITapGestureRecognizer) {
//        print("background pressed")
//        //toggleClearCartUI(show: false)
//    }
//
//    func createClearCartButton() {
//        print("in createClearCartButton | start")
//        var boxHeight = self.view.frame.height / 4.25
//        if isIPhoneX == true {
//            boxHeight = self.view.frame.height / 5
//        } else if isIPhoneSE == true {
//            boxHeight = self.view.frame.height / 3.5
//        }
//
//        backgroundDark.frame = view.frame
//        backgroundDark.layer.zPosition = 100
//        backgroundDark.backgroundColor = UIColor.black.withAlphaComponent(0.7)
//        backgroundDark.isHidden = true
//        view.addSubview(backgroundDark)
//
//        whiteBox.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - (globalXAxisIndent * 2), height: boxHeight)
//        whiteBox.isUserInteractionEnabled = true
//        whiteBox.backgroundColor = .white
//        whiteBox.center = view.center
//        whiteBox.layer.zPosition = 101
//        whiteBox.isHidden = true
//        view.addSubview(whiteBox)
//
//        cartText.frame = CGRect(x: globalXAxisIndent, y: 0, width: 100, height: 20)
//        cartText.textColor = .black
//        cartText.text = "Cart"
//        cartText.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
//
//        cartText.textAlignment = .left
//        cartText.sizeToFit()
//        cartText.frame.origin.y = globalXAxisIndent - 5
//        cartText.backgroundColor  = .clear
//        cartText.isScrollEnabled = false
//        cartText.isEditable = false
//        cartText.layer.zPosition = 102
//        cartText.isUserInteractionEnabled = false
//        cartText.isSelectable = false
//        whiteBox.addSubview(cartText)
//
//        text.frame = CGRect(x: globalXAxisIndent, y: cartText.frame.maxY, width: whiteBox.frame.width - (2 * globalXAxisIndent), height: 150)
//        text.textColor = .black
//        text.text = "You have items from a different day in your cart.  Clear cart and start a new one?"
//        text.backgroundColor  = .clear
//        text.font = UIFont.systemFont(ofSize: 15.5, weight: UIFont.Weight.light)
//
//        text.isScrollEnabled = false
//        text.isEditable = false
//        text.layer.zPosition = 102
//        text.isUserInteractionEnabled = false
//        text.isSelectable = false
//        text.textAlignment = .left
//        whiteBox.addSubview(text)
//
//        let buttonWidth: CGFloat = self.view.frame.width / 2 - ((globalXAxisIndent) + 5)
//        let buttonHeight: CGFloat = 50.0
//
//        yesButton = createFooterButton(text: "CLEAR CART", v: self.view)
//        yesButton.frame = CGRect(x: whiteBox.frame.width - (globalXAxisIndent + 100), y: whiteBox.frame.height - (buttonHeight), width: 100, height: buttonHeight)
//        yesButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        yesButton.tag = 21
//        yesButton.setTitleColor(hexYellow.Color, for: .normal)
//        yesButton.setBackgroundColor(color: UIColor.white, forState: .normal)
//        yesButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold   )
//        yesButton.titleLabel?.textAlignment = .left
//        whiteBox.addSubview(yesButton)
//
//        cancelButton = createFooterButton(text: "CANCEL", v: self.view)
//        cancelButton.frame = CGRect(x: yesButton.frame.minX - ((globalXAxisIndent / 2) + 75), y: yesButton.frame.minY, width: 75, height: buttonHeight)
//        cancelButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        cancelButton.tag = 20
//        cancelButton.titleLabel?.textAlignment = .right
//        cancelButton.setTitleColor(hexYellow.Color, for: .normal)
//        cancelButton.setBackgroundColor(color: UIColor.white, forState: .normal)
//        cancelButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
//        whiteBox.addSubview(cancelButton)
//
//        if isIPhoneSE == true {
//            cartText.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
//            text.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
//
//            yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold   )
//
//            cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
//
//        }
//
//        cancelButton.layer.zPosition = 102
//        yesButton.layer.zPosition = 102
//    }

//    func toggleClearCartUI( show: Bool) {
//        print("in toggleClearCartUI | start")
//        if show == true {
//            tap.isEnabled = true
//            whiteBox.isHidden = false
//            backgroundDark.isHidden = false
//
//        } else {
//            tap.isEnabled = false
//            whiteBox.isHidden = true
//            backgroundDark.isHidden = true
//
//        }
//
//    }

    @objc func addNewItemsToCart() {
        print("in addNewItemsToCart | start")
        tryToPutCartItems(self, completion: tryToPutCartItemsHandler)
    }

    @objc func dismissVC() {

        if comingFrom.cartToMenuDetails == true {
            //comingFromSCartToMenuDetails = false
            comingFrom.cartToMenuDetails = false
            delay(0.5) { //delay
                callNotificationItem("refreshCartPageData")
            }

            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
            self.hero.dismissViewController()
        } else {

            callNotificationItem("refreshPage")
            callNotificationItem("toggleViewCartButtonFromGetCartAPI")
            self.navigationController?.hero.navigationAnimationType = .auto
            self.hero.dismissViewController()
        }

    }
    
    func updateOrderCostFields() {
        print("in updateOrderCostFields | start")


        if isItemAlreadyInCart == true {
            
            orderButton.setTitle("Update cart item", for: .normal)
        } else {
            orderButton.setTitle("Add \(orderCountValue) to cart", for: .normal)
        }
        
        totalPriceValue = Double(orderCountValue) * selectedMenuItem.Price
        totalPrice.text = "GHS \(formatPrice(p: totalPriceValue))"
        orderCount.text = String(orderCountValue)
        totalPrice.centerVertically()
        orderCount.centerVertically()
    }

}
