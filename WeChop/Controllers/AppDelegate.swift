//
//  AppDelegate.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import UserNotifications
import IQKeyboardManagerSwift

import Firebase
import FirebaseInstanceID
import FirebaseMessaging

import Crashlytics
import SwiftyJSON
import EzPopup
import Branch
import SwiftMessages

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    let gcmMessageIDKey = "AAAAvmGGiZs:APA91bFMMfqQ851tFd-vySDyAUXLjSMZ-uWPXLxRM7nhJL4byoOdFYC1Ox7Kbezy2QomSNPp2iPRFxoISjQn_1z7uoFehWcSoqTpfQS4WBKfOSxjtU_3CvErFEXdtIXXvRuEDmBGj9Zo"
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // MARK: Facebook
        FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)

        // MARK: Notificaitons
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            print("in appDelegate | notification received: ", notification)
        }
        UNUserNotificationCenter.current().delegate = self
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        //change status bar color and style
        var prefersStatusBarHidden: Bool {
            return true
        }
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = hexRed.Color
        }
        
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        globalStatusBarHeight = UIApplication.shared.statusBarFrame.height

        // MARK: Branch
        Branch.setUseTestBranchKey(true)
        Branch.getInstance().setDebug()
        branchInstance.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                print("BRANCH IO SUCCESS | params: %@", params as? [String: AnyObject] ?? {})
            }
        })

        // MARK: Keyboard MGMT
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = ""

        // MARK: Firebase
        //FirebaseApp.configure()

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [: ]) -> Bool {
        branchInstance.application(app, open: url, options: options)
        return true
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("notification: \(userInfo)")
        
        if application.applicationState == .inactive {
            print("in inactive state notification: \(userInfo)")
        }

        branchInstance.handlePushNotification(userInfo)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        // BRANCH pass the url to the handle deep link call
        let handled = branchInstance.application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!handled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }

        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        return facebookDidHandle
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("in applicationDidEnterBackground | start")
        
        saveToUserDefaults() //saves session and token ID information
        defaults.synchronize()
        if comingFrom.menu == true { //remove cart button if in menu
            callNotificationItem("removeCartButton")
        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("in applicationWillEnterForeground | start")
        refreshAllDateInformation()
        if comingFrom.menu == true { //refresh menu page
            callNotificationItem("refreshPage")
            //removeCartButton
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("in applicationDidBecomeActive | start")
        
        FBSDKAppEvents.activateApp()
        refreshAllDateInformation()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("in applicationWillTerminate | start")
        saveToUserDefaults() //saves session and token ID information
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: Data) {
        
        print("in didRegisterForRemoteNotificationsWithDeviceToken | start",deviceToken)
        
        var tokenString = ""
        
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("Device Token:", tokenString)
        
        if let refreshedToken = InstanceID.instanceID().token(){
            
            print("print Device registration ID From Firebase: \(String(describing: refreshedToken))")
            
        }
        
        userCurrentDeviceID = deviceToken.hexString
        defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
        defaults.synchronize()
        print("device ID: ", userCurrentDeviceID)
    }
    
    //Start Refresh token
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print("print fcmToken: \(fcmToken)")
        
    }
    
    //recieve firebase message
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("print remote message: \(remoteMessage.appData)")
    }

    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        branchInstance.continue(userActivity)
        return true
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
        
        if isSimulator == true {
            userCurrentDeviceID = "Simulator01"
            defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
            defaults.synchronize()
            print("Device Token: \(userCurrentDeviceID)")
        } else {
            userCurrentDeviceID = "failedToRegister"
            defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
            defaults.synchronize()
            print("Device Token: \(userCurrentDeviceID)")
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //this occurs if the app is open before the banner and notification shows.  This logic will determin wether to show the banner alert
        print("willPresent notification: ")
        dump(notification.request.content.userInfo)
        if let result = decodeNotification(notification.request.content.userInfo) {
            ackNotification(result.payload.id, completion: notificationHandler)

            if result.payload.isVisible != true {
                completionHandler([])
            } else {
                completionHandler([.alert, .sound])
            }
        } else {
            print("here")
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping() -> Void) {

        print("didReceive response notification: ")
        dump(response.notification.request.content.userInfo)
        
        if let result = decodeNotification(response.notification.request.content.userInfo) {
            currentPushNotification = result
            print(currentPushNotification)
            openNotification(result.payload.id, completion: notificationHandler)

            print("notification action: ", currentPushNotification.payload.actionType)
            if currentPushNotification.payload.actionType == "None" {

            } else if currentPushNotification.payload.actionType == "Order" || currentPushNotification.payload.actionType == "OrderOrSnooze" {
                //goto menu
                if comingFrom.menu != true {
                    if userCurrentSessionToken != "" {
                        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuVC") as? MenuVC {
                            if let window = self.window, let rootViewController = window.rootViewController {
                                var currentController = rootViewController
                                while let presentedController = currentController.presentedViewController {
                                    currentController = presentedController
                                }
                                currentController.present(controller, animated: false, completion: nil)
                            }
                        }
                        callNotificationItem("refreshPage")
                    } else {
                        animateUIBanner("Warning", subText: "Please log-in.", color: UIWarningErrorColor)
                    }
                }
            } else if currentPushNotification.payload.actionType == "View" {
                //goto order Details for order

                if userCurrentSessionToken != "" {

                    if fullOrderList.count > 0 {
                        if let order = fullOrderList.first(where: { $0.OrderID == currentPushNotification.orderNumber }) {
                            selectedOrderItem = order

                            //update order data
                            getOrderDetailsData(selectedOrderItem.OrderID, completion: getOrderDetailsSelectedOrderDataHandler)

                            //show order details page
                            print("RCB - order details")
                            callNotificationItem("removeCartButton")
                            //isInMenuPage = false
                            comingFrom.menu = false

                            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrdersDetailVC") as? OrdersDetailVC {
                                if let window = self.window, let rootViewController = window.rootViewController {
                                    var currentController = rootViewController
                                    while let presentedController = currentController.presentedViewController {
                                        currentController = presentedController
                                    }
                                    currentController.present(controller, animated: true, completion: nil)
                                }

                            }
                        } else {
                            animateUIBanner("Warning", subText: "Could not locate specified order.", color: UIWarningErrorColor)
                        }
                    } else {
                        animateUIBanner("Warning", subText: "Could not locate specified order.", color: UIWarningErrorColor)
                    }
                } else {
                    animateUIBanner("Warning", subText: "Please log-in.", color: UIWarningErrorColor)
                }

            } else if currentPushNotification.payload.actionType == "RateOrSnooze" {
                //show ratings page for order
                if userCurrentSessionToken != "" {
                    if fullOrderList.count > 0 {
                        if let order = fullOrderList.first(where: { $0.OrderID == currentPushNotification.orderNumber }) {

                            if order.IsRated == false {

                                selectedOrderItem = order
                                lastOrderIDPromptedForRating = selectedOrderItem.OrderID
                                //save to stop prompt in the future
                                defaults.set(lastOrderIDPromptedForRating, forKey: "lastOrderRated")
                                defaults.synchronize()

                                //show rating page
                                print("RCB - ratingpage")
                                callNotificationItem("removeCartButton")
                                //isInMenuPage = false
                                comingFrom.menu = false

                                if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderRatingVC") as? OrderRatingVC {
                                    if let window = self.window, let rootViewController = window.rootViewController {
                                        var currentController = rootViewController
                                        while let presentedController = currentController.presentedViewController {
                                            currentController = presentedController
                                        }
                                        currentController.present(controller, animated: true, completion: nil)
                                    }
                                }
                            } else {
                                animateUIBanner("Warning", subText: "Could not submit ratings. Please try again later.", color: UIWarningErrorColor)
                            }
                        } else {
                            animateUIBanner("Warning", subText: "Could not locate specified order.", color: UIWarningErrorColor)
                        }
                    } else {
                        animateUIBanner("Warning", subText: "Could not locate specified order.", color: UIWarningErrorColor)
                    }
                } else {
                    animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                }
            } else if currentPushNotification.payload.actionType == "ListMenuDates" {
                //show menu select date picker

                if userCurrentSessionToken != "" {
                    if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuDaySelectionTableVC") as? MenuDaySelectionTableVC {
                        if let window = self.window, let rootViewController = window.rootViewController {
                            var currentController = rootViewController
                            while let presentedController = currentController.presentedViewController {
                                currentController = presentedController
                            }
                            currentController.present(controller, animated: true, completion: nil)
                        }
                    }
                    callNotificationItem("refreshPage")
                } else {
                    animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                }
            }
        }
        completionHandler()
    }
    
}

extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    // [END refresh_token]
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

