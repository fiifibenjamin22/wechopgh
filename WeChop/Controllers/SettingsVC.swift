//
//  SettingsVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/12/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class SettingsVC: UIViewController, UIScrollViewDelegate {
    
    //NOTE I used storyboard for this page because of the number of unique fields and elements
        
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var phonePrefix: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    var navigationLeft: SpringButton = SpringButton() 
    var navigationRight: SpringButton = SpringButton()

    var navigationTitleBorder = SpringButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN SettingsVC | ViewDidLoad")
        //writeToFireBase("in SettingsVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .up)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUserInformation), name: NSNotification.Name(rawValue: "reloadUser"), object: nil) 

        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Settings", v: self.view, leftButton: navigationLeft)
        
        navigationRight = SpringButton(frame: CGRect(x: self.view.frame.width - 70, y: 0, width: 70, height: globalLargeButtonHeight))
        navigationRight.titleLabel?.font = UIFont.Theme.size15

        navigationRight.setTitle("EDIT", for: .normal)
        navigationRight.center.y = navigationLeft.center.y
        navigationRight.setBackgroundColor(color: .clear, forState: .normal)
        navigationRight.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        navigationRight.layer.zPosition = 1002
        navigationRight.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        navigationRight.tag = 13
        navigationRight.setTitleColor(hexDarkGray, for: .normal)
        navigationRight.contentVerticalAlignment = .center
        navigationRight.contentHorizontalAlignment = .center
        navigationRight.isUserInteractionEnabled = true
        
        self.view.addSubview(navigationRight)
        
       setUserInformation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
        setUserInformation()

    }
    
    @objc func setUserInformation() {
        print("IN setUserInformation | start")
        if userData != nil {
            print("IN setUserInformation | not nill")
            firstName.text = userData?.FirstName
            lastName.text = userData?.LastName
            
            let formattedPhone = PartialFormatter().formatPartial("+\((userData?.Phone)!)")
            
            var prefix: String = ""
            var suffix: String = ""

            if let range = formattedPhone.range(of: " ") {
                prefix = formattedPhone.components(separatedBy: " ")[0]
                suffix = String(formattedPhone[range.upperBound...])
            } else {
                prefix = "+\(String(((userData?.Phone)?.prefix(3))!))"
                suffix = String(((userData?.Phone)?.dropFirst(3))!)
            }

            phonePrefix.text = prefix
            phoneNumber.text = suffix
//            
//            if(userData?.Phone)?.prefix(3) == "233" {
//                // TODO update to get string after certain characters
//                phonePrefix.text = "+\(String(((userData?.Phone)?.prefix(3))!))"
//                phoneNumber.text = String(((userData?.Phone)?.dropFirst(3))!)
//            } else if(userData?.Phone)?.prefix(3) == "1" {
//                phonePrefix.text = "+\(String(((userData?.Phone)?.prefix(1))!))"
//                phoneNumber.text = String(((userData?.Phone)?.dropFirst(3))!)
//            } else {
//                phonePrefix.text = "+\(String(((userData?.Phone)?.prefix(3))!))"
//                phoneNumber.text = String(((userData?.Phone)?.dropFirst(3))!)
//            }
            
        }
        
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            callNotificationItem("reloadTables")
            self.navigationController?.hero.navigationAnimationType = .slide(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //edit
            
            segue(name: "EditProfileVCSegueFromSettings", v: self, type: .zoom)
        }
    }

}
