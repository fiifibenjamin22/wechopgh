//
//  AboutVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import StoreKit
import SwiftDate

class AboutVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    let profileSections: [String] = ["Rate Us in the App Store", "Privacy Policy", "Terms and Conditions", "Contact Support"]
    var profileViewPreviousMaxY: CGFloat = 0
    var userCredit: Int = 0
    
    var versionTitle: UIButton = create {
        $0.isUserInteractionEnabled = true
    }

    var signOutButton: UIButton = create {
        $0.isUserInteractionEnabled = true
    }

    var navigationFooterBorder: UIButton = create {
        $0.backgroundColor = hexLightGray
        $0.isUserInteractionEnabled = false
        $0.layer.zPosition = 3
    }
    
    var navigationTitleBorder = SpringButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN ProfileVC | ViewDidLoad")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .up)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "About", v: self.view, leftButton: navigationLeft)

        navigationFooterBorder.frame = CGRect(x: 0, y: self.view.frame.height - globalLargeButtonHeight, width: self.view.frame.width, height: 1)

        if isIPhoneX == true {

            print("is iphoneX??? ")
            navigationFooterBorder.frame = CGRect(x: 0, y: self.view.frame.height - (globalLargeButtonHeight + globalIPhoneXTopYPadding), width: self.view.frame.width, height: 1)
        }

        navigationFooterBorder.center.x = view.center.x
        view.addSubview(navigationFooterBorder)

        signOutButton.frame = CGRect(x: 0, y: navigationFooterBorder.frame.maxY, width: self.view.frame.width, height: globalLargeButtonHeight)

        signOutButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular)
        signOutButton.setTitleColor(hexDarkGray, for: .normal)
        signOutButton.setBackgroundColor(color: UIColor.white, forState: .normal)
        signOutButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        signOutButton.setTitle("Sign Out", for: .normal)
        signOutButton.tag = 28
        signOutButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        signOutButton.isUserInteractionEnabled = true
        signOutButton.contentHorizontalAlignment = .left
        signOutButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        view.addSubview(signOutButton)

        //version information
        versionTitle.frame = CGRect(x: 0, y: navigationFooterBorder.frame.minY - 50, width: self.view.frame.width, height: 50)
        versionTitle.titleLabel?.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.semibold)

        if isIPhoneX == true {
            versionTitle.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        }

        versionTitle.setTitleColor(hexDarkTan.Color, for: .normal)
        versionTitle.setTitle("VERSION \(appVersion)", for: .normal)
        versionTitle.isUserInteractionEnabled = false
        versionTitle.contentHorizontalAlignment = .center
        versionTitle.contentVerticalAlignment = .center
        view.addSubview(versionTitle)

        var tagNum = 0
        createSocialMediaRow()

        for item in profileSections {
            
            let profileImage = getProfileRowImage(num: tagNum)
            createAboutRow(image: profileImage, title: item, tag: tagNum + 20)
            tagNum += 1
        }
        
        profileViewPreviousMaxY = versionTitle.frame.minY - globalLargeButtonHeight
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    func createAboutRow(image: String, title: String, tag: Int) {
        
        //navigationTitle
        let button = UIButton()
        if profileViewPreviousMaxY == 0.0 {
            button.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY + 10, width: self.view.frame.width, height: globalLargeButtonHeight)
        } else {
            button.frame = CGRect(x: 0, y: profileViewPreviousMaxY, width: self.view.frame.width, height: globalLargeButtonHeight)
        }
        
        button.tag = tag
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button.backgroundColor = .clear
        button.isUserInteractionEnabled = true
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.setTitleColor(hexDarkGray, for: .normal)
        button.setBackgroundColor(color: .clear, forState: .normal)
        button.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button.setTitle(title, for: .normal)
        button.contentVerticalAlignment = .center
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        
        let lineView = UIView(frame: CGRect(x: 0, y: button.frame.size.height, width: button.frame.size.width, height: 1))
        lineView.backgroundColor = hexLightGray
        button.addSubview(lineView)
        
        self.view.addSubview(button)
        
        profileViewPreviousMaxY = button.frame.maxY
    }
    
    func createSocialMediaRow() {
        
        profileViewPreviousMaxY  = navigationTitleBorder.frame.maxY 
        
        let button = UIButton()
        button.frame = CGRect(x: 0, y: profileViewPreviousMaxY, width: self.view.frame.width / 4, height: globalLargeButtonHeight)
        button.tag = 24
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button.setImage(UIImage(named: "about_facebook"), for: .normal)
        button.backgroundColor = .clear
        button.isUserInteractionEnabled = true
        button.setBackgroundColor(color: .clear, forState: .normal)
        button.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button.contentVerticalAlignment = .center
        button.contentHorizontalAlignment = .center
        button.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        let lineView = UIView(frame: CGRect(x: 0, y: button.frame.size.height, width: button.frame.size.width, height: 1))
        lineView.backgroundColor = hexLightGray
        button.addSubview(lineView)
        self.view.addSubview(button)
        
        let button2 = UIButton()
        button2.frame = CGRect(x: self.view.frame.width / 4, y: profileViewPreviousMaxY, width: self.view.frame.width / 4, height: globalLargeButtonHeight)
        button2.tag = 25
        button2.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button2.setImage(UIImage(named: "about_twitter"), for: .normal)
        button2.backgroundColor = .clear
        button2.isUserInteractionEnabled = true
        button2.setBackgroundColor(color: .clear, forState: .normal)
        button2.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button2.contentVerticalAlignment = .center
        button2.contentHorizontalAlignment = .center
        button2.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        button2.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        let lineView2 = UIView(frame: CGRect(x: 0, y: button2.frame.size.height, width: button2.frame.size.width, height: 1))
        lineView2.backgroundColor = hexLightGray
        button2.addSubview(lineView2)
        self.view.addSubview(button2)
        
        let button3 = UIButton()
        button3.frame = CGRect(x: (self.view.frame.width / 4) * 2, y: profileViewPreviousMaxY, width: self.view.frame.width / 4, height: globalLargeButtonHeight)
        button3.tag = 26
        button3.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button3.setImage(UIImage(named: "about_instagram"), for: .normal)
        button3.backgroundColor = .clear
        button3.isUserInteractionEnabled = true
        button3.setBackgroundColor(color: .clear, forState: .normal)
        button3.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button3.contentVerticalAlignment = .center
        button3.contentHorizontalAlignment = .center
        button3.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        button3.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        let lineView3 = UIView(frame: CGRect(x: 0, y: button3.frame.size.height, width: button3.frame.size.width, height: 1))
        lineView3.backgroundColor = hexLightGray
        button3.addSubview(lineView3)
        self.view.addSubview(button3)
        
        let button4 = UIButton()
        button4.frame = CGRect(x: (self.view.frame.width / 4) * 3, y: profileViewPreviousMaxY, width: self.view.frame.width / 4, height: globalLargeButtonHeight)
        button4.tag = 27
        button4.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button4.setImage(UIImage(named: "about_linkedin"), for: .normal)
        button4.backgroundColor = .clear
        button4.isUserInteractionEnabled = true
        button4.setBackgroundColor(color: .clear, forState: .normal)
        button4.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button4.contentVerticalAlignment = .center
        button4.contentHorizontalAlignment = .center
        button4.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        button4.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        let lineView4 = UIView(frame: CGRect(x: 0, y: button4.frame.size.height, width: button4.frame.size.width, height: 1))
        lineView4.backgroundColor = hexLightGray
        button4.addSubview(lineView4)
        self.view.addSubview(button4)
        profileViewPreviousMaxY = button4.frame.maxY
    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            callNotificationItem("reloadTables")
            self.navigationController?.hero.navigationAnimationType = .slide(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 20 {
            SKStoreReviewController.requestReview()
        } else if sender.tag == 21 {
            //goto privacy page
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: privacyURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 22 {
            //goto terms page
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: termsURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 23 {
            print("in contact support")
            let number: String = "+233547797803"

            //alert
            let alert = UIAlertController(title: "Contact Support", message: "Having an issue? Contact us using WhatsApp or call us directly.", preferredStyle: .actionSheet)

            alert.addAction( UIAlertAction( title: "WhatsApp", style: .default, handler: { UIAlertAction in
                print("User click WhatsApp button")
                //whatsapp
                UIApplication.shared.openURL (NSURL (string: "whatsapp://send?phone=\(number)")! as URL)

            }))

            alert.addAction(UIAlertAction(title: "Phone", style: .default, handler: { UIAlertAction in
                print("User click Cellphone button")
                //cellphone
                self.callSupport(number)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { UIAlertAction in
            }))

            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            //contact Support
//            if hasInternetCheck() == true {
//                UIApplication.shared.open(URL(string: termsURL)!, options: [:])
//            } else {
//                noInternetWarning()
//            }
        } else if sender.tag == 24 {
            //goto fb
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: facebookURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 25 {
            //goto twitter
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: twitterURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 26 {
            //goto Instagram
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: instagramURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 27 {
            //goto linkedin
            if hasInternetCheck() == true {
                UIApplication.shared.open(URL(string: linkedInURL)!, options: [:])
            } else {
                noInternetWarning()
            }
        } else if sender.tag == 28 {
            signOutUser(self, completion: signOutUserRequestHandler)
        }
    }

    func callSupport(_ number: String) {
        if let url = URL (string: "tel://\(number)"), UIApplication.shared.canOpenURL (url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL (url)
            }
        }
        else {
            print("Your device doesn't support this feature.")
        }
    }
    
    func getProfileRowImage(num: Int) -> String {
        switch num {
        case 0:
            return "profile_icon_grey.png"
        case 1:
            return "officeAddress_icon.png"
        case 2:
            return "payment_icon.png"
        case 3:
            return "share_button.png"
        case 4:
            return "about_icon.png"
        default:
            return "test.png"
        }
    }
}
