//
//  OrdersDetailVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/13/18.
//  Copyright © 2019 WeChop. All rights reserved.
//
import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import SwiftPullToRefresh
import EzPopup

class OrdersDetailVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft = SpringButton() 
    var navigationTitleBorder = SpringButton()
    var scrollView: UIScrollView = UIScrollView()
    var doneButton: SpringButton = SpringButton()
    
    var restaurantTitle: SpringLabel = SpringLabel()
    var orderStatus: SpringLabel = SpringLabel()
    var statusScheduled: SpringLabel = SpringLabel()
    var statusAwaiting: SpringLabel = SpringLabel()
    var statusPrepared: SpringLabel = SpringLabel()
    var statusOnTheWay: SpringLabel = SpringLabel()
    var statusArrived: SpringLabel = SpringLabel()
    
    var orderDetailsTitle: SpringLabel = SpringLabel()
    var orderNumber: SpringLabel = SpringLabel()
    var orderAddress: SpringTextView = SpringTextView()
    var orderDate: SpringLabel = SpringLabel()
    
    var orderVAT: SpringLabel = SpringLabel()
    var orderDeliveryFee: SpringLabel = SpringLabel()
    var orderTotal: SpringLabel = SpringLabel()
    var callForHelp: SpringButton = SpringButton()

    var isCurrentlyRefreshingPage: Bool = false
    var selectedOrderID: String = ""
    //ui elements
    let statusHeight: CGFloat = 25.0
    let statusXIndent: CGFloat = 15.0
    let verticleIndent: CGFloat = 75
    var statusGroup: [SpringLabel] = [SpringLabel]()
    let orderStatusText: [String] = ["Scheduled", "Awaiting Restaurant confirmation", "Food is being prepared", "Food is on the way", "Order has arrived"]
    
    let orderStatusPrefix: String = "●  "
    var grayFont =  UIFont.Theme.size15
    var orderFont = defaultFont14
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN OrdersDetailVC | ViewDidLoad")
//        //writeToFireBase("in OrdersDetailVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .up)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        if isIPhoneSE == true {
            grayFont =  UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
            orderFont = defaultFont10
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOrderPageData), name: NSNotification.Name(rawValue: "refreshOrderPageData"), object: nil) 

        NotificationCenter.default.addObserver(self, selector: #selector(dismissOrderDetailsVC), name: NSNotification.Name(rawValue: "dismissOrderDetailsVC"), object: nil)

        self.view.backgroundColor = UIColor.white
        
        if selectedOrderItem == nil {
            selectedOrderItem = emptyOrder
        }
        
        selectedOrderID = selectedOrderItem.OrderID //saved to use when refreshing order item
        
        createNavigationAndScrollView()
        createOrderUI()
        print("current order: ", selectedOrderItem) 
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }
    
    func createOrderUI() {
        
        if selectedOrderItem.IsComplete != true { //for orders that aren't complete add a refresh control
            
            guard
                let url = Bundle.main.url(forResource: globalRefreshGIFName, withExtension: "gif"),
                let data = try? Data(contentsOf: url) else { return }
            scrollView.spr_setGIFHeader(data: data, isBig: true, height: 200) { [weak self] in
                
                print("in refresh cart orders start")
                self!.isCurrentlyRefreshingPage = true
                
                getOrderDetailsData(selectedOrderItem.OrderID, completion: getOrderDetailsSelectedOrderDataHandler)
                
                delay(1.0, closure: {
                    self!.scrollView.spr_endRefreshing()
                    self!.isCurrentlyRefreshingPage = false
                })
                
            }
            
        }
        statusGroup = [statusScheduled, statusAwaiting, statusPrepared, statusOnTheWay, statusArrived]
        restaurantTitle.frame = CGRect(x: 0, y: 10, width: self.view.frame.width, height: globalLargeButtonHeight)
        restaurantTitle.text = selectedOrderItem.RestaurantName
        restaurantTitle.textColor = newHexBlack1.Color

        restaurantTitle.isUserInteractionEnabled = false
        restaurantTitle.center.x = view.center.x
        restaurantTitle.textAlignment = .center
        restaurantTitle.font = UIFont.Theme.size17Medium
        scrollView.addSubview(restaurantTitle)
        
        let border = SpringButton()
        border.frame = CGRect(x: 0, y: restaurantTitle.frame.maxY, width: view.frame.width, height: 1)
        border.center.x = view.center.x
        border.backgroundColor = hexLightGray
        border.isUserInteractionEnabled = false
        border.layer.zPosition = 3
        scrollView.addSubview(border)
        
        orderStatus.frame = CGRect(x: 15.0, y: border.frame.maxY + 10, width: self.view.frame.width - 20, height: statusHeight)
        orderStatus.text = "Order Status"
        orderStatus.textColor =  newHexBlack1.Color
        orderStatus.isUserInteractionEnabled = false
        orderStatus.textAlignment = .left
        orderStatus.font = UIFont.Theme.size15Semi
        scrollView.addSubview(orderStatus)
        
        statusScheduled.frame = CGRect(x: statusXIndent, y: orderStatus.frame.maxY + 5, width: self.view.frame.width - 20, height: statusHeight)
        statusScheduled.text = orderStatusPrefix + orderStatusText[0]
        statusScheduled.textColor = hexRed.Color
        statusScheduled.layer.zPosition = 6
        statusScheduled.backgroundColor = .clear
        statusScheduled.isUserInteractionEnabled = false
        statusScheduled.textAlignment = .left
        statusScheduled.font = defaultFont14
        scrollView.addSubview(statusScheduled)
        
        createStatusVerticalLine(yLoc: statusScheduled.center.y, indexLabel: 0)
        
        statusAwaiting.frame = CGRect(x: statusXIndent, y: statusScheduled.frame.maxY + 5, width: self.view.frame.width - 20, height: statusHeight)
        statusAwaiting.text = orderStatusPrefix + orderStatusText[1]
        statusAwaiting.textColor = hexRed.Color
        statusAwaiting.layer.zPosition = 6
        statusAwaiting.backgroundColor = .clear
        statusAwaiting.isUserInteractionEnabled = false
        statusAwaiting.textAlignment = .left
        statusAwaiting.font = defaultFont14
        scrollView.addSubview(statusAwaiting)
        
        createStatusVerticalLine(yLoc: statusAwaiting.center.y, indexLabel: 1)
        
        statusPrepared.frame = CGRect(x: statusXIndent, y: statusAwaiting.frame.maxY + 5, width: self.view.frame.width - 20, height: statusHeight)
        statusPrepared.text = orderStatusPrefix + orderStatusText[2]
        statusPrepared.textColor = hexRed.Color
        statusPrepared.layer.zPosition = 6
        statusPrepared.backgroundColor = .clear
        statusPrepared.isUserInteractionEnabled = false
        statusPrepared.textAlignment = .left
        statusPrepared.font = defaultFont14
        scrollView.addSubview(statusPrepared)
        
        createStatusVerticalLine(yLoc: statusPrepared.center.y, indexLabel: 2)
        
        statusOnTheWay.frame = CGRect(x: statusXIndent, y: statusPrepared.frame.maxY + 5, width: self.view.frame.width - 20, height: statusHeight)
        statusOnTheWay.text = orderStatusPrefix + orderStatusText[3]
        statusOnTheWay.textColor = hexRed.Color
        statusOnTheWay.layer.zPosition = 6
        statusOnTheWay.backgroundColor = .clear
        statusOnTheWay.isUserInteractionEnabled = false
        statusOnTheWay.textAlignment = .left
        statusOnTheWay.font = defaultFont14
        scrollView.addSubview(statusOnTheWay)
        
        createStatusVerticalLine(yLoc: statusOnTheWay.center.y, indexLabel: 3)
        
        statusArrived.frame = CGRect(x: statusXIndent, y: statusOnTheWay.frame.maxY + 5, width: self.view.frame.width - 20, height: statusHeight)
        statusArrived.text = orderStatusPrefix + orderStatusText[4]
        statusArrived.textColor = hexRed.Color
        statusArrived.layer.zPosition = 6
        statusArrived.backgroundColor = .clear
        statusArrived.isUserInteractionEnabled = false
        statusArrived.textAlignment = .left
        statusArrived.font = defaultFont14 
        scrollView.addSubview(statusArrived)
        
        for item in statusGroup {
            formatStatusText(label: item)
        }
        
        let border2 = SpringButton()
        border2.frame = CGRect(x: 0, y: statusArrived.frame.maxY + 10, width: view.frame.width, height: 1)
        border2.center.x = view.center.x
        border2.backgroundColor = hexLightGray
        border2.isUserInteractionEnabled = false
        border2.layer.zPosition = 3
        scrollView.addSubview(border2)
        
        orderDetailsTitle.frame = CGRect(x: 15.0, y: border2.frame.maxY + 10, width: self.view.frame.width - 20, height: statusHeight - 5)
        orderDetailsTitle.text = "Order Details"
        orderDetailsTitle.textColor = newHexBlack1.Color
        orderDetailsTitle.isUserInteractionEnabled = false
        orderDetailsTitle.textAlignment = .left
        orderDetailsTitle.font = UIFont.Theme.size15Semi
        scrollView.addSubview(orderDetailsTitle)
        
        orderNumber.frame = CGRect(x: 15.0, y: orderDetailsTitle.frame.maxY + 10, width: self.view.frame.width - 20, height: statusHeight - 5)
        
        //vars for multiColored text:
        let blackColor = [NSAttributedStringKey.foregroundColor:  newHexBlack1.Color]
        let redColor = [NSAttributedStringKey.foregroundColor: hexRed.Color]
        let attrString = NSMutableAttributedString(string: "Order No. \(selectedOrderItem.OrderID)", attributes: blackColor)
        attrString.setAttributes(redColor, range: NSRange(location: 10, length: selectedOrderItem.OrderID.length))
        orderNumber.attributedText = attrString
        orderNumber.isUserInteractionEnabled = false
        orderNumber.textAlignment = .left
        orderNumber.font = UIFont.Theme.size15
        scrollView.addSubview(orderNumber)
        
        orderAddress.frame = CGRect(x: 10.0, y: orderNumber.frame.maxY, width: self.view.frame.width - 20, height: statusHeight - 5)
        let attrString1 = NSMutableAttributedString(string: "Deliver to: \(selectedOrderItem.Address)", attributes:  blackColor)
        attrString1.setAttributes(redColor, range: NSRange(location: 12, length: selectedOrderItem.Address.length))
        orderAddress.attributedText = attrString1
        orderAddress.isUserInteractionEnabled = false
        orderAddress.textAlignment = .left
        orderAddress.font = UIFont.Theme.size15
        orderAddress.sizeToFit()
        orderAddress.centerVertically()
        scrollView.addSubview(orderAddress)
        
        let formattedDate = selectedOrderItem.Date.toDate()
        let text = "\(formattedDate!.toFormat("dd MMM")) '\(formattedDate!.toFormat("yy"))"
        
        orderDate.frame = CGRect(x: 15.0, y: orderAddress.frame.maxY, width: self.view.frame.width - 20, height: statusHeight - 5)
        
        let attrString2 = NSMutableAttributedString(string: "Date: \(text)", attributes: blackColor)
        attrString2.setAttributes(redColor, range: NSRange(location: 6, length: text.length))
        orderDate.attributedText = attrString2
        orderDate.isUserInteractionEnabled = false
        orderDate.textAlignment = .left
        orderDate.font = UIFont.Theme.size15
        scrollView.addSubview(orderDate)
        
        createOrderedItemRows(yMin: orderDate.frame.maxY + 10) // create the rows of ordered items
        
    }
    
    func createOrderedItemRows(yMin: CGFloat) {
        var originalY = yMin
        for item in selectedOrderItem.OrderedItems {
            
            if item.Name != "" {
                print("order count: ", selectedOrderItem.OrderedItems.count)
                
                let orderCount = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 25, height: statusHeight))
                orderCount.backgroundColor = .clear
                orderCount.text = "\(item.Quantity)x"
                orderCount.textColor = newHexBlack1.Color
                orderCount.layer.zPosition = 5
                orderCount.isUserInteractionEnabled = false
                orderCount.textAlignment = .left
                orderCount.font = UIFont.Theme.size15Medium
                scrollView.addSubview(orderCount)
                
                let orderCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: originalY, width: 150, height: statusHeight))
                orderCost.backgroundColor = .clear
                
                if !item.Price.isEmpty && !item.Quantity.isEmpty {
                    orderCost.text = "GHS \(formatPrice(p: (Double(item.Price)! * Double(item.Quantity)!)))"
                } else {
                    orderCost.text = "An Error Occured"
                }
                
                orderCost.textColor = hexDarkGray
                orderCost.layer.zPosition = 6
                orderCost.isUserInteractionEnabled = false
                orderCost.textAlignment = .right
                orderCost.font = orderFont
                scrollView.addSubview(orderCost)
                
                let orderTitle = SpringLabel(frame: CGRect(x: orderCount.frame.maxX, y: originalY, width: self.view.frame.width - (orderCost.frame.width * 0.85), height: statusHeight))
                orderTitle.font = orderFont
                orderTitle.backgroundColor = .clear
                orderTitle.text = "\(item.Name)"
                orderTitle.textColor = hexDarkGray
                orderTitle.layer.zPosition = 5
                orderTitle.isUserInteractionEnabled = false
                orderTitle.textAlignment = .left
                //                orderTitle.sizeToFitHeight()
                scrollView.addSubview(orderTitle)
                
                originalY = orderCount.frame.maxY
            } else {
                print("blank order item")
            }
        }
        
        //VAT
        let vat = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 15, width: 200, height: statusHeight))
        vat.backgroundColor = .clear
        vat.text = "VAT (17.5%)"
        vat.textColor = hexDarkGray
        vat.layer.zPosition = 5
        vat.isUserInteractionEnabled = false
        vat.textAlignment = .left
        vat.font = grayFont
        scrollView.addSubview(vat)
        
        let vatCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: vat.frame.minY, width: 150, height: statusHeight))
        vatCost.backgroundColor = .clear

        print("vat: ", selectedOrderItem.VatCost)
        vatCost.text = "GHS \(selectedOrderItem.VatCost)"
        if selectedOrderItem.VatCost == "" {
            vatCost.text = "\(0.00)"

        }
        vatCost.textColor = hexDarkGray
        vatCost.layer.zPosition = 6
        vatCost.isUserInteractionEnabled = false
        vatCost.textAlignment = .right
        vatCost.font = grayFont
        scrollView.addSubview(vatCost)
        
        originalY = vat.frame.maxY
        
        let delivery = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 150, height: statusHeight))
        delivery.backgroundColor = .clear
        delivery.text = "Delivery fee"
        delivery.textColor = hexDarkGray
        delivery.layer.zPosition = 5
        delivery.isUserInteractionEnabled = false
        delivery.textAlignment = .left
        delivery.font = grayFont
        scrollView.addSubview(delivery)
        
        let deliveryFee = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: originalY, width: 150, height: statusHeight))
        deliveryFee.backgroundColor = .clear
        deliveryFee.text = selectedOrderItem.DeliveryCost

        if selectedOrderItem.DeliveryCost == "" || selectedOrderItem.DeliveryCost == "0.0" {
            deliveryFee.text = "0.00"
        } else {
            deliveryFee.text = "GHS \(selectedOrderItem.DeliveryCost)"
        }

        deliveryFee.textColor = hexDarkGray
        deliveryFee.layer.zPosition = 6
        deliveryFee.isUserInteractionEnabled = false
        deliveryFee.textAlignment = .right
        deliveryFee.font = grayFont
        scrollView.addSubview(deliveryFee)
        
        originalY = delivery.frame.maxY
        
        let discount = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 200, height: statusHeight))
        discount.backgroundColor = .clear
        discount.text = "Discount"
        discount.textColor = newHexGray.Color
        discount.layer.zPosition = 5
        discount.isUserInteractionEnabled = false
        discount.textAlignment = .left
        discount.font = defaultFont14
        scrollView.addSubview(discount)
        
        let discountCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: discount.frame.minY, width: 150, height: statusHeight))
        discountCost.backgroundColor = .clear
        
        if selectedOrderItem.Discount == "" || selectedOrderItem.Discount == "0.0" {
            discountCost.text = "0.00"
        } else {
            discountCost.text = "GHS \(selectedOrderItem.Discount)"
        }
        
        discountCost.textColor = newHexGray.Color
        discountCost.layer.zPosition = 6
        discountCost.isUserInteractionEnabled = false
        discountCost.textAlignment = .right
        discountCost.font = defaultFont14
        scrollView.addSubview(discountCost)
        
        originalY = discount.frame.maxY
        
        let border3: SpringButton = SpringButton()
        border3.frame = CGRect(x: 0, y: originalY + 10, width: view.frame.width, height: 1)
        border3.center.x = view.center.x
        border3.backgroundColor = hexLightGray
        border3.isUserInteractionEnabled = false
        border3.layer.zPosition = 3
        scrollView.addSubview(border3)
        
        originalY = border3.frame.maxY
        
        let total = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 10, width: 200, height: statusHeight))
        total.backgroundColor = .clear
        total.text = "TOTAL"
        total.textColor = newHexBlack1.Color
        total.layer.zPosition = 5
        total.isUserInteractionEnabled = false
        total.textAlignment = .left
        total.font = UIFont.Theme.size15Semi
        scrollView.addSubview(total)
        
        let totalCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: total.frame.minY, width: 150, height: statusHeight))
        totalCost.backgroundColor = .clear
        totalCost.text = "GHS \(formatPrice(p: selectedOrderItem.Amount))"
        totalCost.textColor = newHexBlack1.Color
        totalCost.layer.zPosition = 6
        totalCost.isUserInteractionEnabled = false
        totalCost.textAlignment = .right
        totalCost.font = UIFont.Theme.size15Semi
        scrollView.addSubview(totalCost)
        
        let border2 = SpringButton()
        border2.frame = CGRect(x: 0, y: total.frame.maxY + 10, width: view.frame.width, height: 1)
        border2.center.x = view.center.x
        border2.backgroundColor = hexLightGray
        border2.isUserInteractionEnabled = false
        border2.layer.zPosition = 3
        scrollView.addSubview(border2)
        originalY = border2.frame.maxY

        callForHelp = createFooterButton(text: "Contact Support", v: self.view)
        callForHelp.frame = CGRect(x: 0, y: border2.frame.maxY + verticleIndent, width: self.view.frame.width, height: globalLargeButtonHeight)
        callForHelp.backgroundColor = .clear
        callForHelp.setTitleColor(hexDarkGray.withAlphaComponent(0.5), for: .normal)
        callForHelp.titleLabel?.font = defaultFont14
        callForHelp.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        callForHelp.isUserInteractionEnabled = true
        callForHelp.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        callForHelp.layer.zPosition = 100
        callForHelp.tag = 17
        scrollView.addSubview(callForHelp)
        
        originalY = callForHelp.frame.maxY

        //update scrollView max height
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: originalY + 150)
        
        if isIPhoneX == true {
            scrollView.contentSize = CGSize(width: self.view.frame.width, height: originalY + 175)
        }        
    }
    
    func createStatusVerticalLine(yLoc: CGFloat, indexLabel: Int) {

        let index = orderStatusText.index(of: selectedOrderItem.Status)
        let v = UIView(frame: CGRect(x: statusXIndent + 5.5, y: yLoc, width: 1, height: statusHeight))
        v.backgroundColor = hexRed.Color
        v.layer.zPosition = 5
        v.isUserInteractionEnabled = false
        
        if index != nil {
            if index! < indexLabel {
                v.backgroundColor = newHexGray.Color
            }
        } else {
            
        }
        
        scrollView.addSubview(v)
    }
    
    func formatStatusText(label: SpringLabel) {
        let index = orderStatusText.index(of: selectedOrderItem.Status)
        let indexLabel = orderStatusText.index(of: String(((label.text)?.dropFirst(3))!))
        if index! < indexLabel! {
            label.textColor = newHexGray.Color
        }
    }
    
    func createNavigationAndScrollView() {
        //create header and footer buttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Order Details", v: self.view, leftButton: navigationLeft)
        
        //scrollView
        scrollView.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)

        if isIPhoneX == true {
            scrollView.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)
        }

        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = true
        scrollView.alwaysBounceHorizontal = false
        scrollView.delegate = self
        scrollView.layer.zPosition = 3
        scrollView.backgroundColor = .clear
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: 4000)
        view.addSubview(scrollView)
        
//        if selectedOrderItem.IsComplete {
//            doneButton = createFooterButton(text: "DONE", v: self.view)
//            doneButton.backgroundColor = hexBlack.Color
//        } else {
            doneButton = createFooterButton(text: "Refer a friend, get 5 GHS", v: self.view)
            doneButton.backgroundColor = hexGreen.Color
//        }

        doneButton.isUserInteractionEnabled = true
        doneButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        doneButton.layer.zPosition = 100
        doneButton.tag = 14
        view.addSubview(doneButton)
    }

    @objc func dismissOrderDetailsVC() {
        callNotificationItem("reloadTables")
        self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
        self.hero.dismissViewController()
    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            if isCurrentlyRefreshingPage != true {
                dismissOrderDetailsVC()
            }
            
        } else if sender.tag == 13 {
            //refer a friend
            print("refer button pressed")
        } else if sender.tag == 14 {
//            if selectedOrderItem.IsComplete {
//                if isCurrentlyRefreshingPage != true {
//                    dismissOrderDetailsVC()
//                }
//            } else {

                //shouldShowShareDialogueOnLoad = true
            comingFrom.inviteShowInviteDialogue = true

                let contentVC = InviteVC()
                let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
                present(popupVC, animated: true)

                //showShareDialogue(vc: self, button: sender)
//            }
        } else if sender.tag == 17 {
            //call for help

            let number: String = "+233547797803"

            //alert
            let alert = UIAlertController(title: "Contact Support", message: "Having an issue? Contact us using WhatsApp or call us directly.", preferredStyle: .actionSheet)

            alert.addAction( UIAlertAction( title: "WhatsApp", style: .default, handler: { UIAlertAction in
                print("User click WhatsApp button")
                //whatsapp
                UIApplication.shared.openURL (NSURL (string: "whatsapp://send?phone=\(number)")! as URL)

            }))

            alert.addAction(UIAlertAction(title: "Phone", style: .default, handler: { UIAlertAction in
                print("User click Cellphone button")
                //cellphone
                self.callSupport(number)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { UIAlertAction in
            }))

            self.present(alert, animated: true, completion: {
                print("completion block")
            })

        }
    }
    
    func callSupport(_ number: String) {
        if let url = URL (string: "tel://\(number)"), UIApplication.shared.canOpenURL (url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL (url)
            }
        }
        else {
            print("Your device doesn't support this feature.")
        }
    }
    
    @objc func refreshOrderPageData(_ refreshControl: UIRefreshControl) {
        print("in refresh page")
        
        //updated selected order data
        if let index = fullOrderList.index(where: {$0.OrderID == selectedOrderID}) {
            fullOrderList[index] = selectedOrderItem!
        }
        
        //refresh page
        self.scrollView.spr_endRefreshing()
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        createOrderUI()
        
    }
}
