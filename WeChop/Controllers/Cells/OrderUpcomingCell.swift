//
//  OrderUpcomingCell.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/16/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class OrderUpcomingCell: UITableViewCell {

    @IBOutlet weak var orderViewDetailsButton: ItemSelectionButton!
    @IBOutlet weak var orderCancelButton: ItemSelectionButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
