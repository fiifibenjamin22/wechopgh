//  OrderHistoryCell.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/6/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderAddress: UILabel!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var orderTotalAmount: UILabel!
    @IBOutlet weak var orderDetailsButton: ItemSelectionButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
