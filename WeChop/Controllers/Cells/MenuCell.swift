//
//  MenuCell.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/6/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var cellColorOverlay: UIView!
    @IBOutlet weak var menuItemImageSoldOut: UIImageView!
    @IBOutlet weak var menuItemTitle: UILabel!
    @IBOutlet weak var menuItemDescription: UILabel!
    @IBOutlet weak var menuItemPrice: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
