//  OrderHistoryHeaderCell.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/6/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class OrderHistoryHeaderCell: UITableViewCell {

    @IBOutlet weak var headerRestaurantTitle: UILabel!
    @IBOutlet weak var headerRestaurantTitle2: UILabel!
    @IBOutlet weak var headerExperience: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
