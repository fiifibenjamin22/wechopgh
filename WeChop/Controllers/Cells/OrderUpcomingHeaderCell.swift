//
//  OrderUpcomingHeaderCell.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/16/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit

class OrderUpcomingHeaderCell: UITableViewCell {
    
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderRestaurantName: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderNumberText: UILabel!
    @IBOutlet weak var orderDeliverTo: UILabel!
    @IBOutlet weak var orderCost: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var orderStatusText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
