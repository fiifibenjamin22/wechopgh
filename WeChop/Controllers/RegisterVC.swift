//
//  RegisterVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/26/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring

import NVActivityIndicatorView
import Alamofire

import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import MapKit

class RegisterVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var navigationLeft: SpringButton = SpringButton()
    var activeTextField: UITextField?
    var helloText: SpringButton = SpringButton()
    var nameText: SpringButton = SpringButton()
    var nameImage: SpringButton = SpringButton()
    var nameInput: SpringTextField = SpringTextField()
    var addressText: SpringButton = SpringButton()
    var addressSubText: SpringTextView = SpringTextView()
    var addressImage: SpringButton = SpringButton()
    var addressInput: SpringTextField = SpringTextField()
    var phoneText: SpringButton = SpringButton()
    var phoneSubText: SpringTextView = SpringTextView()
    var phoneImage: SpringButton = SpringButton()
    var phoneInputPre: SpringTextField = SpringTextField()
    var phoneInput: SpringTextField = SpringTextField()
    var continueButton: SpringButton = SpringButton()
    
    //UI elements
    let verticalSpacing: CGFloat = 40
    let labelHeight: CGFloat = 20
    let imageSize: Int = 25
    
    var comingFromTransitionRegister: Bool = false //used to ensure keyboard view resize doesn't occur during segue or seuge dismiss
    
    let phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN RegisterVC | ViewDidLoad")
        //        //writeToFireBase("in RegisterVC viewdidload")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .zoom
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToRegisterVerificationVC), name: NSNotification.Name(rawValue: "moveToRegisterVerificationVC"), object: nil)//used when facebook register completes successfully
        
        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        nameInput.delegate = self
        addressInput.delegate = self
        phoneInputPre.delegate = self
        phoneInput.delegate = self
        nameInput.clearButtonMode = UITextFieldViewMode.whileEditing
        addressInput.clearButtonMode = UITextFieldViewMode.whileEditing
//        phInputPre.clearButtonMode = UITextFieldViewMode.whileEditing
        phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
        
        createFormFieldsUI()
        
    }
    
    @objc func moveToRegisterVerificationVC() {
        
        segue(name: "RegisterVerificationVCSegueFromRegister", v: self, type: .zoom)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("in viewWillAppear | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
        comingFromTransitionRegister = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in viewdidappear | start")
        
        //listen for keyboardshowing or hiding itself
        //        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.registerVCKeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.registerVCKeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //
        delay(0.2) {
            self.comingFromTransitionRegister = false
        }
    }
    
    func createFormFieldsUI() {
        let textColorInput: UIColor = newHexBlack2.Color
        let textColor: UIColor = hexDarkGray
        let labelFont: UIFont =   UIFont.Theme.size15
        var subLabelFont: UIFont =  UIFont.Theme.size13


//        if isIPhoneX == true {
//            subLabelFont =  UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.light)
//        }

        helloText.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + (verticalSpacing * 2.4), width: self.view.frame.width, height: labelHeight)
        helloText.backgroundColor = .clear
        helloText.isUserInteractionEnabled = false
        helloText.titleLabel?.font = UIFont.Theme.size17
        helloText.setTitleColor(textColor, for: .normal)
        helloText.setBackgroundColor(color: .clear, forState: .normal)
        helloText.setTitle("Hello,", for: .normal)
        helloText.contentVerticalAlignment = .bottom
        helloText.contentHorizontalAlignment = .left
        helloText.layer.zPosition = 1
        view.addSubview(helloText)
        
        nameText.frame = CGRect(x: globalXAxisIndent, y: helloText.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        nameText.backgroundColor = .clear
        nameText.isUserInteractionEnabled = false
        nameText.titleLabel?.font = labelFont
        nameText.setTitleColor(newHexBlack2.Color, for: .normal)
        nameText.setBackgroundColor(color: .clear, forState: .normal)
        nameText.setTitle("What's your name?", for: .normal)
        nameText.contentVerticalAlignment = .bottom
        nameText.contentHorizontalAlignment = .left
        nameText.layer.zPosition = 1
        view.addSubview(nameText)
        
        nameInput.frame = CGRect(x: globalXAxisIndent, y: nameText.frame.maxY + 2.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        nameInput.keyboardType = .default
        nameInput.textAlignment = .left
        nameInput.backgroundColor = .clear
        nameInput.autocorrectionType = .no
        nameInput.isSecureTextEntry = false
        nameInput.setBottomBorderRed()
        nameInput.contentVerticalAlignment = .bottom
        nameInput.layer.zPosition = 2
        nameInput.tag = 1
        nameInput.textColor = textColorInput
        nameInput.text = "First and last name"
        nameInput.font = UIFont.Theme.size15
        nameInput.returnKeyType = UIReturnKeyType.next
        
        nameInput.setLeftPaddingPoints(globalXAxisIndent + 5)
        self.view.addSubview(nameInput)
        
        nameImage.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        nameImage.center.y = nameInput.frame.maxY - 10
        nameImage.layer.zPosition = 3
        nameImage.tag = 5
        nameImage.setImage(UIImage(named: "signupForm_name_icon"), for: .normal)
        nameImage.backgroundColor = .clear
        nameImage.isUserInteractionEnabled = false
        self.view.addSubview(nameImage)
        
        addressText.frame = CGRect(x: globalXAxisIndent, y: nameInput.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        addressText.backgroundColor = .clear
        addressText.isUserInteractionEnabled = false
        addressText.titleLabel?.font = labelFont
        addressText.setTitleColor(newHexBlack2.Color, for: .normal)
        addressText.setBackgroundColor(color: .clear, forState: .normal)
        addressText.setTitle("Tell us where you work", for: .normal)
        addressText.contentVerticalAlignment = .bottom
        addressText.contentHorizontalAlignment = .left
        addressText.layer.zPosition = 1
        view.addSubview(addressText)
        
        addressSubText.frame = CGRect(x: globalXAxisIndent - 2.5, y: addressText.frame.maxY + 2.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: 75)
        addressSubText.isUserInteractionEnabled = false
        addressSubText.isSelectable = false
        addressSubText.text = "Your office address tells us where to deliver your lunch. You can always change this under Profile"
        addressSubText.font = subLabelFont
        addressSubText.textColor = newHexGray.Color
        addressSubText.textAlignment = .left
        addressSubText.sizeToFit()
        view.addSubview(addressSubText)
        
        addressInput.frame = CGRect(x: globalXAxisIndent, y: addressSubText.frame.maxY + 5, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        addressInput.keyboardType = .default
        addressInput.textAlignment = .left
        addressInput.backgroundColor = .clear
        addressInput.autocorrectionType = .no
        addressInput.isSecureTextEntry = false
        addressInput.setBottomBorderRed()
        addressInput.text = "Company name and address"
        addressInput.contentVerticalAlignment = .bottom
        addressInput.layer.zPosition = 2
        addressInput.tag = 2
        addressInput.textColor = textColorInput
        addressInput.returnKeyType = UIReturnKeyType.next
        addressInput.font = nameInput.font
        addressInput.setLeftPaddingPoints(globalXAxisIndent + 5)
        
        addressImage.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        addressImage.center.y = addressInput.frame.maxY - 10
        addressImage.layer.zPosition = 3
        addressImage.backgroundColor = .clear
        addressImage.isUserInteractionEnabled = false
        addressImage.tag = 6
        addressImage.setImage(UIImage(named: "SignupForm_search_icon"), for: .normal)
        self.view.addSubview(addressImage)
        
        self.view.addSubview(addressInput)
        
        phoneText.frame = CGRect(x: globalXAxisIndent, y: addressInput.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        phoneText.backgroundColor = .clear
        phoneText.isUserInteractionEnabled = false
        phoneText.titleLabel?.font = UIFont.Theme.size15
        phoneText.setTitleColor(newHexBlack2.Color, for: .normal)
        phoneText.setBackgroundColor(color: .clear, forState: .normal)
        phoneText.setTitle("And your phone number", for: .normal)
        phoneText.contentVerticalAlignment = .bottom
        phoneText.contentHorizontalAlignment = .left
        phoneText.layer.zPosition = 1
        view.addSubview(phoneText)
        
        phoneSubText.frame = CGRect(x: globalXAxisIndent - 2.5, y: phoneText.frame.maxY + 2.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: 75)
        phoneSubText.isUserInteractionEnabled = false
        phoneSubText.isSelectable = false
        phoneSubText.text = "So we can contact you in case of any delivery issues"
        phoneSubText.font = subLabelFont
        phoneSubText.textColor = newHexGray.Color

        phoneSubText.textAlignment = .left
        phoneSubText.sizeToFit()
        view.addSubview(phoneSubText)

        phoneInputPre.frame = CGRect(x: globalXAxisIndent, y: phoneSubText.frame.maxY + 5, width: 65, height: labelHeight + 10)
        phoneInputPre.keyboardType = .numbersAndPunctuation
        phoneInputPre.textAlignment = .left
        phoneInputPre.textColor = textColorInput
        phoneInputPre.text = "+233" // need to validate on geolocate before setting this
        phoneInputPre.contentVerticalAlignment = .bottom
        phoneInputPre.backgroundColor = .clear
        phoneInputPre.autocorrectionType = .no
        phoneInputPre.setBottomBorderRed()
        phoneInputPre.layer.zPosition = 2
        phoneInputPre.tag = 4
        phoneInputPre.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        phoneInputPre.isSecureTextEntry = false
        phoneInputPre.returnKeyType = UIReturnKeyType.next
        phoneInputPre.setLeftPaddingPoints(globalXAxisIndent + 5)
        phoneInputPre.contentVerticalAlignment = .bottom

        phoneInputPre.font = nameInput.font
        self.view.addSubview(phoneInputPre)

        phoneInput.frame = CGRect(x: phoneInputPre.frame.maxX + 10, y: phoneSubText.frame.maxY + 5, width: self.view.frame.width - (globalXAxisIndent + phoneInputPre.frame.maxX + 10), height: labelHeight + 10)
        phoneInput.keyboardType = .numbersAndPunctuation
        phoneInput.textAlignment = .left
        phoneInput.backgroundColor = .clear
        phoneInput.autocorrectionType = .no
        phoneInput.isSecureTextEntry = false
        phoneInput.contentVerticalAlignment = .bottom
        phoneInput.layer.zPosition = 2
        phoneInput.setBottomBorderRed()
        phoneInput.text = "Phone number"
        phoneInput.tag = 3
        phoneInput.textColor = textColorInput
        phoneInput.returnKeyType = UIReturnKeyType.done

        phoneImage.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        phoneImage.center.y = phoneInput.frame.maxY - 10
        phoneImage.layer.zPosition = 3
        phoneImage.backgroundColor = .clear
        phoneImage.isUserInteractionEnabled = false
        phoneImage.tag = 7
        phoneImage.setImage(UIImage(named: "signupForm_phone_icon"), for: .normal)
        self.view.addSubview(phoneImage)
        
        phoneInput.font = nameInput.font
        self.view.addSubview(phoneInput)
        
        continueButton = createFooterButton(text: "Continue", v: self.view)
        continueButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        continueButton.isHidden = true //hide until fields are valid
        
        toggleViewForFaceBookRegister()
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //continue button pressed
            nameInput.resignFirstResponder()
            addressInput.resignFirstResponder()
            phoneInput.resignFirstResponder()
            
            if checkForDefaultValues() {
                saveNewUserInformation()
                
                if sourceView == .registerWC {
                    nameInput.resignFirstResponder()
                    addressInput.resignFirstResponder()
                    phoneInput.resignFirstResponder()
                    
                    segue(name: "RegisterPasswordVCSegueFromRegister", v: self, type: .zoom)
                } else if sourceView == .registerFB {
                    nameInput.resignFirstResponder()
                    addressInput.resignFirstResponder()
                    phoneInput.resignFirstResponder()
                    tryFBRegister(1234, vc: self, completion: tryFBRegisterHandler)                    
                }
            } else {
            }
        }
    }
    
    func toggleViewForFaceBookRegister() { //this changes the page to remove fields not required for facebook
        
        if sourceView == .registerFB {
            helloText.setTitle("Hello,", for: .normal) //update for user name
            
            nameText.isHidden = true
            nameImage.isHidden = true
            nameInput.isHidden = true
            addressText.frame = CGRect(x: globalXAxisIndent, y: helloText.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
            addressSubText.frame = CGRect(x: globalXAxisIndent - 2.5, y: addressText.frame.maxY + 2.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: 35)
            addressInput.frame = CGRect(x: globalXAxisIndent, y: addressSubText.frame.maxY + 5, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
            addressImage.center.y = addressInput.frame.maxY - 10
            phoneText.frame = CGRect(x: globalXAxisIndent, y: addressInput.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
            phoneSubText.frame = CGRect(x: globalXAxisIndent - 2.5, y: phoneText.frame.maxY + 2.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: 35)
            phoneInputPre.frame = CGRect(x: globalXAxisIndent, y: phoneSubText.frame.maxY + 5, width: 65, height: labelHeight + 10)
            phoneInput.frame = CGRect(x: phoneInputPre.frame.maxX + 10, y: phoneSubText.frame.maxY + 5, width: self.view.frame.width - (globalXAxisIndent + phoneInputPre.frame.maxX + 10), height: labelHeight + 10)
            phoneImage.center.y = phoneInput.frame.maxY - 10
            
        } else {
            nameText.isHidden = false
            nameImage.isHidden = false
            nameInput.isHidden = false
        }
    }
    
    func saveNewUserInformation() {
        print("IN saveNewUserInformation | start")
        
        if sourceView == .registerWC {
            if nameInput.text != "" {
                
                guard let firstName = nameInput.text?.split(separator: " ")[0] else {
                    return
                }
                userData?.FirstName = String(firstName)
                
                if let range = nameInput.text!.range(of: " ") { //get everything after space if there is a space
                    userData?.LastName = String(nameInput.text![range.upperBound...])
                    
                    if let uData = userData?.LastName {
                        print(uData)
                    }
                }
            }
        }
        
        if addressInput.text != "" {
            //address check?
            userData?.Address = addressInput.text!
        }
        
        if phoneInput.text != "" {
            userData?.Phone = ("\(phoneInputPre.text!)\(phoneInput.text!)").keepOnlyNumbers
        }
        userData?.LocaleCode = userCurrentLocaleCode
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        nameInput.resignFirstResponder()
        addressInput.resignFirstResponder()
        phoneInput.resignFirstResponder()
        
    }

    //optional func textFieldShouldClear(_ textField: UITextField) -> Bool

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let text = textField.text else { return true }
        let range2: UITextRange? = textField.selectedTextRange

        let newLength: Int = textField.text!.count + string.count - range.length
        //        let numberOnly = NSCharacterSet.init(charactersIn: "+0123456789 ").inverted
        let lettersOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
        
        if textField == nameInput {
            let strValid1 = string.rangeOfCharacter(from: lettersOnly) == nil
            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
            if newLength < 50 && strValid1 == true {
                textField.text = newString.capitalized
                return false
            }
        } else  if textField == addressInput {
            return true
        } else if textField == phoneInputPre {
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        } else if textField == phoneInput {
            guard NSCharacterSet(charactersIn: "0123456789+ ()").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            formatPhoneNumber(location: range2!)
            return true
        }

//        else if textField == phoneInput {
//            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
//            let count = phInputPre.text?.count
//            textField.text = PartialFormatter().formatPartial("\(phInputPre.text ?? "")\(newString)")
//            return false
//        } else if textField == phInputPre {
//            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
//            textField.text = PartialFormatter().formatPartial(newString)
//            return false
//        }
        return false
    }

    //format and validate inputs
    func formatPhoneNumber(location: UITextRange) { //format full phone number
        print("in formatPhoneNumber | start")

        var fullPhoneString = ("\(phoneInputPre.text!)\(phoneInput.text!)").keepOnlyNumbers
        fullPhoneString = "+\(fullPhoneString)"
        let phoneNumberFormatted = PartialFormatter().formatPartial(fullPhoneString)
        phoneTest.text = phoneNumberFormatted

        if phoneNumberFormatted.contains(" ") {
            let prefixCount = phoneNumberFormatted.components(separatedBy: " ")[0].count
            phoneInputPre.text = phoneNumberFormatted.components(separatedBy: " ")[0]
            phoneInput.text = String(phoneNumberFormatted.dropFirst(prefixCount))
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("in textFieldDidBeginEditing")
        
        activeTextField = textField
        
        if textField == nameInput {
            if textField.text == "First and last name" {
                textField.text = ""
            }
        }
        
        if textField == addressInput {
            if textField.text == "Company name and address" {
                textField.text = ""
            }
        }
        
        if textField == phoneInput {
            if textField.text == "Phone number" {
                textField.text = ""
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")
        let range: UITextRange? = textField.selectedTextRange
        if textField == phoneInputPre {
            if textField.text == "+" || textField.text == "" {
                textField.text = "+233"
            }
            if textField.text?.range(of: "+") == nil {
                let value: String = textField.text!
                textField.text = "+\(value)"
            }
        }
        formatPhoneNumber(location: range!)

        activeTextField = nil
        validateAllFormFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        let range: UITextRange? = textField.selectedTextRange
        formatPhoneNumber(location: range!)

        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            if nextTag != 4 {
                nextResponder.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        } else {
            textField.resignFirstResponder()
        }
        
        if checkForDefaultValues() {
            continueButton.isHidden = false
//            if textField == phoneInput {
//                //continueButton.sendActions(for: .touchUpInside)
//            }
        } else {
            continueButton.isHidden = true
        }
        
        return false
    }
    
    func validateAllFormFields() {
        
        if checkForDefaultValues() {
            continueButton.isHidden = false
        } else {
            continueButton.isHidden = true
        }
    }
    
    func checkForDefaultValues() -> Bool {
        print("in checkForDefaultValues | start")
        phoneTest.text = phoneInput.text

        if sourceView == .registerFB {
            if addressInput.text != "Company name and address" && addressInput.text != "" {
                if phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInput.text != "+" {
                    //&& phoneTest.isValidNumber != false {
                    return true
                }
            }
        } else {
            if nameInput.text != "First and last name" && nameInput.text != "" && (nameInput.text?.contains(" "))! {
                if (nameInput.text?.split(separator: " ").count)! > 1 { //make sures there is a last name
                    if addressInput.text != "Company name and address" && addressInput.text != "" {
                        if phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInput.text != "+" {
                            //&& phoneTest.isValidNumber != false {
                            return true
                        }
                    }
                }
            }
        }
        
        if sourceView == .registerWC && nameInput.text != "First and last name" && nameInput.text != "" && addressInput.text != "Company name and address" && addressInput.text != "" && phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInput.text != "+" && !(nameInput.text?.contains(" "))! { //logic to provide user feedback on incorrect fields
            animateIncorrectInputs(field: "name")
            return false
        }
        
        if sourceView == .registerWC && (nameInput.text?.split(separator: " ").count)! < 2 { //make sures there is a last name
            animateIncorrectInputs(field: "name")
            return false
        }
        
        //        let testPhoneResult: String = PartialFormatter().formatPartial(phoneInput.text!)
        if addressInput.text != "Company name and address" && addressInput.text != "" && phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInput.text != "+" {
            //&& phoneTest.isValidNumber != true { //logic to check phone number is correct
//            animateIncorrectInputs(field: "phone")
        }
        
        return false
    }
    
    func animateIncorrectInputs(field: String) {
        print("in animateIncorrectInputs | field: ", field)
        if field == "all" {
            animateIncorrectInput(buttonID: 1, localView: self.view)
            animateIncorrectInput(buttonID: 2, localView: self.view)
            animateIncorrectInput(buttonID: 3, localView: self.view)
            animateIncorrectButton(buttonID: 4, localView: self.view)
            animateIncorrectButton(buttonID: 5, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            animateIncorrectButton(buttonID: 7, localView: self.view)
            animateUIBanner("Invalid Input", subText: "Please try again", color: UIWarningErrorColor, removePending: true)
        } else if field == "name" {
            animateIncorrectInput(buttonID: 1, localView: self.view)
            animateIncorrectButton(buttonID: 5, localView: self.view)
            animateUIBanner("Invalid Input", subText: "Incorrect first or  last name", color: UIWarningErrorColor, removePending: false)
        } else if field == "address" {
            animateIncorrectInput(buttonID: 2, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            animateUIBanner("Invalid Input", subText: "Incorrect address", color: UIWarningErrorColor, removePending: true)
        } else if field == "phone" {
            animateIncorrectInput(buttonID: 3, localView: self.view)
            animateIncorrectInput(buttonID: 4, localView: self.view)
            animateIncorrectButton(buttonID: 7, localView: self.view)
            animateUIBanner("Invalid Input", subText: "Incorrect phone number", color: UIWarningErrorColor, removePending: true)
        }
    }
}
