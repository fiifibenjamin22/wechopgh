//
//  OrdersVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/12/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

//
//  InviteVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/12/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import EzPopup

class OrdersVC: UIViewController, UIScrollViewDelegate {
    
    let upcomingScrollView: UIScrollView = UIScrollView()
    let upcomingTableView: UITableView = UITableView()
    let historyScrollView: UIScrollView = UIScrollView()
    let historyTableView: UITableView = UITableView()
    let selectedOrder = ""
    var navigationLeft: SpringButton = SpringButton() 
    var navigationTitleBorder: SpringButton = SpringButton()
    var doneButton: SpringButton = SpringButton()

    var noOrdersTextView: UITextView = UITextView()
    var orderCount = 0 // count of upcoming orders
    let upcomingButton: UIButton = UIButton()
    let historyButton: UIButton = UIButton()
    let historyAndUpcomingButtonBorder: UIButton = UIButton()
    
    var localHistoryOrders: [OrderData] = [OrderData]()
    var localUpcomingOrders: [OrderData] = [OrderData]()


    
    //cancel order UI
//    var tap1: UITapGestureRecognizer = UITapGestureRecognizer()
//    let backgroundDark: UIView = UIView()
//    let whiteBox: SpringButton = SpringButton()
//    let cartText: UITextView = UITextView()
//    let text: UITextView = UITextView()
//    var cancelButton: SpringButton = SpringButton()
//    var yesButton: SpringButton = SpringButton()

    var cancelOrderID: String = String()

    //for rating section tapped
    var orderTapped: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        print("in viewWillAppear ORdersVC | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

        super.viewWillAppear(true)
        self.historyTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.upcomingTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if comingFrom.ordersUpcomingTableView == true {
            print("in viewWillAppear ORdersVC | to upcoming")
            toggleOrdersView(to: "upcoming")
        } else {
            print("in viewWillAppear ORdersVC | to history")
            toggleOrdersView(to: "history")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN OrdersVC | ViewDidLoad")
        //        //writeToFireBase("in OrdersVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .left)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white


        historyTableView.register(UINib(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: "OrderHistoryCell")
        historyTableView.register(UINib(nibName: "OrderHistoryHeaderCell", bundle: nil), forCellReuseIdentifier: "OrderHistoryHeaderCell")
        upcomingTableView.register(UINib(nibName: "OrderUpcomingCell", bundle: nil), forCellReuseIdentifier: "OrderUpcomingCell")
        upcomingTableView.register(UINib(nibName: "OrderUpcomingHeaderCell", bundle: nil), forCellReuseIdentifier: "OrderUpcomingHeaderCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOrderData), name: NSNotification.Name(rawValue: "refreshOrderData"), object: nil) 
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOrders), name: NSNotification.Name(rawValue: "reloadOrders"), object: nil) 
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeCancelledOrder), name: NSNotification.Name(rawValue: "removeCancelledOrder"), object: nil)
        
        localHistoryOrders = completedOrders
        localUpcomingOrders = upcomingOrders

//        // TODO: Remove this after testing
//
//        if localHistoryOrders.isEmpty == true {
//            localHistoryOrders.append(emptyOrder)
//            localHistoryOrders.append(emptyOrder)
//            localHistoryOrders.append(emptyOrder)
//
//        }


        createNavigationAndScrollViews()
        //createCancelOrderDialogue()
        configureTableView()
        noOrdersTextUpdate()
    }
    
    func configureTableView() {
        print("IN configureTableView | start")
        //setup tableview
        
        //restaurant 1
        self.historyTableView.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: (275 * localHistoryOrders.count) + 120)
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        self.historyTableView.rowHeight = 120
        self.historyTableView.sectionHeaderHeight = 175
        self.historyTableView.alwaysBounceVertical = true // lock table
        self.historyTableView.isScrollEnabled = false
        self.historyTableView.backgroundColor = .clear
        self.historyTableView.center.x = view.center.x
        self.historyTableView.showsVerticalScrollIndicator = false
        self.historyTableView.layer.zPosition = 3
        self.historyTableView.layoutMargins = UIEdgeInsets.zero
        self.historyTableView.separatorInset = UIEdgeInsets.zero
        self.historyScrollView.addSubview(historyTableView)
        
        self.upcomingTableView.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: (200 * localUpcomingOrders.count) + 70)
        self.upcomingTableView.delegate = self
        self.upcomingTableView.dataSource = self
        self.upcomingTableView.rowHeight = 70
        self.upcomingTableView.sectionHeaderHeight = 150
        self.upcomingTableView.alwaysBounceVertical = true // lock table
        self.upcomingTableView.isScrollEnabled = false
        self.upcomingTableView.backgroundColor = .clear
        self.upcomingTableView.center.x = view.center.x
        self.upcomingTableView.showsVerticalScrollIndicator = false
        self.upcomingTableView.layer.zPosition = 3
        //        self.upcomingTableView.layoutMargins = UIEdgeInsets.zero
        self.upcomingTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        //        self.upcomingTableView.separatorInset = UIEdgeInsets.zero
        //        self.upcomingTableView.separatorColor = UIColor.black.withAlphaComponent(0.3)
        self.upcomingScrollView.addSubview(upcomingTableView)
    }
    
    func createNavigationAndScrollViews() {
        print(" IN createNavigationAndScrollViews | start")
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Orders", v: self.view, leftButton: navigationLeft)
        
        upcomingButton.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width / 2, height: globalLargeButtonHeight)
        upcomingButton.tag = 30
        upcomingButton.setTitle("UPCOMING", for: .normal)
        upcomingButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        upcomingButton.setTitleColor(newHexBlack1.Color, for: .normal)

        upcomingButton.titleLabel?.font = UIFont.Theme.size15Semi
        upcomingButton.backgroundColor = .clear
        upcomingButton.isUserInteractionEnabled = true
        upcomingButton.setBackgroundColor(color: .clear, forState: .normal)
        //        upcomingButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        upcomingButton.contentVerticalAlignment = .center
        upcomingButton.contentHorizontalAlignment = .center
        upcomingButton.setBottomBorder()
        self.view.addSubview(upcomingButton)
        
        historyButton.frame = CGRect(x: self.view.frame.width / 2, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width / 2, height: globalLargeButtonHeight)
        historyButton.tag = 31
        historyButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        historyButton.setTitle("HISTORY", for: .normal)
//        historyButton.setTitleColor(hexDarkGray, for: .normal)
//        historyButton.titleLabel?.font = defaultFont14

        historyButton.setTitleColor(newHexBlack1.Color, for: .normal)
        historyButton.titleLabel?.font = UIFont.Theme.size15Semi

        historyButton.backgroundColor = .clear
        historyButton.isUserInteractionEnabled = true
        historyButton.contentVerticalAlignment = .center
        historyButton.contentHorizontalAlignment = .center
        historyButton.setBottomBorder()
        self.view.addSubview(historyButton)
        
        //historyAndUpcomingButtonBorder
        historyAndUpcomingButtonBorder.frame = CGRect(x: 0, y: historyButton.frame.maxY, width: self.view.frame.width / 2, height: 1)
        historyAndUpcomingButtonBorder.backgroundColor = hexRed.Color
        historyAndUpcomingButtonBorder.isUserInteractionEnabled = false
        self.view.addSubview(historyAndUpcomingButtonBorder)
        
        //upcomingScrollView
        upcomingScrollView.frame = CGRect(x: 0, y: historyAndUpcomingButtonBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)
        upcomingScrollView.center.x = self.view.center.x
        upcomingScrollView.isUserInteractionEnabled = true
        upcomingScrollView.alwaysBounceVertical = false
        upcomingScrollView.delegate = self
        upcomingScrollView.backgroundColor = .clear
        upcomingScrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat((200 * (localUpcomingOrders.count + 1))))
        view.addSubview(upcomingScrollView)
        
        //historyScrollView
        historyScrollView.frame = CGRect(x: 0, y: historyAndUpcomingButtonBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)
        historyScrollView.center.x = self.view.center.x
        historyScrollView.isUserInteractionEnabled = true
        historyScrollView.alwaysBounceVertical = false
        historyScrollView.delegate = self
        historyScrollView.backgroundColor = .clear
        historyScrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat((275 * (localHistoryOrders.count + 1) - (275 / 2))))
        historyScrollView.isHidden = true
        view.addSubview(historyScrollView)
        
        //shown if there are no current orders
        noOrdersTextView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - (2.25 * globalXAxisIndent), height: 120 )
        noOrdersTextView.textColor = hexDarkGray.withAlphaComponent(0.6)
        noOrdersTextView.text = "You have no upcoming orders."
        noOrdersTextView.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        noOrdersTextView.backgroundColor = .clear
        noOrdersTextView.isUserInteractionEnabled = false
        noOrdersTextView.isEditable = false
        
        noOrdersTextView.isSelectable = false
        noOrdersTextView.centerVertically()
        noOrdersTextView.textAlignment = .center
        noOrdersTextView.isHidden = true
        noOrdersTextView.center = self.view.center

        doneButton = createFooterButton(text: "Refer a friend, get 5 GHS", v: self.view)
        doneButton.backgroundColor = hexGreen.Color
        doneButton.isHidden = true
        doneButton.isUserInteractionEnabled = true
        doneButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        doneButton.layer.zPosition = 100
        doneButton.tag = 14
        view.addSubview(doneButton)

        self.view.addSubview(noOrdersTextView)
        
    }

//
//    func createCancelOrderDialogue() {
//        print("in createCancelOrderDialogue | start")
//        var boxHeight = self.view.frame.height / 4.25
//        if isIPhoneX == true {
//            boxHeight = self.view.frame.height / 5
//        } else if isIPhoneSE == true {
//            boxHeight = self.view.frame.height / 3.5
//        }
//
//        backgroundDark.frame = view.frame
//        backgroundDark.layer.zPosition = 100
//        backgroundDark.backgroundColor = UIColor.black.withAlphaComponent(0.7)
//        backgroundDark.isHidden = true
//        view.addSubview(backgroundDark)
//
//        whiteBox.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - (globalXAxisIndent * 2), height: boxHeight)
//        whiteBox.isUserInteractionEnabled = true
//        whiteBox.backgroundColor = .white
//        whiteBox.center = view.center
//        whiteBox.layer.zPosition = 101
//        whiteBox.isHidden = true
//        view.addSubview(whiteBox)
//
//        cartText.frame = CGRect(x: globalXAxisIndent, y: 0, width: 200, height: 20)
//        cartText.textColor = .black
//        cartText.text = "Cancel your order"
//        cartText.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
//
//        cartText.textAlignment = .left
//        cartText.sizeToFit()
//        cartText.frame.origin.y = globalXAxisIndent - 5
//        cartText.backgroundColor  = .clear
//        cartText.isScrollEnabled = false
//        cartText.isEditable = false
//        cartText.layer.zPosition = 102
//        cartText.isUserInteractionEnabled = false
//        cartText.isSelectable = false
//        whiteBox.addSubview(cartText)
//
//        text.frame = CGRect(x: globalXAxisIndent, y: cartText.frame.maxY, width: whiteBox.frame.width - (2 * globalXAxisIndent), height: 150)
//        text.textColor = .black
//        text.text = "Would you like to cancel this order?"
//        text.backgroundColor  = .clear
//        text.font = UIFont.systemFont(ofSize: 15.5, weight: UIFont.Weight.light)
//
//        text.isScrollEnabled = false
//        text.isEditable = false
//        text.layer.zPosition = 102
//        text.isUserInteractionEnabled = false
//        text.isSelectable = false
//        text.textAlignment = .left
//        whiteBox.addSubview(text)
//
//        let buttonWidth: CGFloat = self.view.frame.width / 2 - ((globalXAxisIndent) + 5)
//        let buttonHeight: CGFloat = 50.0
//
//        yesButton = createFooterButton(text: "OKAY", v: self.view)
//        yesButton.frame = CGRect(x: whiteBox.frame.width - (globalXAxisIndent + 100), y: whiteBox.frame.height - (buttonHeight), width: 100, height: buttonHeight)
//        yesButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        yesButton.tag = 21
//        yesButton.setTitleColor(hexYellow.Color, for: .normal)
//        yesButton.setBackgroundColor(color: UIColor.white, forState: .normal)
//        yesButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold   )
//        yesButton.titleLabel?.textAlignment = .left
//        whiteBox.addSubview(yesButton)
//
//        cancelButton = createFooterButton(text: "GO BACK", v: self.view)
//        cancelButton.frame = CGRect(x: yesButton.frame.minX - ((globalXAxisIndent / 2) + 75), y: yesButton.frame.minY, width: 75, height: buttonHeight)
//        cancelButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        cancelButton.tag = 20
//        cancelButton.titleLabel?.textAlignment = .right
//        cancelButton.setTitleColor(hexYellow.Color, for: .normal)
//        cancelButton.setBackgroundColor(color: UIColor.white, forState: .normal)
//        cancelButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
//        whiteBox.addSubview(cancelButton)
//
//        if isIPhoneSE == true {
//            cartText.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
//            text.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
//
//            yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold   )
//
//            cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
//
//        }
//
//        cancelButton.layer.zPosition = 102
//        yesButton.layer.zPosition = 102
//    }

    func noOrdersTextUpdate() {
        if localUpcomingOrders.count == 0 && comingFrom.ordersUpcomingTableView == true {
            noOrdersTextView.isHidden = false
            noOrdersTextView.text = "You have no upcoming orders."
            noOrdersTextView.centerVertically()
            noOrdersTextView.center = self.view.center
            
        } else if localHistoryOrders.count == 0 && comingFrom.ordersUpcomingTableView == false {
            noOrdersTextView.isHidden = false
            
            noOrdersTextView.text = "Here's where your orders that have been delivered will show up. Order now and make we chop :)"
            noOrdersTextView.centerVertically()
            noOrdersTextView.center = self.view.center
        } else {
            noOrdersTextView.isHidden = true
        }
    }
    
    func toggleOrdersView(to: String) {
        print("IN toggleOrdersView | start")
        
        let duration = 0.25
        noOrdersTextView.isHidden = true
        
        if comingFrom.ordersUpcomingTableView == true {
            print("IN toggleOrdersView | going to upcoming")
            
            self.historyScrollView.isHidden = true
            
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.historyAndUpcomingButtonBorder.frame = CGRect(x: 0, y: self.historyButton.frame.maxY, width: self.view.frame.width / 2, height: 1)
                
            }, completion: { (value: Bool) in
                self.noOrdersTextUpdate()
                self.upcomingButton.setTitleColor(hexRed.Color, for: .normal)
                self.historyButton.setTitleColor(hexDarkGray, for: .normal)
                self.upcomingScrollView.isHidden = false
                self.doneButton.isHidden = false
            })
        } else if comingFrom.ordersUpcomingTableView == false {
            print("IN toggleOrdersView | going to history")
            self.doneButton.isHidden = true
            self.upcomingScrollView.isHidden = true
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.historyAndUpcomingButtonBorder.frame = CGRect(x: self.view.frame.width / 2, y: self.historyButton.frame.maxY, width: self.view.frame.width / 2, height: 1)
                
            }, completion: { (value: Bool) in
                self.noOrdersTextUpdate()
                self.upcomingButton.setTitleColor(hexDarkGray, for: .normal)
                self.historyButton.setTitleColor(hexRed.Color, for: .normal)
                self.historyScrollView.isHidden = false

            })
        }
    }
    
//    func toggleClearCartUI( show: Bool) {
//        if show == true {
//            tap1.isEnabled = true
//            whiteBox.isHidden = false
//            backgroundDark.isHidden = false
//
//        } else {
//            tap1.isEnabled = false
//            whiteBox.isHidden = true
//            backgroundDark.isHidden = true
//        }
//    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            if comingFrom.ordersToMenu == true {
                comingFrom.ordersToMenu = false
                print("Dismissing to menu from Orders after coming from cart")
                
                currentRootView = .menu
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
                UIApplication.shared.keyWindow?.rootViewController = viewController
                
                navigationController?.hero.navigationAnimationType = .cover(direction: .up)
                self.navigationController?.popToRootViewController(animated: true)
                
                delay(1) {
                    callNotificationItem("refreshPage")
                }
            } else {
                callNotificationItem("refreshPage")
                self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .right)
                self.hero.dismissViewController()
            }

        } else if sender.tag == 14 {
            //shouldShowShareDialogueOnLoad = true
            comingFrom.inviteShowInviteDialogue = true
            let contentVC = InviteVC()
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
            present(popupVC, animated: true)

//        else if sender.tag == 20 {
//            //toggleClearCartUI(show: false)
//
//        }  else if sender.tag == 21 {
//            //clear button pressed on clear car
//            print("cancel order pressed")
//            tryToCancelOrder(cancelOrderID, vc: self, completion: tryToCancelOrderHandler)
//
//        }

        } else if sender.tag == 30 {//go to history
            //onOrdersVCUpcomingView = true
            comingFrom.ordersUpcomingTableView = true

            toggleOrdersView(to: "history")
        } else if sender.tag == 31 {//go to upcoming
            //onOrdersVCUpcomingView = false
            comingFrom.ordersUpcomingTableView = false
            toggleOrdersView(to: "upcoming")
        }
    }
    
    @objc func buttonDetailsPressed(_ sender: UIButton) {
        print("Button Details pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag >= 1000 && sender.tag < 2000 {
            selectedOrderItem = localHistoryOrders[sender.tag - 1000]
            print("going to order history detail")
            segue(name: "OrderDetailsVCSegueFromOrders", v: self, type: .pageIn(direction: .up))
        } else if sender.tag >= 2000 && sender.tag < 200000 {
            selectedOrderItem = localUpcomingOrders[sender.tag - 2000]
            print("going to order upcoming detail")
            segue(name: "OrderDetailsVCSegueFromOrders", v: self, type: .pageIn(direction: .up))
        } else if sender.tag >= 200000 {
            cancelOrderID = localUpcomingOrders[sender.tag - 200000].OrderID
            cancelledOrderIndexSet = IndexSet(integer: sender.tag - 200000)
            //toggleClearCartUI(show: true)

            ///cancel order dialogue

            let alert = UIAlertController(title: "Cancel your order", message: "Would you like to cancel this order?", preferredStyle: UIAlertControllerStyle.alert)

            UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .extraLight)

            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.cancel, handler: { action in

            }))

            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.destructive, handler: { action in
                print("cancel order pressed")
                tryToCancelOrder(self.cancelOrderID, vc: self, completion: tryToCancelOrderHandler)
            }))
            self.present(alert, animated: true) {

            }
        }
    }
    
    @objc func star1Tap(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        //        print("Provide feedback for order sender.tag", sender.tag)
        userRatingSelectedOnOrderPage = 1
        selectedOrderItem = localHistoryOrders[sender.view!.tag - 1000]
        presentRatingPage()
    }
    @objc func star2Tap(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        //        print("Provide feedback for order sender.tag", sender.tag)
        userRatingSelectedOnOrderPage = 2
        selectedOrderItem = localHistoryOrders[sender.view!.tag - 1000]
        presentRatingPage()
    }
    @objc func star3Tap(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        //        print("Provide feedback for order sender.tag", sender.tag)
        userRatingSelectedOnOrderPage = 3
        selectedOrderItem = localHistoryOrders[sender.view!.tag - 1000]
        presentRatingPage()
    }
    @objc func star4Tap(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        //        print("Provide feedback for order sender.tag", sender.tag)
        userRatingSelectedOnOrderPage = 4
        selectedOrderItem = localHistoryOrders[sender.view!.tag - 1000]
        presentRatingPage()
    }
    @objc func star5Tap(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        //        print("Provide feedback for order sender.tag", sender.tag)
        userRatingSelectedOnOrderPage = 5
        selectedOrderItem = localHistoryOrders[sender.view!.tag - 1000]
        presentRatingPage()
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("Provide feedback for order ", sender.view!.tag)
        
        //        if comingFrom.ordersUpcomingTableView == false {
        //            userRatingSelectedOnOrderPage = sender.view!.tag
        //
        //            // init YourViewController
        //            let contentVC = OrderRatingVC()
        //            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
        //            present(popupVC, animated: true)
        //        }
    }
    
    func presentRatingPage() {
        let contentVC = OrderRatingVC()
        let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
        present(popupVC, animated: true)
    }
    
    @objc func refreshOrderData() {
        print("in refreshOrderData | start")
        getUserUpcomingAndCompletedOrders(self)// called when an order rating is updated
    }
    
    @objc func removeCancelledOrder() {
        print("in removeCancelledOrder | start")
        
        if cancelledOrderID != "" {
            print("in removeCancelledOrder | in if cancelledOrderID not blank")
//
//            if let index = fullOrderList.index(where: {$0.OrderID == cancelledOrderID}) {
//                fullOrderList.remove(at: index)
//            }
//
//            if let index = upcomingOrders.index(where: {$0.OrderID == cancelledOrderID}) {
//                upcomingOrders.remove(at: index)
//            }
//            localHistoryOrders = completedOrders
//            localUpcomingOrders = upcomingOrders
//            //refresh from table
//
//            print("in removeCancelledOrder | reloading tableview")
//            self.upcomingTableView.reloadData()
//
//            //reload order data on backend
            refreshOrderData()
        }
        //toggleClearCartUI(show: false)
        cancelledOrderID = "" //clear cancelOrderID
        callNotificationItem("checkToggleUpcomingOrderCount") //not sure if needed
    }
    
    @objc func tapFunctionRemoveOrderUI(sender: UITapGestureRecognizer) {
        print("background pressed")
        //toggleClearCartUI(show: false)
    }

    @objc func reloadOrders() {
//        print("in reloadOrders | start")

        if let spinner =  self.view.viewWithTag(1) as? NVActivityIndicatorView {
            if spinner.isAnimating == false {
                spinner.startAnimating()
            }
        } else {
            createGlobalLoadingSpinner(self.view)
        }

        localHistoryOrders = completedOrders
        localUpcomingOrders = upcomingOrders

        noOrdersTextUpdate()
        self.historyTableView.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: (275 * localHistoryOrders.count) + 120)
        self.upcomingTableView.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: (200 * localUpcomingOrders.count) + 70)
        upcomingScrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat((200 * (localUpcomingOrders.count + 1))))
        self.historyScrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat((275 * (localHistoryOrders.count + 1) - (275 / 2))))

        self.historyTableView.reloadData()
        self.upcomingTableView.reloadData()

        globalLoadingSpinner.stopAnimating()

//        delay(0.2) {
//            globalLoadingSpinner.stopAnimating()
//        }
    }
}

//tableviews
extension OrdersVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.historyTableView { // get cell count for data
            return localHistoryOrders.count
        } else {
            return localUpcomingOrders.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 //section sets cell count not row
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.historyTableView {
            return 175
        } else {
            return 150
        }
    }
    
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //        print("in viewForHeaderInSection | start")
        
        if tableView == self.historyTableView { 
            //            print("in viewForHeaderInSection | HISTORY")
            
            let headerView = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryHeaderCell") as! OrderHistoryHeaderCell
            headerView.headerRestaurantTitle.text = localHistoryOrders[section].RestaurantName
            headerView.headerRestaurantTitle2.text = localHistoryOrders[section].RestaurantName
            headerView.tag = 1000 + Int(section)
            
            if localHistoryOrders[section].Rating == 0 {
                headerView.headerRestaurantTitle2.isHidden = true
                headerView.isUserInteractionEnabled = true
                let tap1 = UITapGestureRecognizer(target: self, action: #selector(star1Tap))
                let tap2 = UITapGestureRecognizer(target: self, action: #selector(star2Tap))
                let tap3 = UITapGestureRecognizer(target: self, action: #selector(star3Tap))
                let tap4 = UITapGestureRecognizer(target: self, action: #selector(star4Tap))
                let tap5 = UITapGestureRecognizer(target: self, action: #selector(star5Tap))
                
                headerView.star1.tag = 1000 + Int(section)
                headerView.star2.tag = 1000 + Int(section)
                headerView.star3.tag = 1000 + Int(section)
                headerView.star4.tag = 1000 + Int(section)
                headerView.star5.tag = 1000 + Int(section)
                
                headerView.star1.addGestureRecognizer(tap1)
                headerView.star2.addGestureRecognizer(tap2)
                headerView.star3.addGestureRecognizer(tap3)
                headerView.star4.addGestureRecognizer(tap4)
                headerView.star5.addGestureRecognizer(tap5)
                headerView.selectionStyle = .none
            } else {
                headerView.isUserInteractionEnabled = false
                headerView.headerRestaurantTitle.isHidden = true
                headerView.headerExperience.isHidden = true
                headerView.star1.isHidden = true
                headerView.star2.isHidden = true
                headerView.star3.isHidden = true
                headerView.star4.isHidden = true
                headerView.star5.isHidden = true
            }
            return headerView
            
        } else {
            //            print("in viewForHeaderInSection | UPCOMING")
            
            let headerView = tableView.dequeueReusableCell(withIdentifier: "OrderUpcomingHeaderCell") as! OrderUpcomingHeaderCell
            headerView.backgroundColor = .white
            
            let formattedDate = localUpcomingOrders[section].Date.toDate()
            if formattedDate != nil {
                let text = "\(formattedDate!.toFormat("dd MMM")) '\(formattedDate!.toFormat("yy"))"
                
                //                let text = "\(formattedDate!.toFormat("dd MMM")) '\(formattedDate!.toFormat("yy"))"
                headerView.orderDate.text = text.uppercased()
            }
            headerView.orderNumberText.textColor = hexRed.Color
            headerView.orderStatusText.textColor = hexRed.Color
            headerView.orderRestaurantName.text = localUpcomingOrders[section].RestaurantName
            headerView.orderNumberLabel.text = "Order number: "
            headerView.orderNumberText.text = localUpcomingOrders[section].OrderID
            headerView.orderDeliverTo.text = "Deliver to: \(localUpcomingOrders[section].Address)"
            headerView.orderCost.text = "Total: GHS \(formatPrice(p: localUpcomingOrders[section].Amount))"
            headerView.orderStatusText.text = localUpcomingOrders[section].Status
            headerView.orderStatusLabel.text = "Status: "
            headerView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            tap.numberOfTapsRequired = 1
            headerView.addGestureRecognizer(tap)
            headerView.selectionStyle = .none
            return headerView
        }
    }
    
    func formatHeaderStars(header: OrderHistoryHeaderCell, stars: Int) -> OrderHistoryHeaderCell {
        
        switch stars {
        case 1:
            header.star1.image = UIImage(named: "star_pinkFilled")
        case 2:
            header.star1.image = UIImage(named: "star_pinkFilled")
            header.star2.image = UIImage(named: "star_pinkFilled")
        case 3:
            header.star1.image = UIImage(named: "star_pinkFilled")
            header.star2.image = UIImage(named: "star_pinkFilled")
            header.star3.image = UIImage(named: "star_pinkFilled")
        case 4:
            header.star1.image = UIImage(named: "star_pinkFilled")
            header.star2.image = UIImage(named: "star_pinkFilled")
            header.star3.image = UIImage(named: "star_pinkFilled")
            header.star4.image = UIImage(named: "star_pinkFilled")
        case 5:
            header.star1.image = UIImage(named: "star_pinkFilled")
            header.star2.image = UIImage(named: "star_pinkFilled")
            header.star3.image = UIImage(named: "star_pinkFilled")
            header.star4.image = UIImage(named: "star_pinkFilled")
            header.star5.image = UIImage(named: "star_pinkFilled")
        default:
            print("in default check for stars")
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.historyTableView { // get cell count for restaurant 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as! OrderHistoryCell
            cell.orderNumber.text = "Order #\(localHistoryOrders[indexPath.section].OrderID)"
            
            let formattedDate = localHistoryOrders[indexPath.section].Date.toDate()
            if formattedDate != nil {
                let text = "\(formattedDate!.toFormat("dd MMM")) '\(formattedDate!.toFormat("yy"))"
                cell.orderDate.text = text.uppercased()
            }
            
            cell.orderAddress.text = localHistoryOrders[indexPath.section].Address
            cell.orderTotalAmount.text = "GHS \(formatPrice(p: localHistoryOrders[indexPath.section].Amount))"
            cell.orderDetailsButton.layer.borderColor = hexDarkGray.cgColor
            cell.orderDetailsButton.layer.borderWidth = 1
            cell.selectionStyle = .none
            cell.orderDetailsButton.tag = 1000 + Int(indexPath.section)
            cell.orderDetailsButton.addTarget(self, action: #selector(buttonDetailsPressed), for: .touchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderUpcomingCell") as! OrderUpcomingCell
            
//            cell.backgroundColor = .clear
//            cell.orderViewDetailsButton.frame = CGRect(x: cell.orderViewDetailsButton.frame.minX, y: cell.orderViewDetailsButton.frame.minY, width: (self.view.frame.width / 2), height: globalLargeButtonHeight)
//            cell.orderViewDetailsButton.isUserInteractionEnabled = true
            cell.orderViewDetailsButton.tag = 2000 + Int(indexPath.section)
            cell.orderViewDetailsButton.addTarget(self, action: #selector(buttonDetailsPressed), for: .touchUpInside)
            cell.orderViewDetailsButton.layer.borderWidth = 1
            cell.orderViewDetailsButton.layer.borderColor = hexDarkGray.cgColor
//
//            cell.orderCancelButton.frame = CGRect(x: cell.orderCancelButton.frame.minX, y: cell.orderCancelButton.frame.minY, width: cell.orderViewDetailsButton.frame.width, height: globalLargeButtonHeight)
//
            cell.orderCancelButton.tag = 200000 + Int(indexPath.section)
            cell.orderCancelButton.layer.borderWidth = 1
            
            //logic to set cancel button to active:
            let status = localUpcomingOrders[indexPath.section].Status
            if status == "Scheduled" || status == "Awaiting Restaurant confirmation" {
                cell.orderCancelButton.addTarget(self, action: #selector(buttonDetailsPressed), for: .touchUpInside)
                cell.orderCancelButton.isUserInteractionEnabled = true
                cell.orderCancelButton.layer.borderColor = hexDarkGray.cgColor
            } else {
                cell.orderCancelButton.isUserInteractionEnabled = false
                cell.orderCancelButton.setTitleColor(hexGray.withAlphaComponent(0.6), for: .normal)
                cell.orderCancelButton.layer.borderColor = hexLightGray.cgColor
            }
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    private func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("IN didSelectRowAt | start")
        
        if tableView == self.historyTableView {
            print("IN didSelectRowAt | history")
            
            if let item = tableView.cellForRow(at: indexPath) as? OrderHistoryHeaderCell {
                print("IN didSelectRowAt | history Header!: ", item)
                
            }
        }
    }
}
