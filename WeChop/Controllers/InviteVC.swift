//
//  InviteVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/12/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import StoreKit
import SwiftDate

class InviteVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    var navigationTitleBorder = SpringButton()
    var orderButton: SpringButton = SpringButton()
    let shareCodeButton: SpringButton = SpringButton()
    var shareText: SpringTextView = SpringTextView()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN InviteVC | ViewDidLoad")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .up)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Free Food", v: self.view, leftButton: navigationLeft)
        
        orderButton = createFooterButton(text: "SHARE LINK", v: self.view)
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.tag = 14
        orderButton.backgroundColor = newHexBlack2.Color
        createMainContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

        print("in InviteVC willappear | start | shouldShowShareDialogueOnLoad: ", comingFrom.inviteShowInviteDialogue)

        if comingFrom.inviteShowInviteDialogue == true {
            //shouldShowShareDialogueOnLoad = false
            comingFrom.inviteShowInviteDialogue = false
            showShareDialogue(vc: self, button: orderButton)
        }
    }

    func createMainContent() {
        shareCodeButton.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: globalLargeButtonHeight)
        shareCodeButton.tag = 13
        shareCodeButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        shareCodeButton.backgroundColor = .clear
        shareCodeButton.isUserInteractionEnabled = true
        shareCodeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        shareCodeButton.setTitleColor(hexRed.Color, for: .normal)
        shareCodeButton.setBackgroundColor(color: .clear, forState: .normal)
        shareCodeButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        shareCodeButton.setTitle(userShareCodeUrl, for: .normal)
        
        shareCodeButton.contentVerticalAlignment = .center
        shareCodeButton.contentHorizontalAlignment = .left
        shareCodeButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent + 6, bottom: 0, right: 0)
        
        let lineView = UIView(frame: CGRect(x: 0, y: shareCodeButton.frame.height, width: view.frame.width, height: 1))
        lineView.backgroundColor = hexLightGray
        shareCodeButton.addSubview(lineView)
        view.addSubview(shareCodeButton)

        shareText.frame = CGRect(x: 0, y: shareCodeButton.frame.maxY + 1, width: view.frame.width, height: 100)
        if isIPhoneSE == true { 
            //shareText.frame = CGRect(x: 0, y: lineView.frame.maxY, width: view.frame.width, height: 75
        }
        
        if userData?.FirstName != "" {
            let name = userData?.FirstName
            shareText.text = "\(name ?? "User"), share your personal link with friends and they will get to use GHS 5 for their first order with \(appTitle). After their meal is delivered you will get GHS 5 to use for your next order."
        } else {
            shareText.text = "Share your personal link with friends and they will get to use GHS 5 for their first order with \(appTitle). After their meal is delivered you will get GHS 5 to use for your next order."
        }
        shareText.textColor = hexDarkGray
        shareText.isUserInteractionEnabled = false
        shareText.isSelectable = false
        shareText.backgroundColor = .clear
        shareText.textAlignment = .left
        shareText.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)

        if isIPhoneX == true {
            shareText.frame = CGRect(x: 0, y: shareCodeButton.frame.maxY + 1, width: view.frame.width, height: 150)
            shareText.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.light)
        }

        shareText.contentInset = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        shareText.centerVertically()
        
        let lineView2 = UIView(frame: CGRect(x: 0, y: shareText.frame.maxY, width: view.frame.width, height: 1))
        lineView2.backgroundColor = hexLightGray
        view.addSubview(lineView2)
        
        view.addSubview(shareText)

    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            callNotificationItem("reloadTables")
            self.navigationController?.hero.navigationAnimationType = .slide(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //goto share link code
            
            animateUIBanner("Success", subText: "Invite code copied.", color: UIWarningSuccessColor, removePending: true)
            UIPasteboard.general.string = userShareCodeUrl

        } else if sender.tag == 14 {
            //goto share link
            showShareDialogue(vc: self, button: sender)
        }
    }
}
