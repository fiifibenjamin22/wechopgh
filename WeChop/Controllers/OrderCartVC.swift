//
//  OrderCartVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/16/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import SwiftPullToRefresh
import EzPopup

class OrderCartVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var scrollView: UIScrollView = UIScrollView()
    
    var navigationLeft = SpringButton() 
    var navigationTitleBorder = SpringButton()
    var referralButton: SpringButton = SpringButton()
    var orderButton: SpringButton = SpringButton()
    var applyPromoCodeButton: SpringButton = SpringButton()
    var orderDate: SpringLabel = SpringLabel()
    var deliverToLabel: SpringLabel = SpringLabel()
    var mapImage: UIImageView = UIImageView(image: userAddressMapImage)
    var addresslabel: SpringTextView = SpringTextView()
    var addresslabel2: SpringTextView = SpringTextView()
    var orderDetailsTitle: SpringLabel = SpringLabel()
    var promoCode: SpringTextField = SpringTextField()
    var cancelOrder: SpringButton = SpringButton()

    //ui elements
    let statusHeight: CGFloat = 25.0
    let statusXIndent: CGFloat = 15.0
    let orderStatusPrefix: String = "●  "
    let grayFont =  UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.thin)
    var orderFont = defaultFont14
    let verticleIndent: CGFloat = 75

    //update cart order item
    var selectedOrderLine: OrderDataLines?

    //logical elements
    var isOnlyOneRestaurantInOrder = true // used to indicate if more than one restaurant is used in order
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN OrderCartVC | ViewDidLoad")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .up)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshCartPageData), name: NSNotification.Name(rawValue: "refreshCartPageData"), object: nil) 
        
        NotificationCenter.default.addObserver(self, selector: #selector(getUserCurrentCartFromCartDetails), name: NSNotification.Name(rawValue: "getUserCurrentCartFromCartDetails"), object: nil)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        if isIPhoneSE == true { 
            orderFont = defaultFont12
        }

        NotificationCenter.default.addObserver(self, selector: #selector(dismissCartVC), name: NSNotification.Name(rawValue: "dismissCartVC"), object: nil) 

        NotificationCenter.default.addObserver(self, selector: #selector(goToOrderVCPageFromOrderComplete), name: NSNotification.Name(rawValue: "goToOrderVCPageFromOrderComplete"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(goToMenuDetailsFromNewMenuData), name: NSNotification.Name(rawValue: "goToMenuDetailsFromNewMenuData"), object: nil)

        self.view.backgroundColor = UIColor.white

        //clear cart button gesture
        //        tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        //        tap.numberOfTapsRequired = 1
        //        tap.isEnabled = false
        //        view.addGestureRecognizer(tap)

        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        scrollView.addGestureRecognizer(tapGesture)
        
        //dismiss keyboard when pressing enter
        promoCode.delegate = self
        promoCode.clearButtonMode = UITextFieldViewMode.whileEditing
        
        createNavigationAndScrollView()
        createMenuUI()
        //createClearCartButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        promoCode.resignFirstResponder()
    }
    
    func createMenuUI() {
        //reload page image and text
        guard
            let url = Bundle.main.url(forResource: globalRefreshGIFName, withExtension: "gif"),
            let data = try? Data(contentsOf: url) else { return }
        scrollView.spr_setGIFHeader(data: data, isBig: true, height: 200) { [weak self] in
            comingFrom.cart = true
            tryToGetServerCartParameters(self!, promoCode: "", completion: tryToGetServerCartParametersHandler)
        }
        
        orderDate.frame = CGRect(x: statusXIndent, y: globalYAxisHeight / 1.8, width: self.view.frame.width - (2 * statusXIndent), height: statusHeight)
        
        orderDate.font = UIFont.Theme.size15Medium

        if isIPhoneSE == true { 
//            orderDate.font = defaultFont14
            orderDate.font = UIFont.Theme.size13Medium
        }
        
        if userCartOrder.Date != "" {
            let shortDate = userCartOrder.Date.toDate()
            let dayOfWeek: String = (shortDate?.weekdayName(.default))!
            
            let dayFormatted: String = "\((shortDate?.day)!)\(getDaySuffix(dayOfMonth: (shortDate?.day)!))"
            
            let month = shortDate!.toFormat("MMMM")
            orderDate.text = "Schedule for \(dayOfWeek), \(dayFormatted) \(month)"
        } else {
            orderDate.text = "Date unavailable"
        }
        
        orderDate.textColor = newHexBlack1.Color
        orderDate.isUserInteractionEnabled = false
        orderDate.textAlignment = .left
        scrollView.addSubview(orderDate)

        mapImage.frame = CGRect(x: statusXIndent, y: orderDate.frame.maxY + 45, width: 100, height: 75)
        mapImage.contentMode =  UIViewContentMode.scaleAspectFill
        mapImage.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
        mapImage.clipsToBounds = true
        
        if userAddressMapImage != nil {
            mapImage.image = userAddressMapImage
        } else {
            mapImage.image = UIImage(named: "map_example")
        }
        mapImage.image =  UIImage(named: "map_example") // TODO need to fix map image for accra addresses
        scrollView.addSubview(mapImage)
        
        let border = SpringButton()
        border.frame = CGRect(x: 0, y: mapImage.frame.minY - 17.5, width: view.frame.width, height: 1)
        border.center.x = view.center.x
        border.backgroundColor = hexLightGray
        border.isUserInteractionEnabled = false
        border.layer.zPosition = 1000
        scrollView.addSubview(border)
        
        addresslabel.frame = CGRect(x: mapImage.frame.maxX + 10, y: mapImage.frame.minY, width: 200.0, height: 40)
        
        if isIPhoneSE == true { 
            addresslabel.frame = CGRect(x: mapImage.frame.maxX + 10, y: mapImage.frame.minY, width: 175.0, height: 40)
        }
        
        addresslabel.textColor = newHexBlack1.Color
        addresslabel.text = userData?.Address
        addresslabel.isUserInteractionEnabled = false
        addresslabel.isSelectable = false
        addresslabel.backgroundColor = .clear
        addresslabel.textAlignment = .left
//        addresslabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        addresslabel.font = UIFont.Theme.size15
        scrollView.addSubview(addresslabel)
        
        addresslabel2.frame = CGRect(x: mapImage.frame.maxX + 10, y: addresslabel.frame.maxY, width: 200.0, height: 25)
        if isIPhoneSE == true { 
        }
        addresslabel2.textColor = newHexGray.Color
        addresslabel2.text = "Meet at the reception"
        addresslabel2.isUserInteractionEnabled = false
        addresslabel2.isSelectable = false
        addresslabel2.backgroundColor = .clear
        addresslabel2.textAlignment = .left
        addresslabel2.font = UIFont.Theme.size13
        scrollView.addSubview(addresslabel2)
        
        let border2 = SpringButton()
        border2.frame = CGRect(x: 0, y: mapImage.frame.maxY + 17.5, width: view.frame.width, height: 1)
        border2.center.x = view.center.x
        border2.backgroundColor = hexLightGray
        border2.isUserInteractionEnabled = false
        border2.layer.zPosition = 3
        scrollView.addSubview(border2)
        
        orderDetailsTitle.frame = CGRect(x: statusXIndent, y: border2.frame.maxY + 25, width: self.view.frame.width, height: statusHeight)
        orderDetailsTitle.text = "Your order"
        orderDetailsTitle.textColor = newHexBlack1.Color
        orderDetailsTitle.isUserInteractionEnabled = false
        orderDetailsTitle.textAlignment = .left
        orderDetailsTitle.font = defaultFont16
        scrollView.addSubview(orderDetailsTitle)

        print("about to create item rows dump:")
        //        dump(userCartOrder)
        let nextYLoc = createOrderedItemRows(yMin: orderDetailsTitle.frame.maxY + 2.5) // create the rows of ordered items
        
        //update scrollView max height
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: nextYLoc + 125)
        
        if isIPhoneX == true {
            scrollView.contentSize = CGSize(width: self.view.frame.width, height: nextYLoc + 150)
        }
    }
    
    func createOrderedItemRows(yMin: CGFloat) -> CGFloat {
        print("in createOrderedItemRows | start")
        var originalY = yMin
        var restaurantListNames: [String] = [String]()
        for item in userCartOrder.OrderedItems.unique(by: {$0.Restaurant}) {
            restaurantListNames.append(item.Restaurant)
        }
        
        if restaurantListNames.count > 1 {
            isOnlyOneRestaurantInOrder = false
        }
        
        for restaurant in restaurantListNames {
            
            let restaurantTitle: SpringLabel = SpringLabel(frame: CGRect(x: 15.0, y: originalY + 5, width: self.view.frame.width - 20, height: statusHeight - 5))
            restaurantTitle.text = "\(restaurant)"
            restaurantTitle.textColor = newHexBlack1.Color
            restaurantTitle.isUserInteractionEnabled = false
            restaurantTitle.textAlignment = .left
            restaurantTitle.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
            
            scrollView.isScrollEnabled = true
            scrollView.addSubview(restaurantTitle)
            
            originalY = restaurantTitle.frame.maxY + 5 

            var i = 0
            for item in userCartOrder.OrderedItems {
                if item.Restaurant == restaurant {
                    let orderHeight = statusHeight - 10
                    let orderCount = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 25, height: orderHeight))
                    orderCount.backgroundColor = .clear
                    orderCount.text = "\(item.Quantity)x"
                    orderCount.textColor = hexRedDark.Color
                    orderCount.layer.zPosition = 5
                    orderCount.isUserInteractionEnabled = false
                    orderCount.textAlignment = .left
                    orderCount.font = orderFont
                    scrollView.addSubview(orderCount)

                    let orderCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: originalY, width: 150, height: orderHeight))
                    orderCost.backgroundColor = .clear
                    orderCost.text = "GHS \(formatPrice(p: (Double(item.Price)! * Double(item.Quantity)!)))"
                    orderCost.textColor = newHexGray.Color
                    orderCost.layer.zPosition = 6
                    orderCost.isUserInteractionEnabled = false
                    orderCost.textAlignment = .right
                    orderCost.font = orderFont
                    scrollView.addSubview(orderCost)

                    var orderTitle = SpringTextView(frame: CGRect(x: orderCount.frame.maxX, y: originalY - 8, width: self.view.frame.width - (orderCost.frame.width * 0.85), height: orderHeight))

                    if isIPhoneSE == true {
                        orderTitle.frame = CGRect(x: orderCount.frame.maxX, y: originalY - 7, width: self.view.frame.width - (orderCost.frame.width * 0.65), height: orderHeight)
                    }
                    orderTitle.textAlignment = .left
                    orderTitle.font = orderFont
                    orderTitle.text = "\(item.Name)"
                    orderTitle.sizeToFit()
                    orderTitle.topVertically()
                    orderTitle.backgroundColor = .clear
                    orderTitle.textColor = hexRedDark.Color
                    orderTitle.layer.zPosition = 5
                    orderTitle.isUserInteractionEnabled = true
                    orderTitle.tag = i
                    let orderTap = UITapGestureRecognizer(target: self, action: #selector(menuItemPressed))
                    orderTap.numberOfTapsRequired = 1
                    orderTitle.addGestureRecognizer(orderTap)
                    scrollView.addSubview(orderTitle)
                    originalY = orderTitle.frame.maxY + 2.5
                }
                i += 1
            }
            originalY = originalY + 10  // add padding for each restaurant section
        }
        //VAT
        
        let vat = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 15, width: 200, height: statusHeight))
        vat.backgroundColor = .clear
        vat.text = "VAT (17.5%)"
        vat.textColor = newHexGray.Color
        vat.layer.zPosition = 5
        vat.isUserInteractionEnabled = false
        vat.textAlignment = .left
        vat.font = defaultFont14
        scrollView.addSubview(vat)
        
        let vatCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: vat.frame.minY, width: 150, height: statusHeight))
        vatCost.backgroundColor = .clear

        vatCost.text = "\(userCartOrder.VatCost)"
        if userCartOrder.VatCost == "" {
            vatCost.text = "\(0.00)"
        }

        vatCost.textColor = newHexGray.Color
        vatCost.layer.zPosition = 6
        vatCost.isUserInteractionEnabled = false
        vatCost.textAlignment = .right
        vatCost.font = defaultFont14
        scrollView.addSubview(vatCost)
        
        originalY = vat.frame.maxY
        
        let delivery = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 200, height: statusHeight))
        delivery.backgroundColor = .clear
        delivery.text = "Delivery fee"
        delivery.textColor = newHexGray.Color
        delivery.layer.zPosition = 5
        delivery.isUserInteractionEnabled = false
        delivery.textAlignment = .left
        delivery.font = defaultFont14
        scrollView.addSubview(delivery)
        
        let deliveryFee = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: delivery.frame.minY, width: 150, height: statusHeight))
        deliveryFee.backgroundColor = .clear

        if userCartOrder.DeliveryCost == "" || userCartOrder.DeliveryCost == "GHS 0.00" {
            deliveryFee.text = "0.00"
        } else {
            deliveryFee.text = "\(userCartOrder.DeliveryCost)"
        }

        deliveryFee.textColor = newHexGray.Color
        deliveryFee.layer.zPosition = 6
        deliveryFee.isUserInteractionEnabled = false
        deliveryFee.textAlignment = .right
        deliveryFee.font = defaultFont14
        scrollView.addSubview(deliveryFee)
        
        originalY = delivery.frame.maxY
        
        let discount = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY, width: 200, height: statusHeight))
        discount.backgroundColor = .clear
        discount.text = "Discount"
        discount.textColor = newHexGray.Color
        discount.layer.zPosition = 5
        discount.isUserInteractionEnabled = false
        discount.textAlignment = .left
        discount.font = defaultFont14
        scrollView.addSubview(discount)
        
        let discountCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: discount.frame.minY, width: 150, height: statusHeight))
        discountCost.backgroundColor = .clear
        
        if userCartOrder.Discount == "" || userCartOrder.Discount == "GHS 0.0" {
            discountCost.text = "0.00"
        } else {
            discountCost.text = "\(userCartOrder.Discount)"
        }
        
        discountCost.textColor = newHexGray.Color
        discountCost.layer.zPosition = 6
        discountCost.isUserInteractionEnabled = false
        discountCost.textAlignment = .right
        discountCost.font = defaultFont14
        scrollView.addSubview(discountCost)
        
        originalY = discount.frame.maxY
        
        let border3: SpringButton = SpringButton()
        border3.frame = CGRect(x: 0, y: originalY + 10, width: view.frame.width, height: 1)
        border3.center.x = view.center.x
        border3.backgroundColor = hexLightGray
        border3.isUserInteractionEnabled = false
        border3.layer.zPosition = 3
        scrollView.addSubview(border3)
        
        originalY = border3.frame.maxY

        let total = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 10, width: 200, height: statusHeight))
        total.backgroundColor = .clear
        total.text = "Total"
        total.textColor = hexDarkGray
        total.layer.zPosition = 5
        total.isUserInteractionEnabled = false
        total.textAlignment = .left
        total.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        scrollView.addSubview(total)
        
        let totalCost = SpringLabel(frame: CGRect(x: self.view.frame.width - (150 + statusXIndent), y: total.frame.minY, width: 150, height: statusHeight))
        totalCost.backgroundColor = .clear
        totalCost.text = "GHS \(formatPrice(p: userCartOrder.Amount))"
        totalCost.textColor = newHexBlack1.Color
        totalCost.layer.zPosition = 6
        totalCost.isUserInteractionEnabled = false
        totalCost.textAlignment = .right
        totalCost.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)

        scrollView.addSubview(totalCost)
        
        let border4 = SpringButton()
        border4.frame = CGRect(x: 0, y: total.frame.maxY + 10, width: view.frame.width, height: 1)
        border4.center.x = view.center.x
        border4.backgroundColor = hexLightGray
        border4.isUserInteractionEnabled = false
        border4.layer.zPosition = 3
        scrollView.addSubview(border4)
        
        originalY = border4.frame.maxY

        let paymentMethod = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 10, width: 200, height: statusHeight - 5))
        paymentMethod.backgroundColor = .clear
        paymentMethod.text = "Payment method"
        paymentMethod.textColor = newHexBlack1.Color
        paymentMethod.layer.zPosition = 5
        paymentMethod.isUserInteractionEnabled = false
        paymentMethod.textAlignment = .left
        paymentMethod.font = defaultFont16
        scrollView.addSubview(paymentMethod)
        
        originalY = paymentMethod.frame.maxY

        let paymentMethodText = SpringLabel(frame: CGRect(x: statusXIndent, y: originalY + 15, width: 200, height: statusHeight))
        paymentMethodText.backgroundColor = .clear
        paymentMethodText.text = "\(getPaymentMethodString())"
        paymentMethodText.textColor = newHexGray.Color
        paymentMethodText.layer.zPosition = 5
        paymentMethodText.isUserInteractionEnabled = false
        paymentMethodText.textAlignment = .left
        paymentMethodText.font = orderFont
        scrollView.addSubview(paymentMethodText)
        
        let paymentMethodTextChangeButton = SpringButton(frame: CGRect(x: paymentMethodText.frame.width + 10, y: originalY + 15, width: self.view.frame.width - (paymentMethodText.frame.width + 20), height: statusHeight))
        paymentMethodTextChangeButton.backgroundColor = .clear
        paymentMethodTextChangeButton.setTitle("CHANGE", for: .normal)
        paymentMethodTextChangeButton.setTitleColor(hexRed.Color, for: .normal)
        paymentMethodTextChangeButton.setTitleColor(hexBlack.Color, for: .highlighted)
        //        paymentMethodTextChangeButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        paymentMethodTextChangeButton.isUserInteractionEnabled = true
        paymentMethodTextChangeButton.contentHorizontalAlignment = .right
        paymentMethodTextChangeButton.tag = 15
        paymentMethodTextChangeButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        paymentMethodTextChangeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        //        scrollView.addSubview(paymentMethodTextChangeButton)
        
        applyPromoCodeButton.frame = CGRect(x: self.view.frame.width - 60, y: paymentMethodText.frame.maxY + verticleIndent, width: 50, height: statusHeight)
        applyPromoCodeButton.backgroundColor = .clear
        applyPromoCodeButton.setTitle("APPLY", for: .normal)
        applyPromoCodeButton.setTitleColor(hexDarkGray, for: .normal)
        applyPromoCodeButton.isUserInteractionEnabled = true
        applyPromoCodeButton.contentHorizontalAlignment = .right
        applyPromoCodeButton.tag = 16
        applyPromoCodeButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        applyPromoCodeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        scrollView.addSubview(applyPromoCodeButton)
        
        promoCode.frame = CGRect(x: statusXIndent, y: paymentMethodText.frame.maxY + verticleIndent, width: self.view.frame.width - (statusXIndent + applyPromoCodeButton.frame.width + statusXIndent), height: statusHeight)
        promoCode.text = "Promo code"
        promoCode.keyboardType = .default
        promoCode.textAlignment = .left
        promoCode.autocapitalizationType = .allCharacters
        promoCode.backgroundColor = .clear
        promoCode.autocorrectionType = .no
        promoCode.isSecureTextEntry = false
        promoCode.setBottomBorderLight()
        promoCode.contentVerticalAlignment = .bottom
        promoCode.font = orderFont
        scrollView.addSubview(promoCode)
        
        cancelOrder = createFooterButton(text: "Clear Cart", v: self.view)
        cancelOrder.frame = CGRect(x: 0, y: applyPromoCodeButton.frame.maxY + verticleIndent, width: self.view.frame.width, height: globalLargeButtonHeight)
        cancelOrder.backgroundColor = .clear
        cancelOrder.setTitleColor(hexDarkGray.withAlphaComponent(0.5), for: .normal)
        cancelOrder.titleLabel?.font = defaultFont14
        cancelOrder.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        cancelOrder.isUserInteractionEnabled = true
        cancelOrder.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cancelOrder.layer.zPosition = 100
        cancelOrder.tag = 17
        scrollView.addSubview(cancelOrder)
        
        return cancelOrder.frame.maxY
    }
    
    func createNavigationAndScrollView() {
        //create header and footer buttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Checkout", v: self.view, leftButton: navigationLeft)
        
        //scrollView
        scrollView.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = true
        scrollView.alwaysBounceHorizontal = false
        scrollView.delegate = self
        scrollView.layer.zPosition = 3
        scrollView.backgroundColor = .clear
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: 100)
        
        view.addSubview(scrollView)
        
        //        referralButton = createFooterButton(text: "Refer a friend, get 5 GHS", v: self.view)
        //        referralButton.backgroundColor = hexGreen.Color
        //        referralButton.isUserInteractionEnabled = true
        //        referralButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        //        view.addSubview(referralButton)
        
        orderButton = createFooterButton(text: "Place order", v: self.view)
        orderButton.frame = CGRect(x: 0, y: self.view.frame.height - globalLargeButtonHeight, width: self.view.frame.width, height: globalLargeButtonHeight)
        
        if isIPhoneX == true {
            orderButton.frame = CGRect(x: 0, y: self.view.frame.height - (globalLargeButtonHeight + globalIPhoneXTopYPadding), width: self.view.frame.width, height: globalLargeButtonHeight + globalIPhoneXTopYPadding)
        }
        
        orderButton.backgroundColor = hexRed.Color
        orderButton.isUserInteractionEnabled = true
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.layer.zPosition = 100
        orderButton.tag = 14
        view.addSubview(orderButton)
    }
    //
    //    func createClearCartButton() {
    //        print("in createClearCartButton | start")
    //        var boxHeight = self.view.frame.height / 4.25
    //        if isIPhoneX == true {
    //            boxHeight = self.view.frame.height / 5
    //        } else if isIPhoneSE == true {
    //            boxHeight = self.view.frame.height / 3.5
    //        }
    //
    //        backgroundDark.frame = view.frame
    //        backgroundDark.layer.zPosition = 100
    //        backgroundDark.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    //        backgroundDark.isHidden = true
    //        view.addSubview(backgroundDark)
    //
    //        whiteBox.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - (globalXAxisIndent * 2), height: boxHeight)
    //        whiteBox.isUserInteractionEnabled = true
    //        whiteBox.backgroundColor = .white
    //        whiteBox.center = view.center
    //        whiteBox.layer.zPosition = 101
    //        whiteBox.isHidden = true
    //        view.addSubview(whiteBox)
    //
    //        cartText.frame = CGRect(x: globalXAxisIndent, y: 0, width: 200, height: 20)
    //        cartText.textColor = .black
    //        cartText.text = "Clear your cart"
    //        cartText.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
    //
    //        cartText.textAlignment = .left
    //        cartText.sizeToFit()
    //        cartText.frame.origin.y = globalXAxisIndent - 5
    //        cartText.backgroundColor  = .clear
    //        cartText.isScrollEnabled = false
    //        cartText.isEditable = false
    //        cartText.layer.zPosition = 102
    //        cartText.isUserInteractionEnabled = false
    //        cartText.isSelectable = false
    //        whiteBox.addSubview(cartText)
    //
    //        text.frame = CGRect(x: globalXAxisIndent, y: cartText.frame.maxY, width: whiteBox.frame.width - (2 * globalXAxisIndent), height: 150)
    //        text.textColor = .black
    //        text.text = "Remove all items from your current cart?"
    //        text.backgroundColor  = .clear
    //        text.font = UIFont.systemFont(ofSize: 15.5, weight: UIFont.Weight.light)
    //
    //        text.isScrollEnabled = false
    //        text.isEditable = false
    //        text.layer.zPosition = 102
    //        text.isUserInteractionEnabled = false
    //        text.isSelectable = false
    //        text.textAlignment = .left
    //        whiteBox.addSubview(text)
    //
    //        let buttonWidth: CGFloat = self.view.frame.width / 2 - ((globalXAxisIndent) + 5)
    //        let buttonHeight: CGFloat = 50.0
    //
    //        yesButton = createFooterButton(text: "OKAY", v: self.view)
    //        yesButton.frame = CGRect(x: whiteBox.frame.width - (globalXAxisIndent + 100), y: whiteBox.frame.height - (buttonHeight), width: 100, height: buttonHeight)
    //        yesButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    //        yesButton.tag = 21
    //        yesButton.setTitleColor(hexYellow.Color, for: .normal)
    //        yesButton.setBackgroundColor(color: UIColor.white, forState: .normal)
    //
    //        yesButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
    //        yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold   )
    //        yesButton.titleLabel?.textAlignment = .left
    //        whiteBox.addSubview(yesButton)
    //
    //        cancelButton = createFooterButton(text: "CANCEL", v: self.view)
    //        cancelButton.frame = CGRect(x: yesButton.frame.minX - ((globalXAxisIndent / 2) + 75), y: yesButton.frame.minY, width: 75, height: buttonHeight)
    //        cancelButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    //        cancelButton.tag = 20
    //        cancelButton.titleLabel?.textAlignment = .right
    //        cancelButton.setTitleColor(hexYellow.Color, for: .normal)
    //        cancelButton.setBackgroundColor(color: UIColor.white, forState: .normal)
    //        cancelButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
    //        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
    //        whiteBox.addSubview(cancelButton)
    //
    //        if isIPhoneSE == true {
    //            cartText.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
    //            text.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
    //
    //            yesButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold   )
    //
    //            cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
    //
    //        }
    //
    //        cancelButton.layer.zPosition = 102
    //        yesButton.layer.zPosition = 102
    //    }

    @objc func refreshCartPageData(_ refreshControl: UIRefreshControl) {
        print("in refresh page")
        
        self.scrollView.spr_endRefreshing()
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        createMenuUI()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "Promo code" {
            textField.text = ""
        }
        
        textField.font = defaultFont16
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField == promoCode {
            if textField.text == "" {
                textField.text = "Promo code"
            } else {
                //validate promo code and apply price changes
            }
            textField.font = orderFont
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == promoCode {
            applyPromoCodeButton.sendActions(for: .touchUpInside)
        }
        return false
    }

    @objc func menuItemPressed(_ sender: UITapGestureRecognizer) {
        print("menuItemPressed pressed: ", sender.view?.tag)

        //get menu item from order item details
        if let index = sender.view?.tag {
            selectedOrderLine = userCartOrder.OrderedItems[index]

            print("userCartOrder.Date: ", userCartOrder.Date)

            guard let cartDate = DateInRegion(userCartOrder.Date, region: currentUserRegion) else {
                animateUIBanner("Warning", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                return
            }

            if cartDate.weekdayName(.default) != currentlySelectedMenuDay.weekdayName(.default) { //cart items are different that loaded menu
                print("menuitemPressed | cart day different than loaded menu!")
                tryToGetMenuDataForUpdateCart((cartDate.toFormat("YYYY-MM-dd")), vc: self, completion: tryToGetMenuDataForUpdateCartHandler)
            } else {
                if todaysMenu[0].Name == selectedOrderLine!.Restaurant {
                    if let id = selectedOrderLine?.ID {
                        selectedMenuItem = todaysMenu[0].MenuItems.first(where: { $0.ID == id })
                    }
                    comingFrom.cartToMenuDetails = true

                    segue(name: "segueToMenuDetailsFromCartVC", v: self, type: .pageIn(direction: .up))
                } else if todaysMenu[1].Name == selectedOrderLine!.Restaurant {
                    if let id = selectedOrderLine?.ID {
                        selectedMenuItem = todaysMenu[1].MenuItems.first(where: { $0.ID == id })
                    }
                    comingFrom.cartToMenuDetails = true
                    segue(name: "segueToMenuDetailsFromCartVC", v: self, type: .pageIn(direction: .up))
                }
            }
        }
}

//Nav buttons pressed actions
@objc func buttonPressed(_ sender: UIButton) {
    print("Button pressed: ", sender.tag)
    animateButtonPress(senderTag: sender.tag, localView: self.view)

    if sender.tag == 12 {
        callNotificationItem("refreshPage")
        self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
        self.hero.dismissViewController()
    } else if sender.tag == 13 {
        //refer a friend
        print("refer button pressed")

    } else if sender.tag == 14 {

        print("here one res: ", isOnlyOneRestaurantInOrder)
        print("here one userWasNotifiedOfTwoRestaurantCart: ", userWasNotifiedOfTwoRestaurantCart)

        if isOnlyOneRestaurantInOrder == false && userWasNotifiedOfTwoRestaurantCart == false {

            let alert = UIAlertController(title: "Multiple Restaurants", message: "\n\((userData?.FirstName)!), your lunch is from multiple restaurants. It will be tracked under separate orders and may be delivered by different couriers.", preferredStyle: UIAlertControllerStyle.alert)

            UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .extraLight)

            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { action in
                userWasNotifiedOfTwoRestaurantCart = true
                defaults.set(userWasNotifiedOfTwoRestaurantCart, forKey: "userWasNotifiedOfTwoRestaurantCart")
                defaults.synchronize()

                BranchAssistant.app.purchaseItems(cart: userCartOrder)
                tryToPlaceCartOrder(self, completion: tryToPlaceCartOrderHandler)

            }))
            self.present(alert, animated: true) {
            }
        } else {
            BranchAssistant.app.purchaseItems(cart: userCartOrder)
            tryToPlaceCartOrder(self, completion: tryToPlaceCartOrderHandler)
        }

    } else if sender.tag == 15 {
        //change payment method
        let contentVC = PaymentVC()
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
        //comingFromCart = true
        comingFrom.cart = true

        present(popupVC, animated: true)
    } else if sender.tag == 16 {
        // apply promo code

        promoCode.resignFirstResponder()
        if promoCode.text != "" && promoCode.text != "Promo code" {
            tryToGetServerCartParameters(self, promoCode: promoCode.text!, completion: tryToGetServerCartParametersPromoCodeHandler)
        }
    } else if sender.tag == 17 {
        print("cancel order pressed")
        let alert = UIAlertController(title: "Clear your cart", message: "Remove all items from your current cart?", preferredStyle: UIAlertControllerStyle.alert)

        UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .extraLight)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { action in

        }))

        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.destructive, handler: { action in
            callNotificationItem("refreshPage")
            tryToDeleteCart(self, completion: tryToDeleteCartHandler)
        }))
        self.present(alert, animated: true) {

        }
    }
    //removed in favor of OTB iOS alerts
    //        else if sender.tag == 20 {
    //            //toggleClearCartUI(show: false)
    //
    //        }  else if sender.tag == 21 {
    //            //clear button pressed on clear car
    //            print("clear cart pressed")
    //
    //            callNotificationItem("refreshPage")
    //            tryToDeleteCart(self, completion: tryToDeleteCartHandler)
    //        }
}

//    func toggleClearCartUI( show: Bool) {
//        if show == true {
//            tap.isEnabled = true
//            whiteBox.isHidden = false
//            backgroundDark.isHidden = false
//
//        } else {
//            tap.isEnabled = false
//            whiteBox.isHidden = true
//            backgroundDark.isHidden = true
//
//        }
//
//    }

//    @objc func tapFunction(sender: UITapGestureRecognizer) {
//        print("background pressed")
//
//        //toggleClearCartUI(show: false)
//    }

@objc func goToMenuDetailsFromNewMenuData() {
    print("in goToMenuDetailsFromNewMenuData | start")

    if let restaurant = selectedOrderLine?.Restaurant {
        if cartMenu[0].Name == restaurant {
            if let id = selectedOrderLine?.ID {
                selectedMenuItem = cartMenu[0].MenuItems.first(where: { $0.ID == id })
                comingFrom.cartToMenuDetails = true
                segue(name: "segueToMenuDetailsFromCartVC", v: self, type: .pageIn(direction: .up))
            }
        } else if cartMenu[1].Name == restaurant {
            if let id = selectedOrderLine?.ID {
                selectedMenuItem = cartMenu[1].MenuItems.first(where: { $0.ID == id })
                comingFrom.cartToMenuDetails = true
                segue(name: "segueToMenuDetailsFromCartVC", v: self, type: .pageIn(direction: .up))
            }
        }
    }
}

@objc func getUserCurrentCartFromCartDetails() {
    tryToGetServerCartParameters(self, promoCode: "", completion: tryToGetServerCartParametersHandler)
}

@objc func goToOrderVCPageFromOrderComplete () {
    callNotificationItem("refreshPage")

    comingFrom.ordersUpcomingTableView = true
    //go to OrderVC from CartVC
    segue(name: "OrderVCSegueFromOrderCartDetails", v: self, type: .cover(direction: .up))
    //comingFromCart = true
    comingFrom.cart = true
}

@objc func dismissCartVC () {
    callNotificationItem("refreshPage")
    self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
    self.hero.dismissViewController()
}
}
