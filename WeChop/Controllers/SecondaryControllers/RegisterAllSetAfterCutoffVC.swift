//
//  RegisterAllSetAfterCutoffVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/29/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import SwiftDate
import Spring

class RegisterAllSetAfterCutoffVC: UIViewController {
    
    var topImage: UIImageView = UIImageView(image: UIImage(named: "afterCutOff_foodImage_rotated.jpg"))
    var allSetLabel: SpringButton = SpringButton()
    var timeLabel: SpringButton = SpringButton()
    let text: UITextView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in RegisterAllSetAfterCutoffVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .fade
        
        view.backgroundColor = UIColor.white
        
        //top image and icon
        topImage.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 1.68)
        topImage.contentMode =  UIViewContentMode.scaleAspectFill
        topImage.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
        topImage.clipsToBounds = true
        topImage.layer.zPosition = 1
        self.view.addSubview(topImage)

        if isIPhoneSE == true { 
            topImage.frame = CGRect(x: 0, y: -50, width: self.view.frame.width, height: self.view.frame.height / 1.68)
        }

        let labelHeight: CGFloat = 25
        
        allSetLabel.frame = CGRect(x: globalXAxisIndent, y: topImage.frame.maxY + labelHeight, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)
        allSetLabel.backgroundColor = .clear
        allSetLabel.isUserInteractionEnabled = false
        allSetLabel.titleLabel?.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.regular)
        allSetLabel.center.x = view.center.x
        allSetLabel.setTitleColor(hexRed.Color, for: .normal)
        allSetLabel.setBackgroundColor(color: .clear, forState: .normal)
        allSetLabel.setTitle("You're all set!", for: .normal)
        allSetLabel.contentVerticalAlignment = .bottom
        allSetLabel.contentHorizontalAlignment = .center
        allSetLabel.layer.zPosition = 1
        view.addSubview(allSetLabel)
        
        timeLabel.frame = CGRect(x: globalXAxisIndent, y: allSetLabel.frame.maxY + 10, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight - 10)
        timeLabel.backgroundColor = .clear
        timeLabel.isUserInteractionEnabled = false
        timeLabel.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.thin)
        timeLabel.center.x = view.center.x
        timeLabel.setTitleColor(hexRed.Color, for: .normal)
        timeLabel.setBackgroundColor(color: .clear, forState: .normal)
        timeLabel.setTitle("Order cut off time: 11am.", for: .normal)
        timeLabel.contentVerticalAlignment = .center
        timeLabel.contentHorizontalAlignment = .center
        timeLabel.layer.zPosition = 1
        view.addSubview(timeLabel)
        
        text.frame = CGRect(x: 0, y: timeLabel.frame.maxY + (labelHeight / 2), width: self.view.frame.width, height: 85)        
        text.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        text.textColor = hexDarkGray
        
        var day = (currentUserTime + 1.days).weekdayName(.default)
        if day == "Saturday" || day == "Sunday" {
            day = "Monday"
            
            if userData?.FirstName != "" && userData?.FirstName != nil {
                text.text = "Welcome \((userData?.FirstName)!)! We start again on \(day) but you can pre-order now. Check out \(day)'s menu and make we chop!"
            } else {
                text.text = "Welcome! We start again on \(day) but you can pre-order now. Check out \(day)'s menu and make we chop!"
            }
        } else {
            if userData?.FirstName != "" && userData?.FirstName != nil {
                text.text = "Welcome \((userData?.FirstName)!)! We start again tomorrow. You can pre-order for tomorrow now. Check out the menu and make we chop!"
            } else {
                text.text = "Welcome! We start again tomorrow. You can pre-order for tomorrow now. Check out the menu and make we chop!"
            }
        }
        
        text.contentInset = UIEdgeInsets (top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        text.backgroundColor  = .clear
        text.isScrollEnabled = true
        text.isEditable = false
        text.layer.zPosition = 10
        text.isUserInteractionEnabled = false
        //        text.sizeToFit()
        //        text.sizeToFit()
        text.contentInset = UIEdgeInsets (top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        text.center.x = view.center.x
        text.textAlignment = .center
        view.addSubview(text)
        
        let orderButton = createFooterButton(text: "Order Now", v: self.view)
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.center.x = view.center.x
        orderButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        orderButton.backgroundColor = hexRed.Color

        
        showNewUserTutorial(originalView: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        if sender.tag == 13 {
            callNotificationItem("refreshPage")
            self.navigationController?.hero.navigationAnimationType = .uncover(direction: .down)
            self.hero.dismissViewController()
        }
    }
}
