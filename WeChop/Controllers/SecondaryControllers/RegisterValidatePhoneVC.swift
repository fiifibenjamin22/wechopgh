//
//  RegisterValidatePhoneVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/26/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class RegisterValidatePhoneVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    var verifyLabel: SpringButton = SpringButton()
    var directionsLabel: SpringButton = SpringButton()
    var directionsLabel2: SpringButton = SpringButton()
    
    var phone1: SpringTextField = SpringTextField()
    var phone2: SpringTextField = SpringTextField()
    var phone3: SpringTextField = SpringTextField()
    var phone4: SpringTextField = SpringTextField()
    var phone5: SpringTextField = SpringTextField()

    var phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid

    var footerText1: UITextView = UITextView()
    var footerText2: UITextView = UITextView()
    
    var validationTryCount: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN RegisterValidatePhoneVC | ViewDidLoad")
        
        validationTryCount = 0
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .cover(direction: .up)
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissToLogin), name: NSNotification.Name(rawValue: "dismissToLogin"), object: nil)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //dismiss keyboard when pressing enter
        phone1.delegate = self
        phone2.delegate = self
        phone3.delegate = self
        phone4.delegate = self
        phone5.delegate = self

        createContent()
        
        self.phone1.becomeFirstResponder() //make first field active
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.phone1.becomeFirstResponder() //make first field active
    }
    
    func createContent() {
        
        let labelHeight: CGFloat = 20
        verifyLabel.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + (view.frame.height / 3.3), width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)
        verifyLabel.backgroundColor = .clear
        verifyLabel.isUserInteractionEnabled = false
        verifyLabel.titleLabel?.font = UIFont.Theme.size17
        verifyLabel.center.x = view.center.x
        verifyLabel.setTitleColor(hexRed.Color, for: .normal)
        verifyLabel.setBackgroundColor(color: .clear, forState: .normal)
        verifyLabel.setTitle("Verification", for: .normal)
        verifyLabel.contentVerticalAlignment = .bottom
        verifyLabel.contentHorizontalAlignment = .center
        verifyLabel.layer.zPosition = 1
        view.addSubview(verifyLabel)
        
        directionsLabel.frame = CGRect(x: globalXAxisIndent, y: verifyLabel.frame.maxY + labelHeight, width: self.view.frame.width - (4 * globalXAxisIndent), height: labelHeight)
        directionsLabel.center.x = view.center.x
        directionsLabel.backgroundColor = .clear
        directionsLabel.isUserInteractionEnabled = false
        directionsLabel.titleLabel?.font = UIFont.Theme.size15
        directionsLabel.setTitleColor(newHexBlack1.Color, for: .normal)
        directionsLabel.setBackgroundColor(color: .clear, forState: .normal)
        directionsLabel.setTitle("Please type the verification code", for: .normal)
        directionsLabel.contentVerticalAlignment = .bottom
        directionsLabel.contentHorizontalAlignment = .center
        directionsLabel.layer.zPosition = 1
        view.addSubview(directionsLabel)
        
        directionsLabel2.frame = CGRect(x: globalXAxisIndent, y: directionsLabel.frame.maxY, width: self.view.frame.width - (4 * globalXAxisIndent), height: labelHeight)
        directionsLabel2.center.x = view.center.x
        directionsLabel2.backgroundColor = .clear
        directionsLabel2.isUserInteractionEnabled = false
        directionsLabel2.titleLabel?.font = UIFont.Theme.size15
        directionsLabel2.setTitleColor(newHexBlack1.Color, for: .normal)
        directionsLabel2.setBackgroundColor(color: .clear, forState: .normal)

        var fullPhoneString: String = ""

        if let phone = userData?.Phone {
            fullPhoneString = phone.keepOnlyNumbers
        }

        if fullPhoneString.contains("+") {
        } else {
            fullPhoneString = "+\(fullPhoneString)"
        }

        let phoneNumberFormatted = PartialFormatter().formatPartial(fullPhoneString)
        phoneTest.text = phoneNumberFormatted
        directionsLabel2.setTitle("sent to \(phoneNumberFormatted)", for: .normal)
        directionsLabel2.contentVerticalAlignment = .bottom
        directionsLabel2.contentHorizontalAlignment = .center
        directionsLabel2.layer.zPosition = 1
        view.addSubview(directionsLabel2)
        
        let numWidth: CGFloat = 40
        let xWidth: CGFloat = 15
        
        let startX: CGFloat = (self.view.center.x) - (numWidth * 2.5) - (xWidth * 2)
        phone1.frame = CGRect(x: startX, y: directionsLabel2.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        phone1.keyboardType = .phonePad
        phone1.textAlignment = .center
        phone1.backgroundColor = .clear
        phone1.autocorrectionType = .no
        phone1.isSecureTextEntry = false
        phone1.contentVerticalAlignment = .center
        phone1.setBottomBorder()
        phone1.text = "0"
        phone1.layer.zPosition = 2
        phone1.tag = 1
        phone1.textColor = newHexBlack2.Color
        phone1.font = UIFont.Theme.size15
        self.view.addSubview(phone1)
        
        phone2.frame = CGRect(x: phone1.frame.maxX + xWidth, y: phone1.frame.minY, width: numWidth, height: labelHeight + 20)
        phone2.keyboardType = .phonePad
        phone2.textAlignment = .center
        phone2.backgroundColor = .clear
        phone2.autocorrectionType = .no
        phone2.isSecureTextEntry = false
        phone2.contentVerticalAlignment = .center
        phone2.setBottomBorder()
        phone2.text = "0"
        phone2.layer.zPosition = 2
        phone2.tag = 2
        phone2.textColor = newHexBlack2.Color
        phone2.font = phone1.font
        self.view.addSubview(phone2)
        
        phone3.frame = CGRect(x: phone2.frame.maxX + xWidth, y: phone1.frame.minY, width: numWidth, height: labelHeight + 20)
        phone3.keyboardType = .phonePad
        phone3.textAlignment = .center
        phone3.backgroundColor = .clear
        phone3.autocorrectionType = .no
        phone3.isSecureTextEntry = false
        phone3.contentVerticalAlignment = .center
        phone3.setBottomBorder()
        phone3.text = "0"
        phone3.layer.zPosition = 2
        phone3.tag = 3
        phone3.textColor = newHexBlack2.Color
        phone3.font = phone1.font
        self.view.addSubview(phone3)
        
        phone4.frame = CGRect(x: phone3.frame.maxX + xWidth, y: phone1.frame.minY, width: numWidth, height: labelHeight + 20)
        phone4.keyboardType = .phonePad
        phone4.textAlignment = .center
        phone4.backgroundColor = .clear
        phone4.autocorrectionType = .no
        phone4.isSecureTextEntry = false
        phone4.contentVerticalAlignment = .center
        phone4.setBottomBorder()
        phone4.text = "0"
        phone4.layer.zPosition = 2
        phone4.tag = 4
        phone4.textColor = newHexBlack2.Color
        phone4.font = phone1.font
        self.view.addSubview(phone4)

        phone5.frame = CGRect(x: phone4.frame.maxX + xWidth, y: phone1.frame.minY, width: numWidth, height: labelHeight + 20)
        phone5.keyboardType = .phonePad
        phone5.textAlignment = .center
        phone5.backgroundColor = .clear
        phone5.autocorrectionType = .no
        phone5.isSecureTextEntry = false
        phone5.contentVerticalAlignment = .center
        phone5.setBottomBorder()
        phone5.text = "0"
        phone5.layer.zPosition = 2
        phone5.tag = 5
        phone5.textColor = newHexBlack2.Color
        phone5.font = phone1.font
        self.view.addSubview(phone5)

        phone1.returnKeyType = UIReturnKeyType.next
        phone2.returnKeyType = UIReturnKeyType.next
        phone3.returnKeyType = UIReturnKeyType.next
        phone4.returnKeyType = UIReturnKeyType.next
        phone5.returnKeyType = UIReturnKeyType.done
        
        //footer text
        footerText1.frame = CGRect(x: 0, y: Int(phone1.frame.maxY + (labelHeight)), width: Int(self.view.frame.width - 100), height: 20)
        
        if isIPhoneSE == true { 
            footerText1.frame = CGRect(x: 0, y: Int(self.view.frame.height - globalLargeButtonHeight), width: Int(self.view.frame.width - 100), height: 20)
        }
        
        footerText1.textColor = newHexGray.Color
        footerText1.text = "Didn't receive the code?"
        footerText1.center.x = self.view.center.x
        footerText1.backgroundColor = .clear
        footerText1.textAlignment = .center
        footerText1.layer.zPosition = 2
        footerText1.isSelectable = false
        footerText1.isEditable = false
        footerText1.isUserInteractionEnabled = true
        footerText1.font = UIFont.Theme.size11
        
        footerText2.frame = CGRect(x: 0, y: Int(footerText1.frame.maxY) - 7, width: Int(self.view.frame.width - 100), height: 20)
        footerText2.textColor = UIColor.gray
        
        let text: NSMutableAttributedString = NSMutableAttributedString(string: "Please ")
        text.addAttribute(NSAttributedStringKey.font,
                          value: UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.bold),
                          range: NSRange(location: 0, length: text.length))
        
        let interactableText: NSMutableAttributedString = NSMutableAttributedString(string: "click here")
        
        interactableText.addAttribute(NSAttributedStringKey.link,
                                      value: "SignInPseudoLink",
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedStringKey.font,
                                      value: UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedStringKey.foregroundColor,
                                      value: hexBlue.Color,
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedStringKey.underlineColor,
                                      value: hexBlue.Color,
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedStringKey.underlineStyle,
                                      value: NSUnderlineStyle.styleSingle.rawValue,
                                      range: NSRange(location: 0, length: interactableText.length))
        
        let text2: NSMutableAttributedString = NSMutableAttributedString(string: " for a new code")
        text2.addAttribute(NSAttributedStringKey.font,
                           value: UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.bold),
                           range: NSRange(location: 0, length: text2.length))
        
        text.append(interactableText)
        text.append(text2)
        
        footerText2.attributedText = text
        footerText2.center.x = self.view.center.x
        footerText2.textColor = hexGray
        footerText2.delegate = self
        footerText2.textAlignment = .center
        footerText2.isEditable = false
        footerText2.isSelectable = true
        
        self.view.addSubview(footerText1)
        self.view.addSubview(footerText2)
        
    }
    
    //allows for web links in footText2
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("in shouldInteractWith | start")
        phone1.resignFirstResponder()
        phone2.resignFirstResponder()
        phone3.resignFirstResponder()
        phone4.resignFirstResponder()
        phone5.resignFirstResponder()

        // code to send new validation code
        requestResendVerifyConfirmationCode(self, completion: requestResendVerifyRequestHandler)
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("IN shouldChangeCharactersIn | start")
        
        let nextTag: Int = textField.tag + 1
        let newLength: Int = textField.text!.count + string.count - range.length
        let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: "0123456789").inverted
        
        let strValid: Bool = string.rangeOfCharacter(from: numberOnly) == nil
        let newString: String = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)// Convert text into NSString in order to use 'stringByReplacingCharactersInRange' function
        
        if textField != phone5 {
            if newLength == 1 && strValid == true {
                textField.text = newString
                textField.setBottomBorderThick()
                if let nextResponder = textField.superview?.viewWithTag(nextTag) {
                    nextResponder.becomeFirstResponder()
                }
                return false
            }
        } else {
            if newLength == 1 && strValid == true {
                textField.text = newString
                textField.setBottomBorderThick()
                startSubmittingCode()
                return false
            }
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    
    func startSubmittingCode() {
        print("In startSubmittingCode | start")
        phone5.becomeFirstResponder()
        phone5.setBottomBorderThick()
        if tryToValidateCode() == true {
            updateUserPhoneNumberAndDismissVC()
        } else {
            print("In startSubmittingCode | failed")
            
            validationTryCount += 1
            
            phone1.resignFirstResponder()
            phone2.resignFirstResponder()
            phone3.resignFirstResponder()
            phone4.resignFirstResponder()
            phone5.resignFirstResponder()

            if validationTryCount > 1 {
                
                animateUIBanner("Invalid Input", subText: "Something went wrong. Please try again.", color: UIWarningErrorColor)
                
                delay(1.5) {
                    self.dismissToLogin()
                }
                
            } else {
                
                phone1.setBottomBorderThickRed()
                phone2.setBottomBorderThickRed()
                phone3.setBottomBorderThickRed()
                phone4.setBottomBorderThickRed()
                phone5.setBottomBorderThickRed()

                animateIncorrectInput(buttonID: 1, localView: self.view)
                animateIncorrectInput(buttonID: 2, localView: self.view)
                animateIncorrectInput(buttonID: 3, localView: self.view)
                animateIncorrectInput(buttonID: 4, localView: self.view)
                animateIncorrectInput(buttonID: 5, localView: self.view)

                animateUIBanner("Invalid Input", subText: "Please try again", color: UIWarningErrorColor)
                
                delay(0.5) { //after buttons are animationed
                    self.phone1.text = "0"
                    self.resetPhoneInput()
                }
            }
        }
    }
    
    @objc func dismissToLogin() {
        print("in dismissToLogin | start")
        self.navigationController?.hero.navigationAnimationType = .uncover(direction: .down)
        self.hero.unwindToViewController(withClass: LoginVC.self)
    }
    
    func tryToValidateCode() -> Bool {
        print("in tryToValidateCode | start")
        
        var submittedCode = Int()
        
        if phone1.text?.count == 1 && phone2.text?.count == 1 && phone3.text?.count == 1 && phone4.text?.count == 1 && phone5.text?.count == 1 {
            submittedCode = Int(phone1.text! + phone2.text! + phone3.text! + phone4.text! + phone5.text!)!
        }
        
        if phoneNumberValidationCode == "" || phoneNumberValidationCode == "0"{
            phoneNumberValidationCode = "99999"
        }
        
        print("in tryToValidateCode | code: ", phoneNumberValidationCode)
        print("in tryToValidateCode | submittedCode: ", submittedCode)
        
        if phoneNumberValidationCode == String(submittedCode) {
            return true
        } else {
            return false
        }
    }
    
    func updateUserPhoneNumberAndDismissVC() {
        print("in updateUserPhoneNumberAndDismissVC | start")
        
        phone5.resignFirstResponder()
        //try to validate code

        userWasNotifiedOfTwoRestaurantCart = false
        defaults.set(userWasNotifiedOfTwoRestaurantCart, forKey: "userWasNotifiedOfTwoRestaurantCart")
        defaults.synchronize()

        if sourceView == .registerWC {
            tryToVerifyConfirmationCode(0, vc: self, completion: tryToVerifyConfirmationCodeHandler)
        } else if sourceView == .registerFB {
            tryToVerifyConfirmationCode(1, vc: self, completion: tryToVerifyConfirmationCodeHandler)
        }
    }
    
    func resetPhoneInput() {
        print("in resetPhoneInput | start")
        
        phone1.setBottomBorder()
        phone1.text = "0"
        phone2.setBottomBorder()
        phone2.text = "0"
        phone3.setBottomBorder()
        phone3.text = "0"
        phone4.setBottomBorder()
        phone4.text = "0"
        phone5.setBottomBorder()
        phone5.text = "0"
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        
        let phones: [SpringTextField] = [phone1, phone2, phone3, phone4, phone5]
        
        for p in phones where p.text == "" {
            p.text = "0"
        }
        
        phone1.resignFirstResponder()
        phone2.resignFirstResponder()
        phone3.resignFirstResponder()
        phone4.resignFirstResponder()
        phone5.resignFirstResponder()

    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            self.navigationController?.hero.navigationAnimationType = .uncover(direction: .down)
            self.hero.dismissViewController()
        }
    }
}
