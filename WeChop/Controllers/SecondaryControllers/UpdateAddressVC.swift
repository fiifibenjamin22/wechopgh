//
//  UpdateAddressVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import MapKit
import Spring
import EzPopup

class UpdateAddressVC: UIViewController {

    var navigationLeft = SpringButton()
    var navigationTitleBorder: SpringButton = SpringButton()
    var activeTextField: UITextField?
    var addressText: SpringButton = SpringButton()
    var addressSubText: SpringTextView = SpringTextView()
    var companyInput: SpringTextField = SpringTextField()
    var addressInput: SpringTextField = SpringTextField()
    var continueButton: SpringButton = SpringButton()

    //UI elements
    let verticalSpacing: CGFloat = 25
    let labelHeight: CGFloat = 20
    let labelFont: UIFont =  UIFont.Theme.size15
    var subLabelFont: UIFont =  UIFont.Theme.size13
    let textColorInput: UIColor = newHexBlack2.Color
    let textColor: UIColor = hexDarkGray
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN UpdateAddressVC | ViewDidLoad")
        self.view.backgroundColor = .white
        sourceView = .addressUpdate

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .zoom

        companyInput.delegate = self
        addressInput.delegate = self
        companyInput.clearButtonMode = UITextFieldViewMode.whileEditing
        addressInput.clearButtonMode = UITextFieldViewMode.whileEditing

        NotificationCenter.default.addObserver(self, selector: #selector(moveToDismissUpdateAddress), name: NSNotification.Name(rawValue: "moveToDismissUpdateAddress"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(moveToRootLoginPage), name: NSNotification.Name(rawValue: "moveToRootLoginPage"), object: nil)

        createContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }

}

// MARK: UI Elements
extension UpdateAddressVC {
    func createContent() {
        print("in create content")
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Update your Delivery Address", v: self.view, leftButton: navigationLeft)

        addressText.frame = CGRect(x: globalXAxisIndent, y: navigationTitleBorder.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        addressText.backgroundColor = .clear
        addressText.isUserInteractionEnabled = false
        addressText.titleLabel?.font = labelFont
        addressText.setTitleColor(newHexBlack2.Color, for: .normal)
        addressText.setBackgroundColor(color: .clear, forState: .normal)
        addressText.setTitle("Enter your new office address", for: .normal)
        addressText.contentVerticalAlignment = .bottom
        addressText.contentHorizontalAlignment = .left
        addressText.layer.zPosition = 1
        view.addSubview(addressText)

        addressSubText.frame = CGRect(x: globalXAxisIndent, y: addressText.frame.maxY + 10, width: self.view.frame.width, height: labelHeight)
        addressSubText.backgroundColor = .clear
        addressSubText.isUserInteractionEnabled = false
        addressSubText.textAlignment = .left
        addressSubText.font = subLabelFont
        addressSubText.text = "Your office address tells us where to deliver your lunch."
        addressSubText.textContainerInset = .zero
        addressSubText.textContainer.lineFragmentPadding = 0
        addressSubText.layer.zPosition = 1
        addressSubText.textColor = newHexGray.Color

        view.addSubview(addressSubText)

        companyInput.frame = CGRect(x: globalXAxisIndent, y: addressSubText.frame.maxY + (verticalSpacing - 10), width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        companyInput.keyboardType = .default
        companyInput.textAlignment = .left
        companyInput.backgroundColor = .clear
        companyInput.autocorrectionType = .no
        companyInput.isSecureTextEntry = false
        companyInput.contentVerticalAlignment = .bottom
        companyInput.layer.zPosition = 2
        companyInput.setBottomBorderRed()
        companyInput.text = "Company name"
        companyInput.tag = 5
        companyInput.textColor = textColorInput
        companyInput.returnKeyType = UIReturnKeyType.next
        self.view.addSubview(companyInput)

        addressInput.frame = CGRect(x: globalXAxisIndent, y: companyInput.frame.maxY + 15, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        addressInput.keyboardType = .default
        addressInput.textAlignment = .left
        addressInput.backgroundColor = .clear
        addressInput.autocorrectionType = .no
        addressInput.isSecureTextEntry = false
        addressInput.contentVerticalAlignment = .bottom
        addressInput.layer.zPosition = 2
        addressInput.setBottomBorderRed()
        addressInput.text = "Address"
        addressInput.tag = 6
        addressInput.textColor = textColorInput
        addressInput.returnKeyType = UIReturnKeyType.done
        self.view.addSubview(addressInput)

        continueButton = createFooterButton(text: "Save", v: self.view)
        continueButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        continueButton.tag = 13
        continueButton.backgroundColor = hexRed.Color
        continueButton.isHidden = false //hide until fields are valid
    }
}

// MARK: UX Elements
extension UpdateAddressVC {

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: SpringButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)

        if sender.tag == 12 {
            if sourceView != .menu {
                moveToDismissUpdateAddress()
            } else {
                moveToRootLoginPage()
            }
        } else if sender.tag == 13 {
            //continue button pressed

            if addressInput.text != "Address" && addressInput.text != "" {
                if var companyText = companyInput.text {
                    if companyText == "Company name" {
                        companyText = ""
                    }
                    setUserAddress(companyText, addressInput.text!, vc: self, completion: setUserAddressHandler)
                }
            } else {
                animateUIBanner("Warning", subText: "Please provide the missing information.", color: UIWarningInformationColor, removePending: true)
            }
        }
    }

    @objc func moveToDismissUpdateAddress() {
    self.navigationController?.hero.navigationAnimationType = .zoomOut
        self.hero.dismissViewController()
    }

    @objc func moveToRootLoginPage() {
        if currentRootView == .login {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
            UIApplication.shared.keyWindow?.rootViewController = viewController
            navigationController?.hero.navigationAnimationType = .cover(direction: .up)
            self.navigationController?.popToRootViewController(animated: true) //go back to login page
        } else {
            currentRootView = .login
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
            UIApplication.shared.keyWindow?.rootViewController = viewController
            navigationController?.hero.navigationAnimationType = .cover(direction: .up)
            self.navigationController?.popToRootViewController(animated: true) //go back to login page
        }
    }
}

extension UpdateAddressVC: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("in textFieldDidBeginEditing")
        activeTextField = textField
        textField.setBottomBorderThickRed()

        if textField == companyInput {
            if textField.text == "Company name" {
                textField.text = ""
            }
        }
        if textField == addressInput {
            if textField.text == "Address" {
                textField.text = ""
            }
        }

    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")
        activeTextField = nil
        textField.setBottomBorderRed()

        if textField == companyInput {
            if textField.text == "" {
                textField.text = "Company name"
            }
        }
        if textField == addressInput {
            if textField.text == "" {
                textField.text = "Address"
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)

        let nextTag = textField.tag + 1
        print("in textFieldShouldReturn: nextTag", nextTag)

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            continueButton.sendActions(for: .touchUpInside)
            textField.resignFirstResponder()
        }

        return false
    }
}
