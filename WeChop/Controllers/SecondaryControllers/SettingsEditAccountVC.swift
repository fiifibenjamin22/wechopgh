//
//  SettingsEditAccountVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/21/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class SettingsEditAccountVC: UIViewController, UITextFieldDelegate {
    
    @IBAction func unwindToSettings(segue: UIStoryboardSegue) { //used to unwind validate phone back 2 viewcontrollers
        //make sure to set the name for the segue in the storyboard to this func name unwindToSettings
        
        firstNameInput.resignFirstResponder()
        lastNameInput.resignFirstResponder()
        saveNewUserInformation()
        
        animateUIBanner("Success", subText: "Phone number verified.", color: UIWarningSuccessColor, removePending: true)

    }
    
    var navigationLeft: SpringButton = SpringButton() 
    var navigationTitle: SpringButton = SpringButton()
    
    var profileImage: UIImageView = UIImageView()
    var firstNameLabel: SpringButton = SpringButton()
    var firstNameInput: SpringTextField = SpringTextField()
    
    var lastNameLabel: SpringButton = SpringButton()
    var lastNameInput: SpringTextField = SpringTextField()
    
    var phoneLabel: SpringButton = SpringButton()
    var phoneInput: SpringButton = SpringButton()
    var verifiedLabel: SpringButton = SpringButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN SettingsEditAccountVC | ViewDidLoad")
        //writeToFireBase("in SettingsEditAccountVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .zoom
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitle.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 10, width: self.view.frame.width - globalXAxisIndent, height: 20)
        navigationTitle.backgroundColor = .clear
        navigationTitle.isUserInteractionEnabled = false
        navigationTitle.titleLabel?.font = UIFont.Theme.size17
        navigationTitle.setTitleColor(newHexBlack1.Color, for: .normal)
        navigationTitle.setBackgroundColor(color: .clear, forState: .normal)
        navigationTitle.setTitle("Edit Account", for: .normal)
        navigationTitle.contentVerticalAlignment = .bottom
        navigationTitle.contentHorizontalAlignment = .left
        navigationTitle.layer.zPosition = 1
        view.addSubview(navigationTitle)
        
        //dismiss keyboard when pressing enter
//        firstNameInput.delegate = self
//        lastNameInput.delegate = self
//        firstNameInput.clearButtonMode = UITextFieldViewMode.whileEditing
//        lastNameInput.clearButtonMode = UITextFieldViewMode.whileEditing
//
        
        setContent()
        recallUserInformation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
        print("in SettingsEditAccountVC will appear | start")
        
        //listen for keyboardshowing or hiding itself
//        NotificationCenter.default.addObserver(self, selector: #selector(SettingsEditAccountVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(SettingsEditAccountVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func indicatePhoneValidated() {
        print("in indicatePhoneValidated | start")
        
        animateUIBanner("Success", subText: "Phone number verified.", color: UIWarningSuccessColor, removePending: true)
    }
    
    func setContent() {
        let textColor = newHexBlack2.Color
        var columnWidth: CGFloat = self.view.frame.width - 150
        var columnHeight: CGFloat = 40
        let labelHeight: CGFloat = 20
        
        if isIPhoneSE == true { 
            columnWidth = self.view.frame.width - 125
            columnHeight = 33
        }
        
        profileImage.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 115, width: 105, height: 105)
        
        if isIPhoneX == true {
            profileImage.frame = CGRect(x: globalXAxisIndent, y: navigationTitle.frame.maxY + 30, width: 105, height: 105)
        }

        if isIPhoneSE == true {
            profileImage.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 115, width: 75, height: 75)
        }
        
        profileImage.image = UIImage(named: "user_image_icon")
        
        profileImage.contentMode =  UIViewContentMode.scaleAspectFill
        profileImage.autoresizingMask = [.flexibleHeight,
                                         .flexibleTopMargin]
        profileImage.clipsToBounds = true
        profileImage.layer.zPosition = 1
        self.view.addSubview(profileImage)
        
        let lineView = UIView(frame: CGRect(x: 0, y: profileImage.frame.maxY + 30, width: view.frame.size.width, height: 1))
        lineView.backgroundColor = hexLightGray
        view.addSubview(lineView)
        
        firstNameLabel.frame = CGRect(x: globalXAxisIndent, y: lineView.frame.maxY + 22.5, width: self.view.frame.width, height: labelHeight)
        firstNameLabel.backgroundColor = .clear
        firstNameLabel.isUserInteractionEnabled = false
        firstNameLabel.titleLabel?.font = UIFont.Theme.size15Medium
        firstNameLabel.setTitleColor(newHexGray.Color, for: .normal)
        firstNameLabel.setBackgroundColor(color: .clear, forState: .normal)
        firstNameLabel.setTitle("First Name", for: .normal)
        firstNameLabel.contentVerticalAlignment = .bottom
        firstNameLabel.contentHorizontalAlignment = .left
        firstNameLabel.layer.zPosition = 1
        view.addSubview(firstNameLabel)

        firstNameInput.frame = CGRect(x: globalXAxisIndent, y: firstNameLabel.frame.maxY - 2, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        
        firstNameInput.keyboardType = .default
        firstNameInput.textAlignment = .left
        firstNameInput.backgroundColor = .clear
        firstNameInput.autocorrectionType = .no
        firstNameInput.isSecureTextEntry = false
        firstNameInput.contentVerticalAlignment = .bottom
        firstNameInput.layer.zPosition = 2
        firstNameInput.tag = 1
        firstNameInput.textColor = newHexGray.Color
        firstNameInput.font = UIFont.Theme.size15
        firstNameInput.isUserInteractionEnabled = false
        self.view.addSubview(firstNameInput)
        
        lastNameLabel.frame = CGRect(x: globalXAxisIndent, y: firstNameInput.frame.maxY + 35, width: self.view.frame.width, height: labelHeight)
        lastNameLabel.backgroundColor = .clear
        lastNameLabel.isUserInteractionEnabled = false
        lastNameLabel.titleLabel?.font = firstNameLabel.titleLabel?.font
        lastNameLabel.setTitleColor(newHexGray.Color, for: .normal)
        lastNameLabel.setBackgroundColor(color: .clear, forState: .normal)
        lastNameLabel.setTitle("Last Name", for: .normal)
        lastNameLabel.contentVerticalAlignment = .bottom
        lastNameLabel.contentHorizontalAlignment = .left
        view.addSubview(lastNameLabel)

        lastNameInput.frame = CGRect(x: globalXAxisIndent, y: lastNameLabel.frame.maxY - 2, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        
        lastNameInput.keyboardType = .default
        lastNameInput.textAlignment = .left
        lastNameInput.backgroundColor = .clear
        lastNameInput.autocorrectionType = .no
        lastNameInput.isSecureTextEntry = false
        lastNameInput.textColor = newHexGray.Color
        lastNameInput.contentVerticalAlignment = .bottom
        lastNameInput.layer.zPosition = 2
        lastNameInput.tag = 2
        lastNameInput.font = firstNameInput.font
        lastNameInput.isUserInteractionEnabled = false
        self.view.addSubview(lastNameInput)
        
        phoneLabel.frame = CGRect(x: globalXAxisIndent, y: lastNameInput.frame.maxY + 45, width: self.view.frame.width, height: labelHeight)
        
        phoneLabel.backgroundColor = .clear
        phoneLabel.isUserInteractionEnabled = false
        phoneLabel.titleLabel?.font = firstNameLabel.titleLabel?.font
        phoneLabel.setTitleColor(newHexGray.Color, for: .normal)
        phoneLabel.setBackgroundColor(color: .clear, forState: .normal)
        phoneLabel.setTitle("Phone Number", for: .normal)
        phoneLabel.contentVerticalAlignment = .bottom
        phoneLabel.contentHorizontalAlignment = .left
        view.addSubview(phoneLabel)
        
        phoneInput.frame = CGRect(x: 0, y: phoneLabel.frame.maxY, width: self.view.frame.width, height: labelHeight + 20)
        phoneInput.contentHorizontalAlignment = .left
        phoneInput.backgroundColor = .clear
        phoneInput.contentVerticalAlignment = .center
        phoneInput.layer.zPosition = 2
        phoneInput.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        phoneInput.tag = 15
        phoneInput.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        phoneInput.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        phoneInput.setTitleColor(hexRed.Color, for: .normal)
        phoneInput.titleLabel?.font = UIFont.Theme.size15

        self.view.addSubview(phoneInput)
        
        verifiedLabel.frame = CGRect(x: self.view.frame.width - globalXAxisIndent - 70, y: lastNameInput.frame.maxY + 32, width: 70, height: globalLargeButtonHeight)
        verifiedLabel.backgroundColor = .clear
        verifiedLabel.isUserInteractionEnabled = false
        verifiedLabel.titleLabel?.font =  UIFont.Theme.size15
        verifiedLabel.setTitleColor(hexGreen.Color, for: .normal)
        verifiedLabel.setTitle("Verified", for: .normal)
        verifiedLabel.contentVerticalAlignment = .center
        verifiedLabel.layer.zPosition = 500
        verifiedLabel.contentHorizontalAlignment = .right
        view.addSubview(verifiedLabel)
    }
    
    func recallUserInformation() {
        print("IN recallUserInformation | start")
        
        if userData != nil {
            firstNameInput.text = userData?.FirstName
            lastNameInput.text = userData?.LastName
            
            let formattedPhone = PartialFormatter().formatPartial("+\((userData?.Phone)!)")
            phoneInput.setTitle(formattedPhone, for: .normal)
        }
    }
    
    func saveNewUserInformation() {
        print("IN saveNewUserInformation | start")

        if let fText = firstNameInput.text?.capitalized {
            userData?.FirstName = fText
        }
        if let lText = lastNameInput.text?.capitalized {
            userData?.LastName = lText
        }
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
//            let f: String = firstNameInput.text!
            let l: String = lastNameInput.text!
            
//            let uf: String = (userData?.FirstName)!
            let ul: String = (userData?.LastName)!
            
            if (firstNameInput.text! != (userData?.FirstName)!) || (l != ul) {
                animateUIBanner("Success", subText: "Phone number updated successfully.", color: UIWarningSuccessColor, removePending: true)
                saveNewUserInformation()
            }
            firstNameInput.resignFirstResponder()
            lastNameInput.resignFirstResponder()
            callNotificationItem("reloadUser")
            
                self.navigationController?.hero.navigationAnimationType = .zoomOut
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //edit fist name
            
        } else if sender.tag == 14 {
            //edit last name
            
        } else if sender.tag == 15 {
            //edit phone
            
//            let f: String = firstNameInput.text!
            let l: String = lastNameInput.text!

//            let uf: String = (userData?.FirstName)!
            let ul: String = (userData?.LastName)!

            if (firstNameInput.text! != (userData?.FirstName)!) || (l != ul) {
                
                animateUIBanner("Success", subText: "Phone number updated successfully.", color: UIWarningSuccessColor, removePending: true)

                saveNewUserInformation()
            }
            firstNameInput.resignFirstResponder()
            lastNameInput.resignFirstResponder()
            segue(name: "EditPhoneVCSegueFromSettings", v: self, type: .zoom)
        }
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        firstNameInput.resignFirstResponder()
        lastNameInput.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
       print("in textFieldDidEndEditing | start")
        
        if textField.text != "" {
            saveNewUserInformation()
            animateUIBanner("Success", subText: "Phone number updated successfully.", color: UIWarningSuccessColor, removePending: true)

        } else if textField.text == "" {
            print("in textFieldDidEndEditing | quote")
            recallUserInformation()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        let nextTag = textField.tag + 1

        if textField.tag == 2 {
            
        }
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        if checkInputsOkay() == true {
            // TODO save userData remotly
            
        } else {
            
        }
        return false
    }
    
    func checkInputsOkay() -> Bool {
        if firstNameInput.text == "" {
            animateIncorrectInput(buttonID: 4, localView: self.view)
            return false
        } else  if lastNameInput.text == "" {
            animateIncorrectInput(buttonID: 5, localView: self.view)
            return false
        } else {
            return true
        }
    }
}
