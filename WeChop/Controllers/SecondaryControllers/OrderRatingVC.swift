//
//  OrderRatingVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/13/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class OrderRatingVC: UIViewController, UITextViewDelegate, UIScrollViewDelegate {
    
    var navigationLeft = SpringButton()
    let scrollView: UIScrollView = UIScrollView()
    
    let star1: UIButton = UIButton()
    let star2: UIButton = UIButton()
    let star3: UIButton = UIButton()
    let star4: UIButton = UIButton()
    let star5: UIButton = UIButton()
    let rDescription: UIButton = UIButton()
    
    let cb1: CheckBox = CheckBox()
    let cb2: CheckBox = CheckBox()
    let cb3: CheckBox = CheckBox()
    let cb4: CheckBox = CheckBox()
    let cb5: CheckBox = CheckBox()
    let cb6: CheckBox = CheckBox()
    
    var feedback1: UIButton = UIButton()
    var feedback2: UIButton = UIButton()
    var feedback3: UIButton = UIButton()
    var feedback4: UIButton = UIButton()
    var feedback5: UIButton = UIButton()
    var feedback6: UIButton = UIButton()
    
    var fieldsToToggle: [UIButton] = [UIButton]()
    var cbToToggle: [CheckBox] = [CheckBox]()
    
    let feedbackText: [String] = ["Food taste", "Food packaging", "Food temperature", "Delivery time", "Order accuracy", "Other"]
    let addComments: UITextView = UITextView()
    
    var tempRating: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN OrderRatingsVC | ViewDidLoad")
//        //writeToFireBase("in OrderRatingsVC viewdidload")

        //reset user feedback object
        userCurrentOrderFeedback = emptyUserFeedback
        userCurrentOrderFeedback.Issues.removeAll()
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .up)
        
        cbToToggle = [cb1, cb2, cb3, cb4, cb5, cb6]
        fieldsToToggle = [feedback1, feedback2, feedback3, feedback4, feedback5, feedback6]
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissOrderRatingVC), name: NSNotification.Name(rawValue: "dismissOrderRatingVC"), object: nil) 
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOrderDataFromRating), name: NSNotification.Name(rawValue: "refreshOrderDataFromRating"), object: nil)

        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        addComments.delegate = self
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = false
        scrollView.backgroundColor = .clear
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 600)
        view.addSubview(scrollView)
        self.view.backgroundColor = UIColor.white
        createContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in viewdidappear | start")
        
        //listen for keyboardshowing or hiding itself
//        NotificationCenter.default.addObserver(self, selector: #selector(OrderRatingVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(OrderRatingVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//
    }
    
    func createContent() {
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        navigationLeft.setImage(UIImage(named: "close_button"), for: .normal)
        navigationLeft.frame = CGRect(x: 0, y: globalLargeButtonHeight - 15, width: 35, height: 35)
        
        if isIPhoneX == true {
            navigationLeft.frame = CGRect(x: 0, y: globalLargeButtonHeight - 15 + globalIPhoneXTopYPadding, width: 35, height: 35)
        }
        
        navigationLeft.center.x = view.center.x
        navigationLeft.removeFromSuperview()
        scrollView.addSubview(navigationLeft)
        
        let ratings: UIButton = UIButton(frame: CGRect(x: 0, y: navigationLeft.frame.maxY + 17.5, width: 100, height: 50))
        ratings.backgroundColor = .clear
        ratings.titleLabel?.font =  UIFont.Theme.size17Medium
        ratings.center.x = view.center.x
        ratings.setTitleColor(newHexBlack1.Color, for: .normal)
        ratings.setTitle("Rating", for: .normal)
        ratings.contentVerticalAlignment = .center
        ratings.contentHorizontalAlignment = .center
        ratings.isUserInteractionEnabled = false
        ratings.layer.zPosition = 10
        scrollView.addSubview(ratings)
        
        let menuSeperatorText = SpringLabel(frame: CGRect(x: 0, y: ratings.frame.maxY - 20, width: self.view.frame.width, height: globalNavIconSize))
        menuSeperatorText.text = "____"
        menuSeperatorText.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        menuSeperatorText.textAlignment = .center
        menuSeperatorText.textColor = UIColor.black
        menuSeperatorText.center.x = self.view.center.x
        menuSeperatorText.backgroundColor = .clear
        menuSeperatorText.isUserInteractionEnabled = false
        menuSeperatorText.layer.zPosition = 9
        scrollView.addSubview(menuSeperatorText)
        
        let resWas: UIButton = UIButton(frame: CGRect(x: 0, y: menuSeperatorText.frame.maxY + 20, width: view.frame.width - (2 * globalXAxisIndent), height: 15))
        resWas.backgroundColor = .clear
        resWas.titleLabel?.font =  UIFont.Theme.size15
//
//        if isIPhoneSE == true {
//            resWas.titleLabel?.font =  UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
//        }
        resWas.setTitleColor(newHexBlack1.Color, for: .normal)
        resWas.setTitle("Rate your experience with: ", for: .normal)
        resWas.sizeToFit()
        resWas.center.x = view.center.x
        resWas.contentVerticalAlignment = .center
        resWas.contentHorizontalAlignment = .center
        resWas.isUserInteractionEnabled = false
        resWas.layer.zPosition = 10
        scrollView.addSubview(resWas)

        let resWas2: UIButton = UIButton(frame: CGRect(x: 0, y: resWas.frame.maxY, width: view.frame.width - (2 * globalXAxisIndent), height: 50))
        resWas2.backgroundColor = .clear
        resWas2.titleLabel?.font =  UIFont.Theme.size15Semi
//
//        if isIPhoneSE == true {
//            resWas2.titleLabel?.font =  UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
//        }
        resWas2.setTitleColor(newHexBlack1.Color, for: .normal)
        resWas2.setTitle("\(selectedOrderItem.RestaurantName)", for: .normal)
        resWas2.sizeToFit()
        resWas2.center.x = view.center.x
        resWas2.contentVerticalAlignment = .center
        resWas2.contentHorizontalAlignment = .center
        resWas2.isUserInteractionEnabled = false
        resWas2.layer.zPosition = 10
        scrollView.addSubview(resWas2)

        let size: Int = 30
        
        let horz: UIStackView = UIStackView(frame: CGRect(x: 0, y: Int(resWas2.frame.maxY + 25), width: size * 5 + 5, height: 30))
        
        horz.backgroundColor = UIColor.gray
        horz.tintColor = UIColor.green
        horz.alignment = .center
        horz.center.x = view.center.x
        horz.axis = .horizontal
        horz.distribution = .fillEqually
        horz.spacing = 0
        horz.contentMode = .scaleToFill
        scrollView.addSubview(horz)
        
        star1.frame = CGRect(x: 0, y: 0, width: size, height: size)
        star1.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
        star1.contentMode = .scaleAspectFit
        horz.addArrangedSubview(star1)
        
        star2.frame = CGRect(x: 0, y: 0, width: size, height: size)
        star2.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
        star2.contentMode = star1.contentMode
        
        horz.addArrangedSubview(star2)
        
        star3.frame = CGRect(x: 0, y: 0, width: size, height: size)
        star3.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
        star3.contentMode = star1.contentMode
        
        horz.addArrangedSubview(star3)
        
        star4.frame = CGRect(x: 0, y: 0, width: size, height: size)
        star4.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
        horz.addArrangedSubview(star4)
        star4.contentMode = star1.contentMode
        
        star5.frame = CGRect(x: 0, y: 0, width: size, height: size)
        star5.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
        horz.addArrangedSubview(star5)
        star5.contentMode = star1.contentMode
        
        star1.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        star2.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        star3.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        star4.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        star5.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        star1.tag = 21
        star2.tag = 22
        star3.tag = 23
        star4.tag = 24
        star5.tag = 25

        formatHeaderStars(stars: userRatingSelectedOnOrderPage + 20)

        rDescription.frame = CGRect(x: 0, y: horz.frame.maxY, width: view.frame.width, height: 50)
        rDescription.backgroundColor = .clear
        rDescription.titleLabel?.font =  UIFont.Theme.size15
        rDescription.center.x = view.center.x
        rDescription.setTitleColor(hexRed.Color, for: .normal)
        rDescription.setTitle("", for: .normal) // TODO update to grab selected order information
        rDescription.contentVerticalAlignment = .center
        rDescription.contentHorizontalAlignment = .center
        rDescription.isUserInteractionEnabled = false
        rDescription.layer.zPosition = 10
        scrollView.addSubview(rDescription)
        
        let fHeight: CGFloat = 31
        let cbSize: CGFloat = 25
        let cbXAxis: CGFloat = globalXAxisIndent
        
        cb1.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb1.tag = 31
        scrollView.addSubview(cb1)
        
        cb2.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb2.tag = 32
        scrollView.addSubview(cb2)
        
        cb3.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb3.tag = 33
        scrollView.addSubview(cb3)
        
        cb4.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb4.tag = 34
        scrollView.addSubview(cb4)
        
        cb5.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb5.tag = 35
        scrollView.addSubview(cb5)
        
        cb6.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cb6.tag = 36
        scrollView.addSubview(cb6)
        
        feedback1.frame = CGRect(x: cb1.frame.maxX + 10, y: rDescription.frame.maxY + 5, width: 200, height: fHeight)
        feedback1.backgroundColor = .clear
        feedback1.titleLabel?.font =  UIFont.Theme.size15
        feedback1.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback1.setTitle(feedbackText[0], for: .normal)
        feedback1.contentVerticalAlignment = .center
        feedback1.contentHorizontalAlignment = .left
        feedback1.isUserInteractionEnabled = true
        feedback1.layer.zPosition = 10
        feedback1.tag = 31
        feedback1.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        scrollView.addSubview(feedback1)
        
        feedback2.frame = CGRect(x: cb2.frame.maxX + 10, y: feedback1.frame.maxY, width: view.frame.width, height: fHeight)
        feedback2.backgroundColor = .clear
        feedback2.titleLabel?.font =  feedback1.titleLabel?.font
        feedback2.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback2.setTitle(feedbackText[1], for: .normal)
        feedback2.contentVerticalAlignment = .center
        feedback2.contentHorizontalAlignment = .left
        feedback2.isUserInteractionEnabled = true
        feedback2.layer.zPosition = 10
        feedback2.tag = 32
        feedback2.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(feedback2)
        
        feedback3.frame = CGRect(x: cb3.frame.maxX + 10, y: feedback2.frame.maxY, width: view.frame.width, height: fHeight)
        feedback3.backgroundColor = .clear
        feedback3.titleLabel?.font =  feedback1.titleLabel?.font
        feedback3.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback3.setTitle(feedbackText[2], for: .normal)
        feedback3.contentVerticalAlignment = .center
        feedback3.contentHorizontalAlignment = .left
        feedback3.isUserInteractionEnabled = true
        feedback3.layer.zPosition = 10
        feedback3.tag = 33
        feedback3.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(feedback3)
        
        feedback4.frame = CGRect(x: cb4.frame.maxX + 10, y: feedback3.frame.maxY, width: view.frame.width, height: fHeight)
        feedback4.backgroundColor = .clear
        feedback4.titleLabel?.font =  feedback1.titleLabel?.font
        feedback4.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback4.setTitle(feedbackText[3], for: .normal)
        feedback4.contentVerticalAlignment = .center
        feedback4.contentHorizontalAlignment = .left
        feedback4.isUserInteractionEnabled = true
        feedback4.layer.zPosition = 10
        feedback4.tag = 34
        feedback4.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(feedback4)
        
        feedback5.frame = CGRect(x: cb5.frame.maxX + 10, y: feedback4.frame.maxY, width: view.frame.width, height: fHeight)
        feedback5.backgroundColor = .clear
        feedback5.titleLabel?.font = feedback1.titleLabel?.font
        feedback5.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback5.setTitle(feedbackText[4], for: .normal)
        feedback5.contentVerticalAlignment = .center
        feedback5.contentHorizontalAlignment = .left
        feedback5.isUserInteractionEnabled = true
        feedback5.layer.zPosition = 10
        feedback5.tag = 35
        feedback5.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(feedback5)
        
        feedback6.frame = CGRect(x: cb6.frame.maxX + 10, y: feedback5.frame.maxY, width: view.frame.width, height: fHeight)
        feedback6.backgroundColor = .clear
        feedback6.titleLabel?.font =  feedback1.titleLabel?.font
        feedback6.setTitleColor(newHexBlack1.Color, for: .normal)
        feedback6.setTitle(feedbackText[5], for: .normal)
        feedback6.contentVerticalAlignment = .center
        feedback6.contentHorizontalAlignment = .left
        feedback6.isUserInteractionEnabled = true
        feedback6.layer.zPosition = 10
        feedback6.tag = 36
        feedback6.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(feedback6)
        
        cb1.center.y = feedback1.center.y
        cb2.center.y = feedback2.center.y
        cb3.center.y = feedback3.center.y
        cb4.center.y = feedback4.center.y
        cb5.center.y = feedback5.center.y
        cb6.center.y = feedback6.center.y
        
        cb1.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cb2.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cb3.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cb4.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cb5.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        cb6.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        cb1.isChecked = false
        cb2.isChecked = false
        cb3.isChecked = false
        cb4.isChecked = false
        cb5.isChecked = false
        cb6.isChecked = false
        
        addComments.frame = CGRect(x: 0, y: feedback6.frame.maxY + 10, width: self.view.frame.width - (2 * cbXAxis), height: 75)
        addComments.center.x = view.center.x
        addComments.textAlignment = .left
        addComments.font = UIFont.Theme.size13
        addComments.textColor = newHexGray.Color
        addComments.text = "Additional Comments"
        addComments.layer.borderColor = hexLightGray.cgColor
        addComments.layer.borderWidth = 1
        addComments.contentInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        addComments.backgroundColor  = .clear
        addComments.isScrollEnabled = true
        addComments.returnKeyType = UIReturnKeyType.done

        scrollView.addSubview(addComments)
        
        let orderButton: SpringButton = createFooterButton(text: "Submit", v: self.view)
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.setBackgroundColor(color: newHexBlack2.Color, forState: .normal)
        
        orderButton.frame = CGRect(x: orderButton.frame.minX, y: orderButton.frame.minY, width: self.view.frame.width - (globalXAxisIndent * 2), height: globalLargeButtonHeight)
        orderButton.center.x = view.center.x
        orderButton.setBackgroundColor(color: hexGray, forState: .highlighted)

        scrollView.contentSize = CGSize(width: self.view.frame.width, height: addComments.frame.maxY + 200)
        
    }
    
    func collectFeedback() {
        
        userCurrentOrderFeedback.Score = String(tempRating)
        let checkButtons = [cb1, cb2, cb3, cb4, cb5, cb6]
        
        for button in checkButtons where button.isChecked == true {
                if button.tag == 31 {
                userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "0"))
                } else if button.tag == 32 {
                    userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "1"))
                } else if button.tag == 33 {
                    userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "2"))
                } else if button.tag == 34 {
                    userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "3"))
                } else if button.tag == 35 {
                    userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "4"))
                } else if button.tag == 36 {
                    userCurrentOrderFeedback.Issues.append(FeedbackIssues(issueNumber: "5"))
                }
        }
        
        if addComments.text != "" && addComments.text != "Additional Comments" {
            userCurrentOrderFeedback.Comment = addComments.text
        }
        
        print("feedback: ")
//        dump(userCurrentOrderFeedback)
    }
    
    func toggleFeedback(action: String) {
        if action == "show" {
            rDescription.isHidden = false
            for item in cbToToggle {
                item.isHidden = false
            }
            for item in fieldsToToggle {
                item.isHidden = false
            }
            addComments.isHidden = false
        } else {
            rDescription.isHidden = true
            for item in cbToToggle {
                item.isHidden = true
            }
            
            for item in fieldsToToggle {
                item.isHidden = true
            }
            addComments.isHidden = true
        }
    }
    
    func formatHeaderStars(stars: Int) {
        print("in formatHeaderStars | stars: ", stars)
        switch stars {
        case 21:
            star1.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star2.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star3.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star4.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star5.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            rDescription.setTitle("Terrible. Issues with", for: .normal) // TODO update to grab selected order information
            toggleFeedback(action: "show")
            tempRating = 1
        case 22:
            star1.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star2.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star3.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star4.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star5.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            rDescription.setTitle("Pretty bad. Issues with", for: .normal) // TODO update to grab selected order information
            toggleFeedback(action: "show")
            tempRating = 2
            
        case 23:
            star1.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star2.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star3.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star4.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            star5.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            rDescription.setTitle("Decent, but had an issue", for: .normal) // TODO update to grab selected order information
            toggleFeedback(action: "show")
            tempRating = 3
            
        case 24:
            star1.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star2.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star3.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star4.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star5.setImage(UIImage(named: "star_pinkOutline"), for: .normal)
            rDescription.setTitle("Pretty good, but had an issue", for: .normal) // TODO update to grab selected order information
            toggleFeedback(action: "show")
            tempRating = 4
            
        case 25:
            star1.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star2.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star3.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star4.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            star5.setImage(UIImage(named: "star_pinkFilled"), for: .normal)
            rDescription.setTitle(" ", for: .normal) // TODO update to grab selected order information
            toggleFeedback(action: "hide")
            tempRating = 5
            
        default:
            print("in default check for stars")
        }

        userRatingSelectedOnOrderPage = 0 //reset Order Screen Stars

    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Additional Comments" {
            textView.text = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("in textFieldShouldReturn | start")
        addComments.resignFirstResponder()
        return false
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    @objc func refreshOrderDataFromRating() {
        print("in refreshOrderDataFromRating | start")
        getUserCompletedOrders(self)// called when an order rating is updated
    }
    
    @objc func dismissOrderRatingVC() {

        self.navigationController?.hero.navigationAnimationType = .zoomOut
        self.hero.dismissViewController()
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        addComments.resignFirstResponder()
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag > 30 {
            
            if sender is CheckBox {
                toggleCheck(tag: sender.tag)
            } else {
                toggleCheck(tag: sender.tag)
            }
        } else if sender.tag > 20 && sender.tag < 30 {
            formatHeaderStars(stars: sender.tag)
        }
        
        if sender.tag == 12 {
            
            dismissOrderRatingVC()
        } else if sender.tag == 13 {
            //submit feedback
            
            addComments.resignFirstResponder()
            
            if tempRating > 0 {
                collectFeedback()
                
                //post to backend
                setOrderRating(selectedOrderItem.OrderID, feedback: userCurrentOrderFeedback, vc: self, completion: setOrderRatingHandler)
                
            } else {
                
                animateUIBanner("Invalid input", subText: "Please rate the order", color: UIWarningErrorColor)

            }
            
        }
    }
    
    func toggleCheck(tag: Int) {
        if let b =  view.viewWithTag(tag) as? CheckBox {
            b.isChecked.negate()
        }
    }
}
