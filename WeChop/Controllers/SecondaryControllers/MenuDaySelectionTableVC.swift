//
//  MenuDaySelectionTableVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/10/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

//
//  MenuDetails.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import Foundation
import UIKit
import Spring
import NVActivityIndicatorView
import Hero
import SwiftDate

class MenuDaySelectionTableVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    var navigationTitleBorder: SpringButton = SpringButton()
    let scrollView: UIScrollView = UIScrollView()
    let dayButtonHeight: Int = 61
    var lastLoc: CGFloat = 50 //used to create list of days buttons
    let borderSize: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN MenuDaySelectionTableVC | ViewDidLoad")
//        //writeToFireBase("in MenuDaySelectionTableVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .down)
        self.view.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(recreateMenu), name: NSNotification.Name(rawValue: "recreateMenu"), object: nil)

        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Select a day", v: self.view, leftButton: navigationLeft)
        
        //scrollView
        scrollView.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: self.view.frame.height)
        
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = true
        scrollView.alwaysBounceHorizontal = false
        scrollView.delegate = self
        scrollView.backgroundColor = .clear
        
        let sHeight = ((CGFloat(dayButtonHeight) + borderSize) * 7) + CGFloat(dayButtonHeight) / 2
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: sHeight)
        view.addSubview(scrollView)
        
        //create day buttons
        var tagNum = 1
        lastLoc = 0
        
        refreshAllDateInformation()
        var currentStartDate = currentUserTime
        
        if currentUserTime.weekdayName(.default) == "Saturday" {
            currentStartDate = currentStartDate + 2.days
        } else if currentUserTime.weekdayName(.default) == "Sunday" {
            currentStartDate = currentStartDate + 1.days
        }
        
        for _ in 1...7 {
            lastLoc = createDayButton(date: currentStartDate, lastLocation: lastLoc, tag: tagNum)
            tagNum += 1
            currentStartDate = currentStartDate + 1.days
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    @objc func recreateMenu() {
        print("in recreateMenu | start")
        callNotificationItem("refreshPage")
        self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .up)
        self.hero.dismissViewController()
    }
    
    func createDayButton(date: DateInRegion, lastLocation: CGFloat, tag: Int) -> CGFloat {
        let button = DaySelectionButton(frame: CGRect(x: 0, y: lastLocation, width: self.view.frame.width, height: CGFloat(dayButtonHeight)))
        button.tag = tag
        button.buttonDate = date
        button.setTitle("\(date.weekdayName(.default)), \(date.day)\(getDaySuffix(dayOfMonth: date.day)) \(date.monthName(.short))", for: .normal)
        button.contentVerticalAlignment = .center
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        button.isUserInteractionEnabled = true
        button.setBackgroundColor(color: UIColor.white, forState: .normal)
        button.setBackgroundColor(color: hexGray.withAlphaComponent(0.1), forState: .highlighted)
        button.setTitleColor(hexYellow.Color, for: .normal)
        if (date.weekdayName(.default) == "Saturday" || date.weekdayName(.default) == "Sunday") || (isCurrentTimeAfterTodaysCutoffTime == true && date.weekdayName(.default) == currentUserTime.weekdayName(.default) ) { //gray out past dates or weekends
            button.setTitleColor(hexLightGray, for: .normal)
            button.isUserInteractionEnabled = false
        }
        button.addTarget(self, action: #selector(dateButtonPressed), for: .touchUpInside)
        scrollView.addSubview(button)
        let buttonBorder = UIButton(frame: CGRect(x: 0, y: button.frame.maxY, width: self.view.frame.width, height: borderSize))
        buttonBorder.backgroundColor = hexLightGray
        buttonBorder.center.x = view.center.x
        buttonBorder.isUserInteractionEnabled = false
        buttonBorder.layer.zPosition = 3
        scrollView.addSubview(buttonBorder)
        return button.frame.maxY + borderSize
    }
    
    //Nav buttons pressed actions
    @objc func dateButtonPressed(_ sender: DaySelectionButton) {
        print("Date selected: ", sender.buttonDate.date)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag != 12 { //if any button besides the navigation back button is pressed
            currentlySelectedMenuDay = sender.buttonDate
        }

        if currentlySelectedMenuDay.weekdayName(.default) != currentUserTime.weekdayName(.default) {

            //didSelectMenuFromSelectionTable = true
            comingFrom.menuDaySelectionPressed = true
            callNotificationItem("refreshPage")
            globalGetUserMenu(self, useSelectedDate: true, fromLogin: false)
        } else {
           dismissVC()
        }
    }
    
    @objc func buttonPressed(_ sender: SpringButton) {
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        dismissVC()
    }
    
    func dismissVC() {
        callNotificationItem("refreshPage")
        self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .up)
        self.hero.dismissViewController()
    }
}
