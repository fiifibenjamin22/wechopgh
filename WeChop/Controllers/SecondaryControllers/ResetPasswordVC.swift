//
//  ResetPasswordVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 10/11/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import IQKeyboardManagerSwift
import FBSDKLoginKit
import MapKit
import Crashlytics

class ResetPasswordVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var navigationLeft: SpringButton = SpringButton()
    //    var navigationTitleBorder: SpringButton = SpringButton()
    
    var activeTextField: UITextField?
    var helloText: SpringButton = SpringButton()
    var nameText: SpringButton = SpringButton()
    var nameImage: SpringButton = SpringButton()
    var nameInput1: SpringTextField = SpringTextField()
    var nameInput2: SpringTextField = SpringTextField()
    var phoneText: SpringButton = SpringButton()
    var phoneImage: SpringButton = SpringButton()
    var phoneInputPre: SpringTextField = SpringTextField()
    var phoneInput: SpringTextField = SpringTextField()
    var continueButton: SpringButton = SpringButton()
    var createAccountButton: SpringButton = SpringButton()
    
    //UI elements
    let verticalSpacing: CGFloat = 40
    let labelHeight: CGFloat = 20
    let imageSize: Int = 25
    
    var comingFromTransitionRegister: Bool = false //used to ensure keyboard view resize doesn't occur during segue or seuge dismiss
    
    let phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN ResetPasswordVC | ViewDidLoad")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .push(direction: .left)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        //        navigationTitleBorder = createNavigationTitle(title: "Reset Password", v: self.view, leftButton: navigationLeft)
        
        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        phoneInput.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
        nameInput1.delegate = self
        nameInput2.delegate = self
        phoneInputPre.delegate = self
        phoneInput.delegate = self
        
        nameInput1.clearButtonMode = UITextFieldViewMode.whileEditing
        nameInput2.clearButtonMode = UITextFieldViewMode.whileEditing
        phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
        
        createFormFieldsUI()
        
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        print("done pressed")
        
        if checkForDefaultValues() {
            continueButton.isHidden = false
            continueButton.sendActions(for: .touchUpInside)
        } else {
            continueButton.isHidden = true
        }
    }
    
    //    @objc func moveToResetVerificationVC() {
    //        segue(name: "ResetPasswordNewPassworVCSegueFromReset", v: self, type: .zoom)
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
        
        print("in viewWillAppear | start")
        comingFromTransitionRegister = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in viewdidappear | start")
        
        delay(0.2) {
            self.comingFromTransitionRegister = false
        }
    }
    
    func createFormFieldsUI() {
        let textColorInput: UIColor = newHexBlack2.Color
        let textColor: UIColor = hexDarkGray
        let labelFont: UIFont =  UIFont.Theme.size15
        
        helloText.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + (verticalSpacing * 2.4), width: self.view.frame.width, height: labelHeight)
        helloText.backgroundColor = .clear
        helloText.isUserInteractionEnabled = false
        helloText.titleLabel?.font = labelFont
        helloText.setTitleColor(textColor, for: .normal)
        helloText.setBackgroundColor(color: .clear, forState: .normal)
        helloText.setTitle("Reset Your Password:", for: .normal)
        helloText.contentVerticalAlignment = .bottom
        helloText.contentHorizontalAlignment = .left
        helloText.layer.zPosition = 1
        view.addSubview(helloText)
        
        nameText.frame = CGRect(x: globalXAxisIndent, y: helloText.frame.maxY + (verticalSpacing), width: self.view.frame.width, height: labelHeight)
        nameText.backgroundColor = .clear
        nameText.isUserInteractionEnabled = false
        nameText.titleLabel?.font = labelFont
        nameText.setTitleColor(newHexBlack2.Color, for: .normal)
        nameText.setBackgroundColor(color: .clear, forState: .normal)
        nameText.setTitle("What's your name?", for: .normal)
        nameText.contentVerticalAlignment = .bottom
        nameText.contentHorizontalAlignment = .left
        nameText.layer.zPosition = 1
        view.addSubview(nameText)
        
        let nameWidth = view.center.x - globalXAxisIndent
        
        nameInput1.frame = CGRect(x: globalXAxisIndent, y: nameText.frame.maxY + 5, width: nameWidth - 10, height: labelHeight + 10)
        
        print("frame: ", nameInput1.frame )
        nameInput1.keyboardType = .default
        nameInput1.textAlignment = .left
        nameInput1.backgroundColor = .clear
        nameInput1.autocorrectionType = .no
        nameInput1.isSecureTextEntry = false
        nameInput1.setBottomBorderRed()
        nameInput1.contentVerticalAlignment = .bottom
        nameInput1.layer.zPosition = 2
        nameInput1.tag = 21
        nameInput1.textColor = textColorInput
        nameInput1.text = "First name"
        nameInput1.font = UIFont.Theme.size15
        nameInput1.returnKeyType = UIReturnKeyType.next
        
        nameInput1.setLeftPaddingPoints(globalXAxisIndent + 5)
        self.view.addSubview(nameInput1)
        
        nameImage.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        nameImage.center.y = nameInput1.frame.maxY - 10
        nameImage.layer.zPosition = 3
        nameImage.tag = 4
        nameImage.setImage(UIImage(named: "signupForm_name_icon"), for: .normal)
        nameImage.backgroundColor = .clear
        nameImage.isUserInteractionEnabled = false
        self.view.addSubview(nameImage)
        
        nameInput2.frame = CGRect(x: self.view.frame.width / 2, y: nameInput1.frame.minY, width: nameWidth, height: labelHeight + 10)
        nameInput2.keyboardType = .default
        nameInput2.textAlignment = .left
        nameInput2.backgroundColor = UIColor.yellow
        nameInput2.autocorrectionType = .no
        nameInput2.isSecureTextEntry = false
        nameInput2.setBottomBorderRed()
        nameInput2.contentVerticalAlignment = .bottom
        nameInput2.layer.zPosition = 2
        nameInput2.tag = 22
        nameInput2.textColor = textColorInput
        nameInput2.text = "Last name"
        nameInput2.font = nameInput1.font
        nameInput2.returnKeyType = UIReturnKeyType.next
        self.view.addSubview(nameInput2)
        
        phoneText.frame = CGRect(x: globalXAxisIndent, y: nameInput1.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        phoneText.backgroundColor = .clear
        phoneText.isUserInteractionEnabled = false
        phoneText.titleLabel?.font = labelFont
        phoneText.setTitleColor(newHexBlack2.Color, for: .normal)
        phoneText.setBackgroundColor(color: .clear, forState: .normal)
        phoneText.setTitle("And your phone number", for: .normal)
        phoneText.contentVerticalAlignment = .bottom
        phoneText.contentHorizontalAlignment = .left
        phoneText.layer.zPosition = 1
        
        view.addSubview(phoneText)

        phoneInputPre.frame = CGRect(x: globalXAxisIndent, y: phoneText.frame.maxY + 5, width: 65, height: labelHeight + 10)
        phoneInputPre.keyboardType = .numbersAndPunctuation
        phoneInputPre.textAlignment = .left
        phoneInputPre.textColor = textColorInput
        phoneInputPre.text = "+233" // need to validate on geolocate before setting this
        phoneInputPre.contentVerticalAlignment = .bottom
        phoneInputPre.backgroundColor = .clear
        phoneInputPre.autocorrectionType = .no
        phoneInputPre.setBottomBorderRed()
        phoneInputPre.layer.zPosition = 2
        phoneInputPre.tag = 24
        phoneInputPre.font = nameInput1.font
        phoneInputPre.isSecureTextEntry = false
        phoneInputPre.returnKeyType = UIReturnKeyType.next
        phoneInputPre.setLeftPaddingPoints(globalXAxisIndent + 5)
        phoneInputPre.contentVerticalAlignment = .bottom

        phoneInputPre.font = nameInput1.font
        self.view.addSubview(phoneInputPre)

        phoneInput.frame = CGRect(x: phoneInputPre.frame.maxX + 10, y: phoneText.frame.maxY + 5, width: self.view.frame.width - (globalXAxisIndent + phoneInputPre.frame.maxX + 10), height: labelHeight + 10)
        phoneInput.keyboardType = .numbersAndPunctuation
        phoneInput.textAlignment = .left
        phoneInput.backgroundColor = .clear
        phoneInput.autocorrectionType = .no
        phoneInput.isSecureTextEntry = false
        phoneInput.contentVerticalAlignment = .bottom
        phoneInput.layer.zPosition = 2
        phoneInput.setBottomBorderRed()
        phoneInput.text = ""
        phoneInput.tag = 23
        phoneInput.textColor = textColorInput
        phoneInput.returnKeyType = UIReturnKeyType.done
//        phoneInput.setLeftPaddingPoints()
        phoneImage.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        phoneImage.center.y = phoneInput.frame.maxY - 10
        phoneImage.layer.zPosition = 3
        phoneImage.backgroundColor = .clear
        phoneImage.isUserInteractionEnabled = false
        phoneImage.tag = 6
        phoneImage.setImage(UIImage(named: "signupForm_phone_icon"), for: .normal)
        self.view.addSubview(phoneImage)
        
        phoneInput.font = nameInput1.font
        self.view.addSubview(phoneInput)
        
        createAccountButton = createFooterButton(text: "Create a new account", v: self.view)
        createAccountButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        createAccountButton.isHidden = false //hide until fields are valid
        createAccountButton.setTitleColor(textColorInput, for: .normal)
        createAccountButton.titleLabel?.font = nameInput1.font
        createAccountButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted  )
        createAccountButton.backgroundColor = .clear
        createAccountButton.tag = 14
        createAccountButton.frame.origin.y = self.view.frame.height - createAccountButton.frame.height
        
        continueButton = createFooterButton(text: "Next", v: self.view)
        continueButton.frame.origin.y = createAccountButton.frame.minY - createAccountButton.frame.height
        continueButton.backgroundColor = hexRed.Color
        continueButton.titleLabel?.font = UIFont.Theme.size17
        continueButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        continueButton.isHidden = false //hide until fields are valid
        
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //done button pressed
            print("in donebuttonpressed")
            nameInput1.resignFirstResponder()
            nameInput2.resignFirstResponder()
            phoneInput.resignFirstResponder()
            
            if checkForDefaultValues() {
                saveNewUserInformation()
                segue(name: "ResetPasswordNewPassworVCSegueFromReset", v: self, type: .push(direction: .left))
            }  else {
            }
            
        }  else if sender.tag == 14 {
            
            print("should be dismissing??")
            //craete new account preessed
            
            loginVCIsUserInLoginAreaAndNotInRegisterArea = false
            callNotificationItem("moveToRegister")
            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
            self.hero.dismissViewController()
            
        }
    }
    
    func saveNewUserInformation() {
        print("IN saveNewUserInformation | start")
        
        if nameInput1.text != "" && nameInput1.text != "First name" {
            userData?.FirstName = nameInput1.text!
        }
        
        if nameInput2.text != "" && nameInput1.text != "Last name" {
            userData?.LastName = nameInput2.text!
        }

        if phoneInput.text != "" {
            userData?.Phone = ("\(phoneInputPre.text!)\(phoneInput.text!)").keepOnlyNumbers
        }

        userData?.LocaleCode = userCurrentLocaleCode
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        print("in dismiss keyboard")
        nameInput1.resignFirstResponder()
        nameInput2.resignFirstResponder()
        phoneInput.resignFirstResponder()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let text = textField.text else { return true }
        let range2: UITextRange? = textField.selectedTextRange

        let newLength: Int = textField.text!.count + string.count - range.length
        //        let numberOnly = NSCharacterSet.init(charactersIn: "+0123456789 ").inverted
        let lettersOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
        
        if textField == nameInput1 {
            let strValid1 = string.rangeOfCharacter(from: lettersOnly) == nil
            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
            if newLength < 50 && strValid1 == true {
                textField.text = newString
                return false
            }
        } else  if textField == nameInput2 {
            let strValid1 = string.rangeOfCharacter(from: lettersOnly) == nil
            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
            if newLength < 50 && strValid1 == true {
                textField.text = newString
                return false
            }
        }  else if textField == phoneInputPre {
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == phoneInput {
            guard NSCharacterSet(charactersIn: "0123456789+ ()").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            formatPhoneNumber(location: range2!)
            return true
        }
        return false
    }

    //format and validate inputs
    func formatPhoneNumber(location: UITextRange) { //format full phone number
        print("in formatPhoneNumber | start")

        var fullPhoneString = ("\(phoneInputPre.text!)\(phoneInput.text!)").keepOnlyNumbers
        fullPhoneString = "+\(fullPhoneString)"
        let phoneNumberFormatted = PartialFormatter().formatPartial(fullPhoneString)
        phoneTest.text = phoneNumberFormatted

        if phoneNumberFormatted.contains(" ") {
            let prefixCount = phoneNumberFormatted.components(separatedBy: " ")[0].count
            phoneInputPre.text = phoneNumberFormatted.components(separatedBy: " ")[0]
            phoneInput.text = String(phoneNumberFormatted.dropFirst(prefixCount))
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == phoneInput {
            if textField.text!.contains("+233") {
                phoneInput.text = "+233"
                return false
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("in textFieldDidBeginEditing")
        
        activeTextField = textField

        if textField == phoneInput || textField == phoneInputPre {
            phoneInput.setBottomBorderThickRed()
            phoneInputPre.setBottomBorderThickRed()
        } else {
            textField.setBottomBorderThickRed()
        }
        if textField == nameInput1 {
            if textField.text == "First name" {
                textField.text = ""
            }
        }
        
        if textField == nameInput2 {
            if textField.text == "Last name" {
                textField.text = ""
            }
        }
        
        if textField == phoneInput {
            if textField.text == "Phone number" {
                textField.text = "+233 "
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")
        activeTextField = nil
        textField.setBottomBorderRed()

        if textField == nameInput1 {
            if textField.text == "" {
                textField.text = "First name"
                
            }
        }
        
        if textField == nameInput2 {
            if textField.text == "" {
                textField.text = "Last name"
            }
        }
        
        let range: UITextRange? = textField.selectedTextRange
        if textField == phoneInputPre {
            phoneInput.setBottomBorderRed()
            phoneInputPre.setBottomBorderRed()
            if textField.text == "+" || textField.text == "" {
                textField.text = "+233"
            }
            if textField.text?.range(of: "+") == nil {
                let value: String = textField.text!
                textField.text = "+\(value)"
            }
        }

        if textField == phoneInput {
            phoneInput.setBottomBorderRed()
            phoneInputPre.setBottomBorderRed()
        }

        formatPhoneNumber(location: range!)
        validateAllFormFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        print("in textFieldShouldReturn")

        let nextTag = textField.tag + 1
        print("in textFieldShouldReturn: nextTag", nextTag)

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            if nextTag != 24 {
                nextResponder.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        } else {
            textField.resignFirstResponder()
        }
        
        if textField == phoneInput {
            if checkForDefaultValues() {
                continueButton.isHidden = false
                continueButton.sendActions(for: .touchUpInside)
            } else {
                continueButton.isHidden = true
            }
        }
        
        return false
    }
    
    func validateAllFormFields() {
        
    }
    
    func checkForDefaultValues() -> Bool {
        
        phoneTest.text = phoneInput.text
        
        if nameInput1.text != "Fist name" && nameInput1.text != "" {
            if nameInput2.text != "Last name" && nameInput2.text != "" {
                if phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInputPre.text != "+" {
                    return true
                }
            }
        }
        
        if  nameInput1.text == "First name" || nameInput1.text == "" || nameInput2.text == "Last name" || nameInput2.text == "" {
            animateIncorrectInputs(field: "name")
        }
        
        if  phoneInput.text == "Phone number" || phoneInput.text == "" || phoneInputPre.text == "+" {
            animateIncorrectInputs(field: "phone")
        }
        
        return false
    }
    
    func animateIncorrectInputs(field: String) {
        
        if field == "all" {
            animateIncorrectInput(buttonID: 21, localView: self.view)
            animateIncorrectInput(buttonID: 22, localView: self.view)
            animateIncorrectInput(buttonID: 23, localView: self.view)
            animateIncorrectInput(buttonID: 24, localView: self.view)
            animateIncorrectButton(buttonID: 4, localView: self.view)
            animateIncorrectButton(buttonID: 5, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            
            animateUIBanner("Warning", subText: "Please provide the missing information.", color: UIWarningInformationColor, removePending: true)
            
        } else if field == "name" {
            animateIncorrectInput(buttonID: 21, localView: self.view)
            animateIncorrectInput(buttonID: 22, localView: self.view)
            
            animateIncorrectButton(buttonID: 4, localView: self.view)
            animateUIBanner("Warning", subText: "Incorrect first and last name.", color: UIWarningErrorColor, removePending: true)
        } else if field == "phone" {
            animateIncorrectInput(buttonID: 23, localView: self.view)
            animateIncorrectInput(buttonID: 24, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            
            animateUIBanner("Warning", subText: "Incorrect phone number.", color: UIWarningErrorColor, removePending: true)
        }
    }
}
