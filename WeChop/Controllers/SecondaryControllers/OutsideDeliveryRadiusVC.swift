//
//  outsideDeliveryRadiusVCClass.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/23/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import EzPopup

class OutsideDeliveryRadiusVC: UIViewController {
    
    let image = UIImageView(image: UIImage(named: "coming soon_image"))
    let text: UITextView = UITextView()
    var footerText2: UIButton = UIButton()
    
    let yourAttributes: [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.5),
        NSAttributedStringKey.foregroundColor: hexBlue.Color,
        NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
    
    let yourAttributesSE: [NSAttributedStringKey: Any] = [
        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10),
        NSAttributedStringKey.foregroundColor: hexBlue.Color,
        NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .fade
        self.navigationController?.hero.navigationAnimationType = .fade

//        NotificationCenter.default.addObserver(self, selector: #selector(dismissVC), name: NSNotification.Name(rawValue: "OutsideDeliveryDismissVC"), object: nil)

        print("IN OutsideDeliveryRadiusVC | ViewDidLoad")
        view.backgroundColor = UIColor.white
        
        image.frame = CGRect(x: 0, y: view.frame.height / 10, width: view.frame.width - 50, height: image.frame.width)
        image.contentMode =  UIViewContentMode.scaleAspectFit
        image.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
        image.center.x = view.center.x
        image.layer.zPosition = 1
        view.addSubview(image)
        
        text.frame = CGRect(x: 0, y: image.frame.maxY - 20, width: self.view.frame.width - (2 * globalXAxisIndent), height: 130)
        
        text.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular)
        text.textColor = hexDarkGray.withAlphaComponent(0.6)
        text.text = "Welcome! \nWe're coming soon to your area. Until then, invite your coworkers to download and you will get 5 GHS for every download and order!"
        
        if userData?.FirstName != nil {
            let name: String = (userData?.FirstName)!
            text.text = "Welcome \(name)! \nWe're coming soon to your area. Until then, invite your coworkers to download and you will get 5 GHS for every download and order!"
        }
        text.backgroundColor  = .clear
        text.isScrollEnabled = true
        text.isEditable = false
        text.layer.zPosition = 10
        text.isUserInteractionEnabled = false
//        text.sizeToFit()
        text.contentInset = UIEdgeInsets (top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        text.center.x = view.center.x
        text.textAlignment = .center
        view.addSubview(text)
        
        let orderButton = createFooterButton(text: "INVITE", v: self.view)
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.frame = CGRect(x: 0, y: text.frame.maxY + 20, width: 100, height: globalLargeButtonHeight - 5)
        orderButton.center.x = view.center.x
        orderButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        orderButton.setBackgroundColor(color: newHexBlack2.Color, forState: .normal)

        orderButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        print("image: ", image.frame)
        
        footerText2.frame = CGRect(x: 0, y: self.view.frame.height - 55, width: view.frame.width, height: 50)
        footerText2.center.x = self.view.center.x
        footerText2.backgroundColor = .clear
        footerText2.contentVerticalAlignment = .center
        footerText2.layer.zPosition = 1
        footerText2.tag = 14
        footerText2.setBackgroundColor(color: .clear, forState: .normal)
        footerText2.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        let attributeString = NSMutableAttributedString(string: "Update offical address",
                                                        attributes: yourAttributes)
        footerText2.setAttributedTitle(attributeString, for: .normal)
        footerText2.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        self.view.addSubview(footerText2)
        
        if isIPhoneSE == true { 
            image.frame = CGRect(x: 0, y: view.frame.height / 12, width: view.frame.width - 50, height: image.frame.width)
            image.center.x = view.center.x
            
            text.frame = CGRect(x: 0, y: image.frame.maxY, width: self.view.frame.width - (2 * globalXAxisIndent), height: 110)
            text.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
            text.center.x = view.center.x

            orderButton.frame = CGRect(x: 0, y: text.frame.maxY + 20, width: 100, height: globalLargeButtonHeight - 10)
            orderButton.center.x = view.center.x
            orderButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
            
            let attributeString = NSMutableAttributedString(string: "Update offical address",
                                                            attributes: yourAttributesSE)
            footerText2.setAttributedTitle(attributeString, for: .normal)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("in outside deliver viewWillAppear | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }

//    func dismissVC() {
//        if let address = userData?.AddressSupported {
//            if address == true {
//                //reload and go to menu
//                callNotificationItem("refreshPage")
//
//                self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
//                self.hero.dismissViewController()
//            }
//        }
//    }

    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 13 {
            print("invite button pressed")

            //shouldShowShareDialogueOnLoad = true
            comingFrom.inviteShowInviteDialogue = true

            let contentVC = InviteVC()
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
            present(popupVC, animated: true)
            
            //showShareDialogue(vc: self, button: sender)
        } else if sender.tag == 14 {
            print("address pressed")
            if sourceView == .addressUpdate {
                
                callNotificationItem("toggleMenuNotAvaiableUIAction")
                self.navigationController?.hero.navigationAnimationType = .fade
                self.hero.dismissViewController()
            } else {
                // pop to update address from login?

                let contentVC = AddressVC()
                let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
                present(popupVC, animated: true)
                
//                segue(name: "AddressVCSegueFromOutsideDelivery", v: self, type: .zoom)
            }
        }
    }
}
