//
//  RegisterPasswordVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/26/18.
//  Copyright © 2019 WeChop. All rights reserved.

import UIKit
import Spring

import NVActivityIndicatorView
import Alamofire

import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit

class RegisterPasswordVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    
    var helloText: SpringButton = SpringButton()
    var phoneText: SpringButton = SpringButton()
    var psw1Input: SpringTextField = SpringTextField()
    var psw2Input: SpringTextField = SpringTextField()
    var psw1Label: SpringTextView = SpringTextView()
    var psw2Label: SpringTextView = SpringTextView()
    var pswImage1: SpringButton = SpringButton()
    var pswImage2: SpringButton = SpringButton()
    var continueButton: SpringButton = SpringButton()
    
    let padding: CGFloat = 25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN RegisterPasswordVC | ViewDidLoad")
        //        //writeToFireBase("in RegisterPasswordVC viewdidload")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .zoom
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToValidatePhoneVC), name: NSNotification.Name(rawValue: "triggerSegueToValidatePhoneWeChop"), object: nil) 
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        psw1Input.delegate = self
        psw2Input.delegate = self
        psw1Input.clearButtonMode = UITextFieldViewMode.whileEditing
        psw2Input.clearButtonMode = UITextFieldViewMode.whileEditing
        
        createFormFieldsUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        print("in viewDidAppear | start")
        
        //listen for keyboardshowing or hiding itself
        //        NotificationCenter.default.addObserver(self, selector: #selector(RegisterPasswordVC.registerPasswordVCkeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(RegisterPasswordVC.registerPasswordVCkeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    @objc func moveToValidatePhoneVC() {
        
        segue(name: "RegisterVerificationVCSegueFromRegisterPassword", v: self, type: .zoom)
    }
    
    func createFormFieldsUI() {
        
        let verticalSpacing: CGFloat = 20
        let textColorInput: UIColor = newHexBlack2.Color
        let textColor: UIColor = hexDarkGray
        let labelHeight: CGFloat = 20
        let labelFont: UIFont =  UIFont.Theme.size15
        let subLabelFont: UIFont =  UIFont.Theme.size13
        
        helloText.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + (verticalSpacing * 6), width: self.view.frame.width, height: labelHeight)
        helloText.backgroundColor = .clear
        helloText.isUserInteractionEnabled = false
        helloText.titleLabel?.font = subLabelFont
        helloText.setTitleColor(hexRed.Color, for: .normal)
        helloText.setBackgroundColor(color: .clear, forState: .normal)
        helloText.setTitle("Phone verified.", for: .normal)
        helloText.contentVerticalAlignment = .bottom
        helloText.contentHorizontalAlignment = .left
        helloText.layer.zPosition = 1
//        view.addSubview(helloText)
        
        phoneText.frame = CGRect(x: globalXAxisIndent, y: helloText.frame.maxY + verticalSpacing, width: self.view.frame.width, height: labelHeight)
        phoneText.backgroundColor = .clear
        phoneText.isUserInteractionEnabled = false
        phoneText.titleLabel?.font =  UIFont.Theme.size15
        phoneText.setTitleColor(newHexBlack2.Color, for: .normal)
        phoneText.setBackgroundColor(color: .clear, forState: .normal)
        phoneText.setTitle("Select a password", for: .normal)
        phoneText.contentVerticalAlignment = .bottom
        phoneText.contentHorizontalAlignment = .left
        phoneText.layer.zPosition = 1
        view.addSubview(phoneText)

        psw1Input.frame = CGRect(x: globalXAxisIndent, y: phoneText.frame.maxY + verticalSpacing, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        psw1Input.keyboardType = .default
        psw1Input.textAlignment = .left
        psw1Input.backgroundColor = .clear
        psw1Input.autocorrectionType = .no
        psw1Input.autocapitalizationType = .none
        psw1Input.spellCheckingType = .no
        psw1Input.isSecureTextEntry = true
        psw1Input.contentVerticalAlignment = .bottom
        psw1Input.layer.zPosition = 2
        psw1Input.setBottomBorder()
        psw1Input.text = ""
        psw1Input.tag = 3
        psw1Input.textColor = textColorInput
        psw1Input.returnKeyType = UIReturnKeyType.next
        psw1Input.font = UIFont.Theme.size15
        psw1Input.setLeftPaddingPoints(globalXAxisIndent + 10)
        view.addSubview(psw1Input)
        
        let imageSize: Int = 25
        
        pswImage1.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        pswImage1.center.y = psw1Input.frame.maxY - 13
        pswImage1.layer.zPosition = 3
        pswImage1.tag = 30
        pswImage1.setImage(UIImage(named: "password_lock")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
        pswImage1.tintColor = hexRed.Color
        pswImage1.backgroundColor = .clear
        pswImage1.isUserInteractionEnabled = false
        self.view.addSubview(pswImage1)
        
        psw2Input.frame = CGRect(x: globalXAxisIndent, y: psw1Input.frame.maxY + verticalSpacing, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        psw2Input.keyboardType = .default
        psw2Input.textAlignment = .left
        psw2Input.backgroundColor = .clear
        psw2Input.autocorrectionType = .no
        psw2Input.autocapitalizationType = .none
        psw2Input.spellCheckingType = .no
        psw2Input.isSecureTextEntry = true
        psw2Input.contentVerticalAlignment = .bottom
        psw2Input.layer.zPosition = 2
        psw2Input.setBottomBorder()
        psw2Input.text = ""
        psw2Input.tag = 4
        psw2Input.textColor = textColorInput
        psw2Input.setLeftPaddingPoints(globalXAxisIndent + 10)
        psw2Input.returnKeyType = UIReturnKeyType.done
        psw2Input.font = UIFont.Theme.size15
        view.addSubview(psw2Input)
        
        pswImage2.frame = CGRect(x: Int(globalXAxisIndent - 5), y: 0, width: imageSize, height: imageSize)
        pswImage2.center.y = psw2Input.frame.maxY - 13
        pswImage2.layer.zPosition = 3
        pswImage2.tag = 40
        pswImage2.setImage(UIImage(named: "password_lock")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
        pswImage2.tintColor = hexRed.Color
        pswImage2.backgroundColor = .clear
        pswImage2.isUserInteractionEnabled = false
        self.view.addSubview(pswImage2)
        
        psw1Label.frame = psw1Input.frame
        psw1Label.isSelectable = false
        psw1Label.font = psw1Input.font
        psw1Label.text = "Create a password"
        psw1Label.textAlignment = .left
        psw1Label.textColor = textColorInput
        psw1Label.backgroundColor = .clear
        psw1Label.isUserInteractionEnabled = false
        psw1Label.layer.zPosition = psw1Input.layer.zPosition + 1
        psw1Label.contentInset = UIEdgeInsets(top: 0, left: globalXAxisIndent + 5, bottom: 0, right: 0)
        view.addSubview(psw1Label)
        
        psw2Label.frame = psw2Input.frame
        psw2Label.backgroundColor = .clear
        psw2Label.font = psw2Input.font
        psw2Label.textAlignment = .left
        psw2Label.isSelectable = false
        psw2Label.contentInset = UIEdgeInsets(top: 0, left: globalXAxisIndent + 5, bottom: 0, right: 0)
        psw2Label.text = "Re-type password"
        psw2Label.textColor = textColorInput
        psw2Label.layer.zPosition = psw2Input.layer.zPosition + 1
        psw2Label.isUserInteractionEnabled = false
        view.addSubview(psw2Label)
        
        toggleLabelFields()
        continueButton = createFooterButton(text: "Done", v: self.view)
        continueButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        continueButton.isHidden = true //hide until fields are valid
        continueButton.backgroundColor = hexRed.Color
        continueButton.titleLabel?.font = UIFont.Theme.size17

    }
    
    func toggleImages() {
        
        if psw1Input.text != "" && psw2Input.text != "" && (psw1Input.text?.count)! > 6 && (psw2Input.text?.count)! > 6 && psw1Input.text == psw2Input.text {
            
            pswImage1.setImage(UIImage(named: "settings_checkmark")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
            pswImage2.setImage(UIImage(named: "settings_checkmark")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
            
            pswImage1.tintColor = hexGreen.Color
            pswImage2.tintColor = hexGreen.Color
        } else {
            
            pswImage1.setImage(UIImage(named: "password_lock")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
            pswImage2.setImage(UIImage(named: "password_lock")?.addImagePadding(x: padding, y: padding).withRenderingMode(.alwaysTemplate), for: .normal)
            
            pswImage1.tintColor = hexRed.Color
            pswImage2.tintColor = hexRed.Color
            
        }
    }
    
    func toggleLabelFields() {
        //        print("in toggleLabelFields | start")
        if psw1Input.text == "" {
            psw1Label.text = "Create a password"
        } else {
            psw1Label.text = ""
        }
        
        if psw2Input.text == "" {
            psw2Label.text = "Re-type password"
        } else {
            psw2Label.text = ""
        }
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //continue button pressed
            if checkForDefaultValues() {
                saveNewUserInformation()
                
                psw1Input.resignFirstResponder()
                psw2Input.resignFirstResponder()
                tryWCRegister(1234, vc: self, completion: tryWCRegisterHandler)
            }
            
        }
    }
    
    func saveNewUserInformation() {
        print("IN saveNewUserInformation | start")
        userRegistrationPassword = psw1Input.text!
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        print("in password screen tapped")
        psw1Input.resignFirstResponder()
        psw2Input.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        toggleImages()
        toggleLabelFields()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        toggleLabelFields()
        toggleImages()
        print("in textFieldDidBeginEditing")
        if textField == psw1Input {
            if textField.text == "Create a password" {
                textField.text = ""
            }
        }
        
        if textField == psw2Input {
            if textField.text == "Re-type password" {
                textField.text = ""
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")
        validateAllFormFields()
        toggleImages()
        toggleLabelFields()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        print("in shouldreturn | start")
        toggleLabelFields()
        
        let nextTag = textField.tag + 1
        print("in shouldreturn | nextTag", nextTag)
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        if checkForDefaultValues() {
            continueButton.isHidden = false
            if textField == psw2Input {
                continueButton.sendActions(for: .touchUpInside)
            }
        } else {
            continueButton.isHidden = true
        }
        
        return false
    }
    
    func validateAllFormFields() {
        
        if checkForDefaultValues() {
            continueButton.isHidden = false
        } else {
            continueButton.isHidden = true
        } 
    }
    
    func checkForDefaultValues() -> Bool {
        if psw1Input.text != "Create a password" && psw1Input.text != "" {
            if psw2Input.text != "Re-type password" && psw2Input.text != "" {
                
                if psw1Input.text == psw2Input.text {
                    
                    if (psw1Input.text?.count)! > 6 {
                        return true
                    } else {
                        
                        animateUIBanner("Invalid Input", subText: "The password should have at least 8 characters.", color: UIWarningInformationColor)
                    }
                } else {
                    
                    animateUIBanner("Invalid Input", subText: "Passwords do not match", color: UIWarningErrorColor)
                }
            }
        }
        return false
    }
}
