//
//  PaymentEditMobileMoney.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 10/25/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring

import NVActivityIndicatorView
import Alamofire

import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import MapKit

class PaymentEditMobileMoneyVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var navigationLeft = SpringButton()
    var navigationTitleBorder = SpringButton()
    var activeTextField: UITextField?
    var networkImage: SpringButton = SpringButton()
    var networkInput: SpringTextField = SpringTextField()
    var phoneImage: SpringButton = SpringButton()
    var phoneInput: SpringTextField = SpringTextField()
    var continueButton: SpringButton = SpringButton()
    
    //UI elements
    let verticalSpacing: CGFloat = 40
    let labelHeight: CGFloat = 20
    let imageSize: Int = 25
    let textColorInput: UIColor = newHexBlack2.Color
    let textColor: UIColor = hexDarkGray
    let inputPadding: CGFloat = 15

    let phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        print("IN PaymentEditMobileMoneyVC | ViewDidLoad")
        //writeToFireBase("in PaymentEditMobileMoneyVC viewdidload")
        
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .push(direction: .left)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Mobile Money", v: self.view, leftButton: navigationLeft)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(moveToRegisterVerificationVC), name: NSNotification.Name(rawValue: "moveToRegisterVerificationVC"), object: nil)//used when facebook register completes successfully
        
        //dismiss keyboard on tap outside field
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        networkInput.delegate = self
        phoneInput.delegate = self
        networkInput.clearButtonMode = UITextFieldViewMode.whileEditing
        phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
        
        createContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("in viewWillAppear | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in viewdidappear | start")
    }
    
    func createContent() {

        networkInput.frame = CGRect(x: globalXAxisIndent, y: navigationTitleBorder.frame.maxY + (verticalSpacing * 1.5) - 5, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        networkInput.keyboardType = .default
        networkInput.textAlignment = .left
        networkInput.backgroundColor = .clear
        networkInput.autocorrectionType = .no
        networkInput.isSecureTextEntry = false
        networkInput.setBottomBorder()
        networkInput.contentVerticalAlignment = .bottom
        networkInput.layer.zPosition = 2
        networkInput.tag = 1
        networkInput.textColor = textColorInput
        networkInput.text = "Network Provider"
        networkInput.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.light)
        networkInput.returnKeyType = UIReturnKeyType.next
        networkInput.setLeftPaddingPoints(globalXAxisIndent + inputPadding)
        self.view.addSubview(networkInput)
        
        networkImage.frame = CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize)
        networkImage.center.y = networkInput.frame.maxY - 10
        networkImage.layer.zPosition = 3
        networkImage.tag = 4
        networkImage.setImage(UIImage(named: "mm_edit"), for: .normal)
        networkImage.backgroundColor = .clear
        networkImage.isUserInteractionEnabled = false
        self.view.addSubview(networkImage)
        
        phoneInput.frame = CGRect(x: globalXAxisIndent, y: networkInput.frame.maxY + verticalSpacing, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight + 10)
        phoneInput.keyboardType = .numbersAndPunctuation
        phoneInput.textAlignment = .left
        phoneInput.backgroundColor = .clear
        phoneInput.autocorrectionType = .no
        phoneInput.isSecureTextEntry = false
        phoneInput.contentVerticalAlignment = .bottom
        phoneInput.layer.zPosition = 2
        phoneInput.setBottomBorder()
        phoneInput.text = "Phone number"
        phoneInput.tag = 3
        phoneInput.textColor = textColorInput
        phoneInput.returnKeyType = UIReturnKeyType.done
        phoneInput.setLeftPaddingPoints(globalXAxisIndent + inputPadding)
        
        phoneImage.frame = CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize)
        phoneImage.center.y = phoneInput.frame.maxY - 10
        phoneImage.layer.zPosition = 3
        phoneImage.backgroundColor = .clear
        phoneImage.isUserInteractionEnabled = false
        phoneImage.tag = 6
        phoneImage.setImage(UIImage(named: "signupForm_phone_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        phoneImage.tintColor = hexDarkGray
        self.view.addSubview(phoneImage)
        
        phoneInput.font = networkInput.font
        self.view.addSubview(phoneInput)
        
        continueButton = createFooterButton(text: "SAVE", v: self.view)
        continueButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        continueButton.backgroundColor = hexBlack.Color
        continueButton.isHidden = false
        
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            self.navigationController?.hero.navigationAnimationType = .autoReverse(presenting: .pull(direction: .left))
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            //continue button pressed
            networkInput.resignFirstResponder()
            phoneInput.resignFirstResponder()
            
            if checkForDefaultValues() {
                saveNewUserInformation()
            } else {
                print("check default failed")
            }
            
        }
    }

    func saveNewUserInformation() {
        print("IN saveNewUserInformation | start")
        
        if networkInput.text != "" {
            
        }
        
        if phoneInput.text != "" {
            
        }
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        networkInput.resignFirstResponder()
        phoneInput.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength: Int = textField.text!.count + string.count - range.length
        let numberOnly = NSCharacterSet.init(charactersIn: "+0123456789 ").inverted
        
        if textField == networkInput {
            let strValid1 = string.rangeOfCharacter(from: numberOnly) == nil
            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
            if newLength < 50 && strValid1 == true {
                textField.text = newString
                return false
            }
        } else if textField == phoneInput {
//            let strValid = string.rangeOfCharacter(from: numberOnly) == nil
            let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)
            textField.text = PartialFormatter().formatPartial(newString)
            return false
        }
        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("in textFieldDidBeginEditing")
        
        activeTextField = textField
        textField.textColor = textColor
        
        if textField == networkInput {
            if textField.text == "Network Provider" {
                textField.text = ""
            }
        }
        
        if textField == phoneInput {
            if textField.text == "Phone number" {
                textField.text = "+233"
            }
        }
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == phoneInput {
            if textField.text!.contains("+233") {
                phoneInput.text = "+233"
                return false
            }
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")
        
        textField.textColor = textColorInput

        activeTextField = nil
        
        if textField == networkInput {
            if textField.text == "" {
                textField.text = "Network Provider"
            }
        }
        
        if textField == phoneInput {
            if textField.text == "" || textField.text == "+233" {
                textField.text = "Phone number"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        continueButton.sendActions(for: .touchUpInside)
        return false
    }
    
    func checkForDefaultValues() -> Bool {
        phoneTest.text = phoneInput.text
            if networkInput.text != "Network Provider" && networkInput.text != "" {
                if phoneInput.text != "Phone number" && phoneInput.text != "" && phoneInput.text != "+" {
                    //&& phoneTest.isValidNumber != false {
                    return true
                }
            }
        return false
    }
    
    func animateIncorrectInputs(field: String) {
        
        if field == "all" {
            animateIncorrectInput(buttonID: 1, localView: self.view)
            animateIncorrectInput(buttonID: 2, localView: self.view)
            animateIncorrectInput(buttonID: 3, localView: self.view)
            animateIncorrectButton(buttonID: 4, localView: self.view)
            animateIncorrectButton(buttonID: 5, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            
            animateUIBanner("Invalid Input", subText: "Please try again", color: UIWarningErrorColor)
            
        } else if field == "Network Provider" {
            animateIncorrectInput(buttonID: 1, localView: self.view)
            animateIncorrectButton(buttonID: 4, localView: self.view)
            
            animateUIBanner("Invalid Input", subText: "Incorrect first and last name", color: UIWarningErrorColor)
            
        } else if field == "address" {
            animateIncorrectInput(buttonID: 2, localView: self.view)
            animateIncorrectButton(buttonID: 5, localView: self.view)
            
            animateUIBanner("Invalid Input", subText: "Incorrect address", color: UIWarningErrorColor)
            
        } else if field == "phone" {
            animateIncorrectInput(buttonID: 3, localView: self.view)
            animateIncorrectButton(buttonID: 6, localView: self.view)
            
            animateUIBanner("Invalid Input", subText: "Incorrect phone number", color: UIWarningErrorColor)
            
        }
    }
}
