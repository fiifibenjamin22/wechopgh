//
//  SettingsEditPhoneVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/21/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class SettingsEditPhoneVC: UIViewController, UITextFieldDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    var phoneLabel: SpringButton = SpringButton()
    var orderButton: SpringButton = SpringButton()
    var phoneInputPre: PhoneNumberTextField = PhoneNumberTextField()
    var phoneInput: PhoneNumberTextField = PhoneNumberTextField()
    var notValidLabel: SpringButton = SpringButton()
    var validateCheck: SpringButton = SpringButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN SettingsEditPhoneVC | ViewDidLoad")
        //writeToFireBase("in SettingsEditPhoneVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .zoom
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToValidatePhoneVC), name: NSNotification.Name(rawValue: "moveToValidatePhoneVC"), object: nil) 
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        orderButton = createFooterButton(text: "Save", v: self.view)
        orderButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        orderButton.setBackgroundColor(color: hexRed.Color, forState: .normal)
        orderButton.setBackgroundColor(color: hexGray, forState: .highlighted)
        //dismiss keyboard when pressing enter
        phoneInput.delegate = self
        phoneInputPre.delegate = self

        phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
        createContent()
        setPreviousPhoneNumber()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    func createContent() {

        guard (userData?.Phone != nil) else { return }

        let textColor = newHexBlack2.Color
        var columnWidth: CGFloat = self.view.frame.width - 150
        var columnHeight: CGFloat = 40
        let labelHeight: CGFloat = 20
        
        if isIPhoneSE == true {
            columnWidth = self.view.frame.width - 125
            columnHeight = 33
        }
        
        phoneLabel.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 95, width: self.view.frame.width, height: labelHeight)
        phoneLabel.backgroundColor = .clear
        phoneLabel.isUserInteractionEnabled = false
        phoneLabel.titleLabel?.font = UIFont.Theme.size15Medium
        phoneLabel.setTitleColor(newHexGray.Color, for: .normal)
        phoneLabel.setBackgroundColor(color: .clear, forState: .normal)
        phoneLabel.setTitle("Phone", for: .normal)
        phoneLabel.contentVerticalAlignment = .bottom
        phoneLabel.contentHorizontalAlignment = .left
        phoneLabel.layer.zPosition = 1
        view.addSubview(phoneLabel)

        phoneInputPre.frame = CGRect(x: globalXAxisIndent, y: phoneLabel.frame.maxY + 20, width: 45, height: labelHeight + 20)
        phoneInputPre.keyboardType = .numbersAndPunctuation
        phoneInputPre.textAlignment = .left
        phoneInputPre.text = "+233" // need to validate on geolocate before setting this
        phoneInputPre.contentVerticalAlignment = .bottom
        phoneInputPre.backgroundColor = .clear
        phoneInputPre.autocorrectionType = .no
        phoneInputPre.contentVerticalAlignment = .center
        phoneInputPre.setBottomBorderThick()
        phoneInputPre.layer.zPosition = 2
        phoneInputPre.tag = 3
        phoneInputPre.textColor = newHexBlack2.Color
        phoneInputPre.font = UIFont.Theme.size15
        phoneInputPre.isSecureTextEntry = false
        phoneInputPre.returnKeyType = UIReturnKeyType.next
        self.view.addSubview(phoneInputPre)

        phoneInput.frame = CGRect(x: phoneInputPre.frame.maxX + 10, y: phoneLabel.frame.maxY + 20, width: self.view.frame.width - (10 + phoneInputPre.frame.maxX + globalXAxisIndent), height: labelHeight + 20)
        phoneInput.keyboardType = .numbersAndPunctuation
        phoneInput.textAlignment = .left
        phoneInput.backgroundColor = .clear
        phoneInput.autocorrectionType = .no
        phoneInput.isSecureTextEntry = false
        phoneInput.contentVerticalAlignment = .center
        phoneInput.setBottomBorderThick()
        phoneInput.layer.zPosition = 2
        phoneInput.tag = 4
        phoneInput.textColor = newHexBlack2.Color
        phoneInput.font = UIFont.Theme.size15
        
        phoneInput.defaultRegion = "GH"
        phoneInput.returnKeyType = UIReturnKeyType.done
        phoneInput.withPrefix = true
        
        self.view.addSubview(phoneInput)
        
        notValidLabel.frame = CGRect(x: globalXAxisIndent, y: phoneInput.frame.maxY + 10, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)
        notValidLabel.backgroundColor = .clear
        notValidLabel.isUserInteractionEnabled = false
        notValidLabel.titleLabel?.font =  UIFont.Theme.size15
        notValidLabel.setTitleColor(hexRedDark.Color, for: .normal)
        notValidLabel.setTitle("Enter a valid phone number", for: .normal)
        notValidLabel.contentVerticalAlignment = .center
        notValidLabel.layer.zPosition = 501
        notValidLabel.contentHorizontalAlignment = .left
        notValidLabel.isHidden = true
//        view.addSubview(notValidLabel)

        //validateCheck
        
        validateCheck.frame = CGRect(x: view.frame.width - globalXAxisIndent - 25, y: 0, width: 25, height: 25)
        validateCheck.setImage(UIImage(named: "settings_checkmark")?.addImagePadding(x: 0, y: 0), for: .normal)
        validateCheck.center.y = phoneInput.center.y
        validateCheck.contentVerticalAlignment = .center
        validateCheck.contentHorizontalAlignment = .center
        validateCheck.backgroundColor = .clear
        validateCheck.isHidden = true
        validateCheck.layer.zPosition = 100
        view.addSubview(validateCheck)
        
//        orderButton.frame.origin.y = notValidLabel.frame.maxY + 50
        
    }
    
    func setPreviousPhoneNumber() {
        if userData != nil {
            if let fullNumber = userData?.Phone {
                let formattedPhone = PartialFormatter().formatPartial("+\(fullNumber)")
                phoneInputPre.text = String(formattedPhone.split(separator: " ").first!)
                phoneInput.text = formattedPhone.replacingOccurrences(of: phoneInputPre.text!, with: "")
            }

        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("IN shouldChangeCharactersIn | start")

        guard let text = textField.text else { return true }

//        let newLength: Int = textField.text!.count + string.count - range.length
//        let numberOnly = NSCharacterSet.init(charactersIn: "+0123456789").inverted
//        let strValid = string.rangeOfCharacter(from: numberOnly) == nil
//        let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)// Convert text into NSString in order to use 'stringByReplacingCharactersInRange' function

        if textField == phoneInputPre {
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }  else if textField == phoneInput {
            guard NSCharacterSet(charactersIn: "0123456789+ ()").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            return true
        }

//
//        if newLength < 19 && strValid == true {
//            if newString.prefix(4) == "+233" {
//                if newLength < 17 && strValid == true {
//                    textField.text = newString
//                    return false
//                }
//            } else {
//                textField.text = newString
//                return false
//            }
//        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("in textFieldDidBeginEditing | start")

        if textField == phoneInputPre {
            if textField.text == "" {
                textField.text = "+233"
            }
        }

        notValidLabel.isHidden = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("in textFieldDidEndEditing | start")
        let validationResults = tryToValidateCode()
        if validationResults == true {
            print("in textFieldDidEndEditing | success")
        } else {
            print("in textFieldDidEndEditing | failed")
        }
        textField.textColor = hexDarkGray
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("in textFieldShouldReturn | start")
        self.view.endEditing(true)
        startSubmittingCode()
        return false
    }
    
    func startSubmittingCode() {
        print("in startSubmittingCode | start")
        
        // todo validate number and update new phone number locally and remotly
        if tryToValidateCode() == true {
            phoneNumberToValidate = ("\(phoneInputPre.text!)\(phoneInput.text!)").keepOnlyNumbers

            self.validateCheck.isHidden = true
            self.phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
            self.phoneInput.resignFirstResponder()
            
            setUserPhoneNumber(phoneNumberToValidate, vc: self, completion: setUserPhoneNumberHandler)
            
        } else {
            phoneInput.resignFirstResponder()
            //            animateIncorrectInput(buttonID: 4, localView: self.view)
            
            delay(0.5) { //after buttons are animationed
                self.phoneInput.becomeFirstResponder()
            }
        }
    }
    
    func tryToValidateCode() -> Bool {
        print("in tryToValidateCode | start")
        
//        if phoneInput.isValidNumber == false {
//            notValidLabel.isHidden = false
//            print("in tryToValidateCode | false")
//            validateCheck.isHidden = true
//            phoneInput.clearButtonMode = UITextFieldViewMode.whileEditing
//            return false
//        } else {
            print("in tryToValidateCode | true")
            validateCheck.isHidden = false
//            notValidLabel.isHidden = true
            phoneInput.clearButtonMode = UITextFieldViewMode.never
            return true
//        }
    }

    @objc func moveToValidatePhoneVC() {
        print("in moveToValidatePhoneVC | start")
        
        segue(name: "ValidatePhoneVCSegueFromSettings", v: self, type: .zoomOut)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        phoneInput.resignFirstResponder()
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            
            //            callNotificationItem("reloadTables")

            self.navigationController?.hero.navigationAnimationType = .zoomOut
            self.hero.dismissViewController()
        } else if sender.tag == 13 {
            startSubmittingCode()
        }
    }
}
