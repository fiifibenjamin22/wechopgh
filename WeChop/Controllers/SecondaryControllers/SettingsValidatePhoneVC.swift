//
//  SettingsValidatePhoneVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/21/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate

class SettingsValidatePhoneVC: UIViewController, UITextFieldDelegate {
    @IBAction func unwindToSettings(unwindSegue: UIStoryboardSegue) {
        
        if unwindSegue.source is SettingsValidatePhoneVC {
            print("Coming from BLUE")
        }
        else if unwindSegue.source is SettingsEditAccountVC {
            print("Coming from RED")
        }
    }
    
    var navigationLeft: SpringButton = SpringButton() 
    var directionsLabel: SpringButton = SpringButton()
    var phoneNumberLabel: SpringButton = SpringButton()
    
    var phone1: SpringTextField = SpringTextField()
    var phone2: SpringTextField = SpringTextField()
    var phone3: SpringTextField = SpringTextField()
    var phone4: SpringTextField = SpringTextField()
    var phone5: SpringTextField = SpringTextField()

    var resendCodeButton: SpringButton = SpringButton()
    var phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid

    var phoneIsValid = false //used to indicate that code matches expected server code
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN SettingsValidatePhoneVC | ViewDidLoad")
        //writeToFireBase("in SettingsValidatePhoneVC viewdidload")

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .fade
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserPhoneNumberAndDismissVC), name: NSNotification.Name(rawValue: "updateUserPhoneNumberAndDismissVC"), object: nil) 
        
        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //dismiss keyboard when pressing enter
        phone1.delegate = self
        phone2.delegate = self
        phone3.delegate = self
        phone4.delegate = self
        phone5.delegate = self

        createContent()
        
        self.phone1.becomeFirstResponder() //make first field active
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.phone1.becomeFirstResponder() //make first field active
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    func createContent() {
        
        let labelHeight: CGFloat = 20

        directionsLabel.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 70, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)

        if isIPhoneX == true {
            directionsLabel.frame = CGRect(x: globalXAxisIndent, y: navigationLeft.frame.maxY + 100, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)
        }

        directionsLabel.backgroundColor = .clear
        directionsLabel.isUserInteractionEnabled = false
        directionsLabel.titleLabel?.font = UIFont.Theme.size15
        directionsLabel.setTitleColor(newHexBlack2.Color, for: .normal)
        directionsLabel.setBackgroundColor(color: .clear, forState: .normal)
        directionsLabel.setTitle("Enter the 4-digit code send to", for: .normal)
        directionsLabel.contentVerticalAlignment = .bottom
        directionsLabel.contentHorizontalAlignment = .left
        directionsLabel.layer.zPosition = 1
        view.addSubview(directionsLabel)
        
        phoneNumberLabel.frame = CGRect(x: globalXAxisIndent, y: directionsLabel.frame.maxY + 12.5, width: self.view.frame.width - (2 * globalXAxisIndent), height: labelHeight)
        phoneNumberLabel.backgroundColor = .clear
        phoneNumberLabel.isUserInteractionEnabled = false
        phoneNumberLabel.titleLabel?.font = UIFont.Theme.size15
        phoneNumberLabel.setTitleColor(newHexBlack2.Color, for: .normal)
        phoneNumberLabel.setBackgroundColor(color: .clear, forState: .normal)
        let formattedNumber = PartialFormatter().formatPartial("+\(phoneNumberToValidate)")
        phoneNumberLabel.setTitle(formattedNumber, for: .normal)
        phoneNumberLabel.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        phoneNumberLabel.contentVerticalAlignment = .bottom
        phoneNumberLabel.contentHorizontalAlignment = .left
        phoneNumberLabel.layer.zPosition = 1
        view.addSubview(phoneNumberLabel)
        
        let numWidth: CGFloat = 40
        let xWidth: CGFloat = 15
        phone1.frame = CGRect(x: globalXAxisIndent, y: phoneNumberLabel.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        
        phone1.keyboardType = .phonePad
        phone1.textAlignment = .center
        phone1.backgroundColor = .clear
        phone1.autocorrectionType = .no
        phone1.isSecureTextEntry = false
        phone1.contentVerticalAlignment = .center
        phone1.setBottomBorder()
        phone1.text = "0"
        phone1.layer.zPosition = 2
        phone1.tag = 1
        phone1.textColor = newHexBlack2.Color
        phone1.font = UIFont.Theme.size15
        self.view.addSubview(phone1)
        
        phone2.frame = CGRect(x: phone1.frame.maxX + xWidth, y: phoneNumberLabel.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        phone2.keyboardType = .phonePad
        phone2.textAlignment = .center
        phone2.backgroundColor = .clear
        phone2.autocorrectionType = .no
        phone2.isSecureTextEntry = false
        phone2.contentVerticalAlignment = .center
        phone2.setBottomBorder()
        phone2.text = "0"
        phone2.layer.zPosition = 2
        phone2.tag = 2
        phone2.textColor = newHexBlack2.Color
        phone2.font = phone1.font
        self.view.addSubview(phone2)
        
        phone3.frame = CGRect(x: phone2.frame.maxX + xWidth, y: phoneNumberLabel.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        phone3.keyboardType = .phonePad
        phone3.textAlignment = .center
        phone3.backgroundColor = .clear
        phone3.autocorrectionType = .no
        phone3.isSecureTextEntry = false
        phone3.contentVerticalAlignment = .center
        phone3.setBottomBorder()
        phone3.text = "0"
        phone3.layer.zPosition = 2
        phone3.tag = 3
        phone3.textColor = newHexBlack2.Color
        phone3.font = phone1.font
        self.view.addSubview(phone3)
        
        phone4.frame = CGRect(x: phone3.frame.maxX + xWidth, y: phoneNumberLabel.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        phone4.keyboardType = .phonePad
        phone4.textAlignment = .center
        phone4.backgroundColor = .clear
        phone4.autocorrectionType = .no
        phone4.isSecureTextEntry = false
        phone4.contentVerticalAlignment = .center
        phone4.setBottomBorder()
        phone4.text = "0"
        phone4.layer.zPosition = 2
        phone4.tag = 4
        phone4.textColor = newHexBlack2.Color
        phone4.font = phone1.font
        self.view.addSubview(phone4)

        phone5.frame = CGRect(x: phone4.frame.maxX + xWidth, y: phoneNumberLabel.frame.maxY + 25, width: numWidth, height: labelHeight + 20)
        phone5.keyboardType = .phonePad
        phone5.textAlignment = .center
        phone5.backgroundColor = .clear
        phone5.autocorrectionType = .no
        phone5.isSecureTextEntry = false
        phone5.contentVerticalAlignment = .center
        phone5.setBottomBorder()
        phone5.text = "0"
        phone5.layer.zPosition = 2
        phone5.tag = 5
        phone5.textColor = newHexBlack2.Color
        phone5.font = phone1.font
        self.view.addSubview(phone5)

        phone1.returnKeyType = UIReturnKeyType.next
        phone2.returnKeyType = UIReturnKeyType.next
        phone3.returnKeyType = UIReturnKeyType.next
        phone4.returnKeyType = UIReturnKeyType.next
        phone5.returnKeyType = UIReturnKeyType.done

        resendCodeButton.frame = CGRect(x: 0, y: phone1.frame.maxY + 100, width: self.view.frame.width, height: globalLargeButtonHeight)
        resendCodeButton.backgroundColor = .clear
        resendCodeButton.isUserInteractionEnabled = true
        resendCodeButton.titleLabel?.font =  UIFont.Theme.size15
        resendCodeButton.setTitleColor(hexBlue.Color, for: .normal)
        resendCodeButton.setTitle("Resend code", for: .normal)
        resendCodeButton.contentVerticalAlignment = .center
        resendCodeButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        resendCodeButton.layer.zPosition = 501
        resendCodeButton.tag = 20
        resendCodeButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: globalXAxisIndent)
        resendCodeButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        resendCodeButton.contentHorizontalAlignment = .left
        view.addSubview(resendCodeButton)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("IN shouldChangeCharactersIn | start")
        
        let nextTag = textField.tag + 1
        let newLength: Int = textField.text!.count + string.count - range.length
        let numberOnly = NSCharacterSet.init(charactersIn: "0123456789").inverted
        
        let strValid = string.rangeOfCharacter(from: numberOnly) == nil
        let newString = ((textField.text)! as NSString).replacingCharacters(in: range, with: string)// Convert text into NSString in order to use 'stringByReplacingCharactersInRange' function

        if textField != phone5 {
            if newLength == 1 && strValid == true {
                textField.text = newString
                textField.setBottomBorderThick()
                if let nextResponder = textField.superview?.viewWithTag(nextTag) {
                    nextResponder.becomeFirstResponder()
                }
                return false
            }
        } else {
            if newLength == 1 && strValid == true {
                textField.text = newString
                textField.setBottomBorderThick()
                startSubmittingCode()
                return false
            }
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    
    func startSubmittingCode() {
        print("in startSubmittingCode | start")
        phone5.becomeFirstResponder()
        phone5.setBottomBorderThick()
        // todo validate number and update new phone number locally and remotly
        if tryToValidateCode() == true {
            tryToVerifyUserPhoneConfirmationCode(phoneNumberToValidate, vc: self, completion: tryToVerifyUserPhoneConfirmationCodeHandler)
            
        } else {
            phone4.resignFirstResponder()
            phone1.setBottomBorderThickRed()
            phone2.setBottomBorderThickRed()
            phone3.setBottomBorderThickRed()
            phone4.setBottomBorderThickRed()
            phone5.setBottomBorderThickRed()

            animateIncorrectInput(buttonID: 1, localView: self.view)
            animateIncorrectInput(buttonID: 2, localView: self.view)
            animateIncorrectInput(buttonID: 3, localView: self.view)
            animateIncorrectInput(buttonID: 4, localView: self.view)
            
            delay(0.5) { //after buttons are animationed
                self.phone1.becomeFirstResponder()
                self.resetPhoneInput()
                self.phone1.text = ""
            }
        }
    }
    
    func tryToValidateCode() -> Bool {
        print("in tryToValidateCode | start")
        var submittedCode = Int()
        
        if phone1.text?.count == 1 && phone2.text?.count == 1 && phone3.text?.count == 1 && phone4.text?.count == 1 && phone4.text?.count == 1 {
            submittedCode = Int(phone1.text! + phone2.text! + phone3.text! + phone4.text! + phone5.text!)!
            print("in tryToValidateCode | submittedCode", submittedCode)

        }

        if phoneNumberValidationCode == "0" || phoneNumberValidationCode == "" {
            phoneNumberValidationCode = "99999"
        }

        print("in tryToValidateCode | phoneNumberValidationCode", phoneNumberValidationCode)
        print("in tryToValidateCode | submittedCode", String(submittedCode))

        if phoneNumberValidationCode == String(submittedCode) {
            phoneIsValid = true
        }
        
        if phoneIsValid == true {
            return true
        } else {
            return false
        }
    }

    @objc func updateUserPhoneNumberAndDismissVC() {
        print("in updateUserPhoneNumberAndDismissVC | start")
        phone5.resignFirstResponder()
            segue(name: "unwindToSettings", v: self, type: .zoomOut)
    }
    
    func resetPhoneInput() {
        print("in resetPhoneInput | start")
        
        phone1.setBottomBorder()
        phone1.text = "0"
        phone2.setBottomBorder()
        phone2.text = "0"
        phone3.setBottomBorder()
        phone3.text = "0"
        phone4.setBottomBorder()
        phone4.text = "0"
        phone5.setBottomBorder()
        phone5.text = "0"
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        phone1.resignFirstResponder()
        phone2.resignFirstResponder()
        phone3.resignFirstResponder()
        phone4.resignFirstResponder()
        phone5.resignFirstResponder()

    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            callNotificationItem("reloadTables")
            self.navigationController?.hero.navigationAnimationType = .zoomOut
            self.hero.dismissViewController()
        } else if sender.tag == 20 {
            // code to send new validation code

            requestResendVerifyConfirmationCode(self, completion: requestResendVerifyRequestHandler)
        }
    }
}
