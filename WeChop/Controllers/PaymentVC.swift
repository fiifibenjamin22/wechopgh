//
//  PaymentVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/12/18.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import EzPopup

class PaymentVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft = SpringButton()
    var navigationTitleBorder = SpringButton()

    let scrollView: UIScrollView = UIScrollView()
    var itemViewPreviousMaxY = CGFloat(0.0)
    let borderColor = hexLightGray

    let ySpacing: CGFloat = 15.0
    var titleHeight: CGFloat = 20.0
    var cbXAxis: CGFloat = 0.0 //checkBox X Axis min
    let cbSize: CGFloat = 20

    var checkBoxes: [CheckBox] = [CheckBox]() //array of all check boxes to interate through to unselect all items except button pressed
    
    var paymentUser: User = emptyNewUser // TODO update and conntect to real user account

    var mmTag: Int = 100
    var ccTag: Int = 1000

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN PaymentVC | ViewDidLoad")
        self.view.backgroundColor = UIColor.white

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .up)

        NotificationCenter.default.addObserver(self, selector: #selector(refreshPage), name: NSNotification.Name(rawValue: "refreshPage"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(refreshUserProfile), name: NSNotification.Name(rawValue: "refreshUserProfile"), object: nil)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }

        if isIPhoneX == true {
            titleHeight = 35.0
        }
        
        cbXAxis = self.view.frame.width - (globalXAxisIndent + cbSize) //checkBox X Axis min
        paymentUser = userData!

        createContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    func createContent() {
        print("in createContent PaymentVC | start")
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Payment", v: self.view, leftButton: navigationLeft)
        
        checkBoxes.removeAll() // clear out array
        mmTag = 100
        ccTag = 1000
        
        scrollView.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: view.frame.width, height: view.frame.height)
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = false
        scrollView.delegate = self
        scrollView.tag = 1
        scrollView.backgroundColor = .clear
        view.addSubview(scrollView)
        
        let mmTitle: SpringButton = SpringButton(frame: CGRect(x: 0, y: ySpacing + 20, width: view.frame.width, height: titleHeight))
        mmTitle.titleLabel?.font = defaultFont14
        mmTitle.setTitle("Mobile Money", for: .normal)
        mmTitle.contentHorizontalAlignment = .left
        mmTitle.titleLabel?.textColor = .white
        mmTitle.backgroundColor = hexGrayPayment.Color
        mmTitle.isUserInteractionEnabled = false
        mmTitle.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        mmTitle.contentVerticalAlignment = .center
        scrollView.addSubview(mmTitle)
        
        itemViewPreviousMaxY = mmTitle.frame.maxY
        
        if paymentUser.PaymentMethod.MobileMoney.isEmpty == false {
            for item in paymentUser.PaymentMethod.MobileMoney {
                createNewMMPaymentLine(item)
            }
        }
        
        let addMMButton: SpringButton = SpringButton()
        
        addMMButton.frame = CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight)
        
        if paymentUser.PaymentMethod.MobileMoney.isEmpty == false {
            addMMButton.frame = CGRect(x: 0, y: itemViewPreviousMaxY + (ySpacing / 2), width: view.frame.width, height: titleHeight)
        }
        
        addMMButton.titleLabel?.font = defaultFont10
        
        addMMButton.setTitle("Add mobile money account", for: .normal)
        addMMButton.contentHorizontalAlignment = .left
        addMMButton.setTitleColor(hexDarkGray, for: .normal)
        addMMButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        addMMButton.backgroundColor = .clear
        addMMButton.isUserInteractionEnabled = true
        addMMButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        addMMButton.contentVerticalAlignment = .center
        addMMButton.tag = 20
        addMMButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        scrollView.addSubview(addMMButton)  <- rmeoved because feature isn't currently supported in API.

        itemViewPreviousMaxY = addMMButton.frame.maxY
//        
//        let ccTitle: SpringButton = SpringButton(frame: CGRect(x: 0, y: itemViewPreviousMaxY + 10, width: view.frame.width, height: titleHeight))
//        ccTitle.titleLabel?.font = defaultFont14
//        ccTitle.setTitle("Credit Card", for: .normal)
//        ccTitle.contentHorizontalAlignment = .left
//        ccTitle.titleLabel?.textColor = .white
//        ccTitle.backgroundColor = hexGrayPayment.Color
//        ccTitle.isUserInteractionEnabled = false
//        ccTitle.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
//        ccTitle.contentVerticalAlignment = .center
//        scrollView.addSubview(ccTitle)
//        
//        itemViewPreviousMaxY = ccTitle.frame.maxY
//        
//        if paymentUser.PaymentMethod.CreditCard.isEmpty == false {
//            for item in paymentUser.PaymentMethod.CreditCard {
//                createNewCCPaymentLine(item)
//            }
//        }
//
//
//        let addCCButton: SpringButton = SpringButton()
//        
//        addCCButton.frame = CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight)
//        
//        if paymentUser.PaymentMethod.CreditCard.isEmpty == false {
//            addCCButton.frame = CGRect(x: 0, y: itemViewPreviousMaxY + (ySpacing / 2), width: view.frame.width, height: titleHeight)
//        }
//        
////        let addCCButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight))
//        addCCButton.titleLabel?.font = addMMButton.titleLabel?.font
//        addCCButton.setTitle("(currently unavailble)", for: .normal)
//        addCCButton.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        addCCButton.contentHorizontalAlignment = .left
//        addCCButton.setTitleColor(hexDarkGray, for: .normal)
//        addCCButton.backgroundColor = .clear
//        addCCButton.isUserInteractionEnabled = false
//        addCCButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
//        addCCButton.contentVerticalAlignment = .center
//        addCCButton.tag = 21
//        addCCButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//
//        scrollView.addSubview(addCCButton)
//        
//        itemViewPreviousMaxY = addCCButton.frame.maxY
        
        let cashTitle: SpringButton = SpringButton(frame: CGRect(x: 0, y: itemViewPreviousMaxY + 10, width: view.frame.width, height: titleHeight))
        cashTitle.titleLabel?.font = defaultFont14
        cashTitle.setTitle("Cash", for: .normal)
        cashTitle.contentHorizontalAlignment = .left
        cashTitle.titleLabel?.textColor = .white
        cashTitle.backgroundColor = hexGrayPayment.Color
        cashTitle.isUserInteractionEnabled = false
        cashTitle.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        cashTitle.contentVerticalAlignment = .center
        scrollView.addSubview(cashTitle)
        
        itemViewPreviousMaxY = cashTitle.frame.maxY

        let cashButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight))
        cashButton.titleLabel?.font = defaultFont14
        cashButton.setTitle("Pay with cash", for: .normal)
        cashButton.contentHorizontalAlignment = .left
        cashButton.setTitleColor(hexDarkGray, for: .normal)
        cashButton.backgroundColor = .clear
        cashButton.isUserInteractionEnabled = true
        cashButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
        cashButton.contentVerticalAlignment = .center
        cashButton.tag = 22
        cashButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        scrollView.addSubview(cashButton)

        if isIPhoneX == true {
            addMMButton.titleLabel?.font = defaultFont14
            mmTitle.titleLabel?.font = defaultFont18
            cashButton.titleLabel?.font = defaultFont18
            cashTitle.titleLabel?.font = defaultFont18
        }

        itemViewPreviousMaxY = scrollView.frame.maxY

        let cashCB: CheckBox = CheckBox()
        
        cashCB.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        cashCB.tag = 23
        cashCB.center.y = cashButton.center.y
        cashCB.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        if paymentUser.PaymentMethod.PaymentType == 0 {
            cashCB.isChecked = true
        } else {
            cashCB.isChecked = false
        }

        checkBoxes.append(cashCB)
        scrollView.addSubview(cashCB)
        
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: itemViewPreviousMaxY)
    }
    
    func createNewMMPaymentLine(_ item: MobileMoney) {
        
        let button: SpringButton = SpringButton()
        if mmTag == 100 { //first item
            button.frame = CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight)
        } else { //subsequent items
            button.frame = CGRect(x: 0, y: itemViewPreviousMaxY, width: view.frame.width, height: titleHeight)
        }
        
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        button.setTitle("XXX XX XX \(String((item.Phone).suffix(3)))", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(hexDarkGray, for: .normal)
        button.backgroundColor = .clear
        button.isUserInteractionEnabled = true
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent + 25, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        button.contentVerticalAlignment = .center
        button.tag = mmTag + 100
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        scrollView.addSubview(button)
        
        let image: UIButton = UIButton()
        image.frame = CGRect(x: globalXAxisIndent, y: 0, width: titleHeight - 5, height: titleHeight - 10)
        image.center.y = button.center.y
//        image.tag = mmTag
        
        if item.Network == 1 {
            image.setImage(UIImage(named: "tigo_mm"), for: .normal)
        } else {
            image.setImage(UIImage(named: "mtn_mm.jpg"), for: .normal)
        }
        image.backgroundColor = .clear
        image.isUserInteractionEnabled = false
//        image.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        scrollView.addSubview(image)

        let mmCB: CheckBox = CheckBox()
        mmCB.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        mmCB.tag = mmTag
        mmCB.center.y = button.center.y
        mmCB.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        mmCB.isChecked = false // TODO update this logic with defined MM selected account
        checkBoxes.append(mmCB)
        scrollView.addSubview(mmCB)
        
        mmTag += 1
        itemViewPreviousMaxY = button.frame.maxY

    }
    
    func createNewCCPaymentLine(_ item: CreditCard) {
        
        let button: SpringButton = SpringButton()
        if ccTag == 1000 { //first item
            button.frame = CGRect(x: 0, y: itemViewPreviousMaxY + ySpacing, width: view.frame.width, height: titleHeight)
        } else { //subsequent items
            button.frame = CGRect(x: 0, y: itemViewPreviousMaxY, width: view.frame.width, height: titleHeight)
        }
        
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        button.setTitle("XXXX XXXX XXXX \(String(String(item.Number)).suffix(4))", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(hexDarkGray, for: .normal)
        button.backgroundColor = .clear
        button.isUserInteractionEnabled = true
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent + 25, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        button.contentVerticalAlignment = .center
        button.tag = ccTag + 100
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        scrollView.addSubview(button)
        
        let image: UIButton = UIButton()
        image.frame = CGRect(x: globalXAxisIndent, y: 0, width: titleHeight - 5, height: titleHeight - 5)
        image.center.y = button.center.y
        image.setImage(UIImage(named: "creditcard"), for: .normal)
        image.backgroundColor = .clear
        image.isUserInteractionEnabled = false
        scrollView.addSubview(image)
        
        let mmCB: CheckBox = CheckBox()
        mmCB.frame = CGRect(x: cbXAxis, y: 0, width: cbSize, height: cbSize)
        mmCB.tag = ccTag
        mmCB.center.y = button.center.y
        mmCB.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        mmCB.isChecked = false // TODO update this logic with defined MM selected account
        checkBoxes.append(mmCB)
        scrollView.addSubview(mmCB)
        
        ccTag += 1
        itemViewPreviousMaxY = button.frame.maxY
        
    }
    
    @objc func refreshPage() {
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        view.subviews.forEach({ $0.removeFromSuperview() })

        createContent()
    }
    
    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {

            if comingFrom.cart == true {
                callNotificationItem("refreshCartPageData")
                //comingFromCart = false
                comingFrom.cart = false
            }

            self.navigationController?.hero.navigationAnimationType = .slide(direction: .down)
            self.hero.dismissViewController()
        } else if sender.tag == 20 {
            //add mobile money

            if comingFrom.cart == true {
                let contentVC = PaymentEditMobileMoneyVC()
                let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
                present(popupVC, animated: true)
            } else {
                segue(name: "EditPaymentVCSegueFromPayment", v: self, type: .pull(direction: .left))
            }

        } else if sender.tag == 21 {
            //add credit car
            
        } else if sender.tag > 21 {
            toggleCheck(tag: sender.tag)
        }
    }
    
    @objc func refreshUserProfile() {
        print("in refreshUserProfile | start")
        
    }
    
    func toggleCheck(tag: Int) {
        print("in toggleCheck | tag", tag)
        
        var finalTag: Int = tag
        var isChecked: Bool = false
        var originalValue: Bool = false
        if tag > 199 && tag < 999 { //this allows text to be a button as well
            finalTag = tag - 100
        } else if tag > 1099 {
            finalTag = tag - 100
        }
        
        print("in toggleCheck | finaltag", finalTag)
        if let b =  self.scrollView.viewWithTag(finalTag) as? CheckBox {
            originalValue = b.isChecked //capture original value
            if finalTag == 23 && b.isChecked == true && userData?.PaymentMethod.MobileMoney.isEmpty == true {
                //do nothing
            } else {
                b.isChecked.negate()
            }

            if b.isChecked == true {
                isChecked = true
            } else {
                isChecked = false
            }
        }
        
        print("in toggleCheck | isChecked", isChecked)

        //uncheck others
        
        if finalTag > 99 && isChecked == true {
            for item in checkBoxes where item.tag != finalTag {
                item.isChecked = false
            }
        }
        
        if finalTag == 23 && isChecked == true { //uncheck mm tags
            for item in checkBoxes where item.tag != 23 {
                item.isChecked = false
            }
        }
        
        //update user
        if let cash = checkBoxes.first(where: {$0.tag == 23}) {

            if originalValue != cash.isChecked { //only change if value changes
                if cash.isChecked == true {
                    paymentUser.PaymentMethod.PaymentType = 0
                    setUserPayment(self, paymentType: 0, completion: setUserPaymentHandler)
                } else if finalTag > 999 { //creditcard
                    paymentUser.PaymentMethod.PaymentType = 2
                    setUserPayment(self, paymentType: 2, completion: setUserPaymentHandler)
                } else { //mobile money
                    paymentUser.PaymentMethod.PaymentType = 1
                    setUserPayment(self, paymentType: 1, completion: setUserPaymentHandler)
                }
            }
        }
        print("test user payment type: ", paymentUser.PaymentMethod.PaymentType)

        //update user in API?
        //refreshUserProfile()?
    }
}
