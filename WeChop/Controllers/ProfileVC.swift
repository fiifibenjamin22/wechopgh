//
//  ProfileVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Alamofire
import SCLAlertView
import Hero
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import Branch

class ProfileVC: UIViewController, UIScrollViewDelegate {
    
    var navigationLeft: SpringButton = SpringButton() 
    var navigationTitleBorder: SpringButton = SpringButton()
    let profileSections: [String] = ["Account settings", "Delivery address", "Free food", "About"]
//    let profileSections: [String] = ["Account settings", "Delivery address", "Payment method", "Free food", "About"]

    var profileViewPreviousMaxY: CGFloat = 0
    var userCredit: Int = 0

    var referralButton: UIButton = create {
        $0.titleLabel?.font = defaultFont14
        $0.setTitleColor(hexRefRed.Color, for: .normal)
        $0.contentVerticalAlignment = .center
        $0.contentHorizontalAlignment = .left
        $0.isUserInteractionEnabled = true
        $0.tag = 25
    }
    
    var navigationFooterBorder: UIButton = create {
        $0.backgroundColor = hexLightGray
        $0.isUserInteractionEnabled = false
        $0.layer.zPosition = 3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN ProfileVC | ViewDidLoad")
        //writeToFireBase("in ProfileVC viewdidload")

//        Crashlytics.sharedInstance().crash() //test
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .right)

        //update UI for device type
        if isIpad == true && isShortIpad == false {
            
        } else if isIpad == true && isShortIpad == true {
            
        } else if isIpad == false && isIPhoneX == true {
            
        } else {
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(signOutSuccessful), name: NSNotification.Name(rawValue: "signOutSuccessful"), object: nil) // after logout goes to login page

        self.view.backgroundColor = UIColor.white
        
        //create headerbuttons
        let navigationLeft = createNavigationLeftBackButton(v: self.view)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        //navigationTitle
        navigationTitleBorder = createNavigationTitle(title: "Profile", v: self.view, leftButton: navigationLeft)
        
//        navigationFooterBorder.frame = CGRect(x: 0, y: self.view.frame.height - globalLargeButtonHeight, width: self.view.frame.width, height: 1)
//        
//        if isIPhoneX == true {
//            
//            print("is iphoneX??? ")
//            navigationFooterBorder.frame = CGRect(x: 0, y: self.view.frame.height - (globalLargeButtonHeight + globalIPhoneXTopYPadding), width: self.view.frame.width, height: 1)
//        }
//        
//        navigationFooterBorder.center.x = view.center.x
//        view.addSubview(navigationFooterBorder)
        
//        footerSignOutTitle.frame = CGRect(x: 0, y: navigationFooterBorder.frame.maxY, width: self.view.frame.width, height: globalLargeButtonHeight)
//
//        footerSignOutTitle.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular)
//        footerSignOutTitle.setTitleColor(hexDarkGray, for: .normal)
//        footerSignOutTitle.setBackgroundColor(color: UIColor.white, forState: .normal)
//        footerSignOutTitle.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
//        footerSignOutTitle.setTitle("Sign Out", for: .normal)
//        footerSignOutTitle.tag = 27
//        footerSignOutTitle.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalXAxisIndent, bottom: 0, right: 0)
//        footerSignOutTitle.isUserInteractionEnabled = true
//        footerSignOutTitle.contentHorizontalAlignment = .left
//        footerSignOutTitle.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
//        view.addSubview(footerSignOutTitle)

        var tagNum = 0
        for item in profileSections {
            let profileImage = getProfileRowImage(num: tagNum)
            createProfileRow(image: profileImage, title: item, tag: tagNum + 20)
            tagNum += 1
        }
        
        createreferralCreditButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

    }
    
    func createreferralCreditButton() {
        referralButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        referralButton.frame = CGRect(x: 0, y: profileViewPreviousMaxY + 10, width: self.view.frame.width, height: globalLargeButtonHeight + 10)
    
        referralButton.setTitle("Referral credit: \(userCredit)", for: .normal)
        if userCredit > 0 {
            referralButton.setTitle("Referral credit: \(userCredit) GHS", for: .normal)
        }
        
        referralButton.contentVerticalAlignment = .center
        referralButton.contentHorizontalAlignment = .left
        referralButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalNavTitleIndentSize, bottom: 0, right: 0)
        referralButton.backgroundColor = hexRefGray.Color
        referralButton.tag = 25
        self.view.addSubview(referralButton)
        
        let refIcon = UIImageView(image: UIImage(named: "referral_image"))
        let imageSize = Int(globalNavIconSize + 10)
        refIcon.frame = CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize)
        refIcon.isUserInteractionEnabled = false
        refIcon.backgroundColor = .clear
        refIcon.center.y = referralButton.center.y
        self.view.addSubview(refIcon)
        
        // TODO add expiring today logic
        
        let refGetMore = UIButton(frame: CGRect(x: 0, y: self.view.frame.width - 100, width: 100, height: 30))
        refGetMore.setTitle("GET MORE", for: .normal)
        refGetMore.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        refGetMore.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        refGetMore.setTitleColor(hexRed.Color, for: .normal)
        refGetMore.isUserInteractionEnabled = true
        refGetMore.contentVerticalAlignment = .center
        refGetMore.contentHorizontalAlignment = .right
        refGetMore.sizeToFit()
        refGetMore.frame.origin.x = self.view.frame.width - (refGetMore.frame.width + 20)
        refGetMore.center.y = referralButton.center.y
        refGetMore.backgroundColor = .clear
        refGetMore.tag = 26
        self.view.addSubview(refGetMore)
    }
 
    func createProfileRow (image: String, title: String, tag: Int) {
        
        //navigationTitle
        let button = UIButton()

        button.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: globalLargeButtonHeight)

        if profileViewPreviousMaxY == 0.0 {
            button.frame = CGRect(x: 0, y: navigationTitleBorder.frame.maxY, width: self.view.frame.width, height: globalLargeButtonHeight)
        } else {
            button.frame = CGRect(x: 0, y: profileViewPreviousMaxY, width: self.view.frame.width, height: globalLargeButtonHeight)
        }

        button.tag = tag
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        button.backgroundColor = .clear
        button.isUserInteractionEnabled = true
//        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.titleLabel?.font = UIFont.Theme.size15

        button.setTitleColor(newHexBlack1.Color, for: .normal)
        button.setBackgroundColor(color: .clear, forState: .normal)
        button.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        button.setTitle(title, for: .normal)
        button.contentVerticalAlignment = .center
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: globalNavTitleIndentSize - 25, bottom: 0, right: 0)
        self.view.addSubview(button)

        let imageSize = Int(globalNavIconSize + 3)
        let image1 = SpringButton(frame: CGRect(x: Int(globalXAxisIndent), y: 0, width: imageSize, height: imageSize))
        image1.center.y = button.center.y
        image1.setImage(UIImage(named: image), for: .normal)
        image1.isUserInteractionEnabled = false
        image1.contentHorizontalAlignment = .left
        image1.contentVerticalAlignment = .center
        image1.backgroundColor = .clear
        self.view.addSubview(image1)
        
        profileViewPreviousMaxY = button.frame.maxY
    }

    //Nav buttons pressed actions
    @objc func buttonPressed(_ sender: DaySelectionButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 12 {
            callNotificationItem("refreshPage")
            self.navigationController?.hero.navigationAnimationType = .pageOut(direction: .left)
            self.hero.dismissViewController()
        } else if sender.tag == 20 {
            //goto settings page

            segue(name: "SettingsVCSegueFromProfile", v: self, type: .pageIn(direction: .down))

        } else if sender.tag == 21 {
            //goto address page
            segue(name: "AddressVCSegueFromProfile", v: self, type: .pageIn(direction: .up))

//        } else if sender.tag == 22 {
//            //goto payment page
//            segue(name: "PaymentVCSegueFromProfile", v: self, type: .pageIn(direction: .up))
//
        } else if sender.tag == 22 || sender.tag == 25 || sender.tag == 26 {
            //goto free food page
            segue(name: "InviteVCSegueFromProfile", v: self, type: .pageIn(direction: .up))
        } else if sender.tag == 23 {
            //goto about page
            segue(name: "AboutVCSegueFromProfile", v: self, type: .pageIn(direction: .up))
        } else if sender.tag == 27 {
//            signOutUser(self, completion: signOutUserRequestHandler)
        }
    }

    @objc func signOutSuccessful() {
        print("in signOutSuccessful | start")

        animateUIBanner("Success", subText: "You have successfully signed out.", color: UIWarningSuccessColor, removePending: true)

        //branch
        BranchEvent.customEvent(withName: "User_Signed_Out").logEvent()

        resetUserInformation() //reset user information
        if currentRootView == .login {
            print("in signOutSuccessful | used login")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
            UIApplication.shared.keyWindow?.rootViewController = viewController

            navigationController?.hero.navigationAnimationType = .cover(direction: .up)
            self.navigationController?.popToRootViewController(animated: true) //go back to login page
        } else {
            print("in signOutSuccessful | used menu")
            currentRootView = .login
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: currentRootView.rawValue)
            UIApplication.shared.keyWindow?.rootViewController = viewController
            
            navigationController?.hero.navigationAnimationType = .cover(direction: .up)
            self.navigationController?.popToRootViewController(animated: true) //go back to login page
        }
    }
    
    func getProfileRowImage (num: Int) -> String {
        switch num {
        case 0:
            return "profile_icon_grey.png"
        case 1:
            return "officeAddress_icon.png"
//        case 2:
//            return "payment_icon.png"
        case 2:
            return "referral.png"
        case 3:
            return "about_icon.png"
        default:
            return "test.png"
        }
    }    
}
