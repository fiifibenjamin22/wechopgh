//
//  MenuVC.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import NVActivityIndicatorView
import Hero
import SwiftDate
import Kingfisher
import EzPopup
import NotificationBannerSwift
import SwiftyGif
import Branch

class MenuVC: UIViewController, NVActivityIndicatorViewable {
    
    var scrollView: UIScrollView = UIScrollView()
    var tableView: UITableView = UITableView()
    var tableView2: UITableView = UITableView()
    
    //loadingMenu
    let loadingMenu: SpringButton = SpringButton()
    let orderCutoff: SpringLabel = SpringLabel()
    var orderCutoffSize: CGFloat = 0.0
    var loadingDots: String = ".." 
    var loadingUIIsDisplayed = false

    //navigation items
    var navigationLeft: SpringButton = SpringButton() 
    var navigationRight: SpringButton = SpringButton()
    var navigationDayText: SpringLabel = SpringLabel()
    var navigationDayIcon: SpringButton = SpringButton()
    var navigationBar: SpringButton = SpringButton()
    var navigationCounter: SpringButton = SpringButton()
    
    //menubox items
    let menuBox: SpringButton  = SpringButton()
    var menuToday: SpringLabel = SpringLabel()
    var menuRestaurantName: SpringLabel = SpringLabel()
    var menuRestaurantSummary: SpringLabel = SpringLabel()
    var menuRestaurantIcon: SpringButton  = SpringButton()
    var orderWarningText: SpringLabel = SpringLabel()
    var orderWarningIcon: SpringButton = SpringButton()
    var menuMenuText: SpringLabel = SpringLabel()
    var orderWarningBox: SpringButton = SpringButton()
    var menuSeperatorText: SpringLabel = SpringLabel()
    
    //menubox2 items
    let menuBox2: SpringButton  = SpringButton()
    var menuToday2: SpringLabel = SpringLabel()
    var menuRestaurantName2: SpringLabel = SpringLabel()
    var menuRestaurantSummary2: SpringLabel = SpringLabel()
    var menuRestaurantIcon2: SpringButton  = SpringButton()
    var orderWarningText2: SpringLabel = SpringLabel()
    var orderWarningIcon2: SpringButton = SpringButton()
    var menuMenuText2: SpringLabel = SpringLabel()
    var orderWarningBox2: SpringButton = SpringButton()
    var menuSeperatorText2: SpringLabel = SpringLabel()
    
    var movingBetweenRestaurantViews: Bool = false
    var viewCartButton: SpringButton = SpringButton()
    var cartButtonCountButton: SpringButton = SpringButton()
    var cartButtonPriceButton: SpringButton = SpringButton()
    
    //Top Restaurant Nav Selector
    let restaurant1Button: UIButton = UIButton()
    let restaurant2Button: UIButton = UIButton()
    let restaurantButtonBorder: UIButton = UIButton()
    var onRestaurant1View: Bool = true // used to ID which view the orders page is on
    let normalFont = defaultFont14
    
    var firstMenuCreated: Bool = false
    var secondMenuCreated: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        print("IN MenuVC | ViewDidLoad")

        defer {
            userRegistrationPhone = ""
            userRegistrationPassword = ""
        }

        createLoadingMenuNotification()

        self.view.backgroundColor = UIColor.white
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.backgroundColor = hexPeach.Color
            //.darkGray
        }, completion: nil )

        comingFrom.menuToUpdateAddress = false

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .cover(direction: .up)
        self.navigationController?.hero.navigationAnimationType = .cover(direction: .up)

        DispatchQueue.global().async {
            self.registerNotifications()
            checkIfUserShouldRateApp()

            //get user information and order history
            userCartOrder = emptyOrder //create empty cart
            userCartOrder.Additionals.removeAll()
            userCartOrder.OrderedItems.removeAll()
        }

        getUserCurrentCart()//get current cart from server if any
        getUserUpcomingAndCompletedOrders(self) //uncomment after testing
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView2.delegate = self
        tableView2.dataSource = self
        
        self.tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView2.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        self.tableView2.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("in willappear | start")

        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

        //this isn't used anymore
        if comingFrom.cartToOrders == true { //this is called after an order is placed to move to the orders page
            //shouldMoveToOrdersFromCart = false
            comingFrom.cartToOrders = false
            //onOrdersVCUpcomingView = true
            comingFrom.ordersUpcomingTableView = true
            print("RCB - view will appear")
            removeCartButton()
            segue(name: "OrdersVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
        }

        //isInMenuPage = true
        comingFrom.menu = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in Menu viewDidAppear | start")
    }

    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPage), name: NSNotification.Name(rawValue: "refreshPage"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(removeCartButton), name: NSNotification.Name(rawValue: "removeCartButton"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(getUserCurrentCart), name: NSNotification.Name(rawValue: "getUserCurrentCart"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(toggleViewCartButtonFromGetCartAPI), name: NSNotification.Name(rawValue: "toggleViewCartButtonFromGetCartAPI"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(toggleUpcomingOrderCount), name: NSNotification.Name(rawValue: "checkToggleUpcomingOrderCount"), object: nil) // updates order cart count in menu page

        NotificationCenter.default.addObserver(self, selector: #selector(toggleDisplayRateOrder), name: NSNotification.Name(rawValue: "toggleDisplayRateOrder"), object: nil) // updates order cart count in menu page

    }

    @objc func getUserCurrentCart() {
        print("in getUserCurrentCart | start | gets cart from API")

        DispatchQueue.global().async {
            tryToGetServerCartParameters(self, promoCode: "", completion: tryToGetServerCartParametersHandler)
        }
    }

}

// MARK: UI Elements
extension MenuVC {
    func createUIElements() {
        print("IN Menu createUIElements | start")

        navigationBar = SpringButton(frame: CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width, height: globalYAxisHeight))

        if isIPhoneX == true {
            navigationBar.frame = CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width, height: globalYAxisHeight)
        }

        navigationBar.backgroundColor = hexRed.Color
        navigationBar.layer.shadowColor = hexDarkGray.cgColor
        navigationBar.layer.shadowOpacity = 0.5
        navigationBar.layer.shadowOffset = globalShadowOffset
        navigationBar.layer.shadowRadius = 1
        navigationBar.layer.zPosition = 1000
        navigationBar.hero.id = "heroBar"
        navigationBar.isUserInteractionEnabled = false
        self.view.addSubview(navigationBar)
        self.view.sendSubview(toBack: navigationBar)

        navigationLeft = SpringButton(frame: CGRect(x: 0, y: 0, width: globalYAxisHeight, height: globalYAxisHeight))
        navigationLeft.hero.id = "navLeft"
        navigationLeft.adjustsImageWhenHighlighted = false
        navigationLeft.center.y = navigationBar.center.y
        navigationLeft.layer.zPosition = 1002
        navigationLeft.setImage(UIImage(named: "Profile_icon.png")?.addImagePadding(x: 200, y: 200), for: .normal)
        navigationLeft.setBackgroundColor(color: hexRedDark.Color, forState: .highlighted)
        navigationLeft.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        navigationLeft.tag = 11
        self.view.addSubview(navigationLeft)

        navigationRight = SpringButton(frame: CGRect(x: self.view.frame.width - (globalXAxisIndent + (globalYAxisHeight / 1.5)), y: 0, width: globalYAxisHeight, height: globalYAxisHeight))
        navigationRight.setImage(UIImage(named: "Orders_icon.png")?.addImagePadding(x: 200, y: 200).withRenderingMode(.alwaysTemplate), for: .normal)
        navigationRight.tintColor = .white
        navigationRight.setBackgroundColor(color: .clear, forState: .normal)
        navigationRight.layer.zPosition = 1002
        navigationRight.adjustsImageWhenHighlighted = false
        navigationRight.setBackgroundColor(color: hexRedDark.Color, forState: .highlighted)
        navigationRight.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        navigationRight.center.y = navigationBar.center.y
        navigationRight.tag = 12
        self.view.addSubview(navigationRight)

        navigationCounter.frame = CGRect(x: navigationRight.frame.maxX - 7, y: navigationRight.frame.minY - 7, width: 15, height: 15)

        navigationCounter.setTitle("0", for: .normal)
        navigationCounter.setTitleColor(hexDarkGray, for: .normal)
        navigationCounter.titleLabel?.font = UIFont.systemFont(ofSize: 8, weight: UIFont.Weight.regular)
        navigationCounter.contentVerticalAlignment = .center
        navigationCounter.isHidden =  true
        navigationCounter.contentHorizontalAlignment = .center
        navigationCounter.setBackgroundColor(color: hexYellowLight.Color, forState: .normal)
        navigationCounter.layer.cornerRadius = navigationCounter.frame.width / 2
        navigationCounter.layer.masksToBounds = true
        navigationCounter.layer.zPosition = 10000
        navigationCounter.isUserInteractionEnabled = false

        navigationCounter.frame.origin.x = navigationRight.frame.maxX - (navigationRight.frame.width / 3) - 8
        navigationCounter.frame.origin.y = navigationRight.frame.minY +  (navigationRight.frame.width / 4) - 5
        self.view.addSubview(navigationCounter)

        navigationDayText = SpringLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: globalNavIconSize))
        navigationDayText.text = "\(currentlySelectedMenuDay.weekdayName(.default))'s Menu"
        navigationDayText.font = UIFont.Theme.size17
        navigationDayText.textAlignment = .center
        navigationDayText.textColor = .white
        navigationDayText.sizeToFit()
        navigationDayText.center.y = navigationBar.center.y
        navigationDayText.center.x = self.view.center.x
        navigationDayText.backgroundColor = .clear
        navigationDayText.isUserInteractionEnabled = true
        navigationDayText.layer.zPosition = 1002
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        tap.numberOfTapsRequired = 1
        navigationDayText.addGestureRecognizer(tap)

        self.view.addSubview(navigationDayText)
        navigationDayIcon.frame = CGRect(x: navigationDayText.frame.maxX - 3, y: 0, width: globalNavIconSize + 5, height: globalNavIconSize + 5)
        navigationDayIcon.setImage(UIImage(named: "chevron.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        navigationDayIcon.center.y = navigationBar.center.y
        navigationDayIcon.tintColor = .white
        navigationDayIcon.layer.zPosition = 1002
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        tap2.numberOfTapsRequired = 1
        navigationDayIcon.addGestureRecognizer(tap2)
        self.view.addSubview(navigationDayIcon)

        if isIPhoneX == true {

            let height = navigationBar.frame.height + (globalIPhoneXTopYPadding / 1.5)
            navigationLeft.center.y = height
            navigationRight.center.y = height
            navigationDayText.center.y = height
            navigationDayIcon.center.y = navigationDayText.center.y
        }

        restaurant1Button.frame = CGRect(x: 0, y: navigationBar.frame.maxY, width: self.view.frame.width / 2, height: globalLargeButtonHeight)
        restaurant1Button.tag = 20

        if todaysMenu.count > 0 {
            restaurant1Button.setTitle(todaysMenu[0].Name, for: .normal)
        } else {
            restaurant1Button.setTitle(" ", for: .normal)
        }

        restaurant1Button.setTitleColor(hexRed.Color, for: .normal)
        restaurant1Button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        restaurant1Button.titleLabel?.font = normalFont
        restaurant1Button.backgroundColor = .white
        restaurant1Button.isUserInteractionEnabled = true
        restaurant1Button.setBackgroundColor(color: .clear, forState: .normal)
        restaurant1Button.contentVerticalAlignment = .center
        restaurant1Button.contentHorizontalAlignment = .center
        restaurant1Button.layer.zPosition = 1000
        restaurant1Button.setBottomBorder()
        self.view.addSubview(restaurant1Button)

        restaurant2Button.frame = CGRect(x: self.view.frame.width / 2, y: navigationBar.frame.maxY, width: self.view.frame.width / 2, height: globalLargeButtonHeight)
        restaurant2Button.tag = 21
        restaurant2Button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        if todaysMenu.count > 1 {
            restaurant2Button.setTitle(todaysMenu[1].Name, for: .normal)
        } else {
            restaurant2Button.setTitle(" ", for: .normal)
        }
        self.restaurant2Button.setTitleColor(hexDarkGray.withAlphaComponent(0.7), for: .normal)
        restaurant2Button.titleLabel?.font = normalFont
        restaurant2Button.backgroundColor = .white
        restaurant2Button.isUserInteractionEnabled = true
        restaurant2Button.contentVerticalAlignment = .center
        restaurant2Button.contentHorizontalAlignment = .center
        restaurant2Button.layer.zPosition = 1000
        restaurant2Button.setBottomBorder()
        self.view.addSubview(restaurant2Button)

        //restaurantButtonBorder
        restaurantButtonBorder.frame = CGRect(x: 0, y: restaurant1Button.frame.maxY, width: self.view.frame.width / 2, height: 1.5)
        restaurantButtonBorder.backgroundColor = hexRed.Color
        restaurantButtonBorder.layer.zPosition = 1001
        restaurantButtonBorder.isUserInteractionEnabled = false
        self.view.addSubview(restaurantButtonBorder)

        //setup scrollview
        scrollView.frame = CGRect(x: 0, y: restaurant1Button.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = false
        scrollView.delegate = self
        scrollView.backgroundColor = .clear

        scrollView.frame = CGRect(x: 0, y: restaurant1Button.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)

        if todaysMenu.count > 0 {

            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[0].MenuItems.count) + 120 )

            if todaysMenu.count > 1 {

                self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[1].MenuItems.count) + 120 )
            }
        } else {
            scrollView.frame = CGRect(x: 0, y: navigationBar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)

            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 240 )
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 240 )
        }

        //recenter
        if todaysMenu.count > 0 {
            //            dump(todaysMenu[0])
            createfirstMenu()
            if todaysMenu.count > 1 {
                createSecondMenu()
            }
        } else {
            //            toggleMenuNotAvaiableUI()
        }
    }

    func createfirstMenu() {
        print("IN createfirstMenu | start")

        firstMenuCreated = true
        //restaurant 1
        menuBox.frame = CGRect(x: 0, y: 25, width: self.view.frame.width - 75, height: 125)

        if isIPhoneX {
            menuBox.frame = CGRect(x: 0, y: Int(globalIPhoneXTopYPadding + 25), width: Int(self.view.frame.width - 75), height: 125)
        }

        menuBox.backgroundColor = .white
        menuBox.center.x = self.view.center.x
        menuBox.layer.borderColor = hexLightGray.cgColor
        menuBox.layer.borderWidth = 1
        menuBox.layer.zPosition = 1
        menuBox.isUserInteractionEnabled = false
        scrollView.addSubview(menuBox)

        menuToday = SpringLabel(frame: CGRect(x: 0, y: menuBox.frame.minY + (menuBox.frame.height / 7), width: self.view.frame.width, height: globalNavIconSize))

        menuToday.text = "Featuring"
        menuToday.font = normalFont
        menuToday.textAlignment = .center
        menuToday.textColor = hexGray
        menuToday.sizeToFit()
        menuToday.center.x = self.view.center.x
        menuToday.backgroundColor = .clear
        menuToday.isUserInteractionEnabled = false
        menuToday.layer.zPosition = 2
        scrollView.addSubview(menuToday)

        menuRestaurantName = SpringLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))

        if todaysMenu.isEmpty == false {
            menuRestaurantName.text = todaysMenu[0].Name
        }

        menuRestaurantName.font = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.regular)
        menuRestaurantName.textAlignment = .center
        menuRestaurantName.textColor = UIColor.black
        menuRestaurantName.sizeToFit()
        menuRestaurantName.center.x = self.view.center.x
        menuRestaurantName.center.y = menuBox.center.y - (menuBox.frame.height / 6)
        menuRestaurantName.backgroundColor = .clear
        menuRestaurantName.isUserInteractionEnabled = false
        menuRestaurantName.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantName)

        menuRestaurantSummary = SpringLabel(frame: CGRect(x: 0, y: menuBox.frame.maxY - ((menuBox.frame.height / 7) + (globalNavIconSize / 2)), width: self.view.frame.width, height: globalNavIconSize))

        if todaysMenu.isEmpty == false {
            menuRestaurantSummary.text = "\(todaysMenu[0].Rating) | \(todaysMenu[0].Genre)"
        }

        menuRestaurantSummary.font = normalFont
        menuRestaurantSummary.textAlignment = .center
        menuRestaurantSummary.textColor = hexGray
        menuRestaurantSummary.sizeToFit()
        menuRestaurantSummary.center.x = self.view.center.x
        menuRestaurantSummary.center.y = menuBox.center.y + (menuBox.frame.height / 4)

        menuRestaurantSummary.backgroundColor = .clear
        menuRestaurantSummary.isUserInteractionEnabled = false
        menuRestaurantSummary.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantSummary)

        menuRestaurantIcon = SpringButton(frame: CGRect(x: menuRestaurantSummary.frame.minX - 22.5, y: 0, width: globalNavIconSize + 5, height: globalNavIconSize + 5))
        menuRestaurantIcon.setImage(UIImage(named: "star_yellow.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        menuRestaurantIcon.center.y = menuRestaurantSummary.center.y
        menuRestaurantIcon.tintColor = hexYellow.Color
        menuRestaurantIcon.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantIcon)

        //recenter
        menuRestaurantSummary.text = "\(todaysMenu[0].Rating) | \(todaysMenu[0].Genre)"
        menuRestaurantSummary.sizeToFit()
        menuRestaurantSummary.center.x = self.view.center.x + (menuRestaurantIcon.frame.width / 2) - 3.75
        menuToday.center.x = self.view.center.x
        menuRestaurantIcon.frame.origin.x = menuRestaurantSummary.frame.minX - (menuRestaurantIcon.frame.width - 2.5)
        menuRestaurantIcon.center.y = menuRestaurantSummary.center.y

        orderWarningBox.frame = CGRect(x: 0, y: menuBox.frame.maxY, width: menuBox.frame.width, height: 40)
        orderWarningBox.backgroundColor = hexPeach.Color
        //hexTan.Color
        orderWarningBox.center.x = self.view.center.x
        orderWarningBox.layer.zPosition = 1
        orderWarningBox.isUserInteractionEnabled = false

        //test drop shadow
        orderWarningBox.layer.shadowColor = hexDarkGray.cgColor
        orderWarningBox.layer.shadowOpacity = 0.5
        orderWarningBox.layer.shadowOffset = globalShadowOffset
        orderWarningBox.layer.shadowRadius = 1
        scrollView.addSubview(orderWarningBox)

        orderWarningText = SpringLabel(frame: CGRect(x: 0, y: menuBox.frame.maxY - (5 + globalNavIconSize), width: orderWarningBox.frame.width, height: globalNavIconSize))
        orderWarningText.text = "Order before 11:00am"
        orderWarningText.font = normalFont
        orderWarningText.textAlignment = .center
        orderWarningText.textColor = UIColor.black
        orderWarningText.sizeToFit()
        orderWarningText.center.x = self.view.center.x
        orderWarningText.center.y = orderWarningBox.center.y
        orderWarningText.backgroundColor = .clear
        orderWarningText.isUserInteractionEnabled = false
        orderWarningText.layer.zPosition = 2
        scrollView.addSubview(orderWarningText)

        orderWarningIcon.frame = CGRect(x: orderWarningText.frame.minX - 25, y: 0, width: globalNavIconSize + 5, height: globalNavIconSize + 5)
        orderWarningIcon.setImage(UIImage(named: "sandTimer.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        orderWarningIcon.center.y = orderWarningBox.center.y
        orderWarningIcon.tintColor = UIColor.black
        orderWarningIcon.layer.zPosition = 2
        scrollView.addSubview(orderWarningIcon)

        //recenter
        orderWarningText.center.y = orderWarningBox.center.y
        orderWarningText.sizeToFit()
        orderWarningText.center.x = self.view.center.x + (orderWarningIcon.frame.width / 2) - 3.75
        orderWarningIcon.frame.origin.x = orderWarningText.frame.minX - (globalNavIconSize + 5)
        orderWarningIcon.center.y = orderWarningBox.center.y

        menuMenuText = SpringLabel(frame: CGRect(x: 0, y: orderWarningBox.frame.maxY + (orderWarningBox.frame.height), width: self.view.frame.width, height: globalNavIconSize))
        menuMenuText.text = "Menu"
        menuMenuText.font = normalFont
        menuMenuText.textAlignment = .center
        menuMenuText.textColor = UIColor.black
        menuMenuText.sizeToFit()
        menuMenuText.center.x = self.view.center.x
        menuMenuText.backgroundColor = .clear
        menuMenuText.isUserInteractionEnabled = false
        menuMenuText.layer.zPosition = 2
        scrollView.addSubview(menuMenuText)

        menuSeperatorText = SpringLabel(frame: CGRect(x: 0, y: menuMenuText.frame.maxY, width: self.view.frame.width, height: globalNavIconSize - 2))
        menuSeperatorText.text = "____"
        menuSeperatorText.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        menuSeperatorText.textAlignment = .center
        menuSeperatorText.textColor = UIColor.black
        menuSeperatorText.sizeToFit()
        menuSeperatorText.center.x = self.view.center.x
        menuSeperatorText.backgroundColor = .clear
        menuSeperatorText.isUserInteractionEnabled = false
        menuSeperatorText.layer.zPosition = 2
        scrollView.addSubview(menuSeperatorText)

        if todaysMenu.count > 0 {
            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[0].MenuItems.count))

        } else {
            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 120 + 120 )
        }

        self.tableView.rowHeight = 120
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.alwaysBounceVertical = true // lock table
        self.tableView.isScrollEnabled = false
        self.tableView.backgroundColor = .clear
        self.tableView.center.x = view.center.x
        self.tableView.separatorColor = UIColor.black.withAlphaComponent(0.3) // change seperator color
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.layer.zPosition = 3
        self.tableView.isHidden = false
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero
        scrollView.addSubview(tableView)
    }

    func createSecondMenu() {
        print("IN createSecondMenu | start")

        secondMenuCreated = true

        //Menu 2
        menuBox2.frame = CGRect(x: 0, y: tableView.frame.maxY + 25, width: self.view.frame.width - 75, height: 125)

        if isIPhoneX {
            menuBox2.frame = CGRect(x: 0, y: Int(tableView.frame.maxY + 50), width: Int(self.view.frame.width - 75), height: 125)
        }

        menuBox2.backgroundColor = .white
        menuBox2.center.x = self.view.center.x
        menuBox2.layer.borderColor = hexLightGray.cgColor
        menuBox2.layer.borderWidth = 1
        menuBox2.layer.zPosition = 1
        menuBox2.isUserInteractionEnabled = false

        //test drop shadow
        menuBox2.layer.shadowColor = hexDarkGray.cgColor
        menuBox2.layer.shadowOpacity = 0.5
        menuBox2.layer.shadowOffset = globalShadowOffset
        menuBox2.layer.shadowRadius = 1

        scrollView.addSubview(menuBox2)

        menuToday2 = SpringLabel(frame: CGRect(x: 0, y: menuBox2.frame.minY + (menuBox2.frame.height / 7), width: self.view.frame.width, height: globalNavIconSize))

        menuToday2.text = "Featuring"
        menuToday2.font = normalFont
        menuToday2.textAlignment = .center
        menuToday2.textColor = hexGray
        menuToday2.sizeToFit()
        menuToday2.center.x = self.view.center.x
        menuToday2.backgroundColor = .clear
        menuToday2.isUserInteractionEnabled = false
        menuToday2.layer.zPosition = 2
        scrollView.addSubview(menuToday2)

        menuRestaurantName2 = SpringLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))

        if todaysMenu.count > 1 {
            menuRestaurantName2.text = todaysMenu[1].Name

        }

        menuRestaurantName2.font = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.regular)
        menuRestaurantName2.textAlignment = .center
        menuRestaurantName2.textColor = UIColor.black
        menuRestaurantName2.sizeToFit()
        menuRestaurantName2.center.x = self.view.center.x
        menuRestaurantName2.center.y = menuBox2.center.y - (menuBox2.frame.height / 6)

        menuRestaurantName2.backgroundColor = .clear
        menuRestaurantName2.isUserInteractionEnabled = false
        menuRestaurantName2.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantName2)

        menuRestaurantSummary2 = SpringLabel(frame: CGRect(x: 0, y: menuBox2.frame.maxY - ((menuBox2.frame.height / 7) + (globalNavIconSize / 2)), width: self.view.frame.width, height: globalNavIconSize))

        if todaysMenu.count > 1 {
            menuRestaurantSummary2.text = "\(todaysMenu[1].Rating) | \(todaysMenu[1].Genre)"
        }

        menuRestaurantSummary2.font = normalFont
        menuRestaurantSummary2.textAlignment = .center
        menuRestaurantSummary2.textColor = hexGray
        menuRestaurantSummary2.sizeToFit()
        menuRestaurantSummary2.center.x = self.view.center.x
        menuRestaurantSummary2.center.y = menuBox2.center.y + (menuBox2.frame.height / 4)

        menuRestaurantSummary2.backgroundColor = .clear
        menuRestaurantSummary2.isUserInteractionEnabled = false
        menuRestaurantSummary2.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantSummary2)

        menuRestaurantIcon2 = SpringButton(frame: CGRect(x: menuRestaurantSummary2.frame.minX - 22.5, y: 0, width: globalNavIconSize + 5, height: globalNavIconSize + 5))
        menuRestaurantIcon2.setImage(UIImage(named: "star_yellow.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        menuRestaurantIcon2.center.y = menuRestaurantSummary2.center.y
        menuRestaurantIcon2.tintColor = hexYellow.Color
        menuRestaurantIcon2.layer.zPosition = 2
        scrollView.addSubview(menuRestaurantIcon2)

        //recenter
        menuRestaurantSummary2.text = "\(todaysMenu[1].Rating) | \(todaysMenu[1].Genre)"
        menuRestaurantSummary2.sizeToFit()
        menuRestaurantSummary2.center.x = self.view.center.x + (menuRestaurantIcon2.frame.width / 2) - 3.75
        menuToday2.center.x = self.view.center.x
        menuRestaurantIcon2.frame.origin.x = menuRestaurantSummary2.frame.minX - (menuRestaurantIcon2.frame.width - 2.5)
        menuRestaurantIcon2.center.y = menuRestaurantSummary2.center.y

        menuMenuText2 = SpringLabel(frame: CGRect(x: 0, y: menuBox2.frame.maxY + (orderWarningBox.frame.height), width: self.view.frame.width, height: globalNavIconSize))
        menuMenuText2.text = "Menu"
        menuMenuText2.font = normalFont
        menuMenuText2.textAlignment = .center
        menuMenuText2.textColor = UIColor.black
        menuMenuText2.sizeToFit()
        menuMenuText2.center.x = self.view.center.x
        menuMenuText2.backgroundColor = .clear
        menuMenuText2.isUserInteractionEnabled = false
        menuMenuText2.layer.zPosition = 2
        scrollView.addSubview(menuMenuText2)

        menuSeperatorText2 = SpringLabel(frame: CGRect(x: 0, y: menuMenuText2.frame.maxY, width: self.view.frame.width, height: globalNavIconSize - 2))
        menuSeperatorText2.text = "____"
        menuSeperatorText2.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        menuSeperatorText2.textAlignment = .center
        menuSeperatorText2.textColor = UIColor.black
        menuSeperatorText2.sizeToFit()
        menuSeperatorText2.center.x = self.view.center.x
        menuSeperatorText2.backgroundColor = .clear
        menuSeperatorText2.isUserInteractionEnabled = false
        menuSeperatorText2.layer.zPosition = 2
        scrollView.addSubview(menuSeperatorText2)

        if todaysMenu.count > 1 {
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText2.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[1].MenuItems.count) + 120 )
        } else {
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText2.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 120 + 120 )

        }

        self.tableView2.rowHeight = 120

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView2.alwaysBounceVertical = true // lock table
        self.tableView2.isScrollEnabled = false
        self.tableView2.backgroundColor = .clear
        self.tableView2.center.x = view.center.x
        self.tableView2.separatorColor = UIColor.black.withAlphaComponent(0.3) // change seperator color
        self.tableView2.showsVerticalScrollIndicator = false
        self.tableView2.layer.zPosition = 3
        self.tableView2.isHidden = false
        self.tableView2.layoutMargins = UIEdgeInsets.zero
        self.tableView2.separatorInset = UIEdgeInsets.zero
        scrollView.addSubview(tableView2)

        let scrollViewHeightBase = ((25 + menuBox.frame.height + (5 * globalNavIconSize)) * 2) + (orderWarningBox.frame.height)
        var scrollViewHeightRowHeight: CGFloat = tableView.frame.height + tableView2.frame.height
        if todaysMenu.count > 0 {
            var totalItemCount = 0
            if todaysMenu.count == 1 {
                totalItemCount = todaysMenu[0].MenuItems.count
            } else if todaysMenu.count > 1 {
                totalItemCount = todaysMenu[0].MenuItems.count + todaysMenu[1].MenuItems.count
            }

            scrollViewHeightRowHeight = CGFloat((CGFloat(totalItemCount) + 1) * 120)
        }
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: scrollViewHeightBase + scrollViewHeightRowHeight)

        //        toggleMenuNotAvaiableUI()

    }

    func createLoadingMenuNotification() {
        print("in createLoadingMenuNotification | start")

        loadingUIIsDisplayed = true
        loadingMenu.frame = CGRect(x: 20, y: 0, width: view.frame.width - 80, height: view.frame.height / 2.5)
        loadingMenu.center = view.center
        loadingMenu.isUserInteractionEnabled = true
        loadingMenu.layer.zPosition = 105
        loadingMenu.backgroundColor = .clear
        loadingMenu.tag = 300
        view.addSubview(loadingMenu)

        let viewYFourths = loadingMenu.frame.height / 5

        orderCutoff.frame = CGRect(x: 0, y: loadingMenu.frame.height - viewYFourths, width: loadingMenu.frame.width, height: 25)
        if isIPhoneSE == true {
            orderCutoff.frame = CGRect(x: 0, y: loadingMenu.frame.height - viewYFourths, width: loadingMenu.frame.width, height: 25)
        }

        orderCutoff.text = "Getting today's menu\(loadingDots)"

        loadingMenuTimer.invalidate()
        loadingMenuTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.refreshLoadingText), userInfo: nil, repeats: true)

        if currentlySelectedMenuDay.day == currentLocalUserTime.day { // if today is after cut off time
            if isCurrentTimeAfterTodaysCutoffTime == true {
                var day = currentlySelectedMenuDay + 1.days

                if day.weekdayName(.default) == "Saturday" {
                    day = day + 2.days
                } else if day.weekdayName(.default) == "Sunday" {
                    day = day + 1.days
                }
                orderCutoff.text = "Loading \(day.weekdayName(.default))'s menu\(loadingDots)"
            }
        } else {
            let day = currentlySelectedMenuDay
            orderCutoff.text = "Loading \(day.weekdayName(.default))'s menu\(loadingDots)"
        }

        orderCutoff.textColor = .white  //.black
        
        orderCutoff.textAlignment = .center
        orderCutoff.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.light)

        if isIPhoneSE == true {
            orderCutoff.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        }

        orderCutoff.isUserInteractionEnabled = false
        orderCutoff.layer.zPosition = 102
        orderCutoff.tag = 301
        orderCutoff.backgroundColor = .clear
        orderCutoff.sizeToFit()
        orderCutoff.frame.origin.x = (loadingMenu.frame.width / 2) - (orderCutoff.frame.width / 2) - 2
        loadingMenu.addSubview(orderCutoff)

        let gifSize: CGFloat = 0.8
        let gif = UIImage(gifName: "\(globalRefreshGIFName).gif", levelOfIntegrity:0.25)
        let imageview = UIImageView(gifImage: gif, loopCount: -1)
        imageview.showFrameAtIndex(49)
        imageview.tag = 302
        imageview.frame = CGRect(x: 0, y: (2 * viewYFourths) - loadingMenu.frame.width * (gifSize / 2), width: loadingMenu.frame.width * gifSize, height: loadingMenu.frame.width * gifSize)
        imageview.center.x = loadingMenu.center.x - 40
        loadingMenu.addSubview(imageview)
    }

    func createMenuNotAvaiableUI() {
//        defer {
//            menuNotAvailableCreated = true
//        }
//        if menuNotAvailableCreated != true {
            print("IN createMenuNotAvaiableUI | start")
            let back: SpringButton = SpringButton(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.height))
            back.backgroundColor = .white
            back.layer.zPosition = 900
            back.tag = 500
            back.isHidden = true

            self.view.addSubview(back)

            let userNote: SpringTextView = SpringTextView(frame: CGRect(x: 0, y: (self.view.frame.height / 2) - 105, width: view.frame.width, height: 77.5))

            userNote.textColor = hexDarkGray
            userNote.contentInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
            userNote.font = UIFont(name: pinkFontBold, size: 24)
            userNote.layer.zPosition = 9001
            userNote.text = "Hello! Our chefs are hard at work \nputting together a menu for you..."
            userNote.isUserInteractionEnabled = false
            userNote.center.x = view.center.x
            userNote.isSelectable = false
            userNote.backgroundColor = .clear
            userNote.centerVertically()
            userNote.textAlignment = .center
            userNote.tag = 501
            userNote.isHidden = true

            self.view.addSubview(userNote)

            let noMenuImage = UIImageView()
            noMenuImage.frame = CGRect(x: 0, y: userNote.frame.maxY + 25, width: self.view.frame.width - 20, height: view.frame.width * 0.33334)

            if isIPhoneSE == true {
                userNote.frame.origin.y = (self.view.frame.height / 2) - 87.5
                noMenuImage.frame.origin.y = userNote.frame.maxY + 10
                userNote.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
                userNote.font = UIFont(name: pinkFontBold, size: 20)
            }

            noMenuImage.image = UIImage(named: "noMenu_illustration.png")
            noMenuImage.contentMode =  UIViewContentMode.scaleAspectFill
            noMenuImage.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
            noMenuImage.center.x = view.center.x
            noMenuImage.backgroundColor = .clear
            noMenuImage.tag = 502
            noMenuImage.isHidden = true
            noMenuImage.clipsToBounds = true
            noMenuImage.layer.zPosition = 9002
            self.view.addSubview(noMenuImage)
//        }

    }

    @objc func removeCartButton() {
        print("removeCartButton | start")
        var removedCartButton: Bool = false
        while removedCartButton != true {
            if let b =  view.viewWithTag(101) as? SpringButton {
                b.removeFromSuperview()
            } else {
                removedCartButton = true
            }
        }
        print("removeCartButton | finish")
    }

}

// MARK: Refresh Methods
extension MenuVC {

    @objc func refreshPage() { //refresh all data fields
        print("IN MenuVC refreshPage | todays menu count", todaysMenu.count)

        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.backgroundColor = .white
        }, completion: nil )

        loadingMenuTimer.invalidate() //stops loading text ... change
        loadingUIIsDisplayed = false //allows cart button to now load

        viewCartButton.removeTarget(nil, action: nil, for: .allEvents)
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        view.subviews.forEach({ $0.removeFromSuperview() })

        createMenuNotAvaiableUI()
        createUIElements()

        if userData?.AddressSupported == false {
            print("IN MenuVC refreshPage | toggleOutsideDeliveryRadius")
            toggleOutsideDeliveryRadius()
        } else if comingFrom.register == true && isCurrentTimeAfterTodaysCutoffTime == true {
            print("IN MenuVC refreshPage | register after cutoff")
            comingFrom.register = false
            removeCartButton()
            comingFrom.menu = false
            segue(name: "AllSetNextDayRegisterFromMenu", v: self, type: .cover(direction: .up))
        } else if comingFrom.register == true {
            comingFrom.register = false
            showNewUserTutorial(originalView: self)
        } else if firstMenuCreated == false {
            print("IN MenuVC refreshPage | firstMenuNotCreated | count: ", todaysMenu.count)

            if todaysMenu.count > 0 {
                print("IN MenuVC refreshPage | createfirstMenu")

                createfirstMenu()
                if todaysMenu.count > 1 {
                    if secondMenuCreated == false {
                        print("IN MenuVC refreshPage | createSecondMenu")
                        createSecondMenu()
                    }
                }
            } else {
                print("IN MenuVC refreshPage | toggleMenuNotAvaiableUI")
                toggleMenuNotAvaiableUI()
            }
        } else {
            if todaysMenu.count > 0 {
                scrollView.isHidden = false
            } else {
                toggleMenuNotAvaiableUIAction()
            }
        }

        refreshMenuDayText()
        tableView.reloadData()
        tableView2.reloadData()

        //move scrollview back to top
        self.scrollView.setContentOffset( // reaset scoll view back to top most point
            CGPoint(x: 0, y: 0), animated: false)

        //setup scrollview
        scrollView.frame = CGRect(x: 0, y: restaurant1Button.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)
        scrollView.center.x = self.view.center.x
        scrollView.isUserInteractionEnabled = true
        scrollView.alwaysBounceVertical = false
        scrollView.delegate = self
        scrollView.backgroundColor = .clear

        if todaysMenu.count > 1 {
            scrollView.frame = CGRect(x: 0, y: restaurant1Button.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)
        } else if todaysMenu.count == 1 {
            scrollView.frame = CGRect(x: 0, y: navigationBar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)
        }

        view.addSubview(scrollView)

        if todaysMenu.count > 0 {
            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[0].MenuItems.count))

        } else {
            scrollView.frame = CGRect(x: 0, y: navigationBar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - navigationBar.frame.maxY)

            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 200 )
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 200 )

        }

        if todaysMenu.count > 0 {

            if currentUserTime.weekdayName(.default) != currentlySelectedMenuDay.weekdayName(.default) || isCurrentTimeAfterTodaysCutoffTime == true {
                orderWarningText.text = "Pre-order for \(currentlySelectedMenuDay.weekdayName(.default))"
            } else {
                orderWarningText.text = "Order before 11:00am"
            }

            //recenter
            orderWarningText.center.y = orderWarningBox.center.y
            orderWarningText.sizeToFit()
            orderWarningText.center.x = self.view.center.x + (orderWarningIcon.frame.width / 2) - 3.75
            orderWarningIcon.frame.origin.x = orderWarningText.frame.minX - (globalNavIconSize + 5)
            orderWarningIcon.center.y = orderWarningBox.center.y

            //restauran 1
            menuRestaurantName.text = todaysMenu[0].Name
            menuToday.text = todaysMenu[0].Tag
            menuToday.sizeToFit()
            menuRestaurantName.sizeToFit()
            menuRestaurantName.center.x = self.view.center.x
            menuRestaurantName.center.x = self.view.center.x
            menuRestaurantName.center.y = menuBox.center.y - (menuBox.frame.height / 6)

            menuRestaurantSummary.text = "\(todaysMenu[0].Rating) | \(todaysMenu[0].Genre)"
            menuRestaurantSummary.sizeToFit()
            menuRestaurantSummary.center.x = self.view.center.x + (menuRestaurantIcon.frame.width / 2) - 3.75
            menuRestaurantSummary.center.y = menuBox.center.y + (menuBox.frame.height / 4)
            menuToday.center.x = self.view.center.x
            menuRestaurantIcon.frame.origin.x = menuRestaurantSummary.frame.minX - (menuRestaurantIcon.frame.width - 2.5)
            menuRestaurantIcon.center.y = menuRestaurantSummary.center.y

        }

        if isIPhoneX == true {
            navigationBar.frame = CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width, height: globalYAxisHeight)

            let height = navigationBar.frame.height + (globalIPhoneXTopYPadding / 1.5)
            navigationLeft.center.y = height
            navigationRight.center.y = height
            navigationDayText.center.y = height
            navigationDayIcon.center.y = navigationDayText.center.y

        }

        if todaysMenu.count > 1 {

            //Menu 2
            menuBox2.frame.origin.y = tableView.frame.maxY + 25

            if isIPhoneX {
                menuBox2.frame.origin.y = tableView.frame.maxY + 50
            }

            menuRestaurantSummary2.frame.origin.y = menuBox2.frame.maxY - ((menuBox2.frame.height / 7) + (globalNavIconSize / 2))

            menuMenuText2.frame.origin.y = menuBox2.frame.maxY + (orderWarningBox.frame.height)
            menuSeperatorText2.frame.origin.y = menuMenuText2.frame.maxY
            menuRestaurantName2.text = todaysMenu[1].Name
            menuRestaurantSummary2.text = "\(todaysMenu[1].Rating) | \(todaysMenu[1].Genre)"
            menuToday2.text = todaysMenu[1].Tag
            menuToday2.sizeToFit()
            menuRestaurantName2.sizeToFit()
            menuRestaurantSummary2.sizeToFit()
            menuRestaurantSummary2.center.x = self.view.center.x + (menuRestaurantIcon2.frame.width / 2) - 3.75

            menuToday2.center.x = self.view.center.x
            menuRestaurantIcon2.frame.origin.x = menuRestaurantSummary2.frame.minX - (menuRestaurantIcon2.frame.width - 2.5)

            menuRestaurantName2.center.x = self.view.center.x
            menuRestaurantName2.center.y = menuBox2.center.y - (menuBox2.frame.height / 6)
            menuRestaurantSummary2.center.y = menuBox2.center.y + (menuBox2.frame.height / 4)
            menuRestaurantIcon2.center.y = menuRestaurantSummary2.center.y
        }

        if todaysMenu.count > 1 {
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText2.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[1].MenuItems.count))
        }

        let scrollViewHeightBase = ((25 + menuBox.frame.height + (5 * globalNavIconSize)) * 2) + (orderWarningBox.frame.height)
        var scrollViewHeightRowHeight: CGFloat = tableView.frame.height + tableView2.frame.height
        if todaysMenu.count > 0 {
            var totalItemCount = 0
            if todaysMenu.count == 1 {

                totalItemCount = todaysMenu[0].MenuItems.count
            } else if todaysMenu.count > 1 {
                totalItemCount = todaysMenu[0].MenuItems.count + todaysMenu[1].MenuItems.count
            }

            scrollViewHeightRowHeight = CGFloat((CGFloat(totalItemCount) + 1) * 120)
        }

        scrollView.contentSize = CGSize(width: self.view.frame.width, height: scrollViewHeightBase + scrollViewHeightRowHeight)

        refreshMenuHeader()
        toggleViewCartButton()
        toggleUpcomingOrderCount()
        globalLoadingSpinner.stopAnimating()

    }

    func refreshTableView() {
        print("IN refreshTableView | start")

        if todaysMenu.count > 0 {
            print("IN refreshTableView | count more than zero")

            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[0].MenuItems.count) + 120 )

            if todaysMenu.count > 1 {

                print("IN refreshTableView | count more than one")

                self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText2.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: (120 * todaysMenu[1].MenuItems.count) + 120 )
            }

        } else {
            self.tableView.frame = CGRect(x: 0, y: Int(menuSeperatorText.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 120 + 120 )
            self.tableView2.frame = CGRect(x: 0, y: Int(menuSeperatorText2.frame.maxY + globalNavIconSize), width: Int(self.view.frame.width), height: 120 + 120 )

        }

        self.tableView.rowHeight = 120
        self.tableView.alwaysBounceVertical = true // lock table
        self.tableView.isScrollEnabled = false
        self.tableView.backgroundColor = .clear
        self.tableView.center.x = view.center.x
        self.tableView.separatorColor = UIColor.black.withAlphaComponent(0.3) // change seperator color
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.layer.zPosition = 3
        self.tableView.isHidden = false
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero

        self.tableView2.rowHeight = 120
        self.tableView2.alwaysBounceVertical = true // lock table
        self.tableView2.isScrollEnabled = false
        self.tableView2.backgroundColor = .clear
        self.tableView2.center.x = view.center.x
        self.tableView2.separatorColor = UIColor.black.withAlphaComponent(0.3) // change seperator color
        self.tableView2.showsVerticalScrollIndicator = false
        self.tableView2.layer.zPosition = 3
        self.tableView2.isHidden = false
        self.tableView2.layoutMargins = UIEdgeInsets.zero
        self.tableView2.separatorInset = UIEdgeInsets.zero

        tableView.reloadData()
        tableView2.reloadData()

    }

    @objc func refreshMenuDayText() {
        print("in refreshMenuDayText | start")

        navigationDayText.text = "\(currentlySelectedMenuDay.weekdayName(.default))'s Menu"
        navigationDayText.sizeToFit()
        navigationDayText.center.x = self.view.center.x

        //
        navigationDayIcon.frame = CGRect(x: navigationDayText.frame.maxX - 3, y: 0, width: globalNavIconSize + 5, height: globalNavIconSize + 5)
        navigationDayIcon.center.y = navigationBar.center.y


        if isIPhoneX == true {
            navigationDayIcon.center.y = navigationDayText.center.y
        }

        if currentUserTime.weekdayName(.default) != currentlySelectedMenuDay.weekdayName(.default) || isCurrentTimeAfterTodaysCutoffTime == true {
            orderWarningText.text = "Pre-order for \(currentlySelectedMenuDay.weekdayName(.default))"
            orderWarningText.center.y = orderWarningBox.center.y
            orderWarningText.sizeToFit()
            orderWarningText.center.x = self.view.center.x + (orderWarningIcon.frame.width / 2) - 3.75
            orderWarningIcon.frame.origin.x = orderWarningText.frame.minX - (globalNavIconSize + 5)
            orderWarningIcon.center.y = orderWarningBox.center.y

        }
    }

    func refreshMenuHeader() {
        print("in refreshMenuHeader | start")
        if todaysMenu.count < 2 { //hide headers if only one menu
            restaurant1Button.isHidden = true
            restaurant2Button.isHidden = true
            restaurantButtonBorder.isHidden = true
            restaurantButtonBorder.isHidden = true
        } else {
            restaurant1Button.isHidden = false
            restaurant2Button.isHidden = false
            restaurantButtonBorder.isHidden = false
            restaurantButtonBorder.isHidden = false
        }

        if todaysMenu.count > 0 {
            restaurant1Button.setTitle(todaysMenu[0].Name, for: .normal)
        } else {
            restaurant1Button.setTitle("Restaurant 1", for: .normal)
        }

        if todaysMenu.count > 1 {
            restaurant2Button.setTitle(todaysMenu[1].Name, for: .normal)
        } else {
            restaurant2Button.setTitle("Restaurant 2", for: .normal)

        }
    }

    @objc func refreshLoadingText() {
        if loadingDots == "." {
            loadingDots = ".."
        } else if loadingDots == ".." {
            loadingDots = "..."

        } else if loadingDots == "..." {
            loadingDots = ""
        } else if loadingDots == "" {
            loadingDots = "."
        }

        orderCutoff.text = "Getting today's menu\(loadingDots)"
        if currentlySelectedMenuDay.day == currentLocalUserTime.day { // if today is after cut off time
            if isCurrentTimeAfterTodaysCutoffTime == true {
                var day = currentlySelectedMenuDay + 1.days

                if day.weekdayName(.default) == "Saturday" {
                    day = day + 2.days
                } else if day.weekdayName(.default) == "Sunday" {
                    day = day + 1.days
                }
                orderCutoff.text = "Loading \(day.weekdayName(.default))'s menu\(loadingDots)"
            }
        } else {
            let day = currentlySelectedMenuDay
            orderCutoff.text = "Loading \(day.weekdayName(.default))'s menu\(loadingDots)"
        }

        orderCutoff.sizeToFit()

        if orderCutoff.frame.width > orderCutoffSize {
            orderCutoffSize = orderCutoff.frame.width
        }

        orderCutoff.frame.origin.x = (loadingMenu.frame.width / 2) - (orderCutoffSize / 2)
    }

    @objc func triggerRemoveNextDayNotification() { //not used anymore
        removeNextDayNotification(v: self.view)
    }

}
// MARK: UX Elements
extension MenuVC {

    @objc func toggleDisplayRateOrder() {
        print("in toggleDisplayRateOrder | start")

        if completedOrders.count > 0 {

            //find newest order
            var newestDate: DateInRegion = "7/30/2000".toDate()!
            var index = 0
            var indexTarget = 0

            for item in completedOrders {
                let date: DateInRegion = item.Date.toDate()!
                if date.isAfterDate(newestDate, granularity: .day) {
                    newestDate = date
                    indexTarget = index
                }
                index += 1
            }

            let latestOrder = completedOrders[indexTarget]

            if latestOrder.IsRated == false && lastOrderIDPromptedForRating != latestOrder.OrderID {
                // init YourViewController

                selectedOrderItem = latestOrder
                lastOrderIDPromptedForRating = selectedOrderItem.OrderID
                //save to stop prompt in the future
                defaults.set(lastOrderIDPromptedForRating, forKey: "lastOrderRated")
                defaults.synchronize()

                //show rating page
                print("RCB - ratingpage")

                removeCartButton()
                //isInMenuPage = false
                comingFrom.menu = false

                let contentVC = OrderRatingVC()
                // Init popup view controller with content is your content view controller
                let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
                present(popupVC, animated: true)

            }
        }
    }

    @objc func toggleUpcomingOrderCount() {
        print("in toggleUpcomingOrderCount | start")
        if upcomingOrders.count > 0 {
            navigationCounter.setTitle(String(upcomingOrders.count), for: .normal)
            navigationCounter.frame.origin.x = navigationRight.frame.maxX - (navigationRight.frame.width / 3) - 8
            navigationCounter.frame.origin.y = navigationRight.frame.minY +  (navigationRight.frame.width / 4) - 5
            navigationCounter.isHidden = false
        } else {
            navigationCounter.setTitle(String(upcomingOrders.count), for: .normal)
            navigationCounter.isHidden = true
        }

    }

    @objc func toggleMenusView(_ destination: String) {
        print("IN toggleMenusView | start")

        let duration = 0.30

        if destination == "restaurantOne" {
            movingBetweenRestaurantViews = true
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseIn, animations: {
                self.restaurantButtonBorder.frame = CGRect(x: 0, y: self.restaurant2Button.frame.maxY, width: self.view.frame.width / 2, height: 1)
                self.scrollView.setContentOffset( // reaset scoll view back to top most point
                    CGPoint(x: 0, y: 0), animated: false)

            }, completion: { (value: Bool) in

                self.toggleMenuButtonBackground(destination)
                self.movingBetweenRestaurantViews = false
            })
        } else if destination == "restaurantTwo" {
            movingBetweenRestaurantViews = true

            UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseIn, animations: {

                self.scrollView.setContentOffset( // reaset scoll view back to top most point
                    CGPoint(x: 0, y: self.menuBox2.frame.minY - (self.restaurant1Button.frame.height / 2)), animated: false)

                self.restaurantButtonBorder.frame = CGRect(x: self.view.frame.width / 2, y: self.restaurant2Button.frame.maxY, width: self.view.frame.width / 2, height: 1)
            }, completion: { (value: Bool) in

                self.toggleMenuButtonBackground(destination)
                self.movingBetweenRestaurantViews = false
            })
        }
    }

    @objc func toggleViewCartButtonFromGetCartAPI() {
        print("in toggleViewCartButtonFromGetCartAPI | start")
        toggleViewCartButton()
    }

    func toggleViewCartButton() {
        if loadingUIIsDisplayed == false {
            if userCartOrder.OrderedItems.count > 0 {
                toggleAnimateCartButton(true)
            } else {
                toggleAnimateCartButton(false)
            }
        }
    }

    func toggleOutsideDeliveryRadius() {
        if let supported = userData?.AddressSupported {
            print("in toggleOutsideDeliveryRadius | start | supported? ", supported)
        }

        if userData?.AddressSupported == false {
            //comingFromMenuToUpdateAddress = true
            comingFrom.menuToUpdateAddress = true
            sourceView = .menu

            print("RCB - toggle outside delivery radius")
            removeCartButton()

            //show unsupported Delivery Page
            let contentVC = OutsideDeliveryRadiusVC()
            // Init popup view controller with content is your content view controller
            //isInMenuPage = false
            comingFrom.menu = false

            let popupVC = PopupViewController(contentController: contentVC, popupWidth: self.view.frame.width, popupHeight: self.view.frame.height)
            present(popupVC, animated: true)
        }
    }

    func toggleMenuNotAvaiableUIAction() {
        print("in toggleMenuNotAvaiableUIAction | start | count: ", todaysMenu.count)
        if todaysMenu.count > 0 {
            scrollView.isHidden = false
            if let b =  view.viewWithTag(502) as? UIImageView {
                b.isHidden = true
            }
            if let b =  view.viewWithTag(501) as? SpringTextView {
                b.isHidden = true
            }
            if let b =  view.viewWithTag(500) as? SpringButton {
                b.isHidden = true
            }
        } else {
            scrollView.isHidden = true
            if let b =  view.viewWithTag(502) as? UIImageView {
                print("in toggleMenuNotAvaiableUIAction  in let b")
                b.isHidden = false
            } else {
                print("warning")
            }
            if let b =  view.viewWithTag(501) as? SpringTextView {
                b.isHidden = false
            } else {
                print("warning2")
            }
            if let b =  view.viewWithTag(500) as? SpringButton {
                b.isHidden = false
            }else {
                print("warning3")
            }
        }

    }

    func toggleMenuNotAvaiableUI() {
        print("in toggleMenuNotAvaiableUI | start")

        if userData?.AddressSupported == false {
            sourceView = .menu
            //comingFromMenuToUpdateAddress = true
            comingFrom.menuToUpdateAddress = true
            print("RCB - toggle menu not available")

            removeCartButton()
            //isInMenuPage = false
            comingFrom.menu = false

            segue(name: "OutsideDeliveryVCSegueFromMenu", v: self, type: .fade)
        } else {
            toggleMenuNotAvaiableUIAction()
        }
    }

    func toggleMenuButtonBackground(_ destination: String) {
        //        print("in toggleMenuButtonBackground | start")

        if destination == "restaurantOne" {
            onRestaurant1View = true
            self.restaurantButtonBorder.frame = CGRect(x: 0, y: self.restaurant2Button.frame.maxY, width: self.view.frame.width / 2, height: 1)
            self.restaurant1Button.setTitleColor(hexRed.Color, for: .normal)
            self.restaurant2Button.setTitleColor(hexDarkGray.withAlphaComponent(0.7), for: .normal)
        } else {
            onRestaurant1View = false
            self.restaurantButtonBorder.frame = CGRect(x: self.view.frame.width / 2, y: self.restaurant2Button.frame.maxY, width: self.view.frame.width / 2, height: 1)
            self.restaurant1Button.setTitleColor(hexDarkGray.withAlphaComponent(0.7), for: .normal)
            self.restaurant2Button.setTitleColor(hexRed.Color, for: .normal)
        }

    }

    func toggleAnimateCartButton(_ show: Bool) {
        //        print("in toggleAnimateCartButton | start")

        let duration: Double = 0.25

        if show == true {
            print("in toggleAnimateCartButton | show")

            var buttonInView: Bool = false
            for subview in view.subviews {
                if subview is SpringButton && subview.tag == 101 {
                    buttonInView = true
                }
            }

            if buttonInView ==  false {
                print("in toggleAnimateCartButton | button not in view")

                //create button
                viewCartButton.frame = CGRect(x: 0, y: self.view.frame.height + 1, width: self.view.frame.width, height: globalLargeButtonHeight)

                if isIPhoneX == true {
                    viewCartButton.frame = CGRect(x: 0, y: self.view.frame.height + 1, width: self.view.frame.width, height: globalLargeButtonHeight + globalIPhoneXTopYPadding)
                }

                viewCartButton.titleLabel?.font = UIFont.Theme.size15
                viewCartButton.setTitleColor(.white, for: .normal)
                viewCartButton.setTitle("View cart", for: .normal)
//                viewCartButton.backgroundColor = hexBlack.Color
                viewCartButton.setBackgroundColor(color: newHexBlack2.Color, forState: .normal)

                viewCartButton.layer.zPosition = 10
                viewCartButton.contentVerticalAlignment = .center
                viewCartButton.contentHorizontalAlignment = .center
                viewCartButton.isUserInteractionEnabled = true
                viewCartButton.isEnabled = true
                viewCartButton.tag = 101
                view.isUserInteractionEnabled = true
                viewCartButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

                cartButtonCountButton.frame = CGRect(x: globalXAxisIndent, y: 0, width: 15, height: 15)

                cartButtonCountButton.titleLabel?.font = UIFont.Theme.size15

                cartButtonCountButton.setTitleColor(.white, for: .normal)
                cartButtonCountButton.contentVerticalAlignment = .center
                cartButtonCountButton.contentHorizontalAlignment = .center
                cartButtonCountButton.isUserInteractionEnabled = false
                cartButtonCountButton.backgroundColor = .clear
                cartButtonCountButton.layer.borderColor = UIColor.white.cgColor
                cartButtonCountButton.layer.cornerRadius = 3
                cartButtonCountButton.layer.masksToBounds = true
                cartButtonCountButton.layer.borderWidth = 1.0
                cartButtonCountButton.layer.zPosition = 11
                cartButtonCountButton.setTitle(String("0"), for: .normal)
                cartButtonCountButton.center.y = viewCartButton.center.y
                view.addSubview(cartButtonCountButton)

                cartButtonPriceButton.frame = CGRect(x: 0, y: 0, width: viewCartButton.frame.width, height: globalLargeButtonHeight)
                cartButtonPriceButton.titleLabel?.font = UIFont.Theme.size15
                cartButtonPriceButton.setTitleColor(.white, for: .normal)
                cartButtonPriceButton.contentVerticalAlignment = .center
                cartButtonPriceButton.contentHorizontalAlignment = .right
                cartButtonPriceButton.isUserInteractionEnabled = false
                cartButtonPriceButton.layer.zPosition = 11
                cartButtonPriceButton.backgroundColor = .clear

                cartButtonPriceButton.setTitleColor(.white, for: .normal)
                cartButtonPriceButton.setTitle(String(format: "GHS %.0f", userCartOrder.Amount), for: .normal)
                cartButtonPriceButton.center.y = viewCartButton.center.y
                cartButtonPriceButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: globalXAxisIndent)
                view.addSubview(cartButtonPriceButton)
                self.view.addSubview(viewCartButton)
            }
            if userCartOrder.OrderedItems.count > 0 {
                var count = 0
                for item in userCartOrder.OrderedItems {
                    count += Int(item.Quantity)!
                }
                cartButtonCountButton.setTitle(String(count), for: .normal)
                cartButtonPriceButton.setTitle(String(format: "GHS %.0f", userCartOrder.Amount), for: .normal)

                if count > 99 {
                    cartButtonCountButton.frame = CGRect(x: globalXAxisIndent, y: 0, width: 30, height: 15)
                } else if count > 9 {
                    cartButtonCountButton.frame = CGRect(x: globalXAxisIndent, y: 0, width: 20, height: 15)
                }
                cartButtonCountButton.center.y = viewCartButton.center.y
            }

            viewCartButton.isHidden = false
            cartButtonCountButton.isHidden = false
            cartButtonPriceButton.isHidden = false

            //animate tempview
            UIView.animate(withDuration: duration, delay: 1.0, options: .curveEaseOut, animations: {
                if isIPhoneX == true {
                    self.viewCartButton.frame.origin.y = self.view.frame.height - (globalLargeButtonHeight + globalIPhoneXTopYPadding)

                } else {
                    self.viewCartButton.frame.origin.y = self.view.frame.height - globalLargeButtonHeight
                }
                self.cartButtonCountButton.center.y = self.viewCartButton.center.y
                self.cartButtonPriceButton.center.y = self.viewCartButton.center.y
            }, completion: { (value: Bool) in
            })

        } else {
            print("in toggleAnimateCartButton | hide")
            print("RCB - toggle animate cart button HIDE")
            self.removeCartButton()
            self.cartButtonCountButton.removeFromSuperview()
            self.cartButtonPriceButton.removeFromSuperview()
        }
    }

    @objc func buttonPressed(_ sender: UIButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)

        if sender.tag == 5 {
            print("button pressed | removing next day")
            //next day notification dismiss
            //            removeNextDayNotification(v: self.view)
        } else if sender.tag == 11 {

            //            var removedCartButton: Bool = false
            //            while removedCartButton != true {
            //                if let b =  view.viewWithTag(101) as? SpringButton {
            //                    b.removeFromSuperview()
            //                } else {
            //                    removedCartButton = true
            //                }
            //            }

            print("RCB - moving to profile")

            removeCartButton()
            //isInMenuPage = false
            comingFrom.menu = false

            segue(name: "ProfileVCSegueFromMenu", v: self, type: .push(direction: .left))
        } else if sender.tag == 12 {
            if upcomingOrders.count > 0 {
                //onOrdersVCUpcomingView = true
                comingFrom.ordersUpcomingTableView = true

                print("RCB - moving to Orders")

                removeCartButton()
                //isInMenuPage = false
                comingFrom.menu = false

                segue(name: "OrdersVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
            } else {
                //go to history tab
                comingFrom.ordersUpcomingTableView = false

                print("RCB - moving to orders")

                removeCartButton()
                //isInMenuPage = false
                comingFrom.menu = false

                segue(name: "OrdersVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
            }
        } else if sender.tag == 101 {
            // show cart details

            print("RCB - moving to cart")

            removeCartButton()
            //isInMenuPage = false
            comingFrom.menu = false
            BranchAssistant.app.viewCart(cart: userCartOrder)

            segue(name: "OrderCartVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
        } else if sender.tag == 20 {
            //rest 1 pressed
            if onRestaurant1View == false {
                onRestaurant1View.negate()
            }
            toggleMenusView("restaurantOne")

        } else if sender.tag == 21 {
            //rest 2 pressed

            if onRestaurant1View == true {
                onRestaurant1View.negate()
            }
            toggleMenusView("restaurantTwo")

        }
    }

    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("Menu Button pressed: ")
        //DaySelectionVCSegueFromMenu

        print("RCB - move to day selector")

        removeCartButton()
        //isInMenuPage = false
        comingFrom.menu = false

        if hasInternetCheck() == true {
            segue(name: "DaySelectionVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
        } else {
            noInternetWarning()
        }
    }
}


// MARK: ScrollView
extension MenuVC: UIScrollViewDelegate {
    func setContentOffset(scrollView: UIScrollView) {

    }

    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView.contentOffset.y <=  menuBox2.frame.minY - (self.restaurant1Button.frame.height / 2) && movingBetweenRestaurantViews == false {
            toggleMenuButtonBackground("restaurantOne")
        } else if movingBetweenRestaurantViews == false {
            toggleMenuButtonBackground("restaurantTwo")
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

    }
}

//tableviews
extension MenuVC: UITableViewDataSource, UITableViewDelegate {

    func disableHeroTableview() {
        tableView.hero.isEnabled = false
        tableView2.hero.isEnabled = false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        print("in numberOfRowsInSection | start")
        if todaysMenu.isEmpty == false {
            if tableView == tableView2 { //restaurant 2 data
                if todaysMenu.count > 1 {
                    return todaysMenu[1].MenuItems.count
                } else {
                    return 0
                }
            } else { //restaurant 1 data
                return todaysMenu[0].MenuItems.count
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        
        var data: MenuData = emptyMenuItem
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        if todaysMenu.isEmpty == false {
            if tableView == tableView2 { //restaurant 2 data
                if todaysMenu.count > 1 {
                    data = todaysMenu[1].MenuItems[indexPath.row]
                }
            } else { //restaurant 1 data
                data = todaysMenu[0].MenuItems[indexPath.row]
            }
        }

        data.HeroID = "2\(indexPath.row)"
        
        if tableView != tableView2 {
            data.HeroID = "1\(indexPath.row)"
        }
        
        cell.layer.zPosition = 1
        cell.hero.id = "menuItem\(data.HeroID)"
//        cell.menuItemDescription.textContainer.maximumNumberOfLines = 2
//        cell.menuItemDescription.isEditable = false
        cell.menuItemDescription.isUserInteractionEnabled = false
//        cell.menuItemDescription.textContainer.lineFragmentPadding = 0
//        cell.menuItemDescription.textContainerInset = .zero
        cell.menuItemDescription.textAlignment = .left
//        cell.menuItemDescription.centerVertically()
        cell.backgroundColor = .clear
        
//        cell.menuItemTitle.frame = CGRect(x: cell.menuItemTitle.frame.minX, y: cell.menuItemTitle.frame.minY, width: cell.frame.width - (cell.frame.width / 8), height: cell.menuItemTitle.frame.height)
//        cell.menuItemDescription.frame = CGRect(x: cell.menuItemDescription.frame.minX, y: cell.menuItemDescription.frame.minY, width: cell.frame.width - (cell.frame.width / 8), height: cell.menuItemDescription.frame.height)
//        cell.menuItemPrice.frame = CGRect(x: cell.menuItemPrice.frame.minX, y: cell.menuItemPrice.frame.minY, width: cell.frame.width - (cell.frame.width / 8), height: cell.menuItemPrice.frame.height)
//        cell.menuItemImage.frame = CGRect(x: cell.menuItemImage.frame.minX - 20, y: cell.menuItemImage.frame.minY, width: cell.frame.width / 8, height: cell.frame.width / 8)

        if isIPhoneSE == true {
            cell.menuItemDescription.numberOfLines = 2
        }

        cell.menuItemTitle.text = data.Name
        cell.menuItemTitle.hero.id = "menuTitle\(data.HeroID)"

        cell.menuItemDescription.text = data.Description
        cell.menuItemDescription.textAlignment = .left
//        cell.menuItemDescription.centerVertically()
        cell.menuItemDescription.hero.id = "menuDetails\(data.HeroID)"
        cell.menuItemDescription.hero.modifiers =  [.contentsRect(cell.menuItemDescription.frame)]
        
        cell.menuItemPrice.text = "GHS \(formatPrice(p: data.Price))"
        cell.menuItemPrice.hero.id = "menuPrice\(data.HeroID)"
        cell.menuItemPrice.hero.modifiers = [.contentsRect(cell.menuItemPrice.frame)]
        
        //cell.menuItemImage.image = data.Image
        cell.menuItemImage.kf.indicatorType = .none
        cell.menuItemImage.kf.setImage(with: data.ImageURL, placeholder: UIImage(named: "no_dishImage"), options: [.transition(.fade(0.2))]) //use .forceRefresh option to test
        
        let imagePadding: CGFloat = cell.frame.height
        cell.menuItemImageSoldOut.image = UIImage(named: "sold_out")?.addImagePadding(x: imagePadding, y: imagePadding)
        cell.menuItemImageSoldOut.isHidden = true
        cell.menuItemImageSoldOut.layer.zPosition = 100
        cell.menuItemImageSoldOut.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        cell.menuItemImage.hero.id = "menuImage\(data.HeroID)"
        
        cell.menuItemImage.hero.modifiers = [
            .arc(intensity: 2),
            .spring(stiffness: 100, damping: 16.0)
        ]
        
        cell.cellColorOverlay.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: cell.frame.height)
        
        cell.cellColorOverlay.layer.zPosition = 99
        cell.cellColorOverlay.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        cell.cellColorOverlay.isHidden = true
        
        if data.Soldout == true {
            cell.isUserInteractionEnabled = false
            cell.menuItemImageSoldOut.isHidden = false
            cell.cellColorOverlay.isHidden = false
        }
        return cell
    }
    
    private func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //        let selectedRow = [indexPath.row][0]
        
        if tableView == tableView2 {
            selectedMenuItem = todaysMenu[1].MenuItems[indexPath.row]
            selectedMenuItem.HeroID = "2\(indexPath.row)"
        } else {
            selectedMenuItem = todaysMenu[0].MenuItems[indexPath.row]
            selectedMenuItem.HeroID = "1\(indexPath.row)"
        }

        print("RCB - move to menu detail")

        removeCartButton()
        //isInMenuPage = false
        comingFrom.menu = false

        //BranchStandardEventViewItem
        BranchAssistant.app.viewItem(item: selectedMenuItem)

        segue(name: "MenuDetailsVCSegueFromMenu", v: self, type: .pageIn(direction: .up))
    }
}
