//
//  ViewController.swift
//  WeChop
//
//Created by Benjamin Acquah on 9/20/19 9/20/19 9/20/19 on 9/20/19 on 8/26/19.
//  Copyright © 2019 WeChop. All rights reserved.
//

import UIKit
import Spring
import Alamofire
import Hero
import PhoneNumberKit
import SwiftDate
import FBSDKLoginKit
import EzPopup

class LoginVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    var topImage: UIImageView = UIImageView(image: UIImage(named: "afterCutOff_foodImage.jpg"))
    var topImageBlack: UIImageView = UIImageView(image: UIImage(named: ""))
    var topImageIcon: UIImageView = UIImageView(image: UIImage(named: "black_128.png")?.withRenderingMode(.alwaysTemplate))
    var loginButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var registerButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    var lineView: UIView = UIView()
    var lineView2: UIView = UIView()
    
    var phText: UITextView = UITextView()
    var phInputPre: SpringTextField = SpringTextField()
    var phInput: SpringTextField = SpringTextField()
    var fbLoginButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var wcLoginButton: SpringButton = SpringButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var pwText: UITextView = UITextView()
    var pwInput: SpringTextField = SpringTextField()
    var pwInputShow: SpringButton = SpringButton()
    var resetPasswordText: SpringButton = SpringButton()
    var orTV: UITextView = UITextView()
    var footerText1: UITextView = UITextView()
    var footerText2: UITextView = UITextView()
    
    var movingToNextInputField = false //used to catch issues where user presses next on keyboard in text input and keyboard will show function is triggered causing glitches.
    var tapGesture: UITapGestureRecognizer = UITapGestureRecognizer()

    // MARK: Validation
    var phoneTest: PhoneNumberTextField = PhoneNumberTextField() // used to check if phone number entered is valid
    var isPhoneNumberValid: Bool = false
    
    // MARK: UI Elements
    var inputOriginalY: CGFloat = 0.0
    var originalLoginLocation: CGFloat = 0.0
    var columnWidth = 0
    var columnHeight = 40
    let normalFont = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 8.5)]
    let largerFont = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.5)]
    let normalFontButton: [NSAttributedStringKey: Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont.systemFont(ofSize: 9.5), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
    let largerFontButton: [NSAttributedStringKey: Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont.systemFont(ofSize: 13.0), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in LoginVC viewdidload")

        createNotifications()
        configureLoginVC()
        createNewUserProfile() //set New user profile as empty
        createUIElements() //main UI creation function
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("in viewWillAppear | start")
        self.setupStatusBar(statusBarBgView: &Utility.statusBarBgView)

        //reset phone and password
        phInputPre.text = "+233"
        phInput.text = ""
        pwInput.text = ""
        
        hideKeyboard()
//        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("in viewdidappear | start")
        hideKeyboard()
    }

    func configureLoginVC() {
        print("IN LoginVC | configureLoginVC")
        self.view.backgroundColor = UIColor.white

        //reset Date information in the event that a user has signed out
        refreshAllDateInformation() // get user date and time information
        setCurrentlySelectedMenuDay() //sets the current day as today

        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.modalAnimationType = .zoom
        self.navigationController?.hero.navigationAnimationType = .pageIn(direction: .up)

        //dismiss keyboard when pressing enter
        phInputPre.delegate = self
        phInput.delegate = self
        pwInput.delegate = self
        phInput.clearButtonMode = UITextFieldViewMode.whileEditing
    }

    // MARK: UI
    func createUIElements() {
        print("IN createUIElements | start")
        columnWidth = Int(self.view.frame.width - 150)
        if isIPhoneSE == true { 
            columnWidth = Int(self.view.frame.width - 125)
            columnHeight = 33
        }
        
        //top image and icon
        topImage.frame = CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width, height: self.view.frame.height / 2.525)
        
        if isIPhoneSE == true { 
            topImage.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height / 2.525)
        }
        
        topImage.contentMode = UIViewContentMode.scaleAspectFill
        topImage.autoresizingMask = [.flexibleHeight, .flexibleTopMargin]
        topImage.clipsToBounds = true
        topImage.layer.zPosition = 1
        self.view.addSubview(topImage)
        
        topImageBlack.frame = topImage.frame
        topImageBlack.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        topImageBlack.layer.zPosition = 2
        self.view.addSubview(topImageBlack)
        
        topImageIcon.frame = CGRect(x: 0, y: globalStatusBarHeight, width: self.view.frame.width / 4, height: self.view.frame.width / 4)
        
        if isIPhoneSE == true { 
            topImageIcon.frame = CGRect(x: 0, y: 0, width: self.view.frame.width / 4, height: self.view.frame.width / 4)
        }
        
        topImageIcon.tintColor = UIColor.white
        topImageIcon.center.x = self.view.center.x
        topImageIcon.center.y = topImage.center.y
        topImageIcon.layer.zPosition = 3
        self.view.addSubview(topImageIcon)
        
        loginButton.frame = CGRect(x: 0, y: topImage.frame.maxY, width: view.frame.width / 2, height: globalLargeButtonHeight)
        if isIPhoneSE == true { 
            loginButton.frame = CGRect(x: 0, y: topImage.frame.maxY, width: view.frame.width / 2, height: globalLargeButtonHeight - 5)
        }
        
        loginButton.setTitle("LOGIN", for: .normal)
        loginButton.setTitleColor(hexRedDark.Color, for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        loginButton.backgroundColor = UIColor.white
        loginButton.isUserInteractionEnabled = true
        loginButton.layer.zPosition = 3
        loginButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        loginButton.tag = 10
        
        let alpha: CGFloat = 0.15
        lineView.frame = CGRect(x: loginButton.frame.minX, y: loginButton.frame.size.height - 2, width: view.frame.width / 2, height: 2)
        lineView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
        lineView.isHidden = true
        
        view.addSubview(loginButton)
        loginButton.addSubview(lineView)
        
        registerButton.frame = CGRect(x: loginButton.frame.maxX, y: topImage.frame.maxY, width: view.frame.width / 2, height: globalLargeButtonHeight)
        
        if isIPhoneSE == true { 
            registerButton.frame = CGRect(x: loginButton.frame.maxX, y: topImage.frame.maxY, width: view.frame.width / 2, height: globalLargeButtonHeight - 5)
        }
        
        registerButton.setTitle("REGISTER", for: .normal)
        registerButton.setTitleColor(UIColor.white, for: .normal)
        registerButton.titleLabel?.font = loginButton.titleLabel?.font
        registerButton.backgroundColor = hexRedDark.Color
        registerButton.layer.zPosition = 3
        registerButton.isUserInteractionEnabled = true
        registerButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        registerButton.tag = 11
        
        view.addSubview(registerButton)
        
        //main buttons
        inputOriginalY = CGFloat(loginButton.frame.maxY) + 15 //used to save original Y location of input fields and buttons

        phText.frame = CGRect(x: 0, y: Int(inputOriginalY), width: columnWidth + 5, height: columnHeight)
        phText.center.x = self.view.center.x
        phText.textAlignment = .left
        phText.centerVertically()
        phText.layer.zPosition = 2
        phText.isEditable = false
        phText.isSelectable = false
        phText.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        phText.backgroundColor = .clear
        
        //resize first letters of text
        let phAttrString = NSMutableAttributedString(string: "PHONE NUMBER", attributes: normalFont)
        phAttrString.setAttributes(largerFont, range: NSRange(location: 0, length: 1))
        phAttrString.setAttributes(largerFont, range: NSRange(location: 6, length: 1))
        phText.attributedText = phAttrString
        phText.textColor = hexRedDark.Color
        phText.layer.zPosition = 3
        self.view.addSubview(phText)
        
        phInputPre.frame = CGRect(x: Int(phText.frame.minX), y: Int(phText.frame.maxY) - 30, width: 50, height: columnHeight)
        
        if isIPhoneSE == true { 
            phInputPre.frame = CGRect(x: Int(phText.frame.minX), y: Int(phText.frame.maxY) - 20, width: 50, height: columnHeight)
        }
        
        phInputPre.keyboardType = .numbersAndPunctuation
        phInputPre.textAlignment = .center
        phInputPre.text = "+233" // need to validate on geolocate before setting this
        phInputPre.contentVerticalAlignment = .bottom
        phInputPre.backgroundColor = .clear
        phInputPre.autocorrectionType = .no
        phInputPre.setBottomBorder()
        phInputPre.layer.zPosition = 2
        phInputPre.tag = 3
        phInputPre.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        phInputPre.isSecureTextEntry = false
        
        phInputPre.returnKeyType = UIReturnKeyType.next
        
        self.view.addSubview(phInputPre)
        
        phInput.frame = CGRect(x: Int(phText.frame.minX) + 58, y: Int(phInputPre.frame.minY), width: columnWidth - 58, height: columnHeight)
        phInput.keyboardType = .numbersAndPunctuation
        phInput.textAlignment = .left
        phInput.contentVerticalAlignment = .bottom
        phInput.backgroundColor = .clear
        phInput.setBottomBorder()
        phInput.layer.zPosition = 2
        phInput.tag = 4
        phInput.returnKeyType = UIReturnKeyType.next
        phInput.font = phInputPre.font
        phInput.autocorrectionType = .no
        phInput.isSecureTextEntry = false
        self.view.addSubview(phInput)
        
        pwText.frame = CGRect(x: 0, y: Int(phInput.frame.maxY) + 18, width: columnWidth + 5, height: columnHeight)
        let pwAttrString = NSMutableAttributedString(string: "PASSWORD", attributes: normalFont)
        pwAttrString.setAttributes(largerFont, range: NSRange(location: 0, length: 1))
        pwText.attributedText = pwAttrString
        pwText.center.x = self.view.center.x
        pwText.textAlignment = .left
        pwText.isEditable = false
        pwText.centerVertically()
        pwText.isSelectable = false
        pwText.backgroundColor = .clear
        pwText.layer.zPosition = 3
        pwText.textColor = hexRedDark.Color
        self.view.addSubview(pwText)
        
        pwInput.frame = CGRect(x: 0, y: Int(pwText.frame.maxY) - 30, width: columnWidth, height: columnHeight)
        
        if isIPhoneSE == true { 
            pwInput.frame = CGRect(x: 0, y: Int(pwText.frame.maxY) - 20, width: columnWidth, height: columnHeight)
        }
        
        pwInput.keyboardType = .default
        pwInput.center.x = self.view.center.x
        pwInput.textAlignment = .left
        pwInput.backgroundColor = .clear
        pwInput.autocorrectionType = .no
        pwInput.isSecureTextEntry = true
        pwInput.setBottomBorder()
        pwInput.contentVerticalAlignment = .bottom
        pwInput.layer.zPosition = 2
        pwInput.tag = 5
        pwInput.returnKeyType = UIReturnKeyType.done
        
        pwInput.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.medium) //made larger when secure text
        self.view.addSubview(pwInput)

        if #available(iOS 10.0, *) { //stop save passwords from coming up on keyboard
            phInputPre.textContentType = UITextContentType("")
            phInput.textContentType = UITextContentType("")
            pwInput.textContentType = UITextContentType("")
        }

        //pwInputShow
        pwInputShow.frame = CGRect(x: pwInput.frame.maxX - 52.5, y: 0, width: 50, height: pwInput.frame.height)
        pwInputShow.center.y = pwInput.center.y + 7.5
        pwInputShow.setTitle("SHOW", for: .normal)
        pwInputShow.contentVerticalAlignment = .center
        pwInputShow.contentHorizontalAlignment = .right
        
        pwInputShow.setTitleColor(hexBlue.Color, for: .normal)
        pwInputShow.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
        pwInputShow.backgroundColor = .clear
        pwInputShow.isUserInteractionEnabled = true
        pwInputShow.layer.zPosition = 3
        pwInputShow.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        pwInputShow.tag = 14
        pwInputShow.isHidden = true
        pwInputShow.layer.zPosition = pwInput.layer.zPosition + 1
        view.addSubview(pwInputShow)
        
        resetPasswordText.frame = CGRect(x: 0, y: Int(pwInput.frame.maxY + 2), width: columnWidth, height: columnHeight + 5)

        if isIPhoneSE == true {
            resetPasswordText.frame = CGRect(x: 0, y: Int(pwInput.frame.maxY + 2), width: columnWidth, height: columnHeight)
        }

        resetPasswordText.setTitle("Forgot password?", for: .normal)
        resetPasswordText.setTitleColor(.black, for: .normal)
        resetPasswordText.titleLabel?.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        resetPasswordText.backgroundColor = .clear
        resetPasswordText.setBackgroundColor(color: hexButtonHighlight.Color, forState: .highlighted)
        resetPasswordText.layer.zPosition = 1
        resetPasswordText.center.x = self.view.center.x
        resetPasswordText.contentHorizontalAlignment = .center
        resetPasswordText.contentVerticalAlignment = .center
        resetPasswordText.isUserInteractionEnabled = true
        resetPasswordText.isHidden = false
        resetPasswordText.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        resetPasswordText.tag = 13
        view.addSubview(resetPasswordText)

        let buttonAdjustment: CGFloat = 10
        wcLoginButton.frame = CGRect(x: 0, y: Int(pwInput.frame.maxY) + columnHeight + 10, width: columnWidth, height: Int(globalLargeButtonHeight - buttonAdjustment))
        
        if isIPhoneSE == true { 
            wcLoginButton.frame = CGRect(x: 0, y: Int(pwInput.frame.maxY) + columnHeight + 5, width: columnWidth, height: Int(globalLargeButtonHeight - (buttonAdjustment + 5)))
        }
        
        originalLoginLocation = wcLoginButton.frame.minY
        
        let wcAttrString = NSMutableAttributedString(string: "LOGIN", attributes: largerFontButton)
        wcLoginButton.setAttributedTitle(wcAttrString, for: .normal)
        wcLoginButton.setTitleColor(UIColor.white, for: .normal)
        wcLoginButton.titleLabel?.font = UIFont.systemFont(ofSize: 12.5, weight: UIFont.Weight.regular)
        wcLoginButton.backgroundColor = hexRed.Color
        wcLoginButton.center.x = self.view.center.x
        wcLoginButton.layer.zPosition = 3
        wcLoginButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        wcLoginButton.tag = 1
        view.addSubview(wcLoginButton)
        
        orTV.frame = CGRect(x: 0, y: Int(wcLoginButton.frame.maxY) + 5, width: columnWidth, height: columnHeight)
        
        orTV.textColor = UIColor.black
        orTV.text = "OR"
        orTV.center.x = self.view.center.x
        orTV.textAlignment = .center
        orTV.centerVertically()
        orTV.layer.zPosition = 2
        orTV.isEditable = false
        orTV.isSelectable = false
        orTV.isUserInteractionEnabled = false
        orTV.font = UIFont.systemFont(ofSize: 9, weight: UIFont.Weight.bold)
        view.addSubview(orTV)
        
        fbLoginButton.frame = CGRect(x: 0, y: Int(orTV.frame.maxY - 10), width: columnWidth, height: Int(globalLargeButtonHeight) - Int(buttonAdjustment))
        
        if isIPhoneSE == true { 
            fbLoginButton.frame = CGRect(x: 0, y: Int(orTV.frame.maxY), width: columnWidth, height: Int(globalLargeButtonHeight - (buttonAdjustment + 5)))
        }
        
        let fbAttrString = NSMutableAttributedString(string: "CONTINUE WITH FACEBOOK", attributes: normalFontButton)
        fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 0, length: 1))
        fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 14, length: 1))
        fbLoginButton.setAttributedTitle(fbAttrString, for: .normal)
        fbLoginButton.titleLabel?.font = wcLoginButton.titleLabel?.font
        fbLoginButton.setTitleColor(UIColor.white, for: .normal)
        fbLoginButton.backgroundColor = hexBlue.Color
        fbLoginButton.center.x = self.view.center.x
        fbLoginButton.layer.zPosition = 3
        fbLoginButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        fbLoginButton.tag = 2
        view.addSubview(fbLoginButton)
        
        //footer text
        footerText1.frame = CGRect(x: 0, y: Int(self.view.frame.height - globalLargeButtonHeight - 5), width: columnWidth + 50, height: 25)

        if isIPhoneSE == true {
            footerText1.frame = CGRect(x: 0, y: Int(self.view.frame.height - globalLargeButtonHeight), width: columnWidth + 50, height: 25)
        }

        if isIPhoneX == true {
            footerText1.frame = CGRect(x: 0, y: Int(self.view.frame.height - (globalLargeButtonHeight + 25)), width: columnWidth + 50, height: 25)
        }

        footerText1.textColor = UIColor.gray
        footerText1.text = "By signing in you agree to our"
        footerText1.center.x = self.view.center.x
        footerText1.backgroundColor = .clear
        footerText1.textAlignment = .center
        footerText1.isSelectable = false
        footerText1.isScrollEnabled = false
        footerText1.isEditable = false
        footerText1.layer.zPosition = 2
        footerText1.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)

        footerText2.frame = CGRect(x: 0, y: Int(footerText1.frame.maxY - 7.5), width: columnWidth + 50, height: 50)
        footerText2.textColor = UIColor.gray
        footerText2.isSelectable = true
        footerText2.isEditable = false
        footerText2.isScrollEnabled = false

        let attributedString = NSMutableAttributedString(string: "Terms of Service and Privacy policy.") //need to update urls
        attributedString.addAttribute(.link, value: termsURL, range: NSRange(location: 0, length: 16))
        attributedString.addAttribute(.link,
                                      value: privacyURL,
                                      range: NSRange(location: 21, length: 14
        ))
        footerText2.attributedText = attributedString

        footerText2.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: hexGray, NSAttributedStringKey.underlineColor.rawValue: hexGray, NSAttributedStringKey.underlineStyle.rawValue: NSUnderlineStyle.styleSingle.rawValue]

        footerText2.center.x = self.view.center.x
        footerText2.textColor = hexGray
        footerText2.backgroundColor = .clear
        footerText2.textAlignment = .center
        footerText2.layer.zPosition = 1
        footerText2.font = UIFont.systemFont(ofSize: 12.5, weight: UIFont.Weight.regular)
        self.view.addSubview(footerText1)
        self.view.addSubview(footerText2)

        toggleFooterFields() //show only if on register page

        lineView2.frame = CGRect(x: 0, y: registerButton.frame.size.height - 2, width: view.frame.width / 2, height: 2)
        lineView2.backgroundColor = UIColor.black.withAlphaComponent(alpha)
        lineView2.layer.zPosition = 100
        lineView2.isHidden = false
        registerButton.addSubview(lineView2)
        
    }

    func createNotifications() {
        //trigger facebook login
        NotificationCenter.default.addObserver(self, selector: #selector(tryFacebookClientLogin), name: NSNotification.Name(rawValue: "tryFacebookLogin"), object: nil)

        //trigger facebook register
        NotificationCenter.default.addObserver(self, selector: #selector(triggerSegueToRegisterVCFromFacebook), name: NSNotification.Name(rawValue: "triggerSegueToRegisterVCFromFacebook"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(animateFailedLoginForWCInputs), name: NSNotification.Name(rawValue: "animateFailedLoginForWCInputs"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentMenu), name: NSNotification.Name(rawValue: "getCurrentMenu"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(moveToMenu), name: NSNotification.Name(rawValue: "moveToMenu"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(moveToRegister), name: NSNotification.Name(rawValue: "moveToRegister"), object: nil)
    }

    func hideKeyboard() {
        pwInput.resignFirstResponder()
        phInputPre.resignFirstResponder()
        phInput.resignFirstResponder()
    }
    
    // MARK: UX
    @objc func buttonPressed(_ sender: SpringButton) {
        print("Button pressed: ", sender.tag)
        animateButtonPress(senderTag: sender.tag, localView: self.view)
        
        if sender.tag == 1 {
            //wechop try to login
            
            //different logic for login or register for each button
            if loginVCIsUserInLoginAreaAndNotInRegisterArea == true {
                print("in ButtonPress | wechop login")

                if checkInputsOkay() == true {
                    //store data
                    userRegistrationPhone = ("\(phInputPre.text!)\(phInput.text!)").keepOnlyNumbers
                    userRegistrationPassword = pwInput.text!
                    print("userRegistrationPhone: ", userRegistrationPhone)
                    tryWCLogin(HTTPMethod.get, vc: self, completion: tryWCLoginHandler)
                } else {
                }
            } else {
                
                if hasInternetCheck() != false {
                    segueToRegisterVC(type: "wechop")
                } else {
                    noInternetWarning()
                }
                
            }
            
        } else if sender.tag == 2 { //this is all handled by facebook login button for both login and register

            //fb button pressed
            if loginVCIsUserInLoginAreaAndNotInRegisterArea == true {
                // FB login button pressed
                print("in ButtonPress | FB login")
                
                if FBSDKAccessToken.currentAccessTokenIsActive() == true {
                    print("in ButtonPress | FB login | HAS Token")
                    if hasInternetCheck() == true {
                        tryFBLogin(HTTPMethod.get, vc: self, completion: tryFBLoginHandler)
                    } else {
                        noInternetWarning()
                    }
                    
                } else {
                    print("in ButtonPress | FB login | NO Token")
                    //will open web browser to first log into facebook
                    tryFacebookClientLogin()
                }
                
            } else {
                // FB Register button pressed
                print("in ButtonPress | FB register")
                
                if FBSDKAccessToken.currentAccessTokenIsActive() == true {
                    loginManager.logOut() 
                }
                tryFacebookClientLogin()
            }
        } else if sender.tag == 10 {
            //go to Login Page UI
            toggleLoginOrRegisterPageUI(sender: "login")
        } else if sender.tag == 11 {
            //register Page UI
            toggleLoginOrRegisterPageUI(sender: "register")
        } else if sender.tag == 13 {
            //reset Password pressed
            dismissAllNotificationBanners()
            segue(name: "ResetPasswordVCSegueFromLogin", v: self, type: .pageIn(direction: .left))
        } else if sender.tag == 14 {
            //show password field
            
            if pwInput.isSecureTextEntry == true {
                pwInput.isSecureTextEntry = false
                pwInputShow.setTitle("HIDE", for: .normal)
                pwInput.font = phInputPre.font
            } else {
                pwInput.isSecureTextEntry = true
                pwInputShow.setTitle("SHOW", for: .normal)
                pwInput.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.medium) //made larger when secure text
            }
        }
        
    }

    func toggleFooterFields() {
        if loginVCIsUserInLoginAreaAndNotInRegisterArea == false {
            footerText1.isHidden = false
            footerText2.isHidden = false
        } else {
            footerText1.isHidden = true
            footerText2.isHidden = true
        }
    }

    func toggleLoginOrRegisterPageUI(sender: String) { //toggles UI for page
        
        print("IN toggleLoginOrRegister | start | sender: ", sender)
        let originalTabValue = loginVCIsUserInLoginAreaAndNotInRegisterArea
        if sender == "login" && loginVCIsUserInLoginAreaAndNotInRegisterArea == false {
            loginVCIsUserInLoginAreaAndNotInRegisterArea.negate()
        } else if sender == "register" && loginVCIsUserInLoginAreaAndNotInRegisterArea == true {
            loginVCIsUserInLoginAreaAndNotInRegisterArea.negate()
        }
        
        if loginVCIsUserInLoginAreaAndNotInRegisterArea == true && originalTabValue !=  loginVCIsUserInLoginAreaAndNotInRegisterArea {
            
            //format for login
            loginButton.backgroundColor = UIColor.white
            loginButton.setTitleColor(hexRedDark.Color, for: .normal)
            registerButton.backgroundColor = hexRedDark.Color
            registerButton.setTitleColor(UIColor.white, for: .normal)
            
            let fbAttrString = NSMutableAttributedString(string: "CONTINUE WITH FACEBOOK", attributes: normalFontButton)
            fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 0, length: 1))
            fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 14, length: 1))
            fbLoginButton.setAttributedTitle(fbAttrString, for: .normal)
            
            let wcAttrString = NSMutableAttributedString(string: "LOGIN", attributes: largerFontButton)
            wcLoginButton.setAttributedTitle(wcAttrString, for: .normal)
            
            //move fields back and unhide
            
            wcLoginButton.frame.origin.y = originalLoginLocation
            orTV.frame.origin.y = wcLoginButton.frame.maxY + 5
            fbLoginButton.frame.origin.y = orTV.frame.maxY - 10
            
            if isIPhoneSE == true { 
                fbLoginButton.frame.origin.y = orTV.frame.maxY
            }
            
            phInputPre.isHidden = false
            phInput.isHidden = false
            pwInput.isHidden = false
            phText.isHidden = false
            pwText.isHidden = false
            pwInputShow.isHidden = false
            
            lineView.isHidden = true
            lineView2.isHidden = false
            
        } else if loginVCIsUserInLoginAreaAndNotInRegisterArea == false && originalTabValue !=  loginVCIsUserInLoginAreaAndNotInRegisterArea {

            hideKeyboard()
            loginButton.backgroundColor = hexRedDark.Color
            loginButton.setTitleColor(UIColor.white, for: .normal)
            
            let fbAttrString = NSMutableAttributedString(string: "LOGIN WITH FACEBOOK", attributes: normalFontButton)
            fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 0, length: 1))
            fbAttrString.setAttributes(largerFontButton, range: NSRange(location: 11, length: 1))
            fbLoginButton.setAttributedTitle(fbAttrString, for: .normal)
            
            let wcAttrString = NSMutableAttributedString(string: "CREATE WECHOP ACCOUNT", attributes: normalFontButton)
            wcAttrString.setAttributes(largerFontButton, range: NSRange(location: 0, length: 1))
            wcAttrString.setAttributes(largerFontButton, range: NSRange(location: 7, length: 1))
            wcAttrString.setAttributes(largerFontButton, range: NSRange(location: 9, length: 1))
            wcAttrString.setAttributes(largerFontButton, range: NSRange(location: 14, length: 1))
            wcLoginButton.setAttributedTitle(wcAttrString, for: .normal)
            
            registerButton.backgroundColor = UIColor.white
            registerButton.setTitleColor(hexRedDark.Color, for: .normal)
            
            //get height of section
            let viewHeight = view.frame.maxY - registerButton.frame.maxY
            let sectionHeight = fbLoginButton.frame.maxY - wcLoginButton.frame.minY
            let yStart = (viewHeight - sectionHeight) / 2
            
            fbLoginButton.frame.origin.y = registerButton.frame.maxY + yStart
            orTV.frame.origin.y = fbLoginButton.frame.maxY + 5
            wcLoginButton.frame.origin.y = orTV.frame.maxY - 10
            
            if isIPhoneSE == true { 
                wcLoginButton.frame.origin.y = orTV.frame.maxY
            }
            phInputPre.isHidden = true
            phInput.isHidden = true
            pwInput.isHidden = true
            phText.isHidden = true
            pwText.isHidden = true
            pwInputShow.isHidden = true
            
            lineView.isHidden = false
            lineView2.isHidden = true
        }

        toggleFooterFields()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }

        let range2: UITextRange? = textField.selectedTextRange

        if textField == pwInput {
            if (textField.text?.count)! > 0 {
                pwInputShow.isHidden = false
            } else {
                pwInputShow.isHidden = true
            }
        }
        
        let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: "+ 0123456789-()").inverted
        let strValid: Bool = string.rangeOfCharacter(from: numberOnly) == nil

        if textField == phInputPre {
            print("using delegate for phtextpre")
            
            if strValid == true {
                let newLength = text.count + string.count - range.length
                return newLength <= 4
            }
            
        } else if textField == phInput {
            guard NSCharacterSet(charactersIn: "0123456789+ ()").isSuperset(of: NSCharacterSet(charactersIn: string) as CharacterSet) else {
                return false
            }
            formatPhoneNumber(location: range2!)

            return true
        } else if textField == pwInput {
            
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }
        return string.rangeOfCharacter(from: CharacterSet.letters) == nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phInputPre || textField == phInput {
//            if phInputPre.text == "+233" && phInput.text == "" {
//            }
        } else {
            if (pwInput.text?.count)! > 0 {
                pwInputShow.isHidden = false
            } else {
                pwInputShow.isHidden = true
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        print("in textFieldDidEndEditing | start")

        let range: UITextRange? = textField.selectedTextRange

        if (pwInput.text?.count)! > 0 {
            pwInputShow.isHidden = false
        } else {
            pwInputShow.isHidden = true
        }
        
        if textField == phInputPre {
            if textField.text == "+" || textField.text == "" {
                textField.text = "+233"
            }
            if textField.text?.range(of: "+") == nil {
                let value: String = textField.text!
                textField.text = "+\(value)"
            }
        }
        formatPhoneNumber(location: range!)
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("in textFieldShouldEndEditing | start")
        if movingToNextInputField == true {
            return false
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        let range: UITextRange? = textField.selectedTextRange

        print("in textFieldShouldReturn | start")
        formatPhoneNumber(location: range!)

        let nextTag = textField.tag + 1
        print("in textFieldShouldReturn | set next tag")
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            movingToNextInputField = true
            nextResponder.becomeFirstResponder()
            delay(1) {
                self.movingToNextInputField = false
            }
        } else {
            textField.resignFirstResponder()
        }
        
        if textField == pwInput {
            if checkInputsOkay() == true {
                wcLoginButton.sendActions(for: .touchUpInside)
            } else {
                // animate incorrect inputs
            }
        }
        return false
    }

    // MARK: Validation
    func formatPhoneNumber(location: UITextRange) { //format full phone number
        print("in formatPhoneNumber | start")

        var fullPhoneString = ("\(phInputPre.text!)\(phInput.text!)").keepOnlyNumbers
        fullPhoneString = "+\(fullPhoneString)"
        let phoneNumberFormatted = PartialFormatter().formatPartial(fullPhoneString)
        phoneTest.text = phoneNumberFormatted
        
        if phoneNumberFormatted.contains(" ") {
            let prefixCount = phoneNumberFormatted.components(separatedBy: " ")[0].count
            phInputPre.text = phoneNumberFormatted.components(separatedBy: " ")[0]
            phInput.text = String(phoneNumberFormatted.dropFirst(prefixCount))
        } else {
            print("in formatPhoneNumber | in else")
        }
        
//        isPhoneNumberValid = phoneTest.isValidNumber //removing client side validation
        isPhoneNumberValid = true

    }
    
    func checkInputsOkay() -> Bool {
        
        if pwInput.text == "" && phInput.text == "" {
            
            animateUIBanner("Input Required", subText: "Please enter your credentials.", color: UIWarningInformationColor, removePending: true)
            animateIncorrectInput(buttonID: 4, localView: self.view)
            animateIncorrectInput(buttonID: 5, localView: self.view)
            return false
        } else if pwInput.text == "" {
            animateUIBanner("Input Required", subText: "Please enter your credentials.", color: UIWarningInformationColor)
            animateIncorrectInput(buttonID: 5, localView: self.view)
            return false
        } else if phInput.text == "" {
            animateUIBanner("Input Required", subText: "Please enter your credentials.", color: UIWarningInformationColor)
            
            animateIncorrectInput(buttonID: 4, localView: self.view)
            return false
        } else if phInputPre.text == "" || phInputPre.text == "+" {
            
            animateUIBanner("Input Required", subText: "Please enter your credentials.", color: UIWarningInformationColor)
            
            animateIncorrectInput(buttonID: 3, localView: self.view)
            return false
        } else if isPhoneNumberValid == false {
            
            animateUIBanner("Input Required", subText: "Please enter your credentials.", color: UIWarningInformationColor)
            
            animateIncorrectInput(buttonID: 4, localView: self.view)
            animateIncorrectInput(buttonID: 3, localView: self.view)
            return false
        } else {
            return true
        }
    }
    
    // MARK: animations
    @objc func animateFailedLoginForWCInputs() {
        print("in animateFailedLoginForWCInputs | start")
        animateIncorrectButton(buttonID: 14, localView: self.view)
        animateIncorrectInput(buttonID: 4, localView: self.view)
        animateIncorrectInput(buttonID: 3, localView: self.view)
        animateIncorrectInput(buttonID: 5, localView: self.view)
    }
    
    // MARK: segue and remote calls

    @objc func moveToRegister() {
        toggleLoginOrRegisterPageUI(sender: "register")
    }

    @objc func getCurrentMenu() {
        print("in LoginVC getCurrentMenu | start")
        //using global function to pass viewcontroller and make standard
        globalGetUserMenu(self, useSelectedDate: false, fromLogin: true)
    }
    
    @objc func moveToMenu() { //called remotly after API completion
        print("in moveToMenu | logingvc | start")
//        resetPasswordText.isHidden = true
        phInputPre.text = "+233"
        phInput.text = ""
        pwInput.text = ""

        self.navigationController?.hero.navigationAnimationType = .none
        self.performSegue(withIdentifier: "MenuVCSegueFromLogin", sender: LoginVC.self)
    }
    
    @objc func tryFacebookClientLogin() {
        print("in tryFacebookClientLogin | start")
        
        hideKeyboard()
        loginManager.loginBehavior = .web //change to web login to completly log out user
        loginManager.logIn(withReadPermissions: ["public_profile"], from: self, handler: { (result, error) -> Void in

            if error == nil {
//                let fbloginresult: FBSDKLoginManagerLoginResult = result!
                print("in tryFacebookClientLogin | fbloginresult", result)
                
                if result!.isCancelled != true {
                    
                    if result!.declinedPermissions.count == 0 && result!.isCancelled == false && result!.token != nil {
                        print("in tryFacebookClientLogin | success")
                        
                        if loginVCIsUserInLoginAreaAndNotInRegisterArea == true {
                            //try login with server
                            getFacebookClientAuthAndID(self)
                            tryFBLogin(HTTPMethod.get, vc: self, completion: tryFBLoginHandler)
                        } else {
                            getFacebookClientAuthAndID(self)
                            getFacebookUserInformation(getFacebookUserInformationHandler)
                            self.triggerSegueToRegisterVCFromFacebook()
                        }
                        
                    } else if result!.declinedPermissions.count > 0 {
                        print("in tryFacebookClientLogin | denied permissions")
                        
                    }
                } else {
                    print("in tryFacebookClientLogin | cancel")
                }
            } else {
                
                if let err = error {
                    print("in tryFacebookClientLogin | error: ", err)
                    //                    //writeToFireBase("in tryFacebookClientLogin | error: \(err)")
                }
                
                animateUIBanner("An error occured", subText: "Please try again later.", color: hexRed.Color, removePending: true)
            }
        })
        
    }
    
    @objc func triggerSegueToRegisterVCFromFacebook() { //called through VC notification

        if hasInternetCheck() != false {
            segueToRegisterVC(type: "facebook")
        } else {
            noInternetWarning()
        }
    }

    func segueToRegisterVC(type: String) {
        print("in segueToRegisterVC | start")
                
        //collapse keyboard if active
        hideKeyboard()

        if type == "facebook" {
            
            sourceView = .registerFB
        } else {
            sourceView = .registerWC
        }
        
        //
        //segue to register vc
        segue(name: "RegisterVCSegueFromLogin", v: self, type: .pageIn(direction: .right))
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("in didRegisterForRemoteNotificationsWithDeviceToken | start")
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        userCurrentDeviceID = token
        defaults.set(userCurrentDeviceID, forKey: "userDeviceID")
        defaults.synchronize()
        print("Device Token: \(token)")
    }
}
